<?php
/*
Template Name: About Page
*/
get_header(); 
?>



<section class="about">
	<div class="container">
		<div class="row">
			<h2 class="about-head">About Us</h2>

			<p>Our bodies are up to 75% water, and staying well-hydrated is critical to our optimum health and survival.</p>

			<p class="about-sub-head">However the quality or purity of water we all have access to is</p> 
			<p class="sub-head-text">a) Highly suspect</p>
			<p class="sub-head-text">b) Deteriorating by the day</p>

			<p>If you're like me, you probably use an aquaguard or RO filter to prepare your water for consumption and cooking. 
			However this filtered water is still not in an optimal condition that supports cell renewal, or prevents ageing and inflammation.</p>

			<p class="about-sub-head">In short, YOUR WATER IS CAUSING</p>

			<p class="sub-head-text">1) Ageing</p>
			<p class="sub-head-text">2) Susceptibility to Inflammation</p>
			<p class="sub-head-text">3) Bloating </p> 

			<p class="about-sub-head">Water For Life is a consultation service designed to help you get the following benefits from your Water:</p>

			<p class="sub-head-text">1) Antioxidant - i.e. Ageing Prevention</p>
			<p class="sub-head-text">2) pH Balancing - i.e. reduced susceptibility to inflammation</p>
			<p class="sub-head-text">3) Hydration</p>

			<p class="contact-link"><a href="#">Contact Us</a> to schedule your Water DEMO today! </p>

		</div>
	</div>
</section>

<?php
get_footer();
?>