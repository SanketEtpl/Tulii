<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AppLayers
 */

get_header(); 
?>
<div class="slider">
    <div class="camera_wrap">
        <div data-src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/picture1.jpg"><div class="camera-caption fadeIn"><p class="title">The <span>best water</span> right at your home</p><p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et <br>dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p></div></div>
        <div data-src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/picture2.jpg"><div class="camera-caption fadeIn"><p class="title">Mountain spring water</p><p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et <br>dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p></div></div>
        <div data-src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/picture3.jpg"><div class="camera-caption fadeIn"><p class="title">Stay healthy, drink spring water</p><p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et <br>dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p></div></div>
    </div>
</div>
<div class="global">
    <div class="center-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 box">
                    <div class="box1">
                        <p class="title">Our Quality</p>
                        <div class="cont">
                            <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                        <div class="more">
                            <a href="#" class="btn-default btn1">Read more <span class="fa fa-arrow-circle-o-right"></span></a>
                        </div>
                        <em></em>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 box">
                    <div class="box2">
                        <p class="title">Our Springs</p>
                        <div class="cont">
                            <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                        <div class="more">
                            <a href="#" class="btn-default btn1">Read more <span class="fa fa-arrow-circle-o-right"></span></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 box">
                    <div class="box3">
                        <p class="title">History</p>
                        <div class="cont">
                            <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                        </div>
                        <div class="more">
                            <a href="#" class="btn-default btn1">Read more <span class="fa fa-arrow-circle-o-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="about-box">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <p class="title">Water from one of the cleanest places on Earth</p>
                <p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore <br>magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
                <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
                <a href="#" class="btn-default btn2">Read more</a>
            </div>
        </div>
    </div>
</div>
<div class="articles-box">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">    
                <div class="box1 maxheight">
                    <p class="title">About Us</p>
                    <p class="description">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute reprehenderi.</p>
                    <a href="#" class="btn-default btn3">Read more</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">    
                <div class="box2 maxheight">
                    <p class="title">Company Reputation</p>
                    <p class="description">Bolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labor.</p>
                    <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute reprehenderi.</p>
                    <a href="#" class="btn-default btn3">Read more</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <h2>Our Products</h2>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="thumb-pad0">
                <div class="thumbnail">
                    <figure><img src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/page1_pic1.jpg" alt=""></figure>
                    <div class="caption">
                        <a href="" class="lnk">Lorem ipsum dolor sit amet co</a>
                        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</p>
                        <a href="#" class="btn-default btn4">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="thumb-pad0">
                <div class="thumbnail">
                    <figure><img src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/page1_pic2.jpg" alt=""></figure>
                    <div class="caption">
                        <a href="" class="lnk">Ipsum dolor sit amet co</a>
                        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</p>
                        <a href="#" class="btn-default btn4">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="thumb-pad0">
                <div class="thumbnail">
                    <figure><img src="http://162.144.204.188/~etpl2013/Demo/waterforcure/wp-content/themes/WaterForLife/img/page1_pic3.jpg" alt=""></figure>
                    <div class="caption">
                        <a href="" class="lnk">Excepteur sint occaeca</a>
                        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididun.</p>
                        <a href="#" class="btn-default btn4">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php


get_footer();
?>