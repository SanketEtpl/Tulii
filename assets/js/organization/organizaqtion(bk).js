function organizationList(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"organization_management/Organization_controller/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  organizationList();
}

function viewOrgEmp($this) // view of students
{
  if($($this).attr("data-row-id")) {            
    $( "h3" ).html( "<h3 class='modal-title'><b>View employee</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"organization_management/Organization_controller/viewEmpList",
         beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
            $("#employeePanel tbody").html(json.rows);
            $('#viewEmpModal').modal('show', {backdrop: 'static'});
         } else {
           toastr.error(json.msg); 
         }
      });
  } else {
    toastr.error("Something went wrong!!!.");         
  }
}