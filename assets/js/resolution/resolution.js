function deleteIssue($this)
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this issue?</b>');
    $(".inner_footer").html('<button type="button" onclick="delIssue('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
    
  } else {
 toastr.error("Something went wrong!!!.");           
 }
}  
function delIssue(key){
 
 $.ajax({
          type: "POST",
          dataType: "json",
          beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
         url: baseURL+"resolution_center/Resolution/deleteIssue",
          data: {"key":key},
        }).success(function (json) {
          if(json.status == "success"){
            toastr.success(json.msg); // success message
             $("#table").dataTable().fnDraw();
            }else{
            toastr.error(json.msg); // error message
          }
      });
    $('.commanPopup').modal('hide');
   // issueList();
}
function issueList()
{
 var table = $('#table').DataTable();
            $('#table').empty();
            table.destroy();

       $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        
         "ajax": {
        "url": baseURL+"resolution_center/Resolution/ajax_list",
       beforeSend: function() {
          var search = $("input[type=search]").val();
          if(search=="")          
            $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              }); 
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"});
        $("#table").css({"opacity":"1"}); 
      },
        "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [0,6,7], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
} 
 

function viewIssue($this)
{
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"resolution_center/Resolution/viewIssue",
         beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
          $("#parent_name").text(json.issueData.parent_name);
          $("#parent_number").text(json.issueData.parent_number);
          $("#service_provider").text(json.issueData.service_provider);
          $("#issue").text(json.issueData.issue);
          $("#issue_date").text(json.issueData.issue_date);
          //alert(json.issueData.status );
          if(json.issueData.issue_status == 1)
            $("#status").text("Pending");
          else
            $("#status").text("Resolved");              
          $('#issuesModal').modal('show', {backdrop: 'static'});

         }
         else{
          toastr.error(json.msg);            
         }
      });
  } else {     
    toastr.error("Something Went wrong!!!.");         
  }  
}
function changeIssueStatus_popup(userId,user_status,active_id,inActive_id)
{ 
var temp = "changeIssueStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure you want to change status?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function changeIssueStatus(userId,user_status,active_id,inActive_id)
{  
  $('.commanPopup').modal('hide');
    $.ajax({
      type : "POST",
      dataType : "json",
      beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      url : baseURL + "resolution_center/Resolution/changeIssueStatus",
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
      console.log(data);
      if(user_status == 2) { 
        var str_success="Issue has been resolved.";
        var str_error="Issue has been resolved failed.";

        $("#"+active_id).removeClass("btn-success");
        $("#"+active_id).addClass("btn-success");

        $("#"+inActive_id).removeClass("btn-success");
        $("#"+inActive_id).addClass("btn-success");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } 
        $("#table").dataTable().fnDraw();
      if(data.status = true) {
        toastr.success(str_success);
      }
      else if(data.status = false) {
        toastr.error(str_error);        
       }
      else {
        toastr.error("Access denied..!");
        }
    }); 
  }  
