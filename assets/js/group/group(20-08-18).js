

function viewMember($this) // view of faq
{
  if($($this).attr("data-row-id")) {            
    $( "h3" ).html( "<h3 class='modal-title'><b>View Members</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"group_management/Group/viewMembersList",
         beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        //console.log(json);
          if(json.status == "success"){
             $("#memberPanel tbody").html(json.rows);
          $('#viewGroupModal').modal('show', {backdrop: 'static'});
         } else {
           toastr.error(json.msg); 
         }
      });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
}