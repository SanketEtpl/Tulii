function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  bookingComplaintsList();
}

function bookingComplaintsList(){
  var bookingList = $('#bookingList').DataTable();
  $('#bookingList').empty();
  bookingList.destroy();
  $('#bookingList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [],
    // Load data for the table's content from an Ajax source
      "ajax": {
        "url": baseURL+"booking_management/Booking_complaints/bookingComplaintsList",
        beforeSend: function() {
           // $("#LoadingDiv").css({"display":"block"});
          var search = $("input[type=search]").val();
          if(search=="")
           $("input[type=search]").on("keyup",function(event) {

            if($("#clear").length == 0) {
               if($(this).val() != ""){
                $("#bookingList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
              } 
            }
            if($(this).val() == "")  
            $("#clear").remove();      
          }); 
          $("input[type=search]").keydown(function(event) {
            k = event.which;
            if (k === 32 && !this.value.length)
                event.preventDefault();
          });
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#bookingList").css({"opacity":"1"});
      },
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,5 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}

function viewBookingComplaintsData($this)
{
  if($($this).attr("data-row-id")){
  $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"booking_management/Booking_complaints/viewBookingComplaintsInfo",
      beforeSend: function() {
      },
      complete: function(){      
    },
      data: {"key":$($this).attr("data-row-id"), "complaint_by":$($this).attr('data-complaint-by')},
  }).success(function (json) {
    if(json.status == "success"){   
      $("#ModalLabel").html('Booking Complaint Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html(json.data);
      $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
      $('.commanPopup').modal('show');
    } else {
      toastr.error(json.msg,"Error:");
      }
    });
  } else {
    toastr.error("Something went wrong!!!.");    
  }
}

$(document).ready(function(){
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });
});