function deletePriceList($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure want to delete price record ?'))
    {
    $("#LoadingDiv").css({"display":"block"}); // loader          
      $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"price_management/Price_list/deletePriceList",
          data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
          if(json.status == "success"){
            $("#LoadingDiv").css({"display":"none"});
            $($this).parents("tr:first").remove(); // remove row
            toastr.success(json.msg); // success message
            $("#table").dataTable().fnDraw();
           }else{
            toastr.error(json.msg); // error message
            $("#LoadingDiv").css({"display":"none"});
          }
      });
    } 
  } else {     
    $("#LoadingDiv").css({"display":"none"}); // loader
    toastr.error("Price record something get wrong.");    
  }
}   

function editPriceData($this) // edit price list data
{
  $('#priceListForm')[0].reset(); //reset of price list value 
  $(".price_per_hour").remove();
  $(".rideType").remove();
  $(".price_per_km").remove();
  if($($this).attr("data-row-id")){ // get price list id 
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"price_management/Price_list/viewPriceInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){            
          $("#user_key").val($($this).attr("data-row-id"));
          $("#rideType").val(json.priceData.pm_id);
          $("#rideType").prop("disabled", true);
          if(json.priceData.pm_id == 1 || json.priceData.pm_id == 3 || json.priceData.pm_id == 5)
          {
            $("#pricePerHour").css({"display":"block"});  
            $("#pricePerKm").css({"display":"none"}); 
            $("#price_per_hour").val(json.priceData.rp_cost);    
          } else {
            $("#pricePerHour").css({"display":"none"});  
            $("#pricePerKm").css({"display":"block"}); 
            $("#price_per_km").val(json.priceData.rp_cost);   
          }
          $('#priceListModal').modal('show', {backdrop: 'static'});        
        }
        else{
            toastr.error(json.msg);
        }
      });
  } else {
    $("#user_key").val('');
    $('#priceListModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){
 $('#priceListForm')[0].reset();
  $('#btnPriceList').click(function(){ // save record of price list  
    var rideType = $("#rideType").val();
    var price_per_hour = $("#price_per_hour").val();
    var price_per_km = $("#price_per_km").val();
    var user_key = $("#user_key").val();
    var flag = 0;
    if(rideType == 1 || rideType == 3 || rideType == 5) {
        if(price_per_hour == ''){
        $(".price_per_hour").remove();  
        $("#price_per_hour").parent().append("<div class='price_per_hour' style='color:red;'>Price per hour field is required.</div>");
        flag = 1;
      } else if(price_per_hour == 0 ) {
        $(".price_per_hour").remove();  
        $("#price_per_hour").parent().append("<div class='price_per_hour' style='color:red;'>Price should be greater than zero.</div>");
        flag = 1;
      } else if(price_per_hour.length >= 4){
        $(".price_per_hour").remove();  
        $("#price_per_hour").parent().append("<div class='price_per_hour' style='color:red;'>Price has been exceeded.</div>");
        flag = 1;
      } else {
        $(".price_per_hour").remove();        
      }
    }
    
    if(rideType == 2 || rideType == 4) {
      if(price_per_km == ''){
        $(".price_per_km").remove();  
        $("#price_per_km").parent().append("<div class='price_per_km' style='color:red;'>Price per km field is required.</div>");
        flag = 1;
      } else if(price_per_km == 0 ) {
        $(".price_per_km").remove();  
        $("#price_per_km").parent().append("<div class='price_per_km' style='color:red;'>Price should be greater than zero.</div>");
        flag = 1;
      } else if(price_per_km.length >= 4 ) {
        $(".price_per_km").remove();  
        $("#price_per_km").parent().append("<div class='price_per_km' style='color:red;'>Price has been exceeded.</div>");
        flag = 1;
      } else {
        $(".price_per_km").remove();        
      }
    }  

    if(rideType == ''){
      $(".rideType").remove();  
      $("#rideType").parent().append("<div class='rideType' style='color:red;'>Please select ride type.</div>");
      flag = 1;
    }else{
      $(".rideType").remove();          
    }
    
    if(flag == 1)
      return false;    
    else
    {
      
      $("#LoadingDiv").css({"display":"block"});
      $.ajax({
        type:"POST",
        url:baseURL+"price_management/Price_list/addPrice",            
        data:{"editData":user_key,"rideType":rideType,"price_per_hour":price_per_hour,"price_per_km":price_per_km},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#priceListModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          }
          else if(response.status == "warning")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#priceListModal").modal('show');
            toastr.warning(response.message); 
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#priceListModal").modal('show');
            toastr.error(response.message);                                       
          }
        }
      });    
    }   
  });  

  $('#addPopUp').on("click",function(){
    $("#rideType").prop("disabled", false);
    $(".price_per_hour").remove();
    $(".rideType").remove();
    $(".price_per_km").remove();
    $("#user_key").val("");
    $("#priceListForm")[0].reset();
    $("#priceListModal").modal('show');    
  });

  $("#rideType").on("change",function(){
    if($(this).val() == 2 || $(this).val() == 4) {
      $("#pricePerHour").css({"display":"none"});  
      $("#pricePerKm").css({"display":"block"});  
    } else {
      $("#pricePerHour").css({"display":"block"});  
      $("#pricePerKm").css({"display":"none"});  
    }
  });
});