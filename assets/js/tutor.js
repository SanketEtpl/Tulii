// var table; 
$(document).ready(function() { 
$("#LoadingDiv").css({"display":"block"}); 
    //datatables
   tutorList();
    $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});  

function tutorList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
 $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"tutor_management/Tutor/tutorList",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,2], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
}

  function clearSearch() 
  { 
    $("input[type=search]").val("");
    $("#clear").remove();
    tutorList();
    // location.reload();
  }
