function loadTutorSubCategoryList()
{
  var tutorList = $('#tutorList').DataTable();
  $('#tutorList').empty();
  tutorList.destroy();
  $('#tutorList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
 
    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": baseURL+"tutor_management/Tutor_subcategory/subcategoryList",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search == "")          
        $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
             if($(this).val() != ""){
              $("#tutorList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
            event.preventDefault();
        });
      },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#tutorList").css({"opacity":"1"}); 
      },
      "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,4], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });  
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadTutorSubCategoryList();    
}

function deleteTutorSubCategory($this)
{
	if($($this).attr("data-row-id")) {
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this tutor sub-category?</b>');
    $(".inner_footer").html('<button type="button" onclick="deltutor('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
  } else {
 		toastr.error("Something went wrong!!!.");           
 	}
}  

function deltutor(key){ 
 	$.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: function() {
	    //$('#LoadingDiv').show();
    },
    complete: function(){
    	$('#LoadingDiv').hide();
    },
    url: baseURL+"tutor_management/Tutor_subcategory/deleteTutorSubCategory",
    data: {"key":key},
    }).success(function (json) {
  	if(json.status == "success") {
      toastr.success(json.msg); // success message
      $("#tutorList").dataTable().fnDraw();
    } else if(json.status == "warning") {
      toastr.warning(json.msg); // success message
      //$("#tutorList").dataTable().fnDraw();
    } else {
    	toastr.error(json.msg); // error message
  	}
 	});
  $('.commanPopup').modal('hide');    
}


$(document).ready(function() {
  $("#LoadingDiv").css({"display":"block"}); 
  //datatables
  loadTutorSubCategoryList();
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#tutorList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
    event.preventDefault();
  });

  $("#addTutorSubCatgoryPopUp").on("click", function(){
    $( "h3").html( "<h3 class='modal-title'><b>Add Tutor Sub-Category</b></h3>" );
    $('#tutorSubCatForm')[0].reset(); //reset of tutor category value 
    $("#user_key").val('');
    $(".tutor_sub_category_name").remove();
    $(".rate_per_hour").remove();
    $("#tutorSubCategoryModal").modal('show',{backdrop:'static'});
  });
});

function btnAddTutorSubCat(){
  var tutorCategory = $("#tutorCategory").val();
  var tutor_sub_category_name = $("#tutor_sub_category_name").val();
  var rate_per_hour = $("#rate_per_hour").val();
  var edit_id = $("#user_key").val();
  var price_regexp = /^(\d*([.](?=\d{1}))?\d+)+((?!\2)[.]\d\d)?$/;

  var flag = 0;
  if(tutorCategory == ''){
    $(".tutorCategory").remove();  
  	$("#tutorCategory").parent().append("<div class='tutorCategory' style='color:red;'>Tutor category field is not empty.</div>");
    flag = 1;
  } else {
    $(".tutorCategory").remove();          
  }

  if(tutor_sub_category_name == ''){
    $(".tutor_sub_category_name").remove();  
    $("#tutor_sub_category_name").parent().append("<div class='tutor_sub_category_name' style='color:red;'>Tutor sub-category field is not empty.</div>");
    flag = 1;
  } else {
    $(".tutor_sub_category_name").remove();          
  }

  if(rate_per_hour == ''){
    $(".rate_per_hour").remove();  
    $("#rate_per_hour").parent().append("<div class='rate_per_hour' style='color:red;'>Rate per hour field is not empty.</div>");
    flag = 1;
  }else if(rate_per_hour == 0){
    $(".rate_per_hour").remove();  
    $("#rate_per_hour").parent().append("<div class='rate_per_hour' style='color:red;'>Zero price not allowed.</div>");
    flag = 1;
  } else if(!price_regexp.test(rate_per_hour)) {
    $(".rate_per_hour").remove();  
    $("#rate_per_hour").parent().append("<div class='rate_per_hour' style='color:red;'>Please enter proper price.</div>");
    flag = 1;
  } else {
    $(".rate_per_hour").remove();            
  }    
  if(flag == 1)
    return false;    
  else
  {            	
  	$.ajax({
      type:"POST",
      url:baseURL+"tutor_management/Tutor_subcategory/addTutorSubCategory",            
      data:{tutorCategory:tutorCategory, tutor_sub_category_name:tutor_sub_category_name, rate_per_hour:rate_per_hour, edit_id:edit_id},
      dataType:"json",
      beforeSend: function() {
       
      },
      complete: function(){
	    
	    },
      async:false,
      success:function(response){                     
      if(response.status == "success") {		           
        $('#tutorSubCategoryModal').modal('hide');
        toastr.success(response.message);       
        $("#tutorList").dataTable().fnDraw();                               
      } else if(response.status == "warning") {			            
        $("#tutorSubCategoryModal").modal('show', {backdrop: 'static'});
        toastr.warning(response.message);        
	    } else {
        $('#tutorSubCategoryModal').modal('hide');
        toastr.error(response.message);                                       
      }      
      }
  	});    
  }   
}


function editTutorSubCategory($this) // edit tutor sub-category data
{
	$('#tutorSubCatForm')[0].reset(); //reset of tutor sub-category value 
	$(".tutorCategory").remove();
  $(".tutor_sub_category_name").remove();
  $(".rate_per_hour").remove();

	$( "h3").html( "<h3 class='modal-title'><b>Edit Tutor Sub-Category</b></h3>" );
	if($($this).attr("data-row-id")){ // get tutor list id 
		$.ajax({
      type:"POST",
      url:baseURL+"tutor_management/Tutor_subcategory/viewTutorSubCategory",            
      dataType:"json",
      data:{"key":$($this).attr("data-row-id")},
      beforeSend: function(){
      },
      complete: function(){		    
	    },
      async:false,
      success:function(response) {   
      if(response.status == "success") {
        $("#user_key").val(response.tutorData.tut_sub_cat_id);
        $("#tutorCategory").val(response.tutorData.tut_cat_id);                                         
        $("#tutor_sub_category_name").val(response.tutorData.subcategory_name);                                         
        $("#rate_per_hour").val(response.tutorData.rate_per_hour);                                         
     	} 
    }
	});
  } else {
    $("#user_key").val('');    
    toastr.error("Something went wrong.");
 	} 	 
 	$('#tutorSubCategoryModal').modal('show');
}

