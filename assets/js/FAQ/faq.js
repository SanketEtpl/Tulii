function deleteFaq($this)
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this faq?</b>');
    $(".inner_footer").html('<button type="button" onclick="delFaq('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
    
  } else {
    toastr.error("Something went wrong!!!.");              
  }
}

function delFaq(key){ 
  $.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: function() {
    },
    complete: function(){  
    },
    url: baseURL+"faq_management/Faq_controller/deleteFAQ",
    data: {"key":key},
  }).success(function (json) {
    if(json.status == "success"){
      toastr.success(json.msg); // success message
      $("#table").dataTable().fnDraw();
      }else{
      toastr.error(json.msg); // error message
    }
});
$('.commanPopup').modal('hide');    
}

function faqList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.    
    "ajax": {
      "url": baseURL+"faq_management/Faq_controller/ajax_list",
      beforeSend: function() {
      var search = $("input[type=search]").val();
      if(search=="")             
      $("input[type=search]").on("keyup",function(event) {
        if($("#clear").length == 0) {
            if($(this).val() != ""){
            $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
          } 
        }
        if($(this).val() == "")  
        $("#clear").remove();      
      }); 
      $("input[type=search]").keydown(function(event) {
        k = event.which;
        if(k === 32 && !this.value.length)
          event.preventDefault();
      });
    },
    complete: function(){
      $("#LoadingDiv").css({"display":"none"}); 
    },
    "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
      "targets": [ 0,5 ], //first column / numbering column
      "orderable": false, //set not orderable
    },
    ],
  });  
} 

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();  
  faqList();
}

function viewFAQ($this) // view of faq
{
  if($($this).attr("data-row-id")){            
    $( "h3" ).html( "<h3 class='modal-title'><b>View FAQ</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"faq_management/Faq_controller/viewFAQ",
      beforeSend: function() {
      },
      complete: function(){      
      },
      data: {"key":$($this).attr("data-row-id"),"role_name":$($this).data('role-name')},
      }).success(function (json) {       
      if(json.status == "success") {
        $("#viewTitle").text(json.faqData.faq_title);
        $("#viewSubTitle").text(json.faqData.faq_sub_title);
        $("#viewDescription").text(json.faqData.faq_description); 
        $("#viewRole").text(json.role_name);
        $('#viewFaqModal').modal('show', {backdrop: 'static'});
      } else {
        toastr.error(json.msg); 
      }
    });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
} 

function viewDesc($this) // view of faq
{
  if($($this).attr("data-row-id")){            
    $( "h3" ).html( "<h3 class='modal-title'><b>View FAQ Description</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"faq_management/Faq_controller/viewFAQ",
      beforeSend: function() {      
      },
      complete: function(){        
      },
      data: {"key":$($this).attr("data-row-id"),"role_name":$($this).data('role-name')},
      }).success(function (json) {        
      if(json.status == "success"){
        $("#viewDescription1").text(json.faqData.faq_description); 
        $('#viewFaqModal1').modal('show', {backdrop: 'static'});
      } else {
        toastr.error(json.msg); 
      }
    });
  } else {
    toastr.error("Something went wrong!!!.");         
  }
} 

function editFAQ($this) // edit faq list data
{  
  $('#faqForm')[0].reset(); //reset of faq list value 
  $(".description").remove();
  $(".title").remove();
  $(".sub_title").remove();
  $(".userRole").remove();  
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit FAQ</b></h3>" );
  if($($this).attr("data-row-id")){ // get faq list id 
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"faq_management/Faq_controller/viewFAQ",
      beforeSend: function() {      
      },
      complete: function() {      
      },
      data: {"key":$($this).attr("data-row-id"),"role_name":$($this).data('role-name')},
      }).success(function (json) {
      if(json.status == "success"){            
        $("#user_key").val($($this).attr("data-row-id"));
        $('#userRole').val(json.faqData.service_id).attr('selected','selected');         
        $("#title").val(json.faqData.faq_title);
        $("#sub_title").val(json.faqData.faq_sub_title);
        $("#description").val(json.faqData.faq_description);
        $('#FAQModal').modal('show', {backdrop: 'static'});        
      } else {
        toastr.error(json.msg);
      }
    });
  } else {
    $("#user_key").val('');
    $('#FAQModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
      if($(this).val() != ""){
      $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
  k = event.which;
  if (k === 32 && !this.value.length)
    event.preventDefault();
  });

  $('#faqForm')[0].reset();
  $('#btnFAQ').click(function(){ // save record of faq list  
    var description = $("#description").val();
    var title = $("#title").val();
    var sub_title = $("#sub_title").val();
    var user_key = $("#user_key").val();
    var userRole = $("#userRole").val();
    var flag = 0;
    if(userRole == ''){
      $(".userRole").remove();  
      $("#userRole").parent().append("<div class='userRole' style='color:red;'>Please select role.</div>");
      flag = 1;
    }else{
      $(".userRole").remove();          
    }

    if(title == ''){
      $(".title").remove();  
      $("#title").parent().append("<div class='title' style='color:red;'>Title field is required.</div>");
      flag = 1;
    }else{
      $(".title").remove();          
    }

    if(sub_title == ''){
      $(".sub_title").remove();  
      $("#sub_title").parent().append("<div class='sub_title' style='color:red;'>Sub title field is required.</div>");
      flag = 1;
    }else{
      $(".sub_title").remove();          
    }

    if(description == ''){
      $(".description").remove();  
      $("#description").parent().append("<div class='description' style='color:red;'>Description field is required.</div>");
      flag = 1;
    }else{
      $(".description").remove();          
    }

    if(flag == 1)
      return false;    
    else
    {      
      $.ajax({
        type:"POST",
        url:baseURL+"faq_management/Faq_controller/addFAQ",            
        data:{"editData":user_key, "title":title,"sub_title":sub_title,"description":description,"userRole":userRole},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#FAQModal").modal('hide');
            $("#table").dataTable().fnDraw();
            toastr.success(response.message);                                      
          } else {
            $("#LoadingDiv").css({"display":"none"});
            $("#FAQModal").modal('show');
            toastr.error(response.message);                                       
          }
        }
      });    
    }   
  });  

  $('#addPopUp').on("click",function(){
    $( "h3" ).html( "<h3 class='modal-title'><b>Add FAQ</b></h3>" );
    $(".title").remove();
    $(".sub_title").remove();
    $(".description").remove();
    $(".userRole").remove();    
    $("#user_key").val("");
    $("#faqForm")[0].reset();
    $("#FAQModal").modal('show');    
  });
});