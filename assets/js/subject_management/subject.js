function deleteSubject($this)
{
  	if($($this).attr("data-row-id")){
	    var kay= "'"+ $($this).attr("data-row-id") +"'";
	    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	    $(".inner_body").html('<b>Are you sure you want to delete this subject?</b>');
	    $(".inner_footer").html('<button type="button" onclick="delSubject('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
	    $('.commanPopup').modal('show');       
    } else {
 		toastr.error("Something went wrong!!!.");           
 	}
}  

function delSubject(key){ 
 	$.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function() {
    	    //$('#LoadingDiv').show();
        },
        complete: function(){
        	$('#LoadingDiv').hide();
        },
         	url: baseURL+"subject_management/Subject/deleteSubject",
          	data: {"key":key},
        }).success(function (json) {
          	if(json.status == "success") {
	            toastr.success(json.msg); // success message
	            $("#table").dataTable().fnDraw();
            } else {
            	toastr.error(json.msg); // error message
          	}
     	});
    $('.commanPopup').modal('hide');    
}

function editSubject($this) // edit subject list data
{
	$('#subjectForm')[0].reset(); //reset of subject list value 
	$(".teacher_name").remove();
	$(".subject_name").remove();
	$("#teacher_name").prop('disabled', true);
	
	//$('#LoadingDiv').show();
	$( "h3" ).html( "<h3 class='modal-title'><b>Edit subject</b></h3>" );
	if($($this).attr("data-row-id")){ // get subject list id 
		
		$.ajax({
		        type:"POST",
		        url:baseURL+"subject_management/Subject/showSubject",            
		         dataType:"json",
		         beforeSend: function() {
                
                },
                complete: function(){
     		    
  			    },
		        async:false,
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			         $("#subject_name").html(response.data); 
			                                          
		         	} 
		        }
	      	});

		$("#subject_name").multiselect('destroy');

	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: baseURL+"subject_management/Subject/viewSubject",
	        data: {"key":$($this).attr("data-row-id")},
	    }).success(function (json) {
	        if(json.status == "success"){            
	        	$("#user_key").val($($this).attr("data-row-id"));
	        	$("#teacher_name").val(json.subData.teacher_name);
	          	//$("#subject_name").val(json.subData.sub_id);
	         var str = json.subData.sub_id;
	         var str_array = str.split(',');
	          $("#subject_name").multiselect('select',str_array); 
	           for(var i = 0; i < str_array.length; i++) {
	         
	          
	          }	
	          	        
	        } else {
	            toastr.error(json.msg);
	        }
	    });
	   
      
  	} else {
    $("#user_key").val('');
    
 	}
 	 $('#LoadingDiv').hide();
 	$('#subjectModal').modal('show');
}

$(document).ready(function(){
	$('#subjectForm')[0].reset();
  	$('#btnSubject').click(function() { // save record of subject list  

  	 var selected = $("#subject_name option:selected");
  	  var subject_name = "";
      selected.each(function () {
       if(subject_name == '') {
                    subject_name +=  $(this).val(); 
                  } else {
                     subject_name += ','+ $(this).val(); 
                  }
          });	

	    var teacher_name = $("#teacher_name").val();
	    var user_key = $("#user_key").val();
	    var flag = 0;
	    if(subject_name == ''){
		    $(".subject_name").remove();  
	    	$("#subject_name").parent().append("<div class='subject_name' style='color:red;'>Please select subject.</div>");
	      	flag = 1;
	    } else {
	      	$(".subject_name").remove();          
	    }

	    if(teacher_name == ''){
	        $(".teacher_name").remove();  
	   	    $("#teacher_name").parent().append("<div class='teacher_name' style='color:red;'>Please enter teacher name.</div>");
	      	flag = 1;
	    } else {
	      	$(".teacher_name").remove();          
	    }

	    if(flag == 1)
	      	return false;    
	    else
	    {      
	      
	      	$.ajax({
		        type:"POST",
		        url:baseURL+"subject_management/Subject/assignTeacher",  
		        beforeSend: function() {
                //$('#LoadingDiv').show();
                },
                complete: function(){
     		    $('#LoadingDiv').hide();
  			    },          
		        data:{"editData":user_key,"subject_name":subject_name,"teacher_name":teacher_name},
		        dataType:"json",
		        async:false,
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			           
			            $("#subjectModal").modal('hide');
			            $("#table").dataTable().fnDraw();
			            toastr.success(response.message);                                      
		         	} else if(response.status == "warning") {
			            
			            $("#subjectModal").modal('show');
			            toastr.warning(response.message); 
			        } else {
			            $("#subjectModal").modal('show');
			            toastr.error(response.message);                                       
		          	}
		        }
	      	});    
	    }   
	});  

  	
  $('#addPopUp').on("click",function(){
   $( "h3" ).html( "<h3 class='modal-title'><b>Assign subject</b></h3>" );
    $(".subject_name").remove();
    $(".teacher_name").remove();
    $("#user_key").val("");
    $("#subjectForm")[0].reset(); 
    $("#teacher_name").prop('disabled', false);
    $.ajax({
		        type:"POST",
		        url:baseURL+"subject_management/Subject/showSubject",            
		         dataType:"json",
		         beforeSend: function() {
                //$('#LoadingDiv').show();
                },
                complete: function(){
     		    $('#LoadingDiv').hide();
  			    },
		        async:false,
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			         $("#subject_name").html(response.data); 
			                                          
		         	} 
		        }
	      	});
    $("#subject_name").multiselect('destroy');
    $("#subject_name").multiselect();
   

    $("#subjectModal").modal('show');    
  });

   $('#addSubjectPopUp').on("click",function(){
   $("#ModalLabel").html('Add subject<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<div class="row"><div class="col-md-6"><div class="form-group"><label class="control-label" for="lbl_subject">Subject<span class="required_star">*</span></label><input placeholder="Subject Name" required="true" id="add_subject_name" name="add_subject_name" maxlength="20" class="form-control name" type="text"><div style="display:none;color:red;" id="error_subject_name" ></div></div></div></div>');
    $(".inner_footer").html('<button data-dismiss="modal" class="btn btn-default" type="button">Close</button> <button class="btn btn-info" type="button" onclick="addSubject();" id="btnAddSubject">Save</button>');
    $('.commanPopup').modal('show');  
  });

 $('#teacher_name').on('change', function () {

                 
                $.ajax({
		        type:"POST",
		        url:baseURL+"subject_management/Subject/showSubject",            
		         dataType:"json",
		         async:false,
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			         $("#subject_name").html(response.data); 
			                                          
		         	} 
		        }
	      	});
		$("#subject_name").multiselect('destroy');

		$.ajax({
	        type: "POST",
	        dataType: "json",
	        url: baseURL+"subject_management/Subject/viewSubject",
	        data: {"teacher_name": this.value},
	    }).success(function (json) {
	        if(json.status == "success"){            
	        if(json.subData.sub_id){
	         var str = json.subData.sub_id;
	         var str_array = str.split(',');
	          $("#subject_name").multiselect('select',str_array); 
	           } else{
	           $("#subject_name").multiselect(); 	
	           }
	          	        
	        } else {
	            //toastr.error(json.msg);
	            $("#subject_name").multiselect(); 
	        }
	    });
       
    });

});

function addSubject(){
	  var add_subject_name = $("#add_subject_name").val();
	   
	    var flag = 0;
	      if(add_subject_name == ''){
		  $("#error_subject_name").css("display","block");
		  $("#error_subject_name").html("Please select subject.");      
	      	flag = 1;
	    } else {
	      	$("#error_subject_name").css("display","none");       
	    }

	     if(flag == 1)
	      	return false;    
	    else
	    {      
	      	
	      	$.ajax({
		        type:"POST",
		        url:baseURL+"subject_management/Subject/addSubject",            
		        data:{subject_name:add_subject_name},
		        dataType:"json",
		         beforeSend: function() {
               // $('#LoadingDiv').show();
                },
                complete: function(){
     		    $('#LoadingDiv').hide();
  			    },
		        async:false,
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			           
			           $('.commanPopup').modal('hide');
			           toastr.success(response.message);                                      
		         	} else if(response.status == "warning") {
			         $("#error_subject_name").html(response.message +".");
			         $("#error_subject_name").css("display","block");
			        } else {
			            toastr.error(response.message);                                       
		          	}
		        }
	      	});    
	    }   
}

function onlyAlphabets(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 7 && charCode < 10) || (charCode == 32) || (charCode == 222) || (charCode == 189) || (charCode == 173))
            return true;
        else
            return false;
    }
    catch (err) {
        alert(err.Description);
    }
}