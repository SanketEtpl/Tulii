$(document).ready(function(){
	$(".name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$(".email").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
    	 /*if(this.value.indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault(); */
		if(this.value.indexOf('--') !== -1 && (e.keyCode == 189))
     	e.preventDefault();
     	if(this.value.indexOf('__') !== -1 && (e.keyCode == 189))
     	 e.preventDefault();
  		if(this.value.indexOf('__') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      	e.preventDefault();
	    if(this.value.indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();

        if ($.inArray(e.keyCode, [190,50,189,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });

    $(".phone").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".address").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191,189,109,173]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });
    $(".age").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".password").keydown(function(e) {        
        k = e.which;
        if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 32 || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
            if ($(this).val().length == 100) {
                if (k == 8) {
                    return true;
                } else {
                    e.preventDefault();
                    return false;
                }
            }
        } else {
        e.preventDefault();
            return false;
        }
       /* if($(this).val().indexOf(' ') !== -1)
        e.preventDefault();*/
        if (k === 32 && !this.value.length)
            e.preventDefault();
    });
    
     $(".contact_number").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });    

    $(".price").keydown(function (e) {    
    if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();    
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13,110,190,32 ]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#message").keydown(function (e) {      
        if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });

     $(".description_valid").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191,222,189]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });

    $(".title_valid").keydown(function(event) {
      k = event.which;
      if ((k >= 65 && k <= 90) || k == 191 || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
        if ($(this).val().length == 100) {
          if (k == 8) {
            return true;
          } else {
            event.preventDefault();
            return false;
          }
        }
      } else {
        event.preventDefault();
        return false;
      }
      if (k === 32 && !this.value.length)
        event.preventDefault();
    });

   $(".charnumber").keydown(function (e) {      
      if(e.keyCode === 32 && !this.value.length)
      e.preventDefault();
    if ($.inArray(e.keyCode, [189,222,32,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
          (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
               return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
      /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     e.preventDefault();*/
     if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     e.preventDefault();
    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
      e.preventDefault();
  });
   
  $(".licenseNo").keydown(function (e) {      
      if(e.keyCode === 32 && !this.value.length)
      e.preventDefault();
    if ($.inArray(e.keyCode, [189,222,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
          (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
               return;
      }
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
      /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     e.preventDefault();*/
     if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     e.preventDefault();
    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
      e.preventDefault();
  });
});