var flag = 1;
function bannerList(){
  var bannerList = $('#bannerList').DataTable();
  $('#bannerList').empty();
  bannerList.destroy();
  $('#bannerList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": baseURL+"banner_management/Banner/bannerList",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search=="")          
        $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
             if($(this).val() != ""){
              $("#bannerList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
              event.preventDefault();
        }); 
       },
      complete: function(){
      $("#LoadingDiv").css({"display":"none"}); 
      $("#bannerList").css({"opacity":"1"});
    },
        "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,2,3,4], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });  
}

function deleteBanner($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this banner?</b>');
    $(".inner_footer").html('<button type="button" onclick="delBanner('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
} 

function delBanner(key){ 
  $.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: function() {
    //  $('#LoadingDiv').show();
    },
    complete: function(){
    //$('#LoadingDiv').hide();
    },
    url: baseURL+"banner_management/Banner/deleteBanner",
    data: {"key":key},
    }).success(function (json) {
    if(json.status == "success"){
      $(this).parents("tr:first").remove();
      $("#bannerList").dataTable().fnDraw();
        toastr.success(json.msg); // success message
      } else {
        toastr.error(json.msg); // error message
    }
  });
  $('.commanPopup').modal('hide');    
}

function BannerStatus_popup(userId,user_status,active_id,inActive_id)
{ 
  var temp = "bannerStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
  $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
  $(".inner_body").html('<b>Are you sure you want to change status?</b>');
  $(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
  $('.commanPopup').modal('show');
}

function bannerStatus(userId,user_status,active_id,inActive_id)
{ 
  $('.commanPopup').modal('hide');   
  $.ajax({
    type : "POST",
    dataType : "json",
    url : baseURL + "banner_management/Banner/bannerStatus",
    data : { userId : userId, user_status: user_status} 
    }).done(function(data){
    if(user_status == 1) { 
      var str_success="Banner image has been activated successfully.";
      var str_error="Banner image has been activation failed.";

      $("#"+active_id).removeClass("btn-active-disable");
      $("#"+active_id).addClass("btn-active-enable");

      $("#"+inActive_id).removeClass("btn-inactive-enable");
      $("#"+inActive_id).addClass("btn-inactive-disable");

      $("#"+active_id).prop('disabled', true);
      $("#"+inActive_id).prop('disabled', false);
    } else {
      $("#"+active_id).removeClass("btn-active-enable");
      $("#"+active_id).addClass("btn-active-disable");

      $("#"+inActive_id).removeClass("btn-inactive-disable ");
      $("#"+inActive_id).addClass("btn-inactive-enable");

      var str_success="Banner image has been inactivated successfully.";
      var str_error="Banner image has been inactivation failed.";

      $("#"+active_id).prop('disabled', false);
      $("#"+inActive_id).prop('disabled', true);
    }
    
    if(data.status = true) {
      toastr.success(str_success);
    } else if(data.status = false) {
      toastr.error(str_error);        
    } else {
      toastr.error("Access denied..!");
    }
  });  
}

function editBanner($this)
{
  $(".banner_img").remove();
  $(".banner_name").remove();
  $('#bannerForm')[0].reset();  
  $("#showFilename").html("No file chosen"); 
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit Banner</b></h3>" );
  $("#parent_id").val();
  if($($this).attr("data-row-id")){
    //$("#LoadingDiv").css({"display":"block"});
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"banner_management/Banner/viewBanner",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
    if(json.status == "success"){     
      //$("#bannerModal").css({"display":"none"});       
      $("#user_key").val($($this).attr("data-row-id"));
      $("#banner_name").val(json.bannerData.title);                        
      $('#bannerModal').modal('show', {backdrop: 'static'});        
    } else{           
      toastr.error(json.msg,"Error:");
    }
  });
  } else {
    $("#user_key").val('');
    $('#bannerModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){ 
  var imgSize = 0;
  $('#banner_img').bind('change', function() {
    imgSize = this.files[0].size;    
  });
  //$("#showFilename").html("No file chosen");    
  $('#addPopUp').on("click",function(){    
    $("#showFilename").html("No file chosen"); 
    $( "h3" ).html( "<h3 class='modal-title'><b>Add banner</b></h3>" );
    $("#user_key").val("");
    $(".banner_name").remove();
    $(".banner_img").remove();
    $("#bannerForm")[0].reset();
    $("#bannerModal").modal('show');   
  });
  $('input[type="file"]').change(function(e){
    var width = 0;
    var height=0;
    var fileUpload = document.getElementById("banner_img");
    var reader = new FileReader();
    if (fileUpload.files[0] && fileUpload.files[0].type.match('image.*')) {
      reader.readAsDataURL(fileUpload.files[0]);
    }
    //reader.readAsDataURL(fileUpload.files[0]);
    reader.onload = function (e) {
    var image = new Image();
    image.src = e.target.result;           
    image.onload = function () {
        var height = this.height;
        var width = this.width;      
        document.getElementById('BannerWHImage').width = width;
        document.getElementById('BannerWHImage').height = height;
      };
    }
  });
  
  $('#btnBanner').click(function(){   
    var banner_name = $("#banner_name").val();
    var banner_img = $("#banner_img").val();
    var w =0;
    var h =0;
    var w = $("#BannerWHImage").attr('width');
    var h = $('#BannerWHImage').attr('height');
    var ext = $('#banner_img').val().split('.').pop().toLowerCase();   
    var flag = 0;
    var size = 2097152;
    if(banner_name == ''){
      $(".banner_name").remove();  
      $("#banner_name").parent().append("<div class='banner_name' style='color:red;'>Banner name field is required.</div>");
      flag = 1;
    }else{
      $(".banner_name").remove();        
    }

    if(banner_img == ''){
      $(".banner_img").remove();  
      $("#banner_img").parent().append("<div class='banner_img' style='color:red;'>Banner image field is required.</div>");
      flag = 1;
    } else if(typeof w === "undefined" || typeof h === "undefined"){
     $(".banner_img").remove();  
      $("#banner_img").parent().append("<div class='banner_img' style='color:red;'>Banner image width and height undefined.</div>");
      flag = 1;
    } else if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
      $(".banner_img").remove();  
      $("#banner_img").parent().append("<div class='banner_img' style='color:red;'>Uploaded file is not a valid image.</div>");
      flag = 1;
    } else if(imgSize >= size){
     $(".banner_img").remove();  
      $("#banner_img").parent().append("<div class='banner_img' style='color:red;'>Banner image should be less than 2MB.</div>");
      flag = 1;
    } else if(w < 1800 || h< 600){
       $(".banner_img").remove();  
        $("#banner_img").parent().append("<div class='banner_img' style='color:red;'>Banner image should be greater than 1800*600.</div>");
        flag = 1;
    } else {
      $(".banner_img").remove();          
    }
    
    if(flag == 1)
      return false;    
    else
    {
      //$("#LoadingDiv").css({"display":"block"});
      var formData = new FormData($('#bannerForm')[0]); 
      $.ajax({
        type:"POST",
        url:baseURL+"banner_management/Banner/addBanner",            
        dataType:"json",
        data:formData,            
        processData: false,
        contentType: false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#bannerModal").css({"display":"none"});
            $("#banner_name").val("");
            $("#banner_img").val("");
            $("#showFilename").html("No file chosen");
            toastr.success(response.message);
            $("#bannerList").dataTable().fnDraw();
            $("#user_key").val('');
            document.getElementById('BannerWHImage').width = 0;
            document.getElementById('BannerWHImage').height = 0;
            $("#bannerModal").modal('hide');
          }
          else
          {
            $("#bannerModal").css({"display":"none"});
            toastr.error(response.message); // success message
            $("#bannerModal").dataTable().fnDraw();
          }
        }
      }); 
    }   
  });    
});
