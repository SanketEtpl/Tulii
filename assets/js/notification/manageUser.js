$(document).ready(function(){
  $(".message").remove(); 
  $('#selectAll').change(function(){
    if(this.checked == true)
      $('.chkListOfUsers').prop("checked",true);
    else
      $('.chkListOfUsers').prop("checked",false);
  });
  $(document).on('change', '[class=chkListOfUsers]', function() {
    $('#selectAll').prop("checked",false);
  }); 

  $("#table .sorting").click(function() {
    $("#selectAll").prop("checked",false);
  });

  $('#btnSendNotification').on("click",function() { 
    if($("input:checkbox[class=chkListOfUsers]:checked").prop('checked') == true) {
    $(".message").remove(); 
    var list = [];
    $("input:checkbox[class=chkListOfUsers]:checked").each(function () {  
      list.push($(this).val());
    });
   // console.log(list);
    $("#user_key").val(list);
      $("#manageNotifModal").modal('show');    
      }
    else{
        $("#ModalLabel").html('Alert<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
        $(".inner_body").html('<b>Please select at least one record.</b>');
        $(".inner_footer").html('<button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>');
        $('.commanPopup').modal('show');   
      //alert("Please select at least one record.");
    }                          
  });

  
$("#btnSave").on("click",function(){
  var message = $("#message").val();
  if(message == ''){
    $(".message").remove();  
    $("#message").parent().append("<div class='message' style='color:red;'>Message field is required.</div>");
    flag = 1;
  }else{
    $(".message").remove();        
  }
  if(message != "")
  {
    //$("#LoadingDiv").css({"display":"block"});
     $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL+"notification_management/Manage_user/sendNotification",
        data: {"message":message,"user_key":$('#user_key').val()},
    }).success(function (json) {
        if(json.status == "success"){
          $("#LoadingDiv").css({"display":"none"});
          $("#manageNotifModal").modal('hide'); 
          toastr.success(json.msg);          
         }else{
            toastr.error(json.msg);
            $("#manageNotifModal").modal('show'); 
            $("#LoadingDiv").css({"display":"none"});
          }
    });
  } 
    $("#message").val("");
    $("input:checkbox[class=chkListOfUsers]:checked").prop('checked',false);
    $("input:checkbox[class=selectAll]:checked").prop('checked',false);

  /*$("#table_filter").click(function(){
    alert("hello");
  });*/
});

});
