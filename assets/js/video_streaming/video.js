jQuery(document).ready(function(){  
 jQuery('#LoadingDiv').show();
  jQuery(document).on("click", ".deleteVideo", function(){
    var video_id = $(this).data("videoid");
    var temp = "delVideo('"+video_id+"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b>Are you sure you want to delete this video?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  });

  jQuery(document).on("click", ".viewVideo", function(){
    var videopath = $(this).data("videopath");
    //var temp = "delVideo('"+videopath+"');"; 
    //onclick="'+temp+'"
    $("#ModalLabel").html('Video<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<video style="width:100%;"  controls class="video-js vjs-default-skin"><source src="'+videopath+'" type="video/webm" >Sorry, your browser doesn\'t support the video element.</video>');
    //$(".inner_body").html('<video style="width:100%;" src="'+videopath+'" controls class="popvideo"></video>');
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');
   
  });

  });

function loadVideoList()
{   
  var table = $('#videoList-grid').DataTable();
              $('#videoList-grid').empty();
            table.destroy();

       $('#videoList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"video_streaming/Video_list/videoList",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")                
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#videoList-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
             },
             complete: function(){
              $('#LoadingDiv').hide();
              $("#videoList-grid").css({"opacity":"1"});
        
        },
       
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,7], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    }); 
}
function delVideo(video_id)
{
  if(video_id){
      
      $.ajax({
            type: "POST",
            dataType: "json",
            url: baseURL+"video_streaming/Video_list/deleteVideo",
            beforeSend: function() {
             //$('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
            data: {"key":video_id},
        }).success(function (json) {
            if(json.status == "success"){
            toastr.success(json.msg);
             $("#videoList-grid").dataTable().fnDraw();
              
             }else{
              toastr.error(json.msg);
            }
        });
     
  } else {     
    toastr.error("Some error has been occured, please try again!!!.");    
  }
  $('.commanPopup').modal('hide');
     
  //loadVideoList();
} 
