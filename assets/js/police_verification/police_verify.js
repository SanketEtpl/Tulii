$(document).ready(function(){
	$('#policeVerifyForm')[0].reset();
  	$('#btnUploadDocuments').click(function() { // save record of subject list  
	    var uploadDocuments = $("#uploadDocuments").val();
      var verify_no = $("#verify_no").val();
	    var user_key = $("#user_key").val();
	    var flag = 0;
	    var ext = $('#uploadDocuments').val().split('.').pop().toLowerCase();   

    	if(uploadDocuments == ''){
		    $(".uploadDocuments").remove();  
	    	$("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>Please upload documents.</div>");
	      	flag = 1;
	    } else {
	    	$(".uploadDocuments").remove();
	    } 

      if(verify_no == ''){
        $(".verify_no").remove();  
        $("#verify_no").parent().append("<div class='verify_no' style='color:red;'>Verification no field is required.</div>");
          flag = 1;
      } else {
        $(".verify_no").remove();
      } 

	    if(flag == 1)
	      	return false;    
	    else
	    {      
	      	//$("#LoadingDiv").css({"display":"block"});
	      	var formData = new FormData($('#policeVerifyForm')[0]);
	      	$.ajax({
		        type:"POST",
		        url:baseURL+"police_verification/Police_verify_doc/addDocuments",            
		        dataType:"json",
		        data: formData,	
		        cache: false,
		        contentType: false,
		        processData: false,		        	        
		        success:function(response){                     
		          	if(response.status == "success")
		          	{
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('hide');
			            $("#table").dataTable().fnDraw();
			            toastr.success(response.message);                                      
		         	} else if(response.status == "warning") {
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('show');
			            toastr.warning(response.message); 
			        } else {
			            $("#LoadingDiv").css({"display":"none"});
			            $("#policeVerifyModal").modal('show');
			            toastr.error(response.message);                                       
		          	}
		        }
	      	});    
	    }   
	}); 

  $('#editbtnUploadDocuments').click(function() { // update record of service provider record  
  var drivingLicense = $("#drivingLicense").val();
  var license_no = $("#license_no").val();
  var valid_from = $("#valid_from").val();
  var valid_until = $("#valid_until").val();
  var car_model = $("#car_model").val();
  var user_key = $("#user_key").val();
  var car_reg_no = $("#car_reg_no").val();
  var car_no_plate = $("#car_no_plate").val();
  var total_experience = $("#total_experience").val();
  var car_seating_capacity = $("#car_seating_capacity").val();

  var uploadCarImage = $("#uploadCarImage").val();
  var uploadScannedCpofLic = $("#uploadScannedCpofLic").val();
  var flag = 0;

  if(car_seating_capacity == ''){
    $(".car_seating_capacity").remove();  
    $("#car_seating_capacity").parent().append("<div class='car_seating_capacity' style='color:red;'>Car seating capacity field is required.</div>");
      flag = 1;
  }else if(car_seating_capacity > 15 ){
    $(".car_seating_capacity").remove();  
    $("#car_seating_capacity").parent().append("<div class='car_seating_capacity' style='color:red;'>Car seating capacity should be less than 15 seats.</div>");
      flag = 1;
  } else {
    $(".car_seating_capacity").remove();
  } 

  if(car_no_plate == ''){
    $(".car_no_plate").remove();  
    $("#car_no_plate").parent().append("<div class='car_no_plate' style='color:red;'>Car number plate field is required.</div>");
      flag = 1;
  } else {
    $(".car_no_plate").remove();
  } 

  if(total_experience == ''){
    $(".total_experience").remove();  
    $("#total_experience").parent().append("<div class='total_experience' style='color:red;'>Total experience field is required.</div>");
      flag = 1;
  } else {
    $(".total_experience").remove();
  } 

  if(car_model == ''){
    $(".car_model").remove();  
    $("#car_model").parent().append("<div class='car_model' style='color:red;'>Car model field is required.</div>");
      flag = 1;
  } else {
    $(".car_model").remove();
  } 

  if(car_reg_no == ''){
    $(".car_reg_no").remove();  
    $("#car_reg_no").parent().append("<div class='car_reg_no' style='color:red;'>Car reg. no field is required.</div>");
      flag = 1;
  } else {
    $(".car_reg_no").remove();
  } 
  
  if(valid_from == ''){
    $(".valid_from").remove();  
    $("#valid_from").parent().append("<div class='valid_from' style='color:red;'>Date valid from field is required.</div>");
      flag = 1;
  } else {
    $(".valid_from").remove();
  } 

  if(valid_until == ''){
    $(".valid_until").remove();  
    $("#valid_until").parent().append("<div class='valid_until' style='color:red;'>Date valid until field is required.</div>");
      flag = 1;
  } else {
    $(".valid_until").remove();
  } 

 // if( user_key == "" ) {
    if( uploadCarImage == '' ) {
      $(".uploadCarImage").remove();  
      $("#uploadCarImage").parent().append("<div class='uploadCarImage' style='color:red;'>Please upload car documents.</div>");
        flag = 1;
    } else {
      $(".uploadCarImage").remove();
    } 

    if( uploadScannedCpofLic == '' ) {
      $(".uploadScannedCpofLic").remove();  
      $("#uploadScannedCpofLic").parent().append("<div class='uploadScannedCpofLic' style='color:red;'>Please upload license documents.</div>");
        flag = 1;
    } else {
      $(".uploadScannedCpofLic").remove();
    }
//  }

  if(drivingLicense == ''){
    $(".drivingLicense").remove();  
    $("#drivingLicense").parent().append("<div class='drivingLicense' style='color:red;'>Driving license field is required.</div>");
      flag = 1;
  } else {
    $(".drivingLicense").remove();
  } 

  if(license_no == ''){
    $(".license_no").remove();  
    $("#license_no").parent().append("<div class='license_no' style='color:red;'>License no field is required.</div>");
      flag = 1;
  } else {
    $(".license_no").remove();
  }

  if(flag == 1)
      return false;    
  else
  {      
    //$("#LoadingDiv").css({"display":"block"});
      var formData = new FormData($('#editpoliceVerifyForm')[0]);
      $.ajax({
        type:"POST",
        url:baseURL+"police_verification/Police_verify_doc/editDocFile",            
        dataType:"json",
        data: formData, 
        cache: false,
        contentType: false,
        processData: false,                     
        success:function(response){                     
            if(response.status == "success")
            {
              $("#LoadingDiv").css({"display":"none"});
              $("#editpoliceVerifyModal").modal('hide');
              $("#table").dataTable().fnDraw();
              toastr.success(response.message);                                      
          } else {
              $("#LoadingDiv").css({"display":"none"});
              $("#editpoliceVerifyModal").modal('show');
              toastr.error(response.message);                                       
            }
        }
      });    
  }   
});     

  $(".close-page-refresh").on("click",function(){
    location.reload(); 
  });

  $('.addfiles').on('click', function() {     
    $('#slider_img').click();return false;
  });
 
  $('input[type="file"][id="uploadDocuments"]').change(function(e){
    var names = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
        names.push('<br />'+ $(this).get(0).files[i].name);
    }
    $('#showFilename').html(names);    
  });

/*  $('.editCarImg').on('click', function() {     
    $('#slider_img').click();return false;
  });*/
 
  $('input[type="file"][id="uploadCarImage"]').change(function(e){
    var names = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
        names.push('<br />'+ $(this).get(0).files[i].name);
    }
    $('#showFilenameCarImg').html(names);    
  });

/*    $('.addfiles').on('click', function() {     
    $('#slider_img').click();return false;
  });*/
 
  $('input[type="file"][id="uploadScannedCpofLic"]').change(function(e){
    var names = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
        names.push('<br />'+ $(this).get(0).files[i].name);
    }
    $('#showFilenameScanCpofLic').html(names);    
  });

   $(document).on("click", ".viewPic", function(){
    var baseurl = $(this).data("baseurl");
    var picname = $(this).data("picname");
    var error_msg = $(this).data("error");
    var headerMsg = $(this).data("headermsg");
    var imgPath = $(this).data("imgpath");
   if(picname){

    var array = picname.split("|");
   // var cnt =1 ;
    var temp= '';
     $("#modalHeader").html('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3><b>'+headerMsg+'</b></h3>');
     $("#modalBody").html('<div id="image-gallery"><div class="image-container"></div><img src="'+imgPath+'assets/images/left.svg" class="prev"/><img src="'+imgPath+'assets/images/right.svg"  class="next"/><div class="footer-info"><span class="current"></span>/<span class="total"></span></div></div> ');
     $("#modalFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
     $('.photoGallery').modal('show'); 
     
      a = new Array();
     for (var i in array){
      var http = new XMLHttpRequest();
      http.open('HEAD', baseurl+array[i], false);
      http.send();
      if(http.status != 404) {
      a[i]=baseurl+array[i]; 
      } else{
      a[i]=imgPath+'assets/images/not_found.png';   
      }
     
     }
    
    var images = Array();
    for(var j=0;j<a.length;j++)
    {
      images.push({small:a[j],big:a[j]}); 
    }  
       
     var curImageIdx = 1,
        total = images.length;
    var wrapper = $('#image-gallery'),
        curSpan = wrapper.find('.current');
    var viewer = ImageViewer(wrapper.find('.image-container'));
 
    //display total count
    wrapper.find('.total').html(total);
 
    function showImage(){
        var imgObj = images[curImageIdx - 1];
        viewer.load(imgObj.small, imgObj.big);
        curSpan.html(curImageIdx);

    }
 
    wrapper.find('.next').click(function(){
         curImageIdx++;
        if(curImageIdx > total) curImageIdx = 1;
        showImage();
    });
 
    wrapper.find('.prev').click(function(){
         curImageIdx--;
        if(curImageIdx < 0) curImageIdx = total;
        showImage();
    });
 
    //initially show image
    showImage();  
     } else{
     toastr.error(error_msg); 
     }
  });

});

function userStatus_popup(userId,user_status,active_id,inActive_id)
{ 
  var temp = "userStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
  $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
  $(".inner_body").html('<b>Are you sure you want to change status?</b>');
  $(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
  $('.commanPopup').modal('show');
}
function userStatus(userId,user_status,active_id,inActive_id)
{ 
 $('.commanPopup').modal('hide');
     $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "police_verification/Police_verify_doc/policeVerifyStatusChange",
      data : { userId : userId, user_status: user_status} 
      }).success(function(data){   
      if(user_status == 1) { 
        
        var str_success="Document verification has been completed successfully";
        var str_error="Document verification has been complete failed";

        $("#"+active_id).removeClass("btn-active-disable");
        $("#"+active_id).addClass("btn-active-enable");

        $("#"+inActive_id).removeClass("btn-inactive-enable");
        $("#"+inActive_id).addClass("btn-inactive-disable");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } else {
          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Document verification has been Incompleted successfully";
          var str_error="Document verification has been Incomplete failed";

          $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
        $("#table").dataTable().fnDraw();
       }
      else if(data.status = false) { 
          toastr.error(str_error);
        }
      else {
          toastr.error("Access denied..!");
        }
    }); 
  //$("#LoadingDiv").css({"display":"none"});
 }

function editPoliceDoc($this)
{
  $(".drivingLicense").remove();
  $(".license_no").remove();
  $(".valid_from").remove();
  $(".valid_until").remove();
  $(".car_model").remove();
  $(".car_reg_no").remove();
  $(".uploadCarImage").remove();
  $(".car_seating_capacity").remove();
  $(".car_no_plate").remove();
  $(".total_experience").remove();
  $(".uploadScannedCpofLic").remove();
  //$( "h3" ).html( "<h3 class='modal-title'><b>Edit documents</b></h3>" );
  $("#showFilenameCarImg").html("No file chosen");
  $("#showFilenameScanCpofLic").html("No file chosen");
  $('#editpoliceVerifyModal').modal('show');
  $("#user_name").val($($this).attr("data-row-username"));
  $('#user_id').val($($this).attr("data-row-id"));
 //$("#LoadingDiv").css({"display":"block"});
    if($($this).attr("data-row-id")){    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL+"police_verification/Police_verify_doc/getDetailsOfServiceProvider",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){                         
          $("#license_no").val(json.downloadData.license_no);    
          $("#valid_from").val(json.downloadData.valid_from_date);    
          $("#valid_until").val(json.downloadData.valid_until_date);    
          $("#drivingLicense").val(json.downloadData.driving_license_type);    
          $("#car_reg_no").val(json.downloadData.car_register_no);    
          $("#total_experience").val(json.downloadData.total_experience);  

          $("#car_no_plate").val(json.downloadData.car_number_plate);  
          $("#car_model").val(json.downloadData.car_model);   
          $("#car_seating_capacity").val(json.downloadData.car_seating_capacity);  
          $("#LoadingDiv").css({"display":"none"});               
         } else {
           toastr.error(json.msg); 
         }
      });
       $("#LoadingDiv").css({"display":"none"});
  }else{
    toastr.error("Something went wrong!!!.");         
  }
}


function downloadDocuments($this)
{
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"police_verification/Police_verify_doc/downloadDoc",
          data: {"key":$($this).attr("data-row-id"),"username":$($this).attr("data-row-username")},
      }).success(function (json) {
       // console.log(json);
          if(json.status == "success"){ 
            var array = Array();
            var array = json.downloadData.police_verify_doc.split('|');
            var imagePath=""
            for(var i=0; i< array.length; i++)
            {
              imagePath += '<div id="'+"image_"+i+'"><a href="'+json.imagePath+array[i]+'" download="'+array[i]+'"><img src="'+json.imagePath+array[i]+'" style="width:400px;height:250px" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download documents"></a><a href="'+json.imagePath+array[i]+'" download="'+array[i]+'" class="btn btn-info"><span class="fa fa-download"></span></a><a data-image-id="'+"image_"+i+'" data-row-userid="'+json.downloadData.user_id+'" data-row-image-path="'+array[i]+'" data-row-username="'+$($this).attr("data-row-username")+'" onclick="deleteDocumentsImage(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><span class="glyphicon glyphicon-remove-circle"></span></a></div>';
            }
            $("#documentsDown").html(imagePath);          
            $('#downloadModal').modal('show', {backdrop: 'static'});

         } else {
           toastr.error(json.msg); 
         }

      });
  }else{
    toastr.error("Something went wrong!!!.");         
  }
}

function deleteDocumentsImage($this) {
  if($($this).attr("data-row-userid")){  
   //$("#LoadingDiv").css({"display":"block"});  
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL+"police_verification/Police_verify_doc/deleteImage",
        data: {"image_id":$($this).attr("data-image-id"),"image":$($this).attr("data-row-image-path"),"user_id":$($this).attr("data-row-userid"),"username":$($this).attr("data-row-username")},
      }).success(function (json) {
          if(json.status == "success"){             
            if(json.deleteStaus == 1) {
              $(json.imageName).remove();
              if(!json.downloadData.trim())
              {
                 $('#downloadModal').modal('hide');
                 //location.reload();
                 $("#table").dataTable().fnDraw();
              }
              toastr.success(json.msg); 
            } else {
               toastr.error("Image not delete successfully .");  
            }

         } else {
           toastr.error(json.msg); 
         }
          $("#LoadingDiv").css({"display":"none"});
      });
  }else{
    toastr.error("Something went wrong!!!.");         
  }

}

function addPoliceDoc($this) {
    //$( "h3" ).html( "<h3 class='modal-title'><b>Add documents</b></h3>" );
    $(".uploadDocuments").remove();    
    $(".verify_no").remove();
    $("#showFilename").html("No file chosen");
    $("#user_key").val($($this).attr("data-row-id"));
    $("#add_user_name").val($($this).attr("data-row-username"));
    $("#btnUploadDocuments").attr('disabled',false);
    $("#policeVerifyForm")[0].reset();
    $("#policeVerifyModal").modal('show');     
}