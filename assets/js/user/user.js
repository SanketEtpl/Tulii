function userCheckEmail()
{
  $(".email1").remove();
  var user_key = $("#user_key1").val();
  var result = 0;
  if(user_key !="")
    return result;
  else
  {
    var email = $("#userEmail").val();  
    $.ajax({
      type:"POST",
      url:baseURL+"childcare_provider/Childcare/check_email_availability",
      data:{'email':email,'user_key':user_key},
      dataType:"json",
      async:false,          
      success:function(response){        
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#userEmail").parent().append("<div class='email1' style='color:red;'>"+ response.message +"</div>");
          $(".email2").remove();
          result = 1;
        }
        else
        {       
          $(".email1").remove();       
        }     
      }
    });  
  }
  return result;
}

function newRecord($this)
{
  if($($this).attr("data-row-id")){
   $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"user_management/UserController/newRecord",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            // $($this).parents("tr:first").remove();
              $("#table").dataTable().fnDraw();      
              var badge = $("#userBadge").html();
              var conInt = parseInt(badge);
              $("#userBadge").html(conInt-1);          
           }
        });      
  } else {      
    toastr.error("Something went wrong!!!.");
  }  
}

function userStatus_popup(userId,user_status,active_id,inActive_id)
{
 
var temp = "userStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure you want to change status?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function userStatus(userId,user_status,active_id,inActive_id)
{ 
 $('.commanPopup').modal('hide');
     $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "user_management/UserController/userStatusChange",
      data : { userId : userId, user_status: user_status} 
      }).success(function(data){   
      if(user_status == 1) { 
        
        var str_success="User profile has been activated successfully.";
        var str_error="User profile has been activation failed.";

        $("#"+active_id).removeClass("btn-active-disable");
        $("#"+active_id).addClass("btn-active-enable");

        $("#"+inActive_id).removeClass("btn-inactive-enable");
        $("#"+inActive_id).addClass("btn-inactive-disable");

        $("#"+active_id).prop('disabled', true);
        $("#"+inActive_id).prop('disabled', false);
        } else {
          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="User profile has been inactivated successfully.";
          var str_error="User profile has been inactivation failed.";

          $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
       }
      else if(data.status = false) { 
          toastr.error(str_error);
        }
      else {
          toastr.error("Access denied..!");
        }
    }); 
  //$("#LoadingDiv").css({"display":"none"});
 }
function deleteUserData($this) // delete record of price list
{
  if($($this).attr("data-row-id")){
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this user?</b>');
    $(".inner_footer").html('<button type="button" onclick="delUser('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');

  } else {     
        toastr.error("Something went wrong!!!.");    
  }
}
function delUser(key){ 
 $.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: function() {
      // $('#LoadingDiv').show();
       },
       complete: function(){
  $('#LoadingDiv').hide();
  },
     url: baseURL+"user_management/UserController/deleteUser",
    data: {"key":key},
  }).success(function (json) {
    if(json.status == "success"){
      toastr.success(json.msg); // success message
      $("#table").dataTable().fnDraw();
      $("#table").css({"opacity":"1"});
      }else{
      toastr.error(json.msg); // error message
    }
});
$('.commanPopup').modal('hide');    
}   

function editUserData($this)
{
  $('#userForm')[0].reset();
   $(".fname").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".userPassword").remove();
    $(".userCpassword").remove();
    $(".phone").remove();
    $(".userRole").remove();
    $(".certificateName").remove(); 
    $(".schoolCollege").remove(); 
    $(".uploadDocuments").remove(); 
    $(".valid_to").remove();  
    $(".valid_from").remove();  
    $(".parentDOB").remove();  
    $(".userDegree").remove(); 
    $(".specialization").remove();  
    $("#showFilename").html("No file chosen");
    $("#categoryDiv").empty();
    $("h3").html("<h3 class='modal-title'><b>Edit user</b></h3>");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"user_management/UserController/viewUserInfo",
          beforeSend: function() {
          // $('#LoadingDiv').show();
             },
             complete: function(){
          $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
        if(json.status == "success_both"){            
            $("#user_key1").val($($this).attr("data-row-id"));
            $("#fname").val(json.parentData.user_name);            
            $("#userEmail").val(json.parentData.user_email);
            $("#userEmail").prop('disabled',true);
            $("#contactNumber").val(json.parentData.user_phone_number);   
            $("#userRole").val(json.parentData.roleId);
            if(json.parentData.roleId == 3 || json.category == 2 || json.category == 3) {
              $("sub_category_div").hide();
            }
            $("#hidePwd").css({"display":"none"});
            $("#categoryDiv").html(json.categoryData);  
            if(json.parentData.user_gender.toLowerCase() == "female")
            $("#spGenderFemale").prop("checked",true);        
            else if(json.parentData.user_gender.toLowerCase() == "male")
            $("#spGenderMale").prop("checked",true);
            else
            $("#spGenderMale").prop("checked",true);

            $("#schoolCollege").val(json.parentData.school);
            $("#userDegree").val(json.parentData.degree).attr('selected','selected');
            $('#specialization').val(json.specializationJSON);
            $("#valid_from").val(json.parentData.education_from);
            $("#valid_to").val(json.parentData.education_to);
            $("#certificateName").val(json.parentData.certification_name); 
            
            //$('#specialization').html(json.specializationJSON);
            // $("#specialization").val(json.parentData.specialization).attr('selected','selected');
            $("#parentDOB").val(json.parentData.user_birth_date);
            $("#parentAge").val(json.parentData.user_age);

            $('#userModal').modal('show', {backdrop: 'static'});

            }
          else if(json.status == "success"){            
            $("#user_key1").val($($this).attr("data-row-id"));
            $("#fname").val(json.parentData.user_name);            
            $("#userEmail").val(json.parentData.user_email);
            $("#userEmail").prop('disabled',true);
            $("#contactNumber").val(json.parentData.user_phone_number);   
            $("#userRole").val(json.parentData.roleId);
            $("#hidePwd").css({"display":"none"});
            if(json.parentData.user_gender.toLowerCase() == "female")
              $("#spGenderFemale").prop("checked",true);        
            else if(json.parentData.user_gender.toLowerCase() == "male")
              $("#spGenderMale").prop("checked",true);
            else
              $("#spGenderMale").prop("checked",true);
            $("#parentDOB").val(json.parentData.user_birth_date);
            $("#parentAge").val(json.parentData.user_age);
            $("#schoolCollege").val(json.parentData.school);
            $("#userDegree").val(json.parentData.degree).attr('selected','selected');
            $('#specialization').html(json.specializationJSON);
            $("#specialization").val(json.parentData.specialization).attr('selected','selected');
            $("#valid_from").val(json.parentData.education_from);
            $("#valid_to").val(json.parentData.education_to);
            $("#certificateName").val(json.parentData.certification_name); 

            $('#userModal').modal('show', {backdrop: 'static'});        
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  } else {
    toastr.error("Something went wrong!!!.");
    $("#user_key1").val('');
    $('#userModal').modal('show', {backdrop: 'static'});
  }
}
function viewUserData($this)
{
  $('#userForm')[0].reset();
   $(".fname").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".userPassword").remove();
    $(".userCpassword").remove();
    $(".phone").remove();
    $(".userRole").remove();
    $(".certificateName").remove(); 
    $(".schoolCollege").remove(); 
    $(".uploadDocuments").remove(); 
    $(".valid_to").remove();  
    $(".valid_from").remove();  
    $(".parentDOB").remove();  
    $(".userDegree").remove(); 
    $(".specialization").remove();  
    $("#showFilename").html("No file chosen");
    $("#categoryDiv").empty();
    $("h3").html("<h3 class='modal-title'><b>View user</b></h3>");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"user_management/UserController/viewUserInfo",
          beforeSend: function() {
          // $('#LoadingDiv').show();
             },
             complete: function(){
          $('#LoadingDiv').hide();
        },
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
       // console.log(json);
        var userrole=$($this).attr('data-role');
        if(json.status == "success_both"){   
            $("#user_key1").val($($this).attr("data-row-id"));
            $("#fname").html(json.parentData.user_name);            
            $("#userEmail").html(json.parentData.user_email);
            $("#userEmail").prop('disabled',true);
            $("#contactNumber").html(json.parentData.user_phone_number);   
            $("#userRole").html(userrole);
            $("#schoolid").html('School/College');
            $("#certiid").html('Certificate name');
            if(json.parentData.roleId == 3 || json.category == 2 || json.category == 3) {
              $("sub_category_div").hide();
            }
            $("#hidePwd").css({"display":"none"});
            $("#specialid").css({"display":"block"});
            $("#addressid").css({"display":"none"});
            $("#edufromid").css({"display":"block"});
            $("#edutoid").css({"display":"block"});
            $("#categoryDiv").html(json.categoryData);  
            /*if(json.parentData.user_gender.toLowerCase() == "female")
               $("#spGenderFemale").prop("checked",true);        
            else if(json.parentData.user_gender.toLowerCase() == "male")
               $("#spGenderMale").prop("checked",true);
            else
              $("#spGenderMale").prop("checked",true);*/

            $("#spGender").html(json.parentData.user_gender);        
            $("#schoolCollege").html(json.parentData.school);
            $("#userDegree").html(json.parentData.degree);
            $('#specialization').html(json.specializationJSON);
            // $("#specialization").val(json.parentData.specialization).attr('selected','selected');
            $("#valid_from").html(json.parentData.education_from);
            $("#valid_to").html(json.parentData.education_to);
            $("#certificateName").html(json.parentData.certification_name); 
            $("#parentDOB").html(json.parentData.user_birth_date);
            $("#parentAge").html(json.parentData.user_age);
            if(json.parentData.upload_certificate){
               $("#uploadDocuments").attr('src', baseURL+'uploads/driver/certification/'+json.parentData.upload_certificate);
            }
            else{
              $("#uploadDocuments").hide();
              $("#uploadDoc").html('No File Uploaded');
            }

            $('#userModal').modal('show', {backdrop: 'static'});

            }
          else if(json.status == "success"){   
            $("#user_key1").val($($this).attr("data-row-id"));
            $("#fname").html(json.parentData.user_name);            
            $("#userEmail").html(json.parentData.user_email);
            $("#userEmail").prop('disabled',true);
            $("#contactNumber").html(json.parentData.user_phone_number);   
            $("#userRole").html(userrole);
            $("#hidePwd").css({"display":"none"});
            /*if(json.parentData.user_gender.toLowerCase() == "female")
              $("#spGenderFemale").prop("checked",true);        
            else if(json.parentData.user_gender.toLowerCase() == "male")
              $("#spGenderMale").prop("checked",true);
            else
              $("#spGenderMale").prop("checked",true);*/
            $("#spGender").html(json.parentData.user_gender);        
            $("#parentDOB").html(json.parentData.user_birth_date);
            $("#parentAge").html(json.parentData.user_age);
            $("#schoolCollege").html(json.parentData.school);
            $("#userDegree").html(json.parentData.degree);
            $("#tutor_address").html(json.parentData.tutor_address);
            $("#schoolid").html('Tutor Organization');
           
            /*$('#specialization').html(json.specializationJSON);
           // $("#specialization").val(json.parentData.specialization).attr('selected','selected');
            $("#valid_from").html(json.parentData.education_from);
            $("#valid_to").html(json.parentData.education_to);
            $("#certificateName").html(json.parentData.certification_name); */
            $("#certiid").html('Address Type');
            $("#certificateName").html(json.parentData.address_type); 
            $("#specialid").css({"display":"none"});
            $("#addressid").css({"display":"block"});
            $("#edufromid").css({"display":"none"});
            $("#edutoid").css({"display":"none"});
            //$("#certificateid").css({"display":"none"});

            if(json.parentData.certificate_doc){
               $("#uploadDocuments").attr('src', baseURL+'uploads/driver/certification/'+json.parentData.upload_certificate);
            }
            $('#userModal').modal('show', {backdrop: 'static'});        
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  } else {
    toastr.error("Something went wrong!!!.");
    $("#user_key1").val('');
    $('#userModal').modal('show', {backdrop: 'static'});
  }
}

$(document).ready(function(){

  $('#btnUserSave').click(function(){     
    var userName = $("#fname").val();
    var email = $("#userEmail").val();
    var phone = $("#contactNumber").val();
    var password = $("#userPassword").val();
    var cpassword= $("#userCpassword").val();
    var editData = $("#user_key1").val();
    var userRole= $("#userRole").val();

    var schoolCollege = $("#schoolCollege").val();
    var userDegree = $("#userDegree").val();
    var specialization = $("#specialization").val();
    var valid_from = $("#valid_from").val();
    var valid_to = $("#valid_to").val();
    var certificateName = $("#certificateName").val();
    var uploadDocuments= $("#uploadDocuments").val();
    var parentDOB = $("#parentDOB").val();

    var spGender = $("input[name=spGender]:checked","#userForm").val();
    if(userRole >2){
      var category= $("#category").val();
    } else {var category=0;}
     if(userRole >2 && category >0){
      var sub_category= $("#sub_category").val();
    } else {var sub_category=0;}
    var pwdPattern =/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/i;
    //var emailPattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var flag = 0;

    if(certificateName == ''){
      $(".certificateName").remove();  
      $("#certificateName").parent().append("<div class='certificateName' style='color:red;'>Certificate name field is required.</div>");
      flag = 1;
    } else if(certificateName.length < 3) {
      $(".certificateName").remove();  
      $("#certificateName").parent().append("<div class='certificateName' style='color:red;'>Certificate name minimum 3 characters are required.</div>");
      flag = 1;
    } else {
      $(".certificateName").remove();        
    }

    if(schoolCollege == ''){
      $(".schoolCollege").remove();  
      $("#schoolCollege").parent().append("<div class='schoolCollege' style='color:red;'>School/College name field is required.</div>");
      flag = 1;
    } else if(schoolCollege.length < 3) {
      $(".schoolCollege").remove();  
      $("#schoolCollege").parent().append("<div class='schoolCollege' style='color:red;'>School/College name minimum 3 characters are required.</div>");
      flag = 1;
    } else {
      $(".schoolCollege").remove();        
    }    
    if(editData == ""){
      if(uploadDocuments == ''){
        $(".uploadDocuments").remove();  
        $("#uploadDocuments").parent().append("<div class='uploadDocuments' style='color:red;'>Upload certificate field is required.</div>");
        flag = 1;
      }else{
        $(".uploadDocuments").remove();          
      }    
    }
    if(valid_from == ''){
      $(".valid_from").remove();  
      $("#valid_from").parent().append("<div class='valid_from' style='color:red;'>Education from date field is required.</div>");
      flag = 1;
    }else{
      $(".valid_from").remove();          
    } 

    if(parentDOB == ''){
      $(".parentDOB").remove();  
      $("#parentDOB").parent().append("<div class='parentDOB' style='color:red;'>Date of birth field is required.</div>");
      flag = 1;
    }else{
      $(".parentDOB").remove();          
    } 

    if(valid_to == ''){
      $(".valid_to").remove();  
      $("#valid_to").parent().append("<div class='valid_to' style='color:red;'>Education to date field is required.</div>");
      flag = 1; //return false;
    }else{
      $(".valid_to").remove();          
    } 

    var eDate = new Date(valid_to);
    var sDate = new Date(valid_from);
    if(valid_to!= '' && valid_from!= '' && sDate > eDate || valid_from == valid_to)
    {
      $(".valid_to").remove();  
      $("#valid_to").parent().append("<div class='valid_to' style='color:red;'>Education to date not greater than education from date or equal.</div>");
      flag = 1;
    }

    if(userDegree == ''){
      $(".userDegree").remove();  
      $("#userDegree").parent().append("<div class='userDegree' style='color:red;'>Degree field is required.</div>");
      flag = 1;
    }else{
      $(".userDegree").remove();          
    } 

    if(specialization == ''){
      $(".specialization").remove();  
      $("#specialization").parent().append("<div class='specialization' style='color:red;'>Specialization field is required.</div>");
      flag = 1;
    }else{
      $(".specialization").remove();          
    } 

    if(email == ''){      
      flag = 1;
      $(".email2").remove(); 
      $(".email1").remove(); 
      $("#userEmail").parent().append("<div class='email2' style='color:red;'>Email field is required.</div>");
    }else{
      if(!validateEmail(email)){
        $(".email2").remove(); 
        $(".email1").remove(); 
        $("#userEmail").parent().append("<div class='email2' style='color:red;'>Incorrect email, Please try again.</div>");
       flag = 1;
      }else{        
        $(".email1").remove(); 
        $(".email2").remove(); 
      }
    }
    if(editData == ""){
      if(password == ''){
        $(".userPassword").remove();  
        $("#userPassword").parent().append("<div class='userPassword' style='color:red;'>Password field is required.</div>");
      flag = 1;
      } else if(!pwdPattern.test(password)) {
        $(".userPassword").remove();  
        $("#userPassword").parent().append("<div class='userPassword' style='color:red;'>Password should be one uppercase, lowercase, digit, special character, no space allowed & minimum 8 characters field are required.</div>");
        flag = 1;
      } else {
        $(".userPassword").remove();        
      }

      if(cpassword == ''){
        $(".userCpassword").remove();  
        $("#userCpassword").parent().append("<div class='userCpassword' style='color:red;'>Confirm password field is required.</div>");
        flag = 1;
      } else if(cpassword != password) {
        $(".userCpassword").remove();  
        $("#userCpassword").parent().append("<div class='userCpassword' style='color:red;'>Password & Confirm password didn't match.</div>");
        flag = 1;
      } else {
        $(".userCpassword").remove();        
      }
    }

    if(phone == ''){     
      flag = 1;
      $(".phone").remove();  
      $("#contactNumber").parent().append("<div class='phone' style='color:red;'>Mobile no field is required.</div>");
    }
    else
    {
      if(phone.length < 10)
      {
        $(".phone").remove();  
        $("#contactNumber").parent().append("<div class='phone' style='color:red;'>Mobile no field must contain minimum 10 digits.</div>");
       flag = 1;
      }
      else
      { 
        $(".phone").remove();  
      }   
    }

    if(userName == ''){
      $(".fname").remove();  
      $("#fname").parent().append("<div class='fname' style='color:red;'>Full name field is required.</div>");
      flag = 1;
    } else if(userName.length < 3) {
      $(".fname").remove();  
      $("#fname").parent().append("<div class='fname' style='color:red;'>Full name minimum 3 characters are required.</div>");
      flag = 1;
    } else {
      $(".fname").remove();        
    }

    if(userRole == '' || userRole == null){
      $(".userRole").remove();  
      $("#userRole").parent().append("<div class='userRole' style='color:red;'>Role field is required.</div>");
      flag = 1;
    }else{
      $(".userRole").remove();          
    }    

    if(userRole == 4 && ($("#category").val() == '' || $("#category").val() == null))
    {
      $(".category").remove();  
      $("#category").parent().append("<div class='category' style='color:red;'>Category field is required.</div>");
      flag = 1; 
    } else {
     $(".category").remove();   
    } 

    if(userRole == 4 && ($("#category").val() == 1 || $("#category").val() == 4) && ( $("#category").val() > 0 ) && ($("#sub_category").val() == '' || $("#sub_category").val() == null))
    {
      $(".sub_category").remove();  
      $("#sub_category").parent().append("<div class='sub_category' style='color:red;'>Sub category field is required.</div>");
      flag = 1; 
    } else {
     $(".sub_category").remove();   
    } 
   //alert(flag);
    var ckk = userCheckEmail();
    if(flag == 1 || ckk == 1)
      return false;    
    else
    {

      // $("#LoadingDiv").css({"display":"block"});
      var formData = new FormData($('#userForm')[0]);
      $.ajax({
        type:"POST",
        url:baseURL+"user_management/UserController/addUser",            
        //data:{"editData":editData,"userRole":userRole,"password":password,"phone":phone,"email":email,"userName":userName,"category":category,"sub_category":sub_category,"gender":spGender},
        dataType: "json",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success:function(response){                     
          if(response.status == "success")
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#userModal").modal('hide');
            $("#table").dataTable().fnDraw();
            $("#table").css({"opacity":"1"});
            toastr.success(response.message);                                      
          }
          else
          {
            $("#LoadingDiv").css({"display":"none"});
            $("#userModal").modal('hide');
            toastr.error(response.message);                                       
          }
        }
      }); 
    }   
  });  

  $('#userDegree').on('change',function(){
    $('#specialization').html('<option value="" selected="">-- Select specialization --</option>'); 
    $('#special').html('<p style="color:red;">Please wait...</p>');  
    var degreeID = $(this).val();         
    if(degreeID){
      $.ajax({
        type:'POST',
        url:baseURL+"user_management/UserController/specializationData",
        data:{"degreeID":degreeID},
        dataType:'json',
        success:function(json){
            $('#special').html('<p style="color:red;">Please wait...</p>');                     
            $('#specialization').html(json.specialization);
            $('#special').html('');                               
        }
      }); 
    }else{            
      $('#specialization').html('<option value="">Select category</option>'); 
    }
  });

  $('input[type="file"][id="uploadDocuments"]').change(function(e){
    var names = [];
    for (var i = 0; i < $(this).get(0).files.length; ++i) {
        names.push('<br />'+ $(this).get(0).files[i].name);
    }
    $('#showFilename').html(names);    
  });

  $('#addPopUpSerPro').on("click",function(){
    $(".fname").remove();
    $(".email2").remove();
    $(".email1").remove();
    $(".userPassword").remove();
    $(".userCpassword").remove();
    $(".phone").remove();
    $(".userRole").remove();
    
    $(".certificateName").remove(); 
    $(".schoolCollege").remove(); 
    $(".uploadDocuments").remove(); 
    $(".valid_to").remove();  
    $(".valid_from").remove();  
    $(".parentDOB").remove();  
    $(".userDegree").remove(); 
    $(".specialization").remove();  
    $("#showFilename").html("No file chosen");

    $("#userForm")[0].reset();
    $("#hidePwd").css("display","block");
    $("#categoryDiv").empty();
    $("#user_key1").val("");
    $("#userEmail").prop('disabled',false);
    $("#userModal").modal('show');
    $("h3").html("<h3 class='modal-title'><b>Add user</b></h3>");
  });
 $(".dob").on("change",function(e){    
  var mdate = $(this).val().toString();
  var yearThen = parseInt(mdate.substring(0,4), 10);
  var monthThen = parseInt(mdate.substring(5,7), 10);
  var dayThen = parseInt(mdate.substring(8,10), 10);
  
  var today = new Date();
  var birthday = new Date(yearThen, monthThen-1, dayThen);
  
  var differenceInMilisecond = today.valueOf() - birthday.valueOf();
  
  var year_age = Math.floor(differenceInMilisecond / 31536000000);
  var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);
  
  var month_age = Math.floor(day_age/30);
  
  day_age = day_age % 30;
  if (isNaN(year_age) || isNaN(month_age) || isNaN(day_age)) {          
    alert("Invalid date of birthday, Please try again.");   
  }
  else if(year_age == 0){
      $("#parentAge").val(month_age + " Month ");
  } else {
    $("#parentAge").val(year_age + " Year " +month_age + " month");
  }

});  

});



function  subCategory() {
  var category_id = $("#category").val();
   if(category_id =='')
    {
      return;
     } else { 
      if($("#sub_category_div")){
       $("#sub_category_div").remove();    
     }    

     $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      data:{category_id: category_id},
      url:baseURL+"user_management/UserController/addSubCategory",    
      beforeSend: function() {
           //  $('#LoadingDiv').show();
             },
             complete: function(){
        //$('#LoadingDiv').hide();
        }
    }).success(function (json) {
        console.log(json);
     $("#categoryDiv").append(json.data);
    
   });   
 }
}

function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

/* function addCategoryList() 
{
 var user_role = $("#userRole").val(); 
 if($("#user_key").val() !='' && (user_role != $("#service_provider_role_id").val()))
 {
 $("#categoryDiv").css("display","none");
    return; 
 } else {
   
 if($("#user_key").val() && user_role == $("#service_provider_role_id").val())
 {
 $("#categoryDiv").css("display","inline"); 
  return;
 }
  if(user_role == $("#service_provider_role_id").val())
    {
     $("#categoryDiv").css("display","inline"); 
     $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url:baseURL+"user_management/UserController/addCategory",    
      beforeSend: function() {
          //   $('#LoadingDiv').show();
             },
             complete: function(){
      //  $('#LoadingDiv').hide();
        }
    }).success(function (json) {
        console.log(json);
     $("#categoryDiv").html(json.data);
    
   });      
 
 }
  else{ 
    $("#categoryDiv").empty();
    return; 
  } 
} 
}  */


function addCategoryList() {
  var user_role = $("#userRole").val();
  if(user_role == $("#service_provider_role_id").val())
    {
    var user_key = $("#user_key1").val();
     $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
       data:{user_key: user_key},
      url:baseURL+"user_management/UserController/addCategory",    
      beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
       // $('#LoadingDiv').hide();
        }
    }).success(function (json) {
        console.log(json);
     $("#categoryDiv").html(json.data);
    
   });      
 
 }
  else{ 
    $("#categoryDiv").empty();
    return; } 
}