function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  serviceUserList();
}

function serviceUserList(){
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [],
    // Load data for the table's content from an Ajax source
      "ajax": {
        "url": baseURL+"user_management/ServiceUserController/ajax_list",
        beforeSend: function() {
           // $("#LoadingDiv").css({"display":"block"});
          var search = $("input[type=search]").val();
          if(search=="")
           $("input[type=search]").on("keyup",function(event) {

            if($("#clear").length == 0) {
               if($(this).val() != ""){
                $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
              } 
            }
            if($(this).val() == "")  
            $("#clear").remove();      
          }); 
          $("input[type=search]").keydown(function(event) {
            k = event.which;
            if (k === 32 && !this.value.length)
                event.preventDefault();
          });
         },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#table").css({"opacity":"1"});
      },
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,5,6 ], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  }); 
}

function newRecord($this)
{
  if($($this).attr("data-row-id")){
   $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"user_management/ServiceUserController/newRecord",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
      if(json.status == "success"){
        //$($this).parents("tr:first").remove();
        $("#table").dataTable().fnDraw();              
      }
    });      
  } else {      
    toastr.error("Something went wrong!!!.");
  }  
}

function userStatus_popup(userId,user_status,active_id,inActive_id)
{ 
  var temp = "userStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
  $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
  $(".inner_body").html('<b>Are you sure you want to change status?</b>');
  $(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
  $('.commanPopup').modal('show');
}

function userStatus(userId,user_status,active_id,inActive_id)
{ 
  $('.commanPopup').modal('hide');
  $.ajax({
    type : "POST",
    dataType : "json",
    url : baseURL + "user_management/ServiceUserController/userStatusChange",
    data : { userId : userId, user_status: user_status} 
  }).success(function(data){   
  if(user_status == 1) {     
    var str_success="User profile has been activated successfully.";
    var str_error="User profile has been activation failed.";

    $("#"+active_id).removeClass("btn-active-disable");
    $("#"+active_id).addClass("btn-active-enable");

    $("#"+inActive_id).removeClass("btn-inactive-enable");
    $("#"+inActive_id).addClass("btn-inactive-disable");

    $("#"+active_id).prop('disabled', true);
    $("#"+inActive_id).prop('disabled', false);
    } else {
      $("#"+active_id).removeClass("btn-active-enable");
      $("#"+active_id).addClass("btn-active-disable");

      $("#"+inActive_id).removeClass("btn-inactive-disable ");
      $("#"+inActive_id).addClass("btn-inactive-enable");

      var str_success="User profile has been inactivated successfully.";
      var str_error="User profile has been inactivation failed.";

      $("#"+active_id).prop('disabled', false);
      $("#"+inActive_id).prop('disabled', true);
    }
    if(data.status = true) {
      toastr.success(str_success);
    } else if(data.status = false) { 
      toastr.error(str_error);
    } else {
      toastr.error("Access denied..!");
    }
  });   
}

function viewServiceUserData($this)
{
  if($($this).attr("data-row-id")){
  $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"user_management/ServiceUserController/viewServiceUserInfo",
      beforeSend: function() {
      },
      complete: function(){      
    },
      data: {"key":$($this).attr("data-row-id")},
  }).success(function (json) {
    if(json.status == "success" && json.userType == "parent"){   
      $("#parent_name").text(json.parentData.parnet_name);            
      $("#parent_email").text(json.parentData.parent_email);
      $("#user_phone_number").text(json.parentData.parnet_phone);
      $("#user_gender").text(json.parentData.parnet_Gender);   
      $("#user_address").text(json.parentData.address);
      $("#user_birth_date").text(json.parentData.user_birth_date);
      $("#user_no_of_kids").text(json.noOfChilds);
      var childs = "";
      $.each(json.childData, function( index, value ) {
        childs +=  "<tr><td>"+(index+1)+"</td><td>"+value.username+"</td><td>"+value.childname+"</td><td>"+value.gender+"</td><td>"+value.age+"</td></tr>";
      });
      $("#kidsList tbody").html(childs);
      $('#parentServiceUserModal').modal('show', {backdrop: 'static'});
    } else if(json.status == "success" && json.userType == "org"){   
        $("#organizationName").html(json.orgDetails.orgName);            
        $("#orgEmail").html(json.orgDetails.email);
        $("#orgAddress").html(json.orgDetails.address);
        $('#orgServiceUserModal').modal('show', {backdrop: 'static'});        
    }else if(json.status == "success" && json.userType == "school"){   
        $("#schoolName").html(json.schoolDetails.schoolName);            
        $("#schoolEmail").html(json.schoolDetails.email);
        $("#schoolAddress").html(json.schoolDetails.address);
        $('#schoolServiceUserModal').modal('show', {backdrop: 'static'});        
      } else {
          toastr.error(json.msg,"Error:");
      }
    });
  } else {
    toastr.error("Something went wrong!!!.");    
  }
}

$(document).ready(function(){
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });
});