/**
 * @author Rajendra Pawar
 */
function loadEnquiryList()
{		
	var table = $('#enquiryList-grid').DataTable();
            $('#enquiryList-grid').empty();
            table.destroy();

       $('#enquiryList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"enquiryList/enquiryList",
            beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,6], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });	
}
jQuery(document).ready(function(){  
 
  jQuery(document).on("click", ".viewEnquiry", function(){
    var tenquiryId = $(this).data("tenquiryid"),
      hitURL = baseURL + "enquiryList/viewEnquiryDetails";
        $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      data : { tenquiryId : tenquiryId }
    }).success(function (json) {
        console.log(json);
    $("#ModalLabel").html('Enquiry Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html(json.data);
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');  
   });      
  }); 

  });