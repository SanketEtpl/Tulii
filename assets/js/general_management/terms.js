function loadTermsList() {
  var table = $('#terms-grid').DataTable();
  $('#terms-grid').empty();
  table.destroy();
  $('#terms-grid').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": baseURL+"general_management/Terms_controller/termsList",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
            if($(this).val() != ""){
              $("#terms-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
            event.preventDefault();
        });
      },
      complete: function(){
      $('#LoadingDiv').hide();
      $("#terms-grid").css({"opacity":"1"});
      },
      "type": "POST"
    },
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
      "targets": [ 0, 3], //first column / numbering column
      "orderable": false, //set not orderable
    },
  ],
  }); 
}

function viewTerms($this) // view of terms
{
  if($($this).attr("data-row-id")){            
    $( "h3" ).html( "<h3 class='modal-title'><b>View terms Description</b><button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></h3>" );
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"general_management/Terms_controller/viewTerms",
      beforeSend: function() {        
      },
      complete: function(){        
      },
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
        $("#viewTitle").text(json.data.title);
        $("#viewDescription").html(json.data.description); 
        $('#viewTermsModal').modal('show', {backdrop: 'static'});
      } else {
         toastr.error(json.msg); 
      }
    });
  } else {
    toastr.error("Something went wrong!!!.");         
  }
} 

function deleteTermsData($this) // delete record of terms list
{
  if($($this).attr("data-row-id")) {
    var key=$($this).attr("data-row-id");             
    var temp = "delTerms('"+key+"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this terms & condition?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  } else {     
    $("#LoadingDiv").css({"display":"none"}); // loader
    toastr.error("Something went wrong!!!.");    
  }
}

function delTerms(key)
{  
  if(key) {      
    $.ajax({
      type: "POST",
      dataType: "json",
      url: baseURL+"general_management/Terms_controller/deleteTerms",
      beforeSend: function() {         
      },
      complete: function() {      
      },
      data: {"key": key},
    }).success(function (json) {
      if(json.status == "success"){           
        toastr.success(json.msg);       
        $("#terms-grid").dataTable().fnDraw();    
      } else {
        toastr.error(json.msg);             
      }
    });
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
  $('.commanPopup').modal('hide');  
} 

function editTermsData($this) // edit terms list data
{
  $('#termsForm')[0].reset(); //reset of terms list value 
  $(".userRole").remove();
  $(".title").remove();
  $(".terms_data").remove();
   
  $( "h3" ).html( "<h3 class='modal-title'><b>Edit terms & condition</b></h3>" );
  if($($this).attr("data-row-id")){ // get terms list id     
    $.ajax({
          type: "POST",
          dataType: "json",
          url: baseURL+"general_management/Terms_controller/viewTerms",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){            
          $("#user_key").val($($this).attr("data-row-id"));
          $("#userRole").val(json.data.role_id);
          $("#title").val(json.data.title);
          CKEDITOR.instances['terms_data'].setData(json.data.description);
          $("#userRole").prop("disabled", true);
          $('#TermsModal').modal('show', {backdrop: 'static'});        
        }
        else{
          toastr.error(json.msg);
        }
      });   
  } else {
    $("#user_key").val('');
    $('#priceListModal').modal('show', {backdrop: 'static'});
  }
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadTermsList();  
}

$(document).ready(function(){

  $('#addTemsPopUp').on("click", function() {
    $("#termsForm")[0].reset();
    CKEDITOR.instances['terms_data'].setData( '' );
    $("#user_key").val("");
    $(".userRole").remove();
    $(".title").remove();   
    $(".terms_data").remove();   
    $("#userRole").prop("disabled", false);
    $("#headerTitle").html( "<h3 class='modal-title' id='headerTitle'><b>Add Terms & Condition</b></h3>" );
    $('#TermsModal').modal('show', {backdrop: 'static'});
  });

  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
      if($(this).val() != ""){
        $("#contact-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
      event.preventDefault();
  });

  $('#btnTerms').click(function(){ // save record of terms & condition list  
    var userRole = $("#userRole").val();
    var title = $("#title").val();
    var user_key = $("#user_key").val();
    var terms_data = CKEDITOR.instances['terms_data'].getData();
    var flag = 0;
    if(userRole == ''){
      $(".userRole").remove();  
      $("#userRole").parent().append("<div class='userRole' style='color:red;'>Please select user role.</div>");
      flag = 1;
    } else {
      $(".userRole").remove();        
    }    
    
    if(title == ''){
      $(".title").remove();  
      $("#title").parent().append("<div class='title' style='color:red;'>Title field is required.</div>");
      flag = 1;
    } else {
      $(".title").remove();        
    }     

    if(terms_data == ''){
      $(".terms_data").remove();  
      $("#terms_data").parent().append("<div class='terms_data' style='color:red;'>Description field is required..</div>");
      flag = 1;
    }else{
      $(".terms_data").remove();          
    }

    if(flag == 1)
      return false;    
    else
    { 
      $.ajax({
        type:"POST",
        url:baseURL+"general_management/Terms_controller/addEditTerms",            
        data:{"editData":user_key, "userRole":userRole, "title":title, "terms_data":terms_data},
        dataType:"json",
        async:false,
        success:function(response){                     
          if(response.status == "success") {
            $("#TermsModal").modal('hide');
            $("#terms-grid").dataTable().fnDraw();
            toastr.success(response.msg);                                      
          } else if(response.status == "warning") {
            $("#TermsModal").modal('show');
            toastr.warning(response.msg); 
          } else {
            $("#TermsModal").modal('show');
            toastr.error(response.msg);                                       
          }
        }
      });    
    }   
  });  
});