jQuery(document).ready(function(){
  jQuery('#LoadingDiv').show();
	jQuery(document).on("click", ".updateAbout", function(){
  	var about_id = $('#about_id').val();	
    var message_body = CKEDITOR.instances['about_data'].getData();
    hitURL = baseURL + "general_management/AboutUs_controller/updateAbout",
	  jQuery.ajax({
			type : "POST",
			dataType : "json",
			url : hitURL,
			beforeSend: function() {
        //$('#LoadingDiv').show();
      },
      complete: function(){
     		$('#LoadingDiv').hide();
  		},
			data : { message_body : message_body, about_id : about_id } 
		}).done(function(data){		
		if(data.status == true) { 
		  toastr.success(data.msg); 
    } else if(data.status == false) {
      toastr.error(data.msg); 
    } else { 
      toastr.error("Access denied..!"); 
    }
    setTimeout(function(){ window.location.reload(true); }, 2000);
    
	  });
	});
});