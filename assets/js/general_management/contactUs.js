function loadContactList() {
    var table = $('#contact-grid').DataTable();
    $('#contact-grid').empty();
    table.destroy();
    $('#contact-grid').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.

    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": baseURL+"general_management/ContactUs_controller/contactList",
        beforeSend: function() {
            var search = $("input[type=search]").val();
            $("input[type=search]").on("keyup",function(event) {
                if($("#clear").length == 0) {
                    if($(this).val() != "") {
                        $("#contact-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                    } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
            }); 
            $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                event.preventDefault();
            });
        },
        complete: function() {
            $('#LoadingDiv').hide();
            $("#contact-grid").css({"opacity":"1"});
        },
        "type": "POST"
    },
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0, 4], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
    }); 
}

function clearSearch() 
{ 
    $("input[type=search]").val("");
    $("#clear").remove();
    loadContactList();  
}

$('document').ready(function(){
  $('table').on("click", ".viewContact", function(){
    var contact_id = $(this).data('contactid');        
    hitURL = baseURL + "general_management/ContactUs_controller/viewContactDetails",
    $.ajax({
        type : "POST",
        dataType : "json",
        url : hitURL,
        beforeSend: function() {
        },
        complete: function(){        
        },
        data : { contact_id : contact_id} 
        }).done(function(response){               
            if(response.status == true) {                 
                $('#contactMessage').text(response.data.contact_message);
                $('#contactUsModal').modal('show', {backdrop: 'static'});
            } else if(response.status == false) {
                toastr.error("Something went wrong."); 
            } else { 
                toastr.error("Access denied..!");
            }
        });        
    });
});