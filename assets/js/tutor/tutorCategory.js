function loadTutorCategoryList()
{
  var tutorList = $('#tutorList').DataTable();
  $('#tutorList').empty();
  tutorList.destroy();
  $('#tutorList').DataTable({  
    "processing": false, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "order": [], //Initial no order.
 
    // Load data for the table's content from an Ajax source
    "ajax": {
      "url": baseURL+"tutor_management/Tutor_category/categoryList",
      beforeSend: function() {
        var search = $("input[type=search]").val();
        if(search == "")          
        $("input[type=search]").on("keyup",function(event) {
          if($("#clear").length == 0) {
             if($(this).val() != ""){
              $("#tutorList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
            } 
          }
          if($(this).val() == "")  
          $("#clear").remove();      
        }); 
        $("input[type=search]").keydown(function(event) {
          k = event.which;
          if (k === 32 && !this.value.length)
            event.preventDefault();
        });
      },
        complete: function(){
        $("#LoadingDiv").css({"display":"none"}); 
        $("#tutorList").css({"opacity":"1"}); 
      },
      "type": "POST"
    },
    
    //Set column definition initialisation properties.
    "columnDefs": [
    { 
        "targets": [ 0,2], //first column / numbering column
        "orderable": false, //set not orderable
    },
    ],
  });  
}

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  loadTutorCategoryList();    
}

function deleteTutorCategory($this)
{
	if($($this).attr("data-row-id")) {
    var kay= "'"+ $($this).attr("data-row-id") +"'";
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html('<b>Are you sure you want to delete this tutor category?</b>');
    $(".inner_footer").html('<button type="button" onclick="deltutor('+kay+');" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');       
  } else {
 		toastr.error("Something went wrong!!!.");           
 	}
}  

function deltutor(key){ 
 	$.ajax({
    type: "POST",
    dataType: "json",
    beforeSend: function() {
	    //$('#LoadingDiv').show();
    },
    complete: function(){
    	$('#LoadingDiv').hide();
    },
    url: baseURL+"tutor_management/Tutor_category/deleteTutorCategory",
    data: {"key":key},
    }).success(function (json) {
  	if(json.status == "success") {
      toastr.success(json.msg); // success message
      $("#tutorList").dataTable().fnDraw();
    } else if(json.status == "warning") {
      toastr.warning(json.msg); // success message
      //$("#tutorList").dataTable().fnDraw();
    } else {
    	toastr.error(json.msg); // error message
  	}
 	});
  $('.commanPopup').modal('hide');    
}


$(document).ready(function() {
  $("#LoadingDiv").css({"display":"block"}); 
  //datatables
  loadTutorCategoryList();
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#tutorList_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
    event.preventDefault();
  });

  $("#addTutorCatgoryPopUp").on("click", function(){
    $('#tutorcatForm')[0].reset(); //reset of tutor category value 
    $(".tutor_category_name").remove();
    $("#tutorcategoryModal").modal('show',{backdrop:'static'});
  });
});

function btnAddTutorCat(){
  var tutor_category_name = $("#tutor_category_name").val();
  var edit_id = $("#user_key").val();
  var flag = 0;
  if(tutor_category_name == ''){
    $(".tutor_category_name").remove();  
  	$("#tutor_category_name").parent().append("<div class='tutor_category_name' style='color:red;'>Tutor category field is not empty.</div>");
    flag = 1;
  } else {
    $(".tutor_category_name").remove();          
  }     
  if(flag == 1)
    return false;    
  else
  {            	
  	$.ajax({
      type:"POST",
      url:baseURL+"tutor_management/Tutor_category/addTutorCategory",            
      data:{category_name:tutor_category_name, edit_id:edit_id},
      dataType:"json",
      beforeSend: function() {
       
      },
      complete: function(){
	    
	    },
      async:false,
      success:function(response){                     
      if(response.status == "success") {		           
        $('#tutorcategoryModal').modal('hide');
        toastr.success(response.message);       
        $("#tutorList").dataTable().fnDraw();                               
      } else if(response.status == "warning") {			            
        $("#tutorcategoryModal").modal('show', {backdrop: 'static'});
        toastr.warning(response.message);        
	    } else {
        $('#tutorcategoryModal').modal('hide');
        toastr.error(response.message);                                       
      }      
      }
  	});    
  }   
}


function editTutorCategory($this) // edit tutor category data
{
	$('#tutorcatForm')[0].reset(); //reset of tutor category value 
	$(".tutor_category_name").remove();
	$( "h3").html( "<h3 class='modal-title'><b>Edit Tutor Category</b></h3>" );
	if($($this).attr("data-row-id")){ // get tutor list id 
		$.ajax({
      type:"POST",
      url:baseURL+"tutor_management/Tutor_category/viewTutorCategory",            
      dataType:"json",
      data:{"key":$($this).attr("data-row-id")},
      beforeSend: function(){
      },
      complete: function(){		    
	    },
      async:false,
      success:function(response) {   
      if(response.status == "success") {
        $("#user_key").val(response.tutorData.tut_cat_id);
        $("#tutor_category_name").val(response.tutorData.category_name);                                         
     	} 
    }
	});
  } else {
    $("#user_key").val('');    
    toastr.error("Something went wrong.");
 	} 	 
 	$('#tutorcategoryModal').modal('show');
}

