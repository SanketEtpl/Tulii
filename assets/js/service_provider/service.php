<!--*
Author : Rajendra pawar 
Page :  service.php
Description : show service provider details
*-->
<?php 
$segment=$this->uri->segment(3); 
$service_info="";
if($segment == 1){
 $service_type =SERVICE_SINGLE_RIDES; 
 $service_info="Rides/".SERVICE_SINGLE_RIDES;
} else if($segment == 2){
 $service_type =SERVICE_MULTIPLE_RIDE_WITH_SAME_DESTINATION;
 $service_info="Rides/".SERVICE_MULTIPLE_RIDE_WITH_SAME_DESTINATION; 
} else if($segment == 3){
 $service_type =SERVICE_SINGLE_RIDES_WITH_DIFFERENT_DESTINATION; 
 $service_info="Rides/".SERVICE_SINGLE_RIDES_WITH_DIFFERENT_DESTINATION; 
} else if($segment == 4){
 $service_type =SERVICE_CHILD_CAR_POOLING; 
 $service_info="Rides/".SERVICE_CHILD_CAR_POOLING; 
} else if($segment == 5){
 $service_type =SERVICE_CHILD_CARE; 
 $service_info="After school care/".SERVICE_CHILD_CARE; 
} else if($segment == 6){
 $service_type =SERVICE_CARE_DRIVER;
 $service_info="After school care/".SERVICE_CARE_DRIVER;  
} else if($segment == 7){
 $service_type =SERVICE_MY_CHILDS_CARE_DRIVER;
 $service_info="After school care/".SERVICE_MY_CHILDS_CARE_DRIVER;  
} else if($segment == 8){
 $service_type =SERVICE_MY_CHILDRENS_CARE_DRIVER;
 $service_info="After school care/".SERVICE_MY_CHILDRENS_CARE_DRIVER;  
} else {
 $service_type =SERVICE_MY_CHILD_TUTOR; 
 $service_info="Premier tutoring/".SERVICE_MY_CHILD_TUTOR; 
} 


?>

<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/tab.css" />
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imageviewer.css" />

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/service_provider/service_provider.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/imageviewer.js" charset="utf-8"></script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-user"></i>Service Provider Management/<?php echo $service_info; ?>
        <small>View/Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">                
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">                
                <div class="box-body table-responsive no-padding">
                
                <table id="serviceList-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                      <thead>
                        <tr>
                           <th>Id no</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Mobile</th>
                           <th>Gender</th>
                           <th style="width:15%;">Status</th>
                           <th>Action</th>       
                        </tr>
                      </thead>
                      <tfoot> 
                        <tr>
                           <th>Id no</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Mobile</th>
                           <th>Gender</th>
                           <th>Status</th>
                           <th>Action</th>
                        </tr>
                      </tfoot>
                </table>       
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              </div>

        </div>
    </section>
</div>


 <script type="text/javascript">
$(function(){
$('a[title]').tooltip();
}); 
  jQuery(document).ready(function(){
  //jQuery("#LoadingDiv").css({"display":"block"}); 
  //jQuery("#LoadingDiv").show();
  loadServiceList('<?php echo $service_type; ?>'); 
  $("input[type=search]").on("keyup",function() {
      if($("#clear").length == 0) {
        if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });
});

/*function clearSearch() 
{ 
  $("input[type=search]").val("");
  location.reload();  
}  */

</script>   



