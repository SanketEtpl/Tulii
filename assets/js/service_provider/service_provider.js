function serviceProviderStatus(userId,user_status,active_id,inActive_id)
{  
  if(confirm('Are you sure you want to change status?'))
  {
    $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "service_provider/ServiceproviderController/serviceProviderChangeStatus",
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
      console.log(data);
      if(user_status == 1) { 
        var str_success="Service provider has been active successfully.";
        var str_error="Service provider has been activation failed.";

        $(active_id).removeClass("btn-active-disable");
        $(active_id).addClass("btn-active-enable");

        $(inActive_id).removeClass("btn-inactive-enable");
        $(inActive_id).addClass("btn-inactive-disable");

        $(active_id).prop('disabled', true);
        $(inActive_id).prop('disabled', false);
        } else {
          $(active_id).removeClass("btn-active-enable");
          $(active_id).addClass("btn-active-disable");

          $(inActive_id).removeClass("btn-inactive-disable ");
          $(inActive_id).addClass("btn-inactive-enable");

          var str_success="Service provider has been inactive successfully.";
          var str_error="Service provider has been inactivation failed.";

          $(active_id).prop('disabled', false);
          $(inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
      }
      else if(data.status = false) {
        toastr.error(str_error);        
       }
      else {
        toastr.error("Access denied..!");
        }
    }); 
  } 
}

function deleteServiceProvider($this)
{
 // $("#LoadingDiv").css({"display":"block"});        
  if($($this).attr("data-row-id")){
    if(confirm(' Are you sure you want to delete this user?'))
    {          
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"service_provider/ServiceproviderController/deleteSerivceProviderInfo",
            data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
            if(json.status == "success"){
              $($this).parents("tr:first").remove();
                $("#LoadingDiv").css({"display":"none"});
                toastr.success(json.msg);
                $("#table").dataTable().fnDraw();              
             }else{
                $("#LoadingDiv").css({"display":"none"});
                toastr.error(json.msg);               
              }
        });
     } 
      $("#LoadingDiv").css({"display":"none"});
  } else {   
   $("#LoadingDiv").css({"display":"none"}); 
    toastr.error("Something went wrong!!!.");      
  }
}   

function viewServicepProvider($this)
{
  //$("#LoadingDiv").css({"display":"block"});                
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"service_provider/ServiceproviderController/viewServiceProviderInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
          $("#LoadingDiv").css({"display":"none"});                
          $("#parent_name").text(json.servicePvdData.user_name);
          $("#parent_email").text(json.servicePvdData.user_email); 
          $("#user_phone_number").text(json.servicePvdData.user_phone_number);
          if(json.servicePvdData.user_gender == "female")
            $("#user_gender").text("Female");
          else if(json.servicePvdData.user_gender == "male")
            $("#user_gender").text("Male"); 
          else
            $("#user_gender").text("-"); 
          $("#user_address").text(json.servicePvdData.user_address);
          $("#user_birth_date").text(json.servicePvdData.user_birth_date);           
         
          $('#parentUserModal').modal('show', {backdrop: 'static'});

         }
         else{
           toastr.error(json.msg);   
          $("#LoadingDiv").css({"display":"none"});
         }
      });
  }else{
     $("#LoadingDiv").css({"display":"none"});
    toastr.error("Something went wrong!!!.");         
  }
  $("#LoadingDiv").css({"display":"none"});
} 

$(document).ready(function(){
  $('#addPopUp').on("click",function(){
    $("#parentProfileModal").modal('show');                              
    });
});
function loadServiceList(category)
{   
  var table = $('#serviceList-grid').DataTable();
            $('#serviceList-grid').empty();
            table.destroy();

       $('#serviceList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"service-provider/serviceList",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
               // $('#LoadingDiv').show();
             
                $("input[type=search]").on("keyup",function() {
                  if($("#clear").length == 0) {
                     if($(this).val() != ""){
                      $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                    } 
                  }
                  if($(this).val() == "")  
                  $("#clear").remove();      
                });
                $("input[type=search]").keydown(function(event) {
                  k = event.which;
                  if (k === 32 && !this.value.length)
                      event.preventDefault();
                });
             
             },
             complete: function(){
            $('#LoadingDiv').hide();
            $('#serviceList-grid').css({"opacity":"1"});
        },
        data : { category : category },
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,5,6], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    }); 
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#serviceList-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  }); 
}
function clearSearch() 
{ 
  $("input[type=search]").val("");
  // location.reload();
  var service_type = $('#service_type').val();
  loadServiceList(service_type);
}
jQuery(document).ready(function(){  
 jQuery('#LoadingDiv').show();
  jQuery(document).on("click", ".viewService", function(){
    var userId = $(this).data("userid"),
      hitURL = baseURL + "service-provider/viewServiceDetails";
        $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      data : { userId : userId }
    }).success(function (json) {
        console.log(json);
    $("#ModalLabel").html('Service Provider Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html(json.data);
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');  
   });      
  }); 

 jQuery(document).on("click", ".deleteService", function(){
    var userid = $(this).data("userid");
    var category = $(this).data("category");
     var temp = "delService('"+userid+"','"+ category +"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b>Are you sure you want to delete this service?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  });

 jQuery(document).on("click", ".viewPic", function(){
  // $('#LoadingDiv').show();
   //setTimeout(function(){  $('#LoadingDiv').hide(); }, 1000);
    var baseurl = $(this).data("baseurl");
    var picname = $(this).data("picname");
    var error_msg = $(this).data("error");
    var headerMsg = $(this).data("headermsg");
    var imgPath = $(this).data("imgpath");
   if(picname){

    var array = picname.split("|");
   // var cnt =1 ;
    var temp= '';
     $("#modalHeader").html(headerMsg+'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
     $("#modalBody").html('<div id="image-gallery"><div class="image-container"></div><img src="'+imgPath+'assets/images/left.svg" class="prev"/><img src="'+imgPath+'assets/images/right.svg"  class="next"/><div class="footer-info"><span class="current"></span>/<span class="total"></span></div></div> ');
     $("#modalFooter").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
     $('.photoGallery').modal('show'); 
     
      a = new Array();
     for (var i in array){
      var http = new XMLHttpRequest();
      http.open('HEAD', baseurl+array[i], false);
      http.send();
      if(http.status != 404) {
      a[i]=baseurl+array[i]; 
      } else{
      a[i]=imgPath+'assets/images/img-not-found.jpg';   
      }
     
     }
    
    var images = Array();
    for(var j=0;j<a.length;j++)
    {
      images.push({small:a[j],big:a[j]}); 
    }  
       
     var curImageIdx = 1,
        total = images.length;
    var wrapper = $('#image-gallery'),
        curSpan = wrapper.find('.current');
    var viewer = ImageViewer(wrapper.find('.image-container'));
 
    //display total count
    wrapper.find('.total').html(total);
 
    function showImage(){
        var imgObj = images[curImageIdx - 1];
        viewer.load(imgObj.small, imgObj.big);
        curSpan.html(curImageIdx);

    }
 
    wrapper.find('.next').click(function(){
         curImageIdx++;
        if(curImageIdx > total) curImageIdx = 1;
        showImage();
    });
 
    wrapper.find('.prev').click(function(){
         curImageIdx--;
        if(curImageIdx < 0) curImageIdx = total;
        showImage();
    });
 
    //initially show image
    showImage();  
     } else{
     toastr.error(error_msg); 
     }
  });

  });

function serviceStatus_popup(userId,user_status,active_id,inActive_id)
{
 
var temp = "serviceStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure you want to change status?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function serviceStatus(userId,user_status,active_id,inActive_id)
{
$('.commanPopup').modal('hide');
jQuery.ajax({
      type : "POST",
      dataType : "json",
      url : hitURL = baseURL + "service-provider/serviceStatus",
      beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
        //$('#LoadingDiv').hide();
        },
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
        console.log(data);
        if(user_status == 1) { 
          var str_success="Service provider has been active successfully.";
          var str_error="Service provider activation failed.";

          $("#"+active_id).removeClass("btn-active-disable");
          $("#"+active_id).addClass("btn-active-enable");

          $("#"+inActive_id).removeClass("btn-inactive-enable");
          $("#"+inActive_id).addClass("btn-inactive-disable");

          $("#"+active_id).prop('disabled', true);
          $("#"+inActive_id).prop('disabled', false);
            }
        else{

          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Service provider has been inactive successfully.";
            var str_error="Service provider inactivation failed.";

            $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
          }
        if(data.status == true) {toastr.success(str_success);

        }
        else if(data.status == false) { toastr.error(str_error); }
        else { alert("Access denied..!"); }
      }); 
}


function delService(userid,category)
{
  //$('#LoadingDiv').show();
  if(userid){
      // $('#LoadingDiv').show();
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"service-provider/deleteServiceUser",
           beforeSend: function() {
             
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
            data: {"key":userid},
        }).success(function (json) {
            if(json.status == "success"){
              $(this).parents("tr:first").remove();
              toastr.success(json.msg);
             $("#table").dataTable().fnDraw();
              
             }else{
              toastr.error(json.msg);
            }
        });
      $('#LoadingDiv').hide();
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
  $('.commanPopup').modal('hide');
  loadServiceList(category);
} 