function serviceProviderStatus(userId,user_status,active_id,inActive_id)
{  
  if(confirm('Are you sure want to change status ?'))
  {
    $.ajax({
      type : "POST",
      dataType : "json",
      url : baseURL + "service_provider/ServiceproviderController/serviceProviderChangeStatus",
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
      console.log(data);
      if(user_status == 1) { 
        var str_success="Service provider has been actived successfully";
        var str_error="Service provider has been activation failed";

        $(active_id).removeClass("btn-active-disable");
        $(active_id).addClass("btn-active-enable");

        $(inActive_id).removeClass("btn-inactive-enable");
        $(inActive_id).addClass("btn-inactive-disable");

        $(active_id).prop('disabled', true);
        $(inActive_id).prop('disabled', false);
        } else {
          $(active_id).removeClass("btn-active-enable");
          $(active_id).addClass("btn-active-disable");

          $(inActive_id).removeClass("btn-inactive-disable ");
          $(inActive_id).addClass("btn-inactive-enable");

          var str_success="Service provider has been inactived successfully";
          var str_error="Service provider has been inactivation failed";

          $(active_id).prop('disabled', false);
          $(inActive_id).prop('disabled', true);
        }
      if(data.status = true) {
        toastr.success(str_success);
      }
      else if(data.status = false) {
        toastr.error(str_error);        
       }
      else {
        toastr.error("Access denied..!");
        }
    }); 
  } 
}

function deleteServiceProvider($this)
{
  $("#LoadingDiv").css({"display":"block"});        
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure want to delete parent user ?'))
    {          
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"service_provider/ServiceproviderController/deleteSerivceProviderInfo",
            data: {"key":$($this).attr("data-row-id")},
        }).success(function (json) {
            if(json.status == "success"){
              $($this).parents("tr:first").remove();
                $("#LoadingDiv").css({"display":"none"});
                toastr.success(json.msg);
                $("#table").dataTable().fnDraw();              
             }else{
                $("#LoadingDiv").css({"display":"none"});
                toastr.error(json.msg);               
              }
        });
     } 
      $("#LoadingDiv").css({"display":"none"});
  } else {   
   $("#LoadingDiv").css({"display":"none"}); 
    toastr.error("Service provider something get wrong.");      
  }
}   

function viewServicepProvider($this)
{
  $("#LoadingDiv").css({"display":"block"});                
  if($($this).attr("data-row-id")){            
    $.ajax({
          type: "POST",
          dataType: "json",
         url: baseURL+"service_provider/ServiceproviderController/viewServiceProviderInfo",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        console.log(json);
          if(json.status == "success"){
          $("#LoadingDiv").css({"display":"none"});                
          $("#parent_name").text(json.servicePvdData.user_name);
          $("#parent_email").text(json.servicePvdData.user_email); 
          $("#user_phone_number").text(json.servicePvdData.user_phone_number);
          if(json.servicePvdData.user_gender == "female")
            $("#user_gender").text("Female");
          else if(json.servicePvdData.user_gender == "male")
            $("#user_gender").text("Male"); 
          else
            $("#user_gender").text("-"); 
          $("#user_address").text(json.servicePvdData.user_address);
          $("#user_birth_date").text(json.servicePvdData.user_birth_date);           
         
          $('#parentUserModal').modal('show', {backdrop: 'static'});

         }
         else{
           toastr.error(json.msg);   
          $("#LoadingDiv").css({"display":"none"});
         }
      });
  }else{
     $("#LoadingDiv").css({"display":"none"});
    toastr.error("service provider something get wrong.");         
  }
  $("#LoadingDiv").css({"display":"none"});
} 

$(document).ready(function(){
  $('#addPopUp').on("click",function(){
    $("#parentProfileModal").modal('show');                              
    });
});
function loadServiceList(category)
{   
  var table = $('#serviceList-grid').DataTable();
            $('#serviceList-grid').empty();
            table.destroy();

       $('#serviceList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"service-provider/serviceList",
            beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
        data : { category : category },
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,5,6], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    }); 
}
jQuery(document).ready(function(){  
 
  jQuery(document).on("click", ".viewService", function(){
    var userId = $(this).data("userid"),
      hitURL = baseURL + "service-provider/viewServiceDetails";
        $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      data : { userId : userId }
    }).success(function (json) {
        console.log(json);
    $("#ModalLabel").html('Service Provider Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html(json.data);
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');  
   });      
  }); 

 jQuery(document).on("click", ".deleteService", function(){
    var userid = $(this).data("userid");
    var category = $(this).data("category");
     var temp = "delService('"+userid+"','"+ category +"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b>Are you sure to delete this service provider ?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  });

  });

function serviceStatus_popup(userId,user_status,active_id,inActive_id)
{
 
var temp = "serviceStatus("+userId+","+user_status+",'"+active_id+"','"+inActive_id+"')";
$("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
$(".inner_body").html('<b>Are you sure want to change status ?</b>');
$(".inner_footer").html('<button type="button" onclick="'+temp+'" class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
$('.commanPopup').modal('show');
}
function serviceStatus(userId,user_status,active_id,inActive_id)
{
$('.commanPopup').modal('hide');
jQuery.ajax({
      type : "POST",
      dataType : "json",
      url : hitURL = baseURL + "service-provider/serviceStatus",
      beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      data : { userId : userId, user_status: user_status} 
      }).done(function(data){
        console.log(data);
        if(user_status == 1) { 
          var str_success="Service provider has been active successfully ";
          var str_error="Service provider activation failed";

          $("#"+active_id).removeClass("btn-active-disable");
          $("#"+active_id).addClass("btn-active-enable");

          $("#"+inActive_id).removeClass("btn-inactive-enable");
          $("#"+inActive_id).addClass("btn-inactive-disable");

          $("#"+active_id).prop('disabled', true);
          $("#"+inActive_id).prop('disabled', false);
            }
        else{

          $("#"+active_id).removeClass("btn-active-enable");
          $("#"+active_id).addClass("btn-active-disable");

          $("#"+inActive_id).removeClass("btn-inactive-disable ");
          $("#"+inActive_id).addClass("btn-inactive-enable");

          var str_success="Service provider has been inactive successfully ";
            var str_error="Service provider inactivation failed";

            $("#"+active_id).prop('disabled', false);
          $("#"+inActive_id).prop('disabled', true);
          }
        if(data.status == true) {toastr.success(str_success);

        }
        else if(data.status == false) { toastr.error(str_error); }
        else { alert("Access denied..!"); }
      }); 
}


function delService(userid,category)
{
  if(userid){
      
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"service-provider/deleteServiceUser",
           beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
            data: {"key":userid},
        }).success(function (json) {
            if(json.status == "success"){
            toastr.success(json.msg);
             
              
             }else{
              toastr.error(json.msg);
            }
        });
     
  } else {     
    toastr.error("User something get wrong.");    
  }
  $('.commanPopup').modal('hide');
  loadServiceList(category)
} 
