/**
 * @author Rajendra Pawar
 */

function loadTutorList()
{		
	var table = $('#tutorList-grid').DataTable();
            $('#tutorList-grid').empty();
            table.destroy();

       $('#tutorList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"tutorList/tutorList",
            beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
     		$('#LoadingDiv').hide();
  			},
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,5], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });	
}
jQuery(document).ready(function(){  
 
  jQuery(document).on("click", ".viewTutor", function(){
    var tutorId = $(this).data("tutorid"),
      hitURL = baseURL + "tutorList/viewTutorDetails";
        $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {
             $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      data : { tutorId : tutorId }
    }).success(function (json) {
        console.log(json);
    $("#ModalLabel").html('Tutor Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html(json.data);
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');  
   });      
  }); 

  });