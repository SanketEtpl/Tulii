/**
 * @author Rajendra Pawar
 */
function loadBookingList(category)
{		
	var table = $('#bookingList-grid').DataTable();
            $('#bookingList-grid').empty();
            table.destroy();

       $('#bookingList-grid').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"bookingList/bookingList",
            beforeSend: function() {
             
             var search = $("input[type=search]").val();
              if(search=="") 
              
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#bookingList-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
             },
             complete: function(){
     		$('#LoadingDiv').hide();
        $('.display').css({"opacity":"1"});
  			},
        data : { category : category },
            "type": "POST"
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [0,7], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],

 
    });	
      $("input[type=search]").on("keyup",function(event) {

        if($("#clear").length == 0) {
           if($(this).val() != ""){
            $("#bookingList-grid_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
          } 
        }
        if($(this).val() == "")  
        $("#clear").remove();      
      }); 
      $("input[type=search]").keydown(function(event) {
        k = event.which;
        if (k === 32 && !this.value.length)
            event.preventDefault();
      });

}
function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  var bookingManagement = $('#bookingManagement').val();
  loadBookingList(bookingManagement);
  // location.reload();

}
jQuery(document).ready(function(){  
 
  jQuery(document).on("click", ".viewBooking", function(){
    //$('#LoadingDiv').show();
    var bookingId = $(this).data("bookingid"),
      hitURL = baseURL + "bookingList/viewBookingDetails";
        $.ajax({
      type : "POST",
      async: false,
      dataType : "json",
      url : hitURL,
      beforeSend: function() {

             
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
      data : { bookingId : bookingId }
    }).success(function (json) {
        console.log(json);
    $("#ModalLabel").html('Booking Details<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $(".inner_body").html(json.data);
    $(".inner_footer").html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>');
    $('.commanPopup').modal('show');  
   });      
  }); 
 jQuery(document).on("click", ".deleteBooking", function(){
    var bookingid = $(this).data("bookingid");
    var category = $(this).data("category");
     var temp = "delBooking('"+bookingid+"','"+ category +"');"; 
    $("#ModalLabel").html('Confirmation<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
      $(".inner_body").html('<b> Are you sure you want to delete this booking?</b>');
    $(".inner_footer").html('<button type="button" onclick="'+temp+'"  class="btn btn-info">Yes</button>&nbsp;<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>');
    $('.commanPopup').modal('show');
  });
  });
function delBooking(bookingid,category)
{
  if(bookingid){
      
      $.ajax({
            type: "POST",
            dataType: "json",
           url: baseURL+"bookingList/deleteBooking",
           beforeSend: function() {
            // $('#LoadingDiv').show();
             },
             complete: function(){
        $('#LoadingDiv').hide();
        },
            data: {"key":bookingid},
        }).success(function (json) {
            if(json.status == "success"){
              $(this).parents("tr:first").remove();
              toastr.success(json.msg);             
              //$("#bookingList-grid").dataTable().fnDraw();
             }else{
              toastr.error(json.msg);
            }
        });
     
  } else {     
    toastr.error("Something went wrong!!!.");    
  }
  $('.commanPopup').modal('hide');
  loadBookingList(category)
} 