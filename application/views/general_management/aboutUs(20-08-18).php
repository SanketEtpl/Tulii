<!--

Author : Rajendra pawar 
Page :  aboutUs.php
Description : about Us  use for about us view and deleted functionality

-->

<script type="text/javascript" src="<?php echo site_url('assets/ckeditor/ckeditor.js'); ?>"></script>


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> General Management/About Us
        <small>Edit</small>
      </h1>
    </section>
    <section class="content">
    <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                </div>
            </div>
        </div>
        <div class="row">            
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box" style="opacity:0">
                <div class="box-header">              
                      <?php
                      $about_message='';
                      $about_id=''; 	
                      if(!empty($userRecords))
                      {  
                      foreach($userRecords as $record)
                      {
                   
                      $about_message = $record->about_message;
                      $about_id= $record->about_id;
                      }	
                      }
                  //  echo $this->ckeditor->editor("about_data",$about_us); 
										
                     ?> 
                      <textarea id="about_data" rows="" cols=""><?php echo $about_message; ?></textarea>   
                      <input type="hidden" id="about_id" name="about_id" value='<?php echo $about_id; ?>' />
                	<div><br></div>
      				 <div class="input-group">
            		 <div class="input-group-btn">
                     <button class="btn btn-sm btn-primary updateAbout"><i class="fa fa-edit"></i> Update</button>
                     </div>
             </div>
           

                </div><!-- /.box-header -->
                
                
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>


  <script>
    /*CKEDITOR.config.toolbar = [
        ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', '', 'Copy', 'Paste', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['mode', 'document', 'doctools'],
    ];
*/    
  CKEDITOR.replace("about_data",
  {
    height: 200,
    on: {
          instanceReady: function (evt) {
          $('.cke').addClass('admin-skin cke-hide-bottom');
        }
      }
  });
    //initSample();
</script> 
<script src="<?php echo base_url(); ?>assets/js/generalManagement.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    setTimeout(function(){ $('#LoadingDiv').hide(); $(".box").css({"opacity":"1"}); }, 1000);
  });

</script>