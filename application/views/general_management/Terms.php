<!--
  Author : Kiran
  Page :  terms.php
  Description : terms & condition add, update and delete functionality
-->
<script type="text/javascript" src="<?php echo site_url('assets/ckeditor/ckeditor.js'); ?>"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> CMS/Terms & condition
      <small>Add/Update/View/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addTemsPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new"><i class="fa fa-plus"></i> Add New</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="terms-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id no</th>
                  <th>User type</th>
                  <th>Title</th>
                  <th style="max-width:200px">Actions</th>
                </tr>
              </thead>
              <tfoot> 
                <tr>
                  <th>Id no</th>
                  <th>User type</th>
                  <th>Title</th>
                  <th style="max-width:200px">Actions</th>
                </tr>
              </tfoot>
            </table>
          </div><!-- /.box-body -->         
        </div><!-- /.box -->
      </div>
    </div>

    <div id="TermsModal" class="modal fade" style="display: none;" aria-hidden="false">
      <form action="" id="termsForm" name="termsForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title" id="headerTitle"><b>Add Terms & Condition</b> </h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">    
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_role">Role<span class="required_star">*</span></label>
                    <select class="form-control kidgender" id="userRole" name="userRole">
                      <option value="" disable="" selected="" hidden="">-- Select role --</option>
                      <?php 
                        if(!empty($roleData)) {                         
                          foreach ($roleData as $key => $value) {
                      ?>                   
                      <option value="<?php echo $value['role_id']; ?>"><?php echo $value['role_name']; ?></option>                    
                      <?php } } else { ?>
                      <option value=""></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>        
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_title">Title<span class="required_star">*</span></label>
                    <input type="text" placeholder="Title" required="true" maxlength="255" id="title" name="title" class="form-control title_valid">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_desc">Description<span class="required_star">*</span></label>
                    <textarea placeholder="Description" required="true" id="terms_data" name="terms_data" class="form-control editor description_valid"></textarea>
                   </div>
                </div>              
              </div>
              <div class="modal-footer">
                <input type="hidden" name="user_key" id="user_key" value="">
                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                <button class="btn btn-info" type="button" id="btnTerms">Save</button>
              </div>
            </div>
          </div>
        </div>      
      </form>
    </div>

    <div class="modal fade" id="viewTermsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel"><b>View Terms Description</b></h3>
          </div>
          <div class="modal-body">      
            <div class="row">
              <div class="col-md-12 rid_tim">
                <label class="col-md-12">Title :</label>
                <div class="rid_info col-md-12 word-break" id="viewTitle"></div>
              </div>
            </div>                     
            <div class="row">
              <div class="col-md-12 rid_tim">
                
                <label class="col-md-12">Description :</label>
                <div class="rid_info col-md-12 word-break" id="viewDescription"></div>
              </div>                        
            </div>                    
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

  </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/general_management/terms.js" type="text/javascript"></script>

<script type="text/javascript">
  jQuery(document).ready(function(){    
    loadTermsList();
  }); 

//CKEDITOR.config.startupMode = 'source';
  CKEDITOR.replace("terms_data",
  {
    height: 100,    
    on: {
      instanceReady: function (evt) {
        $('.cke').addClass('admin-skin cke-hide-bottom');
      }
    }
  });
</script>