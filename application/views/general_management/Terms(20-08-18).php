<!--

Author : Rajendra pawar 
Page :  Terms.php
Description : about Us  use for Terms & Condition view and deleted functionality

-->

<script type="text/javascript" src="<?php echo site_url('assets/ckeditor/ckeditor.js'); ?>"></script>



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> General Management/Terms & Conditions
        <small>Edit</small>
      </h1>
    </section>
    <section class="content">
    <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                </div>
            </div>
        </div>
        <div class="row">            
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box" style="opacity:0">
                <div class="box-header">              
                      <?php
                      $terms_message='';
                      $terms_id=''; 	
                      if(!empty($termsRecords))
                      {  
                      foreach($termsRecords as $record)
                      {
                   
                      $terms_message = $record->terms_message;
                      $terms_id= $record->terms_id;
                      }	
                      }
                  //  echo $this->ckeditor->editor("about_data",$about_us); 
										
                     ?> 
                      <textarea id="terms_data" rows="" cols=""><?php echo $terms_message; ?></textarea>   
                      <input type="hidden" id="terms_id" name="about_id" value='<?php echo $terms_id; ?>' />
                	<div><br></div>
      				 <div class="input-group">
            		 <div class="input-group-btn">
                     <button class="btn btn-sm btn-primary updateTerms"><i class="fa fa-edit"></i> Update</button>
                     </div>
             </div>
           

                </div><!-- /.box-header -->
                
                
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>


  <script>
    /*CKEDITOR.config.toolbar = [
        ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', '', 'Copy', 'Paste', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['mode', 'document', 'doctools'],
    ];*/
CKEDITOR.replace("terms_data",
{
  height: 200,
  on: {
        instanceReady: function (evt) {
        $('.cke').addClass('admin-skin cke-hide-bottom');
      }
    }
});
    //initSample();
</script> 
<script src="<?php echo base_url(); ?>assets/js/generalManagement.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    setTimeout(function(){ $('#LoadingDiv').hide(); $(".box").css({"opacity":"1"}); }, 1000);
  });

</script>