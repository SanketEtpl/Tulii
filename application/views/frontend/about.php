<?php $this->load->view("frontend/header"); ?>
<div class="content">
	<div>
		<div>
			<img src="<?php echo base_url();?>assets/images/professionals.jpg" alt="Image" />
		</div>
		<div>
			<div id="sidebar">
				<h3>Tulii’s extra Safeguards</h3>
				<ul>
					<li id="vision">
						<span>Safe Transitions for Children and Youth</span>
						<p> Tulii will ensure that your kids are safely picked-up and dropped-off only in accordance to their parents’ directions.</p>
					</li>
					<li id="mission">
						<span>Open communication</span>
						<p>Tulii will make every effort to keep you informed with the most up-to-date and accurate information with regards to your kids and we are available 24/7 if you need to reach us.</p>
					</li>
					<li id="wecare">
						<span>Regular Check-Ins</span>
						<p>Tulii will perform regular check-ins on all of our employees on a monthly, quarterly and yearly basis to ensure vehicle checks, performance compliance and safety standards are being met, as per our licensing practices.</p>
					</li>
				</ul>
			</div>
			<div id="aside">
				<span class="first">What is Tulii?</span>
				<p>Tulii is a safe ride-sharing and after-school care network that provides scheduled transportation, childcare and tutoring services for children and youth presently operating in the Capital Region of the City of Edmonton and surrounding areas. Tulii will get kids where they need to go, when they need to go; taking the stress and hassle off working parents. Our services are designed to help parents better manage their children’s day-to-day activities and best plan for their family’s busy schedule.
				Tulii is available at all times, to be delegated by parents, for their children who need safe rides (i.e. to and from school), as well as for after-school care (provided by our team of carefully chosen, highly qualified caregivers and tutors). Tulii’s services will greatly benefit today’s families, while giving back time to parents and offering individualized services for their kids.
				Tulii was created by a team of three parents and professionals; including a father of six, a mother of 2, and a corporately-conscientious modern woman; all with one major goal in common - to empower the community they live in by helping parents.</p>
				<span>Why Was Tulii Started?</span>
				<p>Tulii was created by three insightful people who all have a certain perspective and unique understanding of the pressures that parents face trying to get their kids to and from their various activities, events, appointments, etc. This is coupled with the sense of fear that naturally comes when a parent puts their kid in someone else's car and entrusts them to a stranger. The solution is Tulii’s safe, secure, convenient, reliable, and well-thought out uses, borne out of today’s modern family’s necessity to plan ahead, and fueled by the need for transportation and childcare services for kids in our community...</p>
				<span>How is Tulii different from other ride-share and taxi companies?</span>
				<p>We are a child care network servicing only minors (children & youth under the age of 18 years old). Our primary focus is on children, youth, and their families - unlike these other examples of ride-sharing services (like Uber, TappCar, Lyft, Yellow Taxi, etc.), which are not for kids, and are certainly not safe for kids. We have a highly-qualified, well-trained team of professionals in our company, which have met our extensive background checks and qualifications. Unlike these typical ride-sharing and taxi companies, all of our CareDrivers, ChildCare Providers and Tutors must have childcare experience. We also take into consideration the needs of each individual family that solicits our services.
				Tulii is specifically designed to be a careful ride-sharing and afterschool care service network for busy families who require extra attention and support; especially when parents just can’t do it all. Other ride-sharing companies like Uber, TappCar and Lyft are not insured to carry passengers under the age of 18 years old and are not suitable to minors. In fact, their terms of service (your legal contract with them) specifically prohibit the use of their services by children. And that’s why Tulii was created!</p>
				<span>What makes us even More Unique?</span>
				<p>We are safe ride-sharing and afterschool care network that sends consistent CareDrivers, ChildCare Providers and Tutors for your children and youth when and wherever you need them.</p>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>