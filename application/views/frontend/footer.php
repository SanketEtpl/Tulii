<div id="footer">
		<div>
			<div>
				<span>Follow us</span>
				<a href="https://www.facebook.com/tuliiinc" target="_blank" class="facebook">Facebook</a>
				<a href="https://plus.google.com/u/1/b/102922695468584630228/collection/IeZfbB" target="_blank" class="subscribe">Subscribe</a>
				<a href="https://twitter.com/tuliiinc" target="_blank" class="twitter">Twitter</a>
				<a href="http://www.flickr.com/freewebsitetemplates/" target="_blank" class="flicker">Flickr</a>
			</div>
			<ul>
				<li>
					<a href="<?php echo base_url();?>about"><img src="<?php echo base_url();?>assets/images/playing-in-grass.gif" alt="Image" /></a>
					<p>Tulii is a safe ride-sharing and after-school care network that provides scheduled transportation...</p>
					<a href="<?php echo base_url();?>about" class="readmore">Read more</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>services"><img src="<?php echo base_url();?>assets/images/baby-smiling.gif" alt="Image" /></a>
					<p>Tulii offers a variety of afterschool care services by our team of ChildCare Providers. These include childcare either in the home or out of the home...</p>
					<a href="<?php echo base_url();?>services" class="readmore">Read more</a>
				</li>
			</ul>
		</div>
		<p class="footnote">&copy; tulii. All Rights Reserved.</p>
	</div>
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/owlCarousel.js"></script>
	<script src="http://localhost/tulii/assets/js/form_validation.js"></script>
</body>
</html>