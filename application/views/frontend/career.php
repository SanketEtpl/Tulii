<?php $this->load->view("frontend/header"); ?>
<div class="content">
	<div>
		<div>
			<img src="<?php echo base_url();?>assets/images/calling.jpg" alt="Image" />
		</div>
		<div>
			<div id="sidebar">
				<h3>Career</h3>
				<ul>
					<li id="vision">
						<span>Who is Eligible?</span>
						<p>Everyone can be a driver, Childcare Provider or tutor but not everyone can bea Tulii CareDriver, Childcare Provider or Tutor. We have a stringent selection process that will only guarantee the best applicants. </p>
					</li>					
				</ul>
			</div>
			<div id="aside">
				<h2>In Addition you must be:</h2>
				<ul>
					<li>At least 21 years old</li>
					<li>At least 2 years of experience working with kids or youth</li>
					<li>A valid Driver’s License</li>
					<li>At least 3 years of driving experience with clean driving record</li>
					<li>Own a 2009 or above four door car in a very good condition</li>
					<li>College education and/or experience working in a professional setting or vulnerable sector</li>
					<li>A Clean Enhanced Criminal Record Check</li>
				</ul>
				<ul>
					<h2 class="first">Key Hiring Process</h2>
					<p>Apply today and become part of our amazing Tulii Community!</p>
					<span>INTERVIEW</span>
					<p>Applicants go through a very thorough interview process covering their personality, attitude, reactions to situations and sense of purpose.</p>
					<span>BACKGROUND CHECK</span>
					<p>We go through multiple checks such as address history, sex offender records, driving history and conducting an enhanced police check for every CareDriver, Childcare Provider and Tutor applicant.</p>
					<span>VEHICLE INSPECTION</span>
					<p>Only Tulii-approved vehicles are deployed for rides. We have a strict policy to monitor regular maintenance and perform checks to ensure that appropriate inspections were completed.</p>
				</ul>
				<span>Who are the Tulii Service Providers?</span>
				<p>Tulii CareDrivers, Childcare Providers and Tutors love working with children. They are parents, teachers, nannies, nurses, moms, childcare professionals, post-graduate students and other professionalswith at least two years of experience working with kids or youth. All Tulii service providers (CareDrivers, Childcare Providers & Tutors) pass a rigorous 16-point certification process, have both a clean enhanced police criminal background check and driving record, and have all been handpicked by Tulii experts to guarantee safety and quality service.</p>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>	