<?php $this->load->view("frontend/header"); ?>
<div class="content">
	<div>
		<div>
			<img src="<?php echo base_url();?>assets/images/calling.jpg" alt="Image" />
		</div>
		<div>
			<div id="sidebar">
				<!-- <h3>Career</h3>
				<ul>
					<li id="vision">
						<span>Who is Eligible?</span>
						<p>Everyone can be a driver, Childcare Provider or tutor but not everyone can bea Tulii CareDriver, Childcare Provider or Tutor. We have a stringent selection process that will only guarantee the best applicants. </p>
					</li>					
				</ul> -->
			</div>
			<div id="aside">
				<h2>Tulii’s pricing model:</h2>
				<p>Tulii operates on a pay-per use basis - no contracts or subscriptions required! For your convenience, all payments are cashless and managed through the app.</p>
				<ul>
					<li>Ride Service Rates:
						<p>The fare starts from $17 for the first 5 Kilometers ((minimum fare) and then any additional kilometer will cost $1.50
						Our rates are charged on a Time + Mileage basis
						Carpool attracts a flat rate of $10 per family</p>
					</li>
					<li>Chaperon Service Rates:<p>The chaperon service costs $8 per 15 minutes. Unlike regular nannies, you only pay for the time that you use.</p></li>
					<li>Tutoring Service Rates:<p>The tutoring service costs $30 per hour</p></li>					
				</ul>
				
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>	