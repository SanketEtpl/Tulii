<?php $this->load->view("frontend/header"); ?>
<div class="content">
	<div>
		<div>
			<img src="<?php echo base_url();?>assets/images/baby.jpg" alt="Image" />
		</div>
		<div id="services">
			<div id="sidebar">
				<h3>Tulii provides the following Safe and Quality Services:</h3>
				<ul>
					<li id="vision">
						<span>Afterschool Care</span>
						<p>Tulii offers a variety of afterschool care services by our team of ChildCare Providers. These include childcare either in the home or out of the home...</p>
					</li>
					<li id="mission">
						<span>Rides</span>
						<p>Tulii offers rides that are designed to transport children and youth to and from school, daycare, and after-school commitments by our team of CareDrivers...</p>
					</li>
					<li id="wecare">
						<span>Rides + Afterschool Care Packages</span>
						<p>Tulii offers rides + afterschool care packages, where parents may combine transportation for their child and childcare services in one request... </p>
					</li>
					<li id="wecare">
						<span>Tulii Tutoring</span>
						<p>Tulii’s tutoring services specializes in one-to-one, private, in-home tutoring in ALL major academic subjects, including exam preparation and literacy programs. </p>
					</li>
				</ul>
			</div>
			<div id="aside">
				<span class="first">How Does Tulii Work?</span>
				<ul class="section">
					<li>Get to Know your CareDriver, ChildCare Provider or Tutor 
					You will receive your CareDriver/ChildCare Provider/Tutor picture and profile at least 6 hours prior to the start of the scheduled ride/afterschool care service. .</li>
					<li>Leave Feedback about Rides, Afterschool Care and Tutoring Services 
					At Tulii, we aim on becoming the best ride-sharing and afterschool care network for your kids and meeting all of your family’s needs. We'll use the feedback you provide to help improve our services and to continue to refine our services, in order to meet the highest standards of service delivery to our community members.</li>
					<li>Book/Schedule your Service (Ride and/or Afterschool Care):
					Submit your service request the day before your service is needed prior to noon local standard time using our Tulii App or Website. You can schedule a one-time ride/afterschool care request or a recurring ride/afterschool care request in through our leading-edge Tulii App and Website .</li>
					<li>Track your Child’s Ride and their Whereabouts in Real-Time Using our Tulii App 
					You will be able to check on your child in real-time using our Tulii App on your smartphone device; while on the road, from pick-up location to drop-off destination. Tulii will send updates to your phone about your child’s ride or afterschool care status. You may contact your child’s CareDriver/ChildCare Provider/Tutor at any time during the ride or afterschool care provision .</li>
				</ul>
				
				<span>How Tulii’s Technology Works?</span>
				<div>
					<ul>
						<li><b>Matchmaking </b>
						<p>Our smart technology uses data to customize your CareDriver pool to your preferences.</p></li>
						<li><b>Notifications</b>
						<p>We always keep you in the loop about ride or service progress just as if you were doing it yourself.</p></li>
						<li><b>Tracking</b> 
						<p>You can use our app to track the location of your kid, speed of car and expected time of arrival at all times.</p></li>
						<li><b>Pricing </b>
						<p>Tulii will operate on a pay-per use basis - no contracts or subscriptions required! For your convenience, all payments are cashless and managed through the app</p></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>	