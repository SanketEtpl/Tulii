<?php $this->load->view("frontend/header"); ?>
<div class="content">
	<div>
		<div>
			<img src="<?php echo base_url();?>assets/images/professionals.jpg" alt="Image" />
		</div>
		<div id="blog">
			<div class="sidebar">
				<h2>16 – Point Service Provider Certification Process</h2>
				<h3 class="first">Point Service Provider Certification Process</h3>
				<div>
					<ul>
						<li>Valid Emergency First Aid & CPR Training Certification.</li>
						<li>Adopts the zero tolerance policy for not making physical contact with any electronic device while driving for Tulii.</li>
						<li>Adopts our zero tolerance policy for smoking, drugsand alcohol while driving.</li>
						<li>Adopts the Tulii Rules.</li>
						<li>Personal and professional references checked.</li>
						<li>Completes in-person driver training and orientation.  </li>
						<li>In-person meeting for an interview with a member of the Tulii team.  </li>
						<li>Must have a cell-phone & well-maintained vehicle that has passed a provincial safety inspection (within 30 days).</li>
						<li>Age 21 or older  </li>
						<li>No sex offender record.</li>
						<li>Clean driving record. We run their driving record when we first meet them and continue to on a quarterly basis.</li>
					</ul>					
				</div>				
			</div>
			<div id="aside">
				<span class="first">Our kids use Tulii as well.</span>
				<p>     As parents, we care and worry about our kids and your wonderful children. That's why we have built rigorous safety measures into every service provided by Tulii.
				Safety, security and the assurance that your children are our highest priority comes before anything else. That is why we have created a comprehensive guide for parents outlining what steps Tulii takes, in order to promise to you that we are meeting the highest standard of safety in the ride-sharing industry and caregiving field. All of CareDrivers, ChildCare Providers and Tutors must meet the following guidelines, which are all reviewed through our Alberta Safety Standards licensing process: .</p>
				
				<span>Before the ride, childcare or tutoring service, Tulii requires:</span>
				<ul>
					<li>Unique driver, ChildCare Providers and Tutor requirements that include a minimum of two years experiencein the Human Services field;</li>
					<li>Passing a comprehensive 16 – Point service provider Certification Process and</li>
					<li>Passing a Police Criminal Record Check, including Vulnerable Sector Check (within 6 months).</li>
				</ul>
				
				<span>During the ride, care or tutoring service:</span>
				<ul>
					<li>A personalized and secret code you and your child create that CareDrivers, ChildCare Providers and Tutors confirm upon pickup and arrival to provide service;</li>
					<li>You can track your child's ride and the speed of the car live via the Tulii app and</li>
					<li>We have partnered with Zendrive to monitor driving behavior and ensure CareDrivers are focused, in control, and not texting or talking on the phone while a kid is in the car.</li>
				</ul>
				<span>Behind the scenes:</span>
				<ul>
					<li>Tulii monitors rides in real time, and live customer support is available by phone or email on 24/7 basis;</li>
					<li>We have a specialized, unique insurance for transporting kids that exceeds regulatory requirements and</li>
					<li>Tulii matches the family’s request for service with one of 5 personalized, familiar and trusted CareDriver, ChildCare Providers and Tutor.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("frontend/footer"); ?>