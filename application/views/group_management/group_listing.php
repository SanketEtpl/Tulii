<?php //$this->load->view('includes/header');?>

<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/tab.css" />
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imageviewer.css" />
<script src="<?php echo base_url(); ?>assets/js/group/group.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/imageviewer.js" charset="utf-8"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Group Management
      <small>View</small>      
    </h1>
  </section>
  <section class="content">   
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Group name</th>
                  <th>No of people</th>
                  <th>Owner</th>
                  <th>User Type</th>
                  <th>Status</th>
                  <th>Delete Status</th>                                           
                  <th>Action</th>                                           
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Sr. No.</th>
                  <th>Group name</th>
                  <th>No of people</th>
                  <th>Owner</th>
                  <th>User Type</th>
                  <th>Status</th>
                  <th>Delete Status</th>                                           
                  <th>Action</th>                 
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div> 
    <div class="modal fade" id="groupListingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="modal-title" id="exampleModalLabel"><b>Group details</b></h3>              
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Group name :</label>
                <div class="rid_info" id="group_name"></div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Description:</label>
                <div class="rid_info" id="description"></div>
              </div>
            </div>   
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>No of peoples :</label>
                <div class="rid_info" id="no_of_peoples"></div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Group last date :</label>
                <div class="rid_info" id="group_last_date"></div>
              </div>                      
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover" id="groupList">
                <thead>
                  <tr>
                    <th>Id.no</th> 
                    <th>Member name</th>
                    <th>Date of birth</th>
                    <th>Gender</th>
                    <th>Address</th>                              
                  </tr>
                </thead>
                <tbody>                        
                </tbody>             
              </table>            
            </div>                
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>  
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 

  $(document).ready(function() { 
    $("#LoadingDiv").css({"display":"block"});
    groupList();
  });

</script>

