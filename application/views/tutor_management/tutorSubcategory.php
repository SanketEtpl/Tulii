<?php // $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-list"></i>Tutor Management/Tutor Sub-category List
      <small>Add/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addTutorSubCatgoryPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add tutor sub-catgory"><i class="fa fa-plus"></i> Add tutor sub-catgory</a>          
        </div>
       </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
            <table id="tutorList" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id. no</th>                    
                  <th>Category name</th>
                  <th>Sub-category name</th>                                       
                  <th>Rate per hour</th>                                       
                  <th>Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Category name</th>
                  <th>Sub-category name</th>                                       
                  <th>Rate per hour</th>                                       
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    
    <div id="tutorSubCategoryModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
      <form action="" id="tutorSubCatForm" name="tutorSubCatForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Add tutor sub-category</b> </h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_tc">Tutor category<span class="required_star">*</span></label>
                    <select class="form-control" id="tutorCategory" name="tutorCategory">
                      <option value="" disable="" selected="" hidden=""> Select tutor category</option>
                        <?php                   
                          if(!empty($categoryList)) {                         
                            foreach ($categoryList as $key => $value) {                           
                        ?>
                          <option value="<?php echo $value['id']; ?>"><?php echo ucfirst($value['category_name']); ?></option>                    
                        <?php  } } else { ?>
                        <option value=""></option>
                        <?php } ?>
                    </select>
                  </div>
                </div> 
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group ui-widget">
                    <label class="control-label" for="tutorsubname">Tutor sub-category name<span class="required_star">*</span></label><br>
                    <input type = "text" class="form-control name" maxlength="50" id = "tutor_sub_category_name" />             
                  </div>
                </div>   
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group ui-widget">
                    <label class="control-label" for="ratePerhour">Rate per hour<span class="required_star">*</span></label><br>
                    <input type = "text" class="form-control price" maxlength="7" id = "rate_per_hour" />
                  </div>
                </div>   
              </div>                  
            </div>
            <div class="modal-footer">
              <input type="hidden" name="user_key" id="user_key">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" onclick="btnAddTutorSubCat()">Save</button>
            </div>
          </div>
        </div>
      </form>
    </div> 
  </section>
<script src="<?php echo base_url(); ?>assets/js/tutor/tutorSubcategory.js"></script>
