<?php // $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-list"></i>Tutor Management/Tutor Category List
      <small>Add/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addTutorCatgoryPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add tutor catgory"><i class="fa fa-plus"></i> Add tutor catgory</a>          
        </div>
       </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
            <table id="tutorList" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id. no</th>                    
                  <th>Category name</th>                                       
                  <th>Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Category name</th>                                       
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    
    <div id="tutorcategoryModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
      <form action="" id="tutorcatForm" name="tutorcatForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Add tutor category</b> </h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group ui-widget">
                  <label class="control-label" for="tutorname">Tutor category name<span class="required_star">*</span></label><br>
                  <input type = "text" class="form-control name" maxlength="50" id = "tutor_category_name" />
                </div>
              </div>   
            </div>                 
            </div>
            <div class="modal-footer">
              <input type="hidden" name="user_key" id="user_key">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" onclick="btnAddTutorCat()">Save</button>
            </div>
          </div>
        </div>
      </form>
    </div> 
  </section>
<script src="<?php echo base_url(); ?>assets/js/tutor/tutorCategory.js"></script>
