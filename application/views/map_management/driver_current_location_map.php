<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-map-marker"></i> Map management/Driver current location
      <small>Map</small>
    </h1>
  </section>
  <section class="content">    
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Driver name</th>
                    <th>Email</th>
                    <th>Contact number</th>
                    <th style="width: 40px !important">Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                    <th>Driver name</th>
                    <th>Email</th>
                    <th>Contact number</th>
                    <th style="width: 40px !important">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div class="modal fade" id="viewdriverCurrentMapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
          <h3 class="modal-title" id="exampleModalLabel"><b>View driver current location</b></h3>
        </div>
        <div class="modal-body"> 
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
              <head>
              <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
              <meta charset="utf-8">
              <title>Simple Polylines</title>
              <style>
                /* Always set the map height explicitly to define the size of the div
                 * element that contains the map. */
                #map {
                  height: 300px;
                }
                /* Optional: Makes the sample page fill the window. */
                html, body {
                  height: 300px;
                  margin: 0;
                  padding: 0;
                }
              </style>
            </head>                     
            <body>
            <div id="map"></div>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY;?>">
            </script>
          </body>
          </div>
        </div>
      </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  </div>
  </section>
</div>
<script type="text/javascript"> 
//var table; 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
    //datatables
    driverCurrentLocationMapList();
});

</script>
<script src="<?php echo base_url(); ?>assets/js/map/driverLocationMap.js"></script>

