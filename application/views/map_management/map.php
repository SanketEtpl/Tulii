<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Simple Polylines</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 14,
          center: new google.maps.LatLng(18.559662, 73.789351),
          //mapTypeId: 'terrain'
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var flightPlanCoordinates = [
          {lat: 18.559662, lng: 73.789351},
          {lat: 18.559994, lng: 73.788757},
          {lat: 18.560995, lng: 73.787636},
          {lat: 18.561367, lng: 73.786987},

          {lat: 18.561423, lng: 73.786198},
          {lat: 18.561570, lng: 73.785421},
          {lat: 18.561656, lng: 73.784648},

          {lat: 18.563570, lng: 73.782627},
          {lat: 18.563778, lng: 73.782252},
          {lat: 18.563972, lng: 73.781468},

        ];
        //console.log(flightPlanCoordinates);
        //alert(flightPlanCoordinates);
        //return false;
        var flightPath = new google.maps.Polyline({
          path: flightPlanCoordinates,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });
       
       /** Start point marker **/
        var marker = new google.maps.Marker({
          position: {lat: 18.559662, lng: 73.789351},
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
        });

          /** driver ride point marker **/
        var marker = new google.maps.Marker({
          position: {lat: 18.561656, lng: 73.784648},
          map: map,
          icon: 'car.png'
        });
        
        /** End point marker **/
        var marker = new google.maps.Marker({
          position: {lat: 18.563972, lng: 73.781468},
          map: map,
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
        
        flightPath.setMap(map);

      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBD5NeyT_tKxswCM6YHXRXZCDbb7NDIuko&callback=initMap">
    </script>
  </body>
</html>
