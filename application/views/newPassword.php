<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Tulii | Admin System Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .error{
      color:red;
    }
</style>
<script type="text/javascript">
 var baseURL = "<?php echo base_url(); ?>";
</script>
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><a href="#"><img src="<?php echo base_url();?>assets/images/logo.png" width="260"></a></a>
      </div><!-- /.login-logo -->
     <!--  <div class="login-logo">
        <a href="#"><b>Tulii</b><br>Admin System</a>
      </div> --><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Reset Password</p>
        <div id="changeMessage"></div> 
        <?php $this->load->helper('form'); ?>
        <?php
        $this->load->helper('form');
        $error = $this->session->flashdata('notsend');
        if($error)
        {
            ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $this->session->flashdata('notsend'); ?>                    
            </div>
        <?php } ?>
        
        <form action="" method="post" novalidate>
          <div class="form-group has-feedback">
            <input type="password" class="form-control password" placeholder="Password" value="<?php echo  set_value('password'); ?>" id="changePassword" name="password" maxlength="10" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php echo form_error('password', '<div class="error">', '</div>'); ?>
            <input type="hidden" name="token" id="token"  value="<?php echo $mydata['user_device_token']; ?>" required />
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $mydata['service_user_id']; ?>" required />
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control password" placeholder="Confirm Password" id="changeConfirmPassword" name="cpassword" value="<?php echo  set_value('cpassword'); ?>" maxlength="10" required />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            <?php echo form_error('cpassword', '<div class="error">', '</div>'); ?>

          </div>
          <div class="row">
            <div class="col-xs-8">    
              <!-- <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>  -->                       
            </div><!-- /.col -->
            <div class="col-xs-4">
              <input type="button" class="btn btn-primary btn-block btn-flat" value="Submit" id="changePwd"/>
            </div><!-- /.col -->
          </div>
        </form>
        
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <script type="text/javascript">
      $(document).ready(function(){      
        $("#changePwd").on("click",function(){
          var password = $("#changePassword").val();
          var cpassword = $("#changeConfirmPassword").val();
          var token = $("#token").val();
          var user_id = $("#user_id").val();
          var flag = 0;        
           if(password == ''){
            $(".changePassword").remove();  
            $("#changePassword").parent().append("<div class='changePassword' style='color:red;'>Password field is required.</div>");
            flag = 1;
          }else{
            $(".changePassword").remove();        
          }

          if(cpassword == ''){
            $(".changeConfirmPassword").remove();  
            $("#changeConfirmPassword").parent().append("<div class='changeConfirmPassword' style='color:red;'>Confirm password field is required.</div>");
            flag = 1;
          } else if(cpassword != password) {
            $(".changeConfirmPassword").remove();  
            $("#changeConfirmPassword").parent().append("<div class='changeConfirmPassword' style='color:red;'>Password & Confirm password didn't match.</div>");
            flag = 1;
          } else {
            $(".changeConfirmPassword").remove();        
          }
          if(flag == 1)
            return false;    
          else
          {
           
            $.ajax({
              type:"POST",
              url:baseURL+"createPasswordUser",            
              data:{"password":password,"user_id":user_id,"token":token},
              dataType:"json",
              async:false,
              success:function(response){  
                if(response.status == true){
                  $('#changeMessage').html('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+response.message+'</div>');
                  setInterval(function(){ document.location.href = response.redirect; }, 5000);                        
                }
                else
                {
                  $('#changeMessage').html('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+response.message+'</div>');
                 setInterval(function(){ document.location.href = response.redirect; }, 5000);                  
                }
              }
            }); 
          }  
        });
       $(".password").keydown(function(e) {        
          k = e.which;
          if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 32 || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
              if ($(this).val().length == 100) {
                  if (k == 8) {
                      return true;
                  } else {
                      e.preventDefault();
                      return false;
                  }
              }
          } else {
          e.preventDefault();
              return false;
          }
          if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
          e.preventDefault();
          if (k === 32 && !this.value.length)
              e.preventDefault();
      });
  });
    </script>
  </body>
</html>