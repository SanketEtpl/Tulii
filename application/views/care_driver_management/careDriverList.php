<!--*

Author : Rajendra pawar 
Page :  careDriverList.php
Description : care driver list use for care driver managment functionality

*-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/careDriverManagement.js" charset="utf-8"></script>
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/careDriverManagement.css" />

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Care Driver Management/Care Driver
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                 <button type="button" onclick="openAddCarePopup();" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</a></button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                
                <div class="box-body table-responsive no-padding">
                <table id="careDriverList-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
                   <thead>
                       <tr>
                      <th>Sr. no</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Status</th>
                      <th >Actions</th>
                    </tr>
                 </thead>
                 <tfoot> 
                 <tr>
                      <th>Sr. no</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Status</th>
                      <th >Actions</th>
                    </tr>
                    </tfoot>
                </table>       
                </div><!-- /.box-body -->
                
              </div><!-- /.box -->
              </div>
        </div>
    </section>
</div>
 <script type="text/javascript">
  jQuery(document).ready(function(){
loadList(); 
}); 
 </script> 




