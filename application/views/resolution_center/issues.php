<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-search"></i> Resolution center/issues
      <small>View/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Ticket no</th>
                    <th>Name</th>
                    <th>Mobile no</th>
                    <th>Service provider</th>
                    <th>Issue</th>
                    <th>Issue date</th>  
                    <th style="width:18%;">Status</th>             
                    <th style="width:12%;">Action</th>                        
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Ticket no</th>
                  <th>Name</th>
                  <th>Mobile no</th>
                  <th>Service provider</th>
                  <th>Issue</th>
                  <th>Issue date</th> 
                  <th style="width:18%;">Status</th>             
                  <th style="width:12%;">Action</th>      
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
   <div class="modal fade" id="issuesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><h3 class="modal-title"><b>Issue details</b></h3></h3>
            </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Parent name :</label>
                <div class="rid_info" id="parent_name"></div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Parent number:</label>
                <div class="rid_info" id="parent_number"></div>
              </div>
            </div>   
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Service provider :</label>
                <div class="rid_info" id="service_provider"></div>
              </div> 
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Issue :</label>
                <div class="rid_info" id="issue"></div>
              </div>             
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Issue date :</label>
                <div class="rid_info" id="issue_date"></div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Status :</label>
                <div class="rid_info" id="status"></div>
              </div>
            </div>                   
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"});
  //datatables
  issueList();   
  $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  // location.reload();
  issueList();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/resolution/resolution.js"></script>