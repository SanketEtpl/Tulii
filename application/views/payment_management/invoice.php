<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Tulii</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- <link rel="stylesheet" type="text/css" href="style.css"> -->
		<style>
		.wrapper{
			float:left;
			width:100%;
		}
		.body_bg_color{
			float:left;
			width:100%;
			text-align:center;
			}
		.main_center_div{
			margin: 0 auto;
			width:100%;
			}	
		.invoice_header{
			float:left;
			width:100%;
			background-color:#fdfdfd;
			border-bottom: 1px solid #dde5f1;
			}
		.left_header{
			float:left;
			width:28%;
			padding: 7px 0px;
			padding-left:15px;
			}
		.left_header a > img{
			float: left;
			width: 25%;
			}
		.right_header{
			float:right;
			width:48%;
			}
		.invoice_title{
			padding:20px 25px;
			color: #425065;
			font-family: sans-serif;
			font-size: 15px;
			text-align: right;
			line-height: 150%;
			font-weight: bold;
			letter-spacing: 2px;
			}
		.invoice_discription{
			float:left;
			width:100%;
			background-color:#fdfdfd;
			border-bottom: 1px solid #dde5f1;
		}	
		.invoice_discription_info{
			padding: 15px 25px;
			color: #727e8d;
			font-family: sans-serif;
			font-size: 13px;
			font-weight: lighter;
			text-align: left;
			line-height: 22px;
		}	
		.invoice_discription_info_link{
			text-decoration: none;
			color: #51bfe2;
			font-weight: bold;
		}
		.invoice_info_wrapper{
			float: left;
			width:100%;
			background-color: #fdfdfd;
			border-bottom: 1px solid #dde5f1;
			}
		.invoice_info_left{
			float: left;
			padding: 15px 0px;
			width: 49.9%;
			background-color: #fdfdfd;
			border-right: 1px solid #dde5f1;
			}	
		.invoice_email_icon{
			float: left;
			width: 9%;
			padding-left:8%;
		}	
		.invoice_home_icon{
			float: left;
			width: 9%;
			padding-left:8%;
		}	
		.invoice_send_to{
			width: 80%;
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			text-align: left;
			float: right;
			padding-left: 15px;
			text-align: left;
			line-height: 17px;
			font-weight: lighter;
			margin-top:-20px;
			}	
		.invoice_send_name{
			width:80%;
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: bold;
			text-align: left;
			line-height: 23px;
			float: left;
			padding-left: 68px;
			margin-top: -5px;	
			}	
		.invoice_po_add{
			float:left;
			width:91%;
			margin-top:7px;
			color: #727e8d;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: left;
			line-height: 20px;
			padding-left: 26px;
			}	
		.invoice_info_right{
			float: left;
			padding: 15px 0px;
			width: 49.9%;
			background-color: #fdfdfd;
			}	
		.invoice_information_wrapp{
			float: left;
			width:100%;
			background-color: #fdfdfd;
			border-bottom: 1px solid #dde5f1;
			}	
		.invoice_sub_info{
			float:left;
			width:100%;
			}	
		.invoice_no{
			float:left;
			padding:20px 0px;
			width:33.2%;
			background-color:#fdfdfd;
			border-right: 1px solid #dde5f1; 
		}	
		.invoice_no_img{
			float:left;
			width:15%;
			padding-left: 20px;
			}	
		.invoice_no_title{
			float:left;
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			text-align: left;
			line-height: 19px;
			font-weight: lighter;
			padding-left: 6%;
			}	
		.invoice_no_id{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: bold;
			text-align: left;
			line-height: 23px;
			padding-left: 30.5%;
			float: left;
			width: 100%;
			margin-top: -5px;
		}	
		.invoice_date{
			float:left;
			padding:20px 0px;
			width:33.2%;
			background-color:#fdfdfd;
			border-right: 1px solid #dde5f1; 
		}	
		.invoice_date_img{
			float:left;
			width:15%;
			padding-left: 20px;
			}	
		.invoice_date_title{
			float:left;
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			text-align: left;
			line-height: 19px;
			font-weight: lighter;
			padding-left: 6%;
			}	
		.invoice_date_id{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: bold;
			text-align: left;
			line-height: 23px;
			padding-left:30.5%;
			float: left;
			width: 100%;
			margin-top: -5px;
		}	
		.invoice_total{
			float:left;
			padding:20px 0px;
			width:33.2%;
			background-color:#fdfdfd;
			}	
		.invoice_total_img{
			float:left;
			width:15%;
			padding-left: 20px;
			}	
		.invoice_total_title{
			float:left;
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			text-align: left;
			line-height: 19px;
			font-weight: lighter;
			padding-left: 6%;
			}	
		.invoice_total_id{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: bold;
			text-align: left;
			line-height: 23px;
			padding-left: 30.5%;
			float: left;
			width: 100%;
			margin-top: -5px;
			}
		.invoicediscriptionwrapp{
			float: left;
			width:100%;
			background-color: #fdfdfd;
			border-bottom: 1px solid #dde5f1;
			background-color: #51bfe2;
			}	
		.invoicediscription_width{
			float:left;
			width:100%;
			}	
		.invoice_dis_title{
			float:left;
			padding:15px 0px;
			width:33.3%;
			background-color:#51bfe2;
			border-right: 1px solid #dde5f1;
			}	
		.invoice_dis_text{
			color: #ffffff;
			font-family: sans-serif;
			font-size: 14px;
			text-align: center;
			line-height: 27px;
			font-weight: bold;
			}	
		.invoice_pri_title{
			float:left;
			padding:15px 0px;
			width:22%;
			background-color:#51bfe2;
			border-right: 1px solid #dde5f1;
			}	
		.invoice_pri_text{
			color: #ffffff;
			font-family: sans-serif;
			font-size: 14px;
			text-align: center;
			line-height: 27px;
			font-weight: bold;
			}	
		.invoice_qua_title{
			float:left;
			padding:15px 0px;
			width:22%;
			background-color:#51bfe2;
			border-right: 1px solid #dde5f1;
			}	
		.invoice_qua_text{
			color: #ffffff;
			font-family: sans-serif;
			font-size: 14px;
			text-align: center;
			line-height: 27px;
			font-weight: bold;
			}	
		.invoice_tot_title{
			float:left;
			padding:15px 0px;
			width:22%;
			background-color:#51bfe2;
			}	
		.invoice_total_text{
			color: #ffffff;
			font-family: sans-serif;
			font-size: 14px;
			text-align: center;
			line-height: 27px;
			font-weight: bold;
			}	
		.discriptionwrapp{
			float: left;
			width:100%;
			background-color:#eff3f7;
			border-bottom: 1px solid #dde5f1;
			}	
		.discription_text{
			float:left;
			width:100%;
			}	
		.discription_dis_title{
			float:left;
			padding:15px 0px;
			width:33.3%;
			background-color:#eff3f7;
			border-right: 1px solid #dde5f1;
			}	
		.discription_dis_text{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			text-align: left;
			line-height: 19px;
			font-weight: lighter;
			padding: 0px 25px;	
			}		
		.invoice_pri_dis_title{
			float:left;
			padding:22px 0px;
			width:22%;
			background-color:#eff3f7;
			border-right: 1px solid #dde5f1;
			}	
		.invoice_pri_dis_text{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}	
		.invoice_tot_dis_title{
			float:left;
			padding:22px 0px;
			width:22%;
			background-color:#eff3f7;
			}	
		.invoice_total_dis_text{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}		
		.taxwrapp{
			float: left;
			width:100%;
			background-color:#eff3f7;
			border-bottom: 1px solid #dde5f1;
			}	
		.tax_wrapp{
			float:left;
			width:100%;
			}		
		.tax_company_img{
			float: left;
			padding: 58px 0px;
			width: 55.5%;
			background-color: #eff3f7;
			border-right: 1px solid #dde5f1;
			}	
		.tax_company_name{
			float:left;
			width:100%;
			color: #425065;
			font-family: sans-serif;
			font-size: 14px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}	
		.tax_company_section{
			float:left;
			width:	44.3%;
			background-color: #eff3f7;
			}
		.sub_total{
			float:left;
			padding:15px 0px;
			width:49.5%;
			background-color:#eff3f7;
			border-right: 1px solid #dde5f1;
			border-bottom: 1px solid #dde5f1;
			}
		.sub_total_text{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}
		.sub_total{
			float:left;
			padding:15px 0px;
			width:49.5%;
			background-color:#eff3f7;
			border-right: 1px solid #dde5f1;
			border-bottom: 1px solid #dde5f1;
			}
		.sub_total_text{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}	
		.sub_total_amount{
			float:left;
			padding:15px 0px;
			width:50%;
			background-color:#eff3f7;
			border-bottom: 1px solid #dde5f1;
			}	
		.tax_total_section{
			float:left;
			width:100%;
			background-color: #51bfe2;
			}	
		.tax_total{
			float:left;
			padding:15px 0px;
			width:49.5%;
			background-color:#51bfe2;
			border-bottom: 1px solid #dde5f1;
			}	
		.tax_total_text{
			float:left;
			padding:15px 0px;
			width:49.5%;
			background-color:#51bfe2;
			border-bottom: 1px solid #dde5f1;
			border-right: 1px solid #dde5f1;
			}	
		.tax_total_amt{
			color: #ffffff;
			font-family: sans-serif;
			font-size: 14px;
			text-align: center;
			line-height: 27px;
			font-weight: bold;
			}	
		.paymentmethodwrapp{
			float:left;
			width:100%;
			background-color:#fdfdfd;
			}	
		.payment_method{
			float:left;
			width:100%;
			text-align:center;
			}	
		.should_be_made{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			padding: 10px 36px;
			}	
		.payment_method_type{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}	
		.company_add_link{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			padding-top: 5px;
			}	
		.company_add_link_1{
			color: #425065;
			font-family: sans-serif;
			font-size: 12px;
			font-weight: lighter;
			text-align: center;
			line-height: 23px;
			}	
		.thank_us{
			padding: 7px 36px;
			color: #51bfe2;
			font-family: sans-serif;
			font-size: 14px;
			text-align: center;
			font-weight: bold;
			line-height: 190%;
			}	
	
			
		</style>
	</head>
	<body>
		<div class="wrapper">
			<div class="body_bg_color">
				<div class="main_center_div">
					<div class="invoice_header">
						<div class="left_header">
							<a href="index.html">
								<img src="<?php base_url();?>assets/images/pdfImages/logo.png" alt="logo" />
							</a>
						</div>
						<div class="right_header">
							<div class="invoice_title">
								INVOICE
							</div>
						</div>
					</div>
					<!-- DIscription Section -->
					<div class="invoice_discription">
						<div class="invoice_discription_info">
							Dear <?php echo $user_name;?>,<br>
							Your payment for your online order placed on <a href="#." class="invoice_discription_info_link">www.tulli.ca</a> on <?php echo $bookingDT;?> has 
							been approved (order reference number: <?php echo $order_id;?>). Please note that "tulli.ca" will appear on 
							your card statement, instead of Tulli. To get further payment support for your purchase, 
							please sign-up using your email address to Tulli at <a href="#." class="invoice_discription_info_link">https://tulli.ca</a>
						</div>
					</div>
					<!-- Invoice Info -->
					<div class="invoice_info_wrapper">
						<div class="invoice_info_left">
							<div class="invoice_email_icon">
								<img src="<?php base_url();?>assets/images/pdfImages/email.png" alt="email" />
							</div>
							<div class="invoice_send_to">
								Invoice Send To
							</div>
							<div class="invoice_send_name">
								<?php echo $user_name;?>
							</div>	
							<div class="invoice_po_add">
								<?php echo $user_address;?><br>
								<a href="#." class="invoice_discription_info_link"><?php echo $user_email;?></a>
							</div>
						</div>
						<div class="invoice_info_right">
							<div class="invoice_home_icon">
								<img src="<?php base_url();?>assets/images/pdfImages/home.png" alt="home" />
							</div>
							<div class="invoice_send_to">
								Invoice Sent From
							</div>
							<div class="invoice_send_name">
								Tulii Inc.
							</div>	
							<div class="invoice_po_add">
								Unit #4 - 10309 117 Street NW Edmonton,
								Alberta T5K 1X9 Canada  <br>
								<a href="#" class="invoice_discription_info_link">info@tulii.ca</a>
							</div>
						</div>
					</div>
					<!-- Invoice Info -->
					<div class="invoice_information_wrapp">
						<div class="invoice_sub_info">
							<div class="invoice_no">
								<div class="invoice_no_img">
									<img src="<?php base_url();?>assets/images/pdfImages/barcode.png" alt="Barcode" />
								</div>
								<div class="invoice_no_title">
									Invoice No
								</div>
								<div class="invoice_no_id">
									#<?php echo $invoice_no; ?>
								</div>
							</div>
							<div class="invoice_date">
								<div class="invoice_date_img">
									<img src="<?php base_url();?>assets/images/pdfImages/cal.png" alt="calender" />
								</div>
								<div class="invoice_date_title">
									Invoice Date
								</div>
								<div class="invoice_date_id">
									<?php echo $updated_at;?>
								</div>
							</div>
							<div class="invoice_total">
								<div class="invoice_total_img">
									<img src="<?php base_url();?>assets/images/pdfImages/dollar.png" alt="dollar" />
								</div>
								<div class="invoice_total_title">
									Invoice Total
								</div>
								<div class="invoice_total_id">
									$<?php echo $amount;?>
								</div>
							</div>
						</div>
					</div>
					<!-- invoice discription header -->
					<div class="invoicediscriptionwrapp">
						<div class="invoicediscription_width">
							<div class="invoice_dis_title">
								<div class="invoice_dis_text">
									Description
								</div>
							</div>
							<div class="invoice_pri_title">
								<div class="invoice_pri_text">
									Price
								</div>
							</div>
							<div class="invoice_qua_title">
								<div class="invoice_qua_text">
									Quantity
								</div>
							</div>
							<div class="invoice_tot_title">
								<div class="invoice_total_text">
									Total 
								</div>
							</div>
						</div>
					</div>
					<!-- invoice discription -->
					<div class="discriptionwrapp">
						<div class="discription_text">
							<div class="discription_dis_title">
								<div class="discription_dis_text">
									<?php echo $description;?>
								</div>
							</div>
							<div class="invoice_pri_dis_title">
								<div class="invoice_pri_dis_text">
									$<?php echo $amount; ?>
								</div>
							</div>
							<div class="invoice_pri_dis_title">
								<div class="invoice_pri_dis_text">
									<?php echo $quantity;?>
								</div>
							</div>
							<div class="invoice_tot_dis_title">
								<div class="invoice_total_dis_text">
									$<?php echo $amount; ?> 
								</div>
							</div>
						</div>
					</div>
					<!-- invoice discription & tax -->
					<div class="taxwrapp">
						<div class="tax_wrapp">
							<div class="tax_company_img">
								<img src="<?php base_url();?>assets/images/pdfImages/signature.png" alt="signature" />
								<div class="tax_company_name">
									Tulii.ca
								</div>
							</div>
							<div class="tax_company_section">
								<div class="sub_total">
									<div class="sub_total_text">
										Sub Total
									</div>
								</div>
								<div class="sub_total_amount">
									<div class="sub_total_text">
										$<?php echo $amount;?>
									</div>
								</div>
								<div class="sub_total">
									<div class="sub_total_text">
										Vat %20
									</div>
								</div>
								<div class="sub_total_amount">
									<div class="sub_total_text">
										$<?php echo ($amount * 20)/100; ?>
									</div>
								</div>
								<div class="sub_total">
									<div class="sub_total_text">
										Shipping
									</div>
								</div>
								<div class="sub_total_amount">
									<div class="sub_total_text">
										$<?php echo ($amount * 5)/100; ?> 
									</div>
								</div>
								<!-- -->
								<div class="tax_total_section">
									<div class="tax_total_text">
										<div class="tax_total_amt">
											Total 	
										</div>
									</div>
									<div class="tax_total">
										<div class="tax_total_amt">
											$<?php echo $amount + (($amount * 20)/100) + (($amount * 5)/100) ; ?>  	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Payment Made Information -->
					<div class="paymentmethodwrapp">
						<div class="payment_method">
							<div class="should_be_made">
								Payments should be made within 30 days with one of the options below, or you can enter 
								any note here if necessary, you have much space:
							</div>
							<div class="payment_method_type">
								<b>Payment Methods:</b> Cheque, PayPal, WesternUnion, BACS
							</div>
							<div class="payment_method_type">
								<b>We accept:</b> MasterCard, Visa, AmericanExpress
							</div>
							<div class="payment_method_type">
								<b>PayPal Email Address:</b> <a href="#." class="invoice_discription_info_link">info@tulii.ca</a>
							</div>
							<div class="company_add_link">
								<a href="#." class="invoice_discription_info_link">www.tulii.ca</a>
							</div>
							<div class="company_add_link_1">
								<a href="#." class="invoice_discription_info_link">info@tulii.ca</a>
							</div>
							
							<div class="thank_us">
								THANK YOU VERY MUCH FOR CHOOSING OUR PRODUCT
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>