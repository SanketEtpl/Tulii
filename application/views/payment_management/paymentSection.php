<?php //$this->load->view('includes/header'); ?>
<style type="text/css">
.faqDesc {
  width:15px; 
}
</style>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-credit-card-alt"></i> Payment management/Payment section
      <small>View/Pay now/On hold</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id. no</th>
                  <th>Driver name</th>
                  <th>Source location</th>
                  <th>Destination location</th>
                  <th>Date</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Rating</th>
                  <th style="width:190px;">Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Driver name</th>
                  <th>Source location</th>
                  <th>Destination location</th>
                  <th>Date</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Rating</th>                 
                  <th style="width:190px;">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
  </div>

  <div class="modal fade" id="viewPaymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title" id="exampleModalLabel"><b>View payment section</b></h3>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Driver name :</label>
              <div class="rid_info" id="viewDriverName"></div>
            </div>    
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Source location :</label>
              <div class="rid_info" id="viewSourceLoc"></div>
            </div>                    
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Destination location :</label>
              <div class="rid_info" id="viewDestinationLoc"></div>
            </div> 
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Trip Date :</label>
              <div class="rid_info" id="viewDate"></div>
            </div>                       
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Pick-Up Time :</label>
              <div class="rid_info" id="viewTripStartTime"></div>
            </div>  
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Drop-OFF Time :</label>
              <div class="rid_info" id="viewTripEndTime"></div>
            </div>           
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Price :</label>
              <div class="rid_info" id="viewPrice"></div>
            </div>  
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Status :</label>
              <div class="rid_info" id="viewStatus"></div>
            </div>           
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
              <label class="test-alignment">Rating :</label>
              <div class="rid_info" id="viewRating"></div>
            </div>                         
          </div>                 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
  //datatables
  paymentSectionList();
  $("input[type=search]").val("");
  $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

  $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();  
  paymentSectionList();
}
</script>
<script src="<?php echo base_url(); ?>assets/js/payment/paymentSection.js"></script>
