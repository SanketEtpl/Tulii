<?php //$this->load->view('includes/header');?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Service User Management
      <small>View</small>      
    </h1>
  </section>
  <section class="content">   
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Mobile no</th>
                    <th>Types</th>
                    <th style='width: 110px;'>Status</th>
                    <th>Action</th>                                           
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile no</th>
                  <th>Types</th>
                  <th style='width: 110px;'>Status</th>
                  <th>Action</th>                 
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>   

    <div class="modal fade" id="parentServiceUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h3 class="modal-title" id="exampleModalLabel"><b>Parent details</b></h3>
                  
              </div>
              <div class="modal-body">
                   <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Full name :</label>
                          <div class="rid_info" id="parent_name"></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Email:</label>
                          <div class="rid_info" id="parent_email"></div>
                      </div>
                  </div>   
                  <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Mobile no :</label>
                          <div class="rid_info" id="user_phone_number"></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Gender:</label>
                          <div class="rid_info" id="user_gender"></div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Address :</label>
                          <div class="rid_info" id="user_address"></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Date of birth :</label>
                          <div class="rid_info" id="user_birth_date"></div>
                      </div>
                  </div>  
                  <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>No. of childs :</label>
                          <div class="rid_info" id="user_no_of_kids"></div>
                      </div>                       
                  </div>  
                  <div class="">
                    <h4 class="" id="exampleModalLabel"><b>Child details</b></h4>                    
                  </div>
                  
                   <div class="box-body table-responsive no-padding">
                      <table class="table table-hover" id="kidsList">
                         <thead>
                          <tr>
                            <th>Sr. no</th> 
                            <th>Username</th>
                            <th>Child name</th>
                            <th>Gender</th>
                            <th>Age</th>                              
                          </tr>
                         </thead>
                        <tbody>                        
                        </tbody>             
                      </table>            
                    </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div> 

  <div class="modal fade" id="orgServiceUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="exampleModalLabel"><b>Organization details</b></h3>
                  
              </div>
              <div class="modal-body">
                   <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Organization name :</label>
                          <div class="rid_info" id="organizationName"></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Email:</label>
                          <div class="rid_info" id="orgEmail"></div>
                      </div>
                  </div>   
                  <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Address :</label>
                          <div class="rid_info" id="orgAddress"></div>
                      </div>                      
                  </div>                  
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div> 

    <div class="modal fade" id="schoolServiceUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="exampleModalLabel"><b>School details</b></h3>
                  
              </div>
              <div class="modal-body">
                   <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>School name :</label>
                          <div class="rid_info" id="schoolName"></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Email:</label>
                          <div class="rid_info" id="schoolEmail"></div>
                      </div>
                  </div>   
                  <div class="row">
                      <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                          <label>Address :</label>
                          <div class="rid_info" id="schoolAddress"></div>
                      </div>                      
                  </div>                  
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div> 
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 

$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"});
    //datatables
    serviceUserList();
});


</script>
<script src="<?php echo base_url(); ?>assets/js/user/serviceUser.js"></script>
