<?php //$this->load->view('includes/header');?>

<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/tab.css" />
<link rel = "stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/imageviewer.css" />
<script src="<?php echo base_url(); ?>assets/js/user/serviceProvider.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url(); ?>aassets/js/user/serviceProvider.js" charset="utf-8"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/imageviewer.js" charset="utf-8"></script>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Service Provider Management
      <small>View</small>      
    </h1>
  </section>
  <section class="content">   
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Mobile no</th>
                    <th>Types</th>
                    <th style='width: 110px;'>Status</th>
                    <th>Action</th>                                           
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile no</th>
                  <th>Types</th>
                  <th style='width: 110px;'>Status</th>
                  <th>Action</th>                 
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>   
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 

$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"});
    //datatables
    serviceProviderList();
});

</script>

