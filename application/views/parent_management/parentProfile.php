<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-tasks"></i> Parent Management/parent info
      <small>Add/View/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add new"><i class="fa fa-plus"></i> Add new</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Mobile no</th>
                    <th>Gender</th>
                    <th style="width:15%;">Status</th>
                    <th style="width:17%;">Action</th>                 
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Full name</th>
                  <th>Email</th>
                  <th>Mobile no</th>
                  <th>Gender</th>
                  <th style="width:15%;">Status</th>
                  <th style="width:17%;">Action</th>              
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>

    <div id="parentProfileModal" class="modal fade" data-backdrop="static" style="display: none;" aria-hidden="false">
      <form action="" enctype="multipart/form-data" id="parentProfileForm" name="parentProfileForm" novalidate>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
              <h3 class="modal-title"><b>Add parent</b></h3>
            </div>
            <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group" id="parentName">
                    <label class="control-label" for="lbl_name">Full name<span class="required_star">*</span></label>
                    <input type="hidden" id="user_key" name="user_key">
                    <input type="hidden" id="parent_id" name="parent_id">
                    <input type="hidden" id="count_id" name="count_id" value="1">
                    <input type="text" placeholder="Full name" id="name" required="true" name="name" class="form-control name" maxlength="50" autocomplete="off">
                  </div>
                </div>              
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="lbl_gender">Gender<span class="required_star">*</span></label><br>
                      <input type="radio" name="gender" class="parentGender" id="genderMale" value="male" checked> Male
                      <input type="radio" name="gender" class="parentGender" id="genderFemale" value="female"> Female                  
                  </div>
                </div>
            </div>    
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_email">Email<span class="required_star">*</span></label> 
                  <input type="text" placeholder="Email" id="email" onblur="checkEmail()" maxlength="50" required="true" name="email" class="form-control email" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group" id="parentContact">
                  <label class="control-label" for="phone">Mobile no<span class="required_star">*</span></label>
                  <input type="text" placeholder="Mobile no" required="true" maxlength="10" id="phone" name="phone" class="form-control phone">
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="phone">No. of childs<span class="required_star">*</span></label>
                  <input type="text" placeholder="No. of childs" required="true" maxlength="2" id="noOfKis" name="noOfKis" value="1" disabled="true" class="form-control age">
                </div>
              </div> 
               <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group" id="perentAddress">
                  <label class="control-label" for="lbl_address">Address<span class="required_star">*</span></label>
                  <input type="text" placeholder="Address" id="address" name="address" class="form-control address" maxlength="100" required="true">
                </div>
              </div>                                                                                                              
            </div> 
            <div class="">
              <h4 class=""><b>Add child</b></h4>              
            </div>     

            <div class="table-responsive">             
              <table class="table table-bordered" id="dynamic_field"> 
                <thead>
                <tr>
                  <th>Name</th> 
                  <th>Gender</th>
                  <th>Date of birth</th>
                  <th>Age</th>     
                  <th><button type="button" name="add" id="add" onClick="addrow()" class="btn btn-success">Add More</button></th>                        
                </tr>
                </thead>
                <tbody>
                
                </tbody>
              </table>                 
            </div> 
            <div class="map" id="map" style="width: 100%; height: 300px;display:none"></div>
            </div>
            <div class="modal-footer">
              <input type="hidden" name="lat" id="lat">
              <input type="hidden" name="lng" id="lng">
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info" type="button" id="btnParentProfileSave">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>

   <div class="modal fade" id="parentUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="exampleModalLabel"><b>Parent details</b></h3>
                    
                </div>
                <div class="modal-body">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>Full name :</label>
                            <div class="rid_info" id="parent_name"></div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>Email:</label>
                            <div class="rid_info" id="parent_email"></div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>Mobile no :</label>
                            <div class="rid_info" id="user_phone_number"></div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>Gender:</label>
                            <div class="rid_info" id="user_gender"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>Address :</label>
                            <div class="rid_info" id="user_address"></div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>Date of birth :</label>
                            <div class="rid_info" id="user_birth_date"></div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                            <label>No. of childs :</label>
                            <div class="rid_info" id="user_no_of_kids"></div>
                        </div>                       
                    </div>  
                    <div class="">
                      <h4 class="" id="exampleModalLabel"><b>Child details</b></h4>                    
                    </div>
                    
                     <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="kidsList">
                           <thead>
                            <tr>
                              <th>Sr. no</th> 
                              <th>Child name</th>
                              <th>Gender</th>
                              <th>Age</th>                              
                            </tr>
                           </thead>
                          <tbody>                        
                          </tbody>             
                        </table>            
                      </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<!--     <div class="modal fade" id="parentUserModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Parent Details<b></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                     <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label>Parent name :</label>
                            <div class="rid_info" id="parent_name"></div>
                        </div>
                        <div class="col-md-6 rid_tim">
                            <label>Email:</label>
                            <div class="rid_info" id="parent_email"></div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label>Phone number :</label>
                            <div class="rid_info" id="user_phone_number"></div>
                        </div>
                        <div class="col-md-6 rid_tim">
                            <label>Gender:</label>
                            <div class="rid_info" id="user_gender"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label>Address :</label>
                            <div class="rid_info" id="user_address"></div>
                        </div>
                        <div class="col-md-6 rid_tim">
                            <label>Date of birth :</label>
                            <div class="rid_info" id="user_birth_date"></div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label>Parent no of kids :</label>
                            <div class="rid_info" id="user_no_of_kids"></div>
                        </div>                       
                    </div>  
                    <h3>Kids Details</h3>
                     <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="kidsList">
                           <thead>
                            <tr>
                              <th>Sr. no</th> 
                              <th>Kid name</th>
                              <th>Gender</th>
                              <th>Age</th>                              
                            </tr>
                           </thead>
                          <tbody>                        
                          </tbody>             
                        </table>            
                      </div>                  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> -->
  </section>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
  //datatables
  parentList();
 $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});

function clearSearch() 
{ 
  $("input[type=search]").val("");
  $("#clear").remove();
  parentList();
  // location.reload();
}
</script>
 <script>
  function initialize() {
     var latlng = new google.maps.LatLng(28.5355161,77.39102649999995);
      var map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 13
      });
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
     });
      var input = document.getElementById('address');
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      var geocoder = new google.maps.Geocoder();
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.bindTo('bounds', map);
      var infowindow = new google.maps.InfoWindow();   
      autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
              window.alert("Autocomplete's returned place contains no geometry.");
              return;
          }
    
          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
          } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);
          }
         
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);          
      
          bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng());
          infowindow.setContent(place.formatted_address);
          infowindow.open(map, marker);
         
      });
      // this function will work on marker move event into map 
      google.maps.event.addListener(marker, 'dragend', function() {
          geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {        
                bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
            }
          }
          });
      });
  }
  function bindDataToForm(address,lat,lng){     
     document.getElementById('lat').value = lat;
     document.getElementById('lng').value = lng;
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  </script>
<script src="<?php echo base_url(); ?>assets/js/parentJS/parentProfileNew.js"></script>
