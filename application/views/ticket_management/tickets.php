<?php //$this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-question"></i> Ticket management
      <small>View</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity: 0">
              <thead>
                <tr>
                  <th>Id. no</th>
                  <th>Ticket subject</th>
                  <th>Ticket service</th>
                  <th>Ticket comment</th>
                  <th style="width:18%;">Ticket Status</th>             
                  <th style="width:12%;">Action</th>                        
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Ticket subject</th>
                  <th>Ticket service</th>
                  <th>Ticket comment</th>
                  <th style="width:18%;">Ticket Status</th>             
                  <th style="width:12%;">Action</th>      
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
  </section>
</div>
<?php //$this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
  $(document).ready(function() { 
    $("#LoadingDiv").css({"display":"block"});
    //datatables
    loadTicketsList();     
  });
</script>
<script src="<?php echo base_url(); ?>assets/js/ticket_management/tickets.js"></script>