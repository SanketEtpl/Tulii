<?php // $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-list"></i> Tutor list
      <small>Add/View</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addTutorCatgoryPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add tutor catgory"><i class="fa fa-plus"></i> Add tutor catgory</a>
          <a class="btn btn-primary" id="addTutorSubCatgoryPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add tutor sub-catgory"><i class="fa fa-plus"></i> Add tutor sub-catgory</a>
          <!-- <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add tutor"><i class="fa fa-plus"></i> Add tutor</a> -->
        </div>
       </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Tutor name</th> 
                    <th>Category name</th>                                       
                    <th>Sub-category name</th>
                    <th style="width:110px !important">Status</th>                                       
                    <th style="width:80px !important">Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Tutor name</th> 
                  <th>Category name</th>                                       
                  <th>Sub-category name</th>
                  <th style="width:110px !important">Status</th>                                       
                  <th style="width:70px !important">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    
    <div id="tutorcategoryModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="tutorcatForm" name="tutorcatForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add tutor category</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group ui-widget">
                <label class="control-label" for="tutorname">Tutor category name<span class="required_star">*</span></label><br>
                <input type = "text" class="form-control name" maxlength="50" id = "tutor_category_name" />             
              </div>
            </div>   
          </div>                 
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" onclick="btnAddTutorCat()">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>

    <div id="addtutorsubcatModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="tutorSubCatForm" name="tutorSubCatForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add tutor sub-category</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="control-label" for="lbl_tc">Tutor category<span class="required_star">*</span></label>
                <select class="form-control" id="tutorCategory" name="tutorCategory">
                <option value="" disable="" selected="" hidden=""> Select tutor category</option>
                <?php                   
                  if(!empty($tutorCat)){                         
                    foreach ($tutorCat as $key => $value) {                                                                    
                ?>
                  <option value="<?php echo $value['id']; ?>"><?php echo ucfirst($value['category_name']); ?></option>                    
                  <?php  } } else { ?>
                  <option value=""></option>
                  <?php } ?>
                </select>
              </div>
            </div> 
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group ui-widget">
                <label class="control-label" for="tutorsubname">Tutor sub-category name<span class="required_star">*</span></label><br>
                <input type = "text" class="form-control name" maxlength="50" id = "tutor_sub_category_name" />             
              </div>
            </div>   
          </div>                 
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" onclick="btnAddTutorSubCat()">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div>
    <div class="modal fade" id="tutorListModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
              <h3 class="modal-title"></h3><h3 class="modal-title"><b>Tutor details</b></h3>
            </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label> Tutor name :</label>
                <div class="rid_info text-capitalize" id="tutorName"></div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Tutor category:</label>
                <div class="rid_info text-capitalize" id="viewTutorCategory"></div>
              </div>
            </div>   
            <br />
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12 rid_tim" id="tutorTablePrice">
              </div>
            </div>
           <!--  <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Mobile :</label>
                <div class="rid_info" id="mobile"></div>
              </div> 
              <div class="col-md-6 col-sm-6 col-xs-12 rid_tim">
                <label>Status :</label>
                <div class="rid_info" id="isReading"></div>
              </div>             
            </div>    -->                                                    
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
<!--     <div id="addTutorModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="tutorForm" name="tutorForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title changeHeader"><b>Add tutor</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
             <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="control-label" for="lbl_ttc">Tutor name<span class="required_star">*</span></label>
                <select class="form-control" id="tutorName" name="tutorName">
                <option value="" disable="" selected="" hidden=""> Select tutor name</option>
                <?php                   
                  if(!empty($tutors)){                         
                    foreach ($tutors as $key => $value) {                                                                    
                ?>
                  <option value="<?php echo $value['user_id']; ?>"><?php echo ucfirst($value['user_name']); ?></option>                    
                  <?php  } } else { ?>
                  <option value=""></option>
                  <?php } ?>
                </select>
              </div>
            </div> 
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
                <label class="control-label" for="lbl_atc">Tutor category<span class="required_star">*</span></label>
                <select class="form-control" id="category" name="category">
                <option value="" disable="" selected="" hidden=""> Select tutor category</option>
                <?php                   
                  if(!empty($tutorCat)){                         
                    foreach ($tutorCat as $key => $value) {                                                                    
                ?>
                  <option value="<?php echo $value['id']; ?>"><?php echo ucfirst($value['category_name']); ?></option>                    
                  <?php  } } else { ?>
                  <option value=""></option>
                  <?php } ?>
                </select>
              </div>
            </div> 
            </div>
            <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group mulselect">
                  <label class="control-label" for="lbl_type">Tutor sub-category name<span class="required_star">*</span></label>
                 <br>
                 <select class="form-control" size="4" multiple="multiple" id="sub_category" name="sub_category">    
                  </select>
                  </div>
            </div>          
          </div>                 
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" onclick="btnAddTutor()">Save</button>
          </div>
        </div>
      </div>
      </form>
    </div> -->
  </section>
<script type="text/javascript"> 
// var table; 
$(document).ready(function() { 
  
$("#LoadingDiv").css({"display":"block"}); 
    //datatables
   tutorList();
    $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});  

function tutorList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
  $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"tutor_list/Tutor_list/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,3,4,5], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
}

  function clearSearch() 
  { 
    $("input[type=search]").val("");
    $("#clear").remove();
    tutorList();
    // location.reload();
  }

/*  $( function() {
     $('#sub_category').multiselect({
                includeSelectAllOption: true
   });
  });*/
  </script>
<script src="<?php echo base_url(); ?>assets/js/tutor_list/tutor_details.js"></script>
<?php $this->load->view('includes/footer'); ?>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap-multiselect.js"></script>
<link href="<?php echo base_url(); ?>assets/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" /> -->
