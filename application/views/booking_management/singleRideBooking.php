<!--*
Author : Kiran n 
Page :  singleRideBooking.php
Description : Single ride booking List  use for track booking details in admin
*-->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/singleRide.js" charset="utf-8"></script>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    <i class="fa fa-check-square"></i> Booking Management/Single ride
    <?php 
    if($this->session->userdata('role') == ROLE_PARENTS) {
      echo "<small>View</small>"; 
    } else {
      echo "<small>View/Delete</small>"; 
    }
    ?>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">                
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">                
          <input type="hidden" name="bookingManagement" id="bookingManagement" value="Rides"> 
          <div class="box-body table-responsive no-padding">
            <table id="bookingList-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
              <thead>
                <tr>
                  <th>Sr. no</th>
                  <th>Parent Name</th>
                  <th>Kid's Name</th>
                  <th>Booking Date</th>
                  <th>Trip Date</th>
                  <th>Trip Status</th>
                  <th>Duration</th>
                  <th>Total Price</th>
                  <th>Payment Status</th>
                  <th style="max-width:80px !important">Action</th>
                </tr>
              </thead>
              <tfoot> 
                <tr>
                  <th>Sr. no</th>
                  <th>Parent Name</th>
                  <th>Kid's Name</th>
                  <th>Booking Date</th>
                  <th>Trip Date</th>
                  <th>Trip Status</th>
                  <th>Duration</th>
                  <th>Total Price</th>
                  <th>Payment Status</th>
                  <th style="max-width:80px !important">Action</th>
                </tr>
              </tfoot>
            </table>       
          </div><!-- /.box-body -->          
        </div><!-- /.box -->
      </div>
    </div>
  </section>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#LoadingDiv').show();
    loadSingleRideBookingList();    
  }); 
</script>   
    




