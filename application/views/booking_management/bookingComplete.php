<!--*
Author : Kiran n 
Page :  bookingComplete.php
Description : Booking completed List use for track booking details in admin
*-->

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    <i class="fa fa-check-square"></i> Booking Management/Booking Completed(Ride, Care & Tutor)
      <small>View</small> 
    </h1>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">                
      </div>
    </div>
    <ul class="nav nav-tabs nav-justified">
      <li class="active"><a data-toggle="tab" href="#ride">Ride Completed</a></li>
      <li><a data-toggle="tab" onclick="loadBookingCompletedCare()" href="#care">Care Completed</a></li>
      <li><a data-toggle="tab" onclick="loadBookingCompletedTutor()" href="#tutor">Tutor Completed</a></li>
    </ul>
    <div class="tab-content">
      <div id="ride" class="tab-pane fade in active">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">                
              <div class="box-body table-responsive no-padding">
                <table id="bookingCompletedRideList"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%" style="opacity:0">
                  <thead>
                    <tr>
                      <th>Sr.No.</th>
                      <th>Customer Name</th>
                      <th>Booking Number</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Total Cost</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                       <th>Sr.No.</th>
                      <th>Customer Name</th>
                      <th>Booking Number</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Total Cost</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>       
              </div><!-- /.box-body -->          
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div id="care" class="tab-pane fade">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">                
              <div class="box-body table-responsive no-padding">
                <table id="bookingCompletedCareList"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%"
                >
                  <thead>
                    <tr>
                      <th>Id.no</th>
                      <th>Care Location</th>
                      <th>Care Start Date</th>
                      <th>Care End Date</th>                      
                      <th>Care Start Time</th>
                      <th>Care End Time</th>
                      <th>Cost</th>                      
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                      <th>Id.no</th>
                      <th>Care Location</th>
                      <th>Care Start Date</th>
                      <th>Care End Date</th>                      
                      <th>Care Start Time</th>
                      <th>Care End Time</th>
                      <th>Cost</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>       
              </div><!-- /.box-body -->          
            </div><!-- /.box -->
          </div>
        </div>
      </div>
      <div id="tutor" class="tab-pane fade">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">                
              <div class="box-body table-responsive no-padding">
                <table id="bookingCompletedTutorList"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%"
                >
                  <thead>
                    <tr>
                      <th>Id.no</th>
                      <th>Tutor Category</th>
                      <th>Tutor Sub-category</th>
                      <th>Tutor Start Date</th>                      
                      <th>Tutor End Time</th>
                      <th>Estimate Cost</th>                      
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot> 
                    <tr>
                      <th>Id.no</th>
                      <th>Tutor Category</th>
                      <th>Tutor Sub-category</th>
                      <th>Tutor Start Date</th>                      
                      <th>Tutor End Time</th>
                      <th>Estimate Cost</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>       
              </div><!-- /.box-body -->          
            </div><!-- /.box -->
          </div>
        </div>
      </div>    
    </div>
  </section>

</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/booking/bookingComplete.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#LoadingDiv').show();
    loadBookingCompletedRideList();    
  }); 
</script>   
    





