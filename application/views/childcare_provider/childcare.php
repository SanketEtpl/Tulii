<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users"></i> Child care management
      <small>Add, Edit, Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)"><i class="fa fa-plus"></i> Add New</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Sr. no</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Sr. no</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Gender</th>
                  <th>Address</th>
                  <th>Status</th>
                  <th>Action</th> 
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    <div id="childcareModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="childcareForm" name="childcareForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Childcare information</h4>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group" id="parentName">
                  <label class="control-label" for="lbl_name">Full Name *</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="Full name" id="fname" required="true" name="fname" class="form-control" maxlength="50" autocomplete="off">
                </div>
              </div>              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lbl_gender">Gender *</label><br>
                    <input type="radio" name="gender" class="childGender" id="genderMale" value="male" checked> Male
                    <input type="radio" name="gender" class="childGender" id="genderFemale" value="female"> Female                  
                </div>
              </div>
          </div>    
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lbl_email">Email id *</label> 
                  <input type="text" placeholder="Email id" id="email" maxlength="50" onBlur="checkEmail()" required="true" name="email" class="form-control email" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" id="parentContact">
                  <label class="control-label" for="phone">Contact number *</label>
                  <input type="text" placeholder="Contact number" required="true" maxlength="20" id="phone" name="phone" class="form-control contact_number">
                </div>
              </div> 
            </div>
            <div class="row">
               <div class="col-md-6">
                <div class="form-group" id="perentAddress">
                  <label class="control-label" for="lbl_address">Address *</label>
                  <textarea placeholder="Address" id="address" name="address" class="form-control" maxlength="100" required="true" rows="4" autocomplete="off"></textarea> 
                </div>
              </div>                                                                                                              
            </div>                
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnChildcare">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
   <div class="modal fade" id="viewChildcareModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Childcare Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                     <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-6">Full name :</label>
                            <div class="rid_info col-md-6" id="child_name"></div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-3">Email:</label>
                            <div class="rid_info col-md-9" id="child_email"></div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-6">Phone number :</label>
                            <div class="rid_info col-md-6" id="user_phone_number"></div>
                        </div>
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-6">Gender:</label>
                            <div class="rid_info col-md-6" id="user_gender"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 rid_tim">
                            <label class="col-md-4">Address :</label>
                            <div class="rid_info col-md-8" id="user_address"></div>
                        </div>                        
                    </div>                      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> 
  </section>
</div>
<?php $this->load->view('includes/footer'); ?>
<script type="text/javascript"> 
var table; 
$(document).ready(function() { 
    //datatables
    table = $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"childcare_provider/Childcare/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              $("#LoadingDiv").css({"display":"block"}); 
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,5,6 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
});
</script>
<script src="<?php echo base_url(); ?>assets/js/childcare/childcare.js"></script>
