<style type="text/css">
#content {
    margin: 10px;
    border: 1px solid #D0D0D0;
    box-shadow: 0 0 8px #D0D0D0; float:left ; position: relative;

}
.goback {
    float: left;
    width: auto;
    position: absolute;
    bottom: 20%;
    right: 20%;
}
.goback a {
    padding: 8px 15px;
    background: #10d8ea;
    text-decoration: none;
    margin: 0 auto 10px 0;
    display: inline; color: #fff;
}
</style>
<div class="content-wrapper">    
    <section class="content-header">
      <h1>
        Access Denied
        <small>No direct script access allowed</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-center">
                <img src="<?php echo base_url() ?>assets/images/access-denied.png" alt="Access Denied Image" />
            </div>
            <div class="col-xs-12 text-center goback">
                <a href="<?php echo base_url(); ?>">Go Back to Home</a>
            </div>
        </div>        
    </section>
</div>