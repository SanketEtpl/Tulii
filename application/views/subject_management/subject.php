<?php // $this->load->view('includes/header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-list"></i> Subject management/subject manage
      <small>Add/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">    

      <div class="col-xs-12 text-right">
        <div class="form-group">
         <a class="btn btn-primary" id="addSubjectPopUp" style="border-radius: 20px;" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add subject"><i class="fa fa-plus"></i> Add subject</a>

          <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Assign subject"><i class="fa fa-plus"></i> Assign subject</a>
        </div>
       </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
            <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                  <tr>
                    <th>Id. no</th>
                    <th>Teacher name</th> 
                    <th>Subject name</th>                                       
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Teacher name</th> 
                  <th>Subject name</th>
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
    
    <div id="subjectModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="subjectForm" name="subjectForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title"><b>Add subject</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        </div>
          <div class="modal-body">
            <div class="row">
                                
   
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div class="form-group ui-widget">
                <label class="control-label" for="teacher_name">Teachers<span class="required_star">*</span></label><br>
                <input type = "text" class="form-control name" maxlength="20" id = "teacher_name" />             
              </div>
            </div>  

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group mulselect">
                  <label class="control-label" for="lbl_type">Subjects<span class="required_star">*</span></label>
                 <br>
                 <select class="form-control" size="4" multiple="multiple" id="subject_name" name="subject_name">    
                  </select>
                  </div>
              </div>
          </div>                 
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnSubject">Save</button>
          </div>
        </div>
      </form>
    </div>               
  </section>
    <script type="text/javascript"> 
// var table; 
$(document).ready(function() { 
$("#LoadingDiv").css({"display":"block"}); 
    //datatables
   subjectList();
    $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
});  

function subjectList()
{
  var table = $('#table').DataTable();
  $('#table').empty();
  table.destroy();
 $('#table').DataTable({  
        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": baseURL+"subject_management/Subject/ajax_list",
            beforeSend: function() {
              var search = $("input[type=search]").val();
              if(search=="")
              
                $("input[type=search]").on("keyup",function(event) {

                if($("#clear").length == 0) {
                   if($(this).val() != ""){
                    $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
                  } 
                }
                if($(this).val() == "")  
                $("#clear").remove();      
              }); 
              $("input[type=search]").keydown(function(event) {
                k = event.which;
                if (k === 32 && !this.value.length)
                    event.preventDefault();
              });
             },
            complete: function(){
            $("#LoadingDiv").css({"display":"none"}); 
            $("#table").css({"opacity":"1"}); 
          },
            "type": "POST"
        },
        
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,2,3], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
    });  
}

  function clearSearch() 
  { 
    $("input[type=search]").val("");
    $("#clear").remove();
    subjectList();
    // location.reload();
  }

  $( function() {
    $( "#teacher_name" ).autocomplete({
      source: function (teacher_name, result) {
        var tag = $('#teacher_name').val();
                $.ajax({
                    url: baseURL+"subject_management/Subject/teacherList",
                    data: 'keyword=' + tag,            
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                    result($.map(data, function (item) {
                    return item.teacher_name;
                    
                        }));
                    }
                });
            }
    });
     $('#subject_name').multiselect({
                includeSelectAllOption: true
   });
  });
  </script>
<script src="<?php echo base_url(); ?>assets/js/subject_management/subject.js"></script>
<?php $this->load->view('includes/footer'); ?>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-multiselect.js"></script>
<link href="<?php echo base_url(); ?>assets/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
