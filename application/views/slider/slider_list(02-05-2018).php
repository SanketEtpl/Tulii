<?php $this->load->view('includes/header'); ?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-image"></i> Slider management/upload slider
      <small>Add/Delete</small>
    </h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12 text-left">
        <div class="form-group">
          <form action="" id="sliderForm" enctype="multipart/form-data" name="sliderForm" novalidate>
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title"><b>Add slider</b> </h3>
              </div>
              <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </div>
              <div class="modal-body">
               <div class="row">           
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lbl_pk">Slider name *</label>
                    <input type="text" placeholder="Slider name" required="true" maxlength="100" id="slider_name" name="slider_name" class="form-control name">
                  </div>
                </div>
              </div>
               <div class="row">           
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="exampleInputFile">Select Slider Images (max 2Mb size)<span class="required-star">*</span><a style="color:red;" href="#" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="1. Select jpg | png | jpeg | gif type image. 2. Image max size should 2 MB. 3. Image Dimension should be greater than 1800x600 or equal.">(Image Guideline)</a></label>
                    <div>
                      <button class="addfiles">Choose file</button>
                      <input type="file" id="slider_img" name="slider_img" style='display: none;' accept="image/x-png, image/gif, image/jpeg , image/jpg">                        
                      <span id="showFilename">No file chosen</span>
                    </div>                                    
                  </div>
                </div>
              </div>                                     
              </div>
              <img src="" id="SliderWHImage" style="display:none" value="sdfsadf"/>
              
              <!-- <div id="imghieght"></div> -->
              <div class="modal-footer text-left">
                <button class="btn btn-info " type="button" id="btnSlider"  onclick="Upload()">Save file</button>
              </div>
            </div>
          </div>
          </form>
          <!-- <a class="btn btn-primary" id="addPopUp" href="javascript:void(0)"><i class="fa fa-plus"></i> Add New</a> -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding slider_table">
            <table id="table" class="table table-hover" cellspacing="0" width="100%">
              <thead>
                  <tr>
                    <th>Id no</th>
                    <th>Slider name</th>
                    <th>Image</th>
                    <th>Status</th>
                    <th>Action</th>                    
                  </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id no</th>
                  <th>Slider name</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>
  </div>
  </section>
</div>
<?php $this->load->view('includes/footer'); ?>

<script type="text/javascript"> 

$(document).ready(function() { 
   
  $('.addfiles').on('click', function() {     
    $('#slider_img').click();return false;
 });
  $('input[type="file"]').change(function(e){
    var fileName = e.target.files[0].name;
    $('#showFilename').html(fileName);    
});

  $('[data-toggle="tooltip"]').tooltip();
    //datatables
  sliderList();  
  $("input[type=search]").val("");
    $("input[type=search]").on("keyup",function() {
    if($("#clear").length == 0) {
       if($(this).val() != ""){
        $("#table_filter label").append('<div id="clear"><button class="btn btn-primary" type="button" id="clearText" onClick="clearSearch()"><i class="fa fa-times-circle"></i></button></div>');    
      } 
    }
    if($(this).val() == "")  
    $("#clear").remove();      
  });

    $("input[type=search]").keydown(function(event) {
    k = event.which;
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

});
  function clearSearch() 
  { 
    $("input[type=search]").val("");
    $("#clear").remove();
    sliderList();
    // location.reload();
  }

</script>
 <script src="<?php echo base_url(); ?>assets/js/slider/slider.js"></script>
