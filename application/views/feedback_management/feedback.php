<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-comments"></i> Feedback management
      <small>Add/Edit/Delete</small>
    </h1>
  </section>
  <section class="content">  
    <div class="row">    
      <div class="col-xs-12 text-right">
        <div class="form-group">
          <a class="btn btn-primary" id="addFeedbackPopUp" style="border-radius: 20px;" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add feedback"><i class="fa fa-plus"></i> Add feedback</a>          
        </div>
       </div>
    </div>  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="table" class="table table-hover" cellspacing="0" width="100%" style="opacity:0">
              <thead>
                <tr>
                  <th>Id. no</th>
                  <th>Feedback topic</th>
                  <th>User type</th>
                  <th style="width: 100px !important">Action</th>                    
                </tr>
              </thead>
              <tbody>
              </tbody>

              <tfoot>
                <tr>
                  <th>Id. no</th>
                  <th>Feedback topic</th>
                  <th>User type</th>
                  <th style="width: 100px !important">Action</th>                    
                </tr>
              </tfoot>
            </table>         
          </div><!-- /.box-body -->       
          <div class="box-footer clearfix">
            <?php //echo $this->pagination->create_links(); ?>
          </div>
        </div><!-- /.box -->
      </div>
    </div>    
    <div id="feedbackModal" data-backdrop="static" class="modal fade" style="display: none;" aria-hidden="false">
    <form action="" id="feedbackForm" name="feedbackForm" novalidate>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h3 class="modal-title headerTitle"><b>Add feedback topic</b> </h3>
          </div>
          <div class="alert alert-danger alert-dismissable" style="display:none" id="errorPopUp">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_role">Role type<span class="required_star">*</span></label>
                  <select class="form-control" id="roleType" name="roleType">
                  <option value="" disable="" selected="" hidden=""> Select role</option>
                  <?php 
                    if(!empty($roleData)) {                         
                      foreach ($roleData as $key => $value) {                             
                  ?>
                    <option value="<?php echo $value['roleId']; ?>"><?php echo $value['role']; ?></option>                    
                    <?php  } } else { ?>
                    <option value=""></option>
                    <?php } ?>
                  </select>
                </div>
              </div> 
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                  <label class="control-label" for="lbl_fb">Feedback topic<span class="required_star">*</span></label>
                  <textarea name="feedback" id="feedback" maxlength="255" autocomplete="off" class="form-control description_valid"></textarea>                  
                </div>                
              </div>
            </div>                 
          </div>
          <div class="modal-footer">
            <input type="hidden" name="user_key" id="user_key" value="">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="button" id="btnFeedback">Save</button>
          </div>
        </div>
      </form>
    </div>    
  </section>
</div>
<script type="text/javascript"> 
//var table; 
$(document).ready(function() { 
  $("#LoadingDiv").css({"display":"block"}); 
    //datatables
    feedbackList();
});

</script>
<script src="<?php echo base_url(); ?>assets/js/feedback/feedback.js"></script>

