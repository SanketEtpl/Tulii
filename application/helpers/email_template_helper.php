<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


function email_header()
{
	$CI = & get_instance();
	$html_header ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Mail Templates</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="generator" content="Geany 0.21" />
</head>

<body style="padding:0px; margin:0px;background-color:#fff;">
<div class="mail_templet" style="background-color:none; padding:5px 0px; margin:0% 0 0 0; float:left; width:100%;">
<center>
<table style="width:650px; border:2px solid #cccccc;" cellpadding="0" cellspacing="0" class="mail_temp_table">

<tr style="text-align:left;">
<td height="57" style="background-color:#fff;padding:10px 20px; text-align:left;">

<img src="'.$CI->config->item("hostname").'assets/images/logo.png" width="150" style="text-align:left; float:left; display:block; margin:0px;">

</td>
</tr>

<tr> 
<td style="background-color:#ffffff;padding:0px 20px;">
<hr style="border: 0;color: #ededeb;background-color:#ededeb;height:1px;width:100%;text-align: left; padding:0px; margin:6px 0 0 0px;">
<div>';
return $html_header;
}

function email_footer()
{
	$CI = & get_instance();
	$year = date('Y');
	$html_footer = '
	<hr style="border: 0;color: #ededeb;background-color:#ededeb;height:1px;width:100%;text-align: left;margin:10px 0 0;">
	</td>
	</tr>				
	<tr><td height="40" style="background:#fff; font-size:16px; text-align: left; color: #000000;padding-left:20px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">Warm Regards,</td></tr>
	
	<tr><td height="25" style="background:#fff; font-size:16px; text-align: left; color: #000000;padding-left:20px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;">Tulii</td></tr>
	<tr>				
	<td height="40" style="background:#fff;color: #000000;padding-left:20px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:16px;">This is a system generated e-mail, please do not reply to this message.</td>				
	</tr>
	<tr>				
	<td height="40" style="text-align:left;background:#fff;color: #484848;padding-left:20px;font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:13px;">Copyright '.$year.' &copy;Tulii, All Rights Reserved.</td>				
	</tr>
	</table>
	</center>
	</div></body>
</html>';
return $html_footer;
}
?>
