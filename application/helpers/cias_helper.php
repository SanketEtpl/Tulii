<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * This function is used to print the content of any data
 */
function pre($data)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

/**
 * This function used to get the CI instance
 */
if(!function_exists('get_instance'))
{
    function get_instance()
    {
        $CI = &get_instance();
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 */
if(!function_exists('getHashedPassword'))
{
    function getHashedPassword($plainPassword)
    {
        return password_hash($plainPassword, PASSWORD_DEFAULT);
    }
}

/**
 * This function used to generate the hashed password
 * @param {string} $plainPassword : This is plain text password
 * @param {string} $hashedPassword : This is hashed password
 */
if(!function_exists('verifyHashedPassword'))
{
    function verifyHashedPassword($plainPassword, $hashedPassword)
    {
        return password_verify($plainPassword, $hashedPassword) ? true : false;
    }
}

/**
 * This method used to get current browser agent
 */
if(!function_exists('getBrowserAgent'))
{
    function getBrowserAgent()
    {
        $CI = get_instance();
        $CI->load->library('user_agent');

        $agent = '';

        if ($CI->agent->is_browser())
        {
            $agent = $CI->agent->browser().' '.$CI->agent->version();
        }
        else if ($CI->agent->is_robot())
        {
            $agent = $CI->agent->robot();
        }
        else if ($CI->agent->is_mobile())
        {
            $agent = $CI->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }

        return $agent;
    }
}

if(!function_exists('setProtocol'))
{
    function setProtocol()
    {
        $CI = &get_instance();
                    
        $CI->load->library('email');
        
        $config['protocol'] = PROTOCOL;
        $config['mailpath'] = MAIL_PATH;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USER;
        $config['smtp_pass'] = SMTP_PASS;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $CI->email->initialize($config);
        
        return $CI;
    }
}

if(!function_exists('emailConfig'))
{
    function emailConfig()
    {
        $CI->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['mailpath'] = MAIL_PATH;
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
    }
}

if(!function_exists('resetPasswordEmail'))
{
    function resetPasswordEmail($detail)
    {
        $data["data"] = $detail;
        // pre($detail);
        // die;
        
        $CI = setProtocol();        
        
        $CI->email->from(EMAIL_FROM, FROM_NAME);
        $CI->email->subject("Reset Password");
        $CI->email->message($CI->load->view('email/resetPassword', $data, TRUE));
        $CI->email->to($detail["email"]);
        $status = $CI->email->send();
        
        return $status;
    }
}

if(!function_exists('setFlashData'))
{
    function setFlashData($status, $flashMsg)
    {
        $CI = get_instance();
        $CI->session->set_flashdata($status, $flashMsg);
    }
}

if(!function_exists('is_ajax_request'))
{
    function is_ajax_request()
    {
        $CI = & get_instance();
        //echo "hello ".$CI;exit;
        if(!$CI->input->is_ajax_request()) {
            //exit('No direct script access allowed');
            $CI->load->view("access");
        }
        else
        {
            return true;
        }    
    }
}

if(!function_exists('is_user_logged_in'))
{
//     function is_user_logged_in()
//     {
//         $CI = & get_instance();
//         if($CI->session->userdata("userId"))
//         {
//             $sessionArr = $CI->session->userdata("userId");
//             return true;
//         }
//         else
//         {
//             return false;
//         }
//     }
function is_user_logged_in()
{
    $CI = & get_instance();
    if($CI->session->userdata("isLoggedIn"))
    {
        return true;
    }
    else
    {
        return false;
    }
}

}
if(!function_exists('is_ajax_request'))
{
    function is_ajax_request()
    {
        $CI = & get_instance();
        if(!$CI->input->is_ajax_request()) {
          exit('No direct script access allowed');
        }
        else
        {
            return true;
        }
        
    }
}

if(!function_exists('sendEmail'))
{
    function sendEmail($email,$userName,$message=NULL,$subject=NULL)
    {
        $CI = & get_instance();
        $CI->load->library("email");
        $CI->load->helper('email_template_helper');         
        $hostname = $CI->config->item('hostname');
        $config['mailtype'] ='html';
        $config['charset'] ='iso-8859-1';
        $CI->email->initialize($config);
        $from  = EMAIL_FROM; 
        $CI->messageBody  = email_header();
        // if(!empty($message)) {
        //     $CI->messageBody  .= $message;
        // }  else {
            $CI->messageBody  .= '<tr> 
            <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                <p>Hello '.$userName.',</p>
                    <p> '.$message.'.</p>
                    Tulii admin will review your profile and active your account within 24 hours.<br/></p>
                                       
                </td>
            </tr>
            ';      
        // }              
        if(empty($subject)) 
        $subject =  'Tulii register by admin';   
        $CI->messageBody  .= email_footer();
        $CI->email->from($from, FROM_NAME);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($CI->messageBody);
        $send = $CI->email->send();
        return $send;
    }
}

if(!function_exists('is_email_exist'))
{
    function is_email_exist($email)
    {
        $CI = &get_instance();
        $CI->load->model('common_model');
        $resultEmail = $CI->Common_model->select('user_id', TB_USERS, array('user_email' => $email));
        if( count($resultEmail) > 0 ) {
            return true;
        } else {
            return false;
        }

    }
}

// Checked group is exist or not
if(!function_exists('is_group_exist'))
{
    function is_group_exist($groupName)
    {
        $CI = &get_instance();
        $CI->load->model('common_model');
        $resultGroupName = $CI->Common_model->select('group_id, group_name', TB_GROUP, array('group_name' => $groupName));
        
        if( count($resultGroupName) > 0 ) {
            return true;
        } else {
            return false;
        }
    }
}


if(!function_exists('is_authorized'))
{
    function is_authorized()
    {
        $CI = &get_instance();
        $CI->load->model('common_model');
        //date_default_timezone_set("Asia/Kolkata");
        $headers = apache_request_headers();
        
        if(empty($headers['Authorizationtoken'])) {
            $CI->response(array("status"=>false,"message"=> 'Authorization key can not be empty'), 200);exit;    
        } else {
            $result = $CI->Common_model->select('user_id,user_device_id,updated_at,user_device_token',TB_USERS,array('user_device_token'=>$headers['Authorizationtoken']));
            if(count($result) > 0) {
                $userId = isset($result[0]['user_id'])?$result[0]['user_id']:'';
                $user_device_id = isset($result[0]['user_device_id'])?$result[0]['user_device_id']:'';
                $currentDate = date('Y-m-d H:i:s');            
                $hourdiff = explode('.',floor((strtotime($currentDate) - strtotime($result[0]['updated_at']))/3600));
               
                if($hourdiff[0] >= 1) {
                    if (isset($userId) && $userId > 0) {
                        if(isset($user_device_id) && $user_device_id != ""){
                           $cond = array("user_id"=>$userId);
                            $ssss= explode(",", $user_device_token);
                            if (in_array($headers['Authorizationtoken'], $ssss))
                            {
                                unset($ssss[array_search($headers['Authorizationtoken'],$ssss)]);
                               $test = implode(',', $ssss);
                                $CI->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_device_id'=>$test,'user_device_token'=>''));
                            }
                            $CI->response(array('status' => true, 'message' => 'You are logout, token has been expired.'), 200);exit;               
                        } else {
                           $CI->response(array('status' => false, 'message' => 'Device id can not empty'), 200);exit;
                        }
                    } else {
                         $CI->response(array('status' => false, 'message' => 'User id can not empty'), 200);exit;
                    }
                } else { 
                    $CI->common_model->update(TB_USERS,array("user_device_token"=>$headers['Authorizationtoken']),array('updated_at'=>date("Y-m-d H:i:s")));
                    return false;
                }
            } else {
                return true;
            }
        }
    }
}


if(!function_exists('multiple_pics_uploads'))
{
    function multiple_pics_uploads($FILES)
    {
        $CI = &get_instance();
        $CI->load->model('common_model');
        $flag = 0;
        $filename = "";
        $filePath = "";

        $uploadCertFileName   = "";
        $uploadCarFileName    = "";
        $uploadLicensFileName = "";
        $uploadCarNumFileName = "";
        $dynamicUploadedFile  = "";                   
        $temp                 = "";
        $fileImages           = $_FILES;

        $allFiles             = array();

        if ( !empty($fileImages) ) {
            foreach ( $fileImages as $key => $value ) {
                $files_name   = array();
                $countAllFile = count($_FILES[$key]['name']);     
                if ( strcmp($key,"certificate_pics") == 0 ) {
                    $filename = "certificate";
                    //$filePath = 'uploads/driver/certification/';            
                } else if ( strcmp($key,"licence_scan_copy") == 0 ) {
                    $filename = 'license';
                    //$filePath = 'uploads/driver/licenseCopy/';            
                } else if ( strcmp($key,"car_pic") == 0 ) {
                    $filename = 'car';
                   // $filePath = 'uploads/driver/carPics/';            
                } else if ( strcmp($key,"car_number_plate_pic") == 0 ) {                   
                    $filename = 'car_number';
                    $filePath = 'uploads/driver/carNumberPlate/';            
                }      

                $temp         = $filename;                           
                
                for ( $i=0; $i < $countAllFile; $i++ ) {
                    $_FILES[$temp]['name']      = $_FILES[$key]['name'][$i];
                    $_FILES[$temp]['type']      = $_FILES[$key]['type'][$i];
                    $_FILES[$temp]['tmp_name']  = $_FILES[$key]['tmp_name'][$i];
                    $_FILES[$temp]['error']     = $_FILES[$key]['error'][$i];
                    $_FILES[$temp]['size']      = $_FILES[$key]['size'][$i];    
                    $files_name[$i]['name']     = $_FILES[$temp]['name'];
                    $files_name[$i]['type']     = $_FILES[$temp]['type'];
                    $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                    $files_name[$i]['error']    = $_FILES[$temp]['error'];
                    $files_name[$i]['size']     = $_FILES[$temp]['size'];
                }
                $allFiles[$temp] = $files_name;                              
            }
            
            // uploaded multiple images
            foreach ( $allFiles as $key => $value ) {

                for ( $i=0, $j=0; $j < count($value); $j++ ) {                                     
                    if ( strcmp($key,"certificate") == 0 ) {
                        $dynamicUploadedFile = multiple_image_upload( $value[$j], "uploads/driver/certification/" ); 
                        if ( is_array($dynamicUploadedFile) ) {
                            $dynamicUploadedFile['message'] = $dynamicUploadedFile['message']." for certificate";
                            $CI->response($dynamicUploadedFile, 200); exit;
                        } else {
                            $uploadCertFileName .= "|".$dynamicUploadedFile;    
                        }
                    } else if ( strcmp($key,"license") == 0 ) {
                        $dynamicUploadedFile = multiple_image_upload( $value[$j], "uploads/driver/licenseCopy/" ); 
                        if ( is_array($dynamicUploadedFile) ) {
                            $dynamicUploadedFile['message'] = $dynamicUploadedFile['message']." for license";
                            $CI->response($dynamicUploadedFile, 200); exit;
                        } else {
                            $uploadLicensFileName .= "|".$dynamicUploadedFile;    
                        }
                    } else if ( strcmp($key,"car") == 0 ) {
                        $dynamicUploadedFile = multiple_image_upload( $value[$j], "uploads/driver/carPics/" ); 
                        if ( is_array($dynamicUploadedFile) ) {
                            $dynamicUploadedFile['message'] = $dynamicUploadedFile['message']." for car picture";
                            $CI->response($dynamicUploadedFile, 200); exit;
                        } else {
                            $uploadCarFileName .= "|".$dynamicUploadedFile;    
                        }
                    } else if ( strcmp($key,"car_number") == 0 ) {
                        $dynamicUploadedFile = multiple_image_upload( $value[$j], "uploads/driver/carNumberPlate/" ); 
                        if ( is_array($dynamicUploadedFile) ) {
                            $dynamicUploadedFile['message'] = $dynamicUploadedFile['message']." for car number";
                            $CI->response($dynamicUploadedFile, 200); exit;
                        } else {
                            $uploadCarNumFileName .= "|".$dynamicUploadedFile;    
                        }
                    } 
                    $i++;                                                     
                }
            }

            // array
            $userdata = array(
                "certificate" =>trim($uploadCertFileName,"|"),
                "license" =>trim($uploadLicensFileName,"|"),
                "car" =>trim($uploadCarFileName,"|"),
                "car_number" =>trim($uploadCarNumFileName,"|")
                );
            return $userdata;                                            
            
        } else {
            $CI->response(array("status" => false, "message" => "Please upload pictures."), 200); exit;  
        }        
    }
}

if(!function_exists('sendEmailTemplate'))
{
    function sendEmailTemplate($email, $message, $subject)
    {
        $CI = & get_instance();
        $CI->load->library("email");
        $CI->load->helper('email_template_helper');         
        $hostname = $CI->config->item('hostname');
        $config['mailtype'] ='html';
        $config['charset'] ='iso-8859-1';
        $CI->email->initialize($config);
        $from  = EMAIL_FROM; 
        $CI->messageBody  = email_header();
        $CI->messageBody  .= $message;
        $CI->messageBody  .= email_footer();
        $CI->email->from($from, FROM_NAME);
        $CI->email->to($email);
        $CI->email->subject($subject);
        $CI->email->message($CI->messageBody);
        $send = $CI->email->send();
        return $send;
    }
}

if(!function_exists('upload_video_on_amazon'))
{
    function upload_video_on_amazon($FILES)
    {
        $CI = &get_instance();
        $CI->load->library('S3');
        $CI->load->helper('string');
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = getExtension($name);
        $compress_image_name = "";
        if(strlen($name) > 0)
        {
            $valid_formats = array("webm", "avi", "mov","mkv", "WEBM", "AVI", "MOV", "MKV","mp4","MP4");
            if(in_array($ext,$valid_formats))
            {   
                $maxsize = (1024*1024)*5;
                if($size<($maxsize))
                {
                    $bucket = BUCKET; 
                    $s3 = new S3(awsAccessKey, awsSecretKey); 
                   // print_r($s3);
                    $s3->putBucket($bucket, S3::ACL_PUBLIC_READ);
                    $actual_image_name = "krntest/".random_string('md5').time().".".$ext;
                    if($s3->putObjectFile($tmp, $bucket , $actual_image_name, S3::ACL_PUBLIC_READ) )
                    {
                        $s3file = 'http://'.$bucket.'.s3.amazonaws.com/'.$actual_image_name;
                        return array("success" => 1, "fullpath" => $s3file, "videoname" => $actual_image_name, "msg" => "File uploaded has been successfully.");
                    }
                    else
                    {
                        return array("success" => 0, "msg" => "Upload Fail.");
                    }
                }
                else
                {
                    return array("success" => 0, "msg" => "You can not upload more that 5 Mb");
                }
            }
            else
            {
                return array("success" => 0, "msg" => "Invalid file, please upload video file.");
            }
        }
        else
        {
            return array("success" => 0, "msg" => "Please select video file.");
        }
    }
}

if(!function_exists('getExtension'))
{
    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }

        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return strtolower($ext);
    }
}

if(!function_exists("sendPushNotification"))
{
    function sendPushNotification( $device_id, $message, $data, $user_type)
    {     
        $content = array(
            "en" => $message
        );

        if($user_type == ROLE_PARENTS || $user_type == SCHOOL || $user_type == ORGANIZATION)
        {
            $app_id = ONSIGNALKEYPARENT;
        }else{
            $app_id = ONSIGNALKEYSP;
        }

        $fields = array(
            'app_id'             => $app_id,
            'include_player_ids' =>(array)$device_id,
            'data'               => $data,
            'contents'           => $content
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
       // print_r($response);exit;            
        curl_close($ch);
    }
}

if(!function_exists('sendOTP')){
    function sendOTP($msg, $to)
    {
       $field = array(
            "sender_id" => "FSTSMS",
            "message" => $msg,
            "language" => "english",
            "route" => "p",
            "numbers" => $to,
            "flash" => "1",
            "variables" => "{#AA#}|{#CC#}",
            "variables_values" => "123456787|asdaswdx"
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.fast2sms.com/dev/bulk",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($field),
          CURLOPT_HTTPHEADER => array(
            "authorization: JSQ1tX7LhCYmy59b2KERVO3oHfiIzBrjWDTMFGZ8xq0PkNs4u6I5J7eDsMHlN1jctk2odQyAST3PFE46",
            "cache-control: no-cache",
            "accept: */*",
            "content-type: application/json"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return 1;
        }
    }
}

if(!function_exists('invoiceGenerateInPDF'))
{
    function invoiceGenerateInPDF($content, $title, $subject, $filename, $invoice)
    {
        // Include the main TCPDF library (search for installation path).
        require_once APPPATH.'third_party/tcpdf/tcpdf.php';
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Tulii');
        $pdf->SetTitle($title);
        $pdf->SetSubject($subject);
        $pdf->SetKeywords('tulii invoice, PDF');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $pdf->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

        // set text shadow effect
        $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

        // Set some content to print
        $html = $content;


        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output($filename, 'I');
    }
}

if(!function_exists('upload_image'))
{
    function upload_image($FILES, $uploaddir = './uploads/') {
        if (!is_dir($uploaddir)) {
         mkdir($uploaddir, 0777, TRUE);

        }
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = getExtension($name);
        //$compress_image_name = "";
        if (strlen($name) > 0) {
            //$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            $valid_formats = array("jpg", "png", "jpeg", "PNG", "JPG", "JPEG");
            if (in_array($ext, $valid_formats)) {
                $file_name = underscore($name);
                $path = $name;
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $user_img = rand() . '_profile.' . $ext;
                $uploadfile = $uploaddir . $user_img;
                $maxsize = (1024 * 1024) * 2;
                if ($size < ($maxsize)) {
                    //Upload profile image
                    if (move_uploaded_file($tmp, $uploadfile)) {
                        return array("success" => 1, "msg" => "File uploaded successfully.", 'filename' => $user_img);
                    } else {
                        return array("success" => 0, "msg" => "Some error has been occurred, please try again!!!");
                    }
                } else {
                    return array("success" => 0, "msg" => "You can not upload more that 2 Mb");
                }
            } else {
                return array("success" => 0, "msg" => "Invalid file, please upload image file.");
            }
        } else {
            return array("success" => 0, "msg" => "Please select image file.");
        }
    }
}

if(!function_exists('underscore'))
{
    function underscore($str)
    {
        return preg_replace('/[\s]+/', '_', trim(MB_ENABLED ? mb_strtolower($str) : strtolower($str)));
    }
}

if(!function_exists('multiple_image_upload'))
{
    function multiple_image_upload($FILES, $uploaddir = './uploads/') {
        if (!is_dir($uploaddir)) {
            mkdir($uploaddir, 0777, TRUE);
        }        
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = getExtension($name);
        //$compress_image_name = "";
        if (strlen($name) > 0) {
            //$valid_formats = array("jpg", "png", "gif", "bmp", "jpeg", "PNG", "JPG", "JPEG", "GIF", "BMP");
            $valid_formats = array("jpg", "png", "jpeg", "PNG", "JPG", "JPEG","PDF","pdf");
            if (in_array($ext, $valid_formats)) {
                $file_name = underscore($name);
                $file_name =explode('.', $file_name);
                $path = $name;
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $user_img = $file_name[0].''.rand() . '.' . $ext;
                $uploadfile = $uploaddir . $user_img;
                $maxsize = (1024 * 1024) * 2;
                if ($size < ($maxsize)) {
                    //Upload profile image
                    if (move_uploaded_file($tmp, $uploadfile)) {
                        return $user_img;
                    } else {
                        return array("status" => "false", "message" => "Some error has been occurred, please try again!!!");
                    }
                } else {
                    return array("status" => "false", "message" => "You can not upload more that 2 Mb");
                }
            } else {
                return array("status" => "false", "message" => "Invalid file, please upload image file");
            }
        } else {
            return array("status" => "false", "message" => "Please select image file");
        }
    }
}

if(!function_exists('upload_video'))
{
    function upload_video($booking_id=0,$FILES, $uploaddir = './video/') {
        if (!is_dir($uploaddir)) {
         mkdir($uploaddir, 0777, TRUE);
         
        }
        $time=time();
        $name = $FILES['name'];
        $size = $FILES['size'];
        $tmp = $FILES['tmp_name'];
        $ext = getExtension($name);
        //$compress_image_name = "";
        if (strlen($name) > 0) {
            $valid_formats = array("avi", "flv", "wmv", "mov", "mp4", "webm", "mkv", "vob", "ogm", "mpeg","3gp");
            if (in_array(strtolower($ext), $valid_formats)) {
                $file_name = underscore($name);
                $path = $name;
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $user_video = $booking_id.'_'.$time.'.'. $ext;
                $uploadfile = $uploaddir . $user_video;
               // $maxsize = (1024 * 1024) * 2;
               // if ($size < ($maxsize)) {
                    //Upload profile image
                    if (move_uploaded_file($tmp, $uploadfile)) {
                        return array("success" => 1, "msg" => "File uploaded successfully.", 'filename' => $user_video);
                    } else {
                        return array("success" => 0, "msg" => "Some error has been occurred, please try again!!!");
                    }
                //} else {
                 //   return array("success" => 0, "msg" => "You can not upload more that 2 Mb");
              //  }
            } else {
                return array("success" => 0, "msg" => "Invalid file, please upload image file.");
            }
        } else {
            return array("success" => 0, "msg" => "Please select image file.");
        }
    }
}

if(!function_exists('getUser'))
{
    function getUser($userId)
    {
        $CI = &get_instance();
        $CI->load->model('common_model');

        return $CI->common_model->select('user_type_id',TB_SERVICE_USERS,array('service_user_id' => $userId));
       
    }
}

if(!function_exists('verifyOtp'))
{
    function verifyOtp($id,$status,$otp)
    {
        $CI = &get_instance();
        $CI->load->model('common_model');

        if('start' == $status)
        {
            $cond  = array('book_det_id' => $id,'startOtp' => $otp);
        }
        if('stop' == $status)
        {
            $cond  = array('book_det_id' => $id,'endOtp' => $otp);
        }

        $data  = $CI->common_model->select('count(*) as cnt',TB_BOOKING_DETAILS,$cond);
        if(1 == $data[0]['cnt'])
        {
            return 1;
        }
        else
        {
            return 0;
        }

       
    }
}




?>