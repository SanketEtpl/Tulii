<?php 

/*
Author : Kiran n 
Page :  Booking_model.php
Description : Booking model use for track booking details in admin
*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Booking_model extends CI_Model
{   
    private function _get_datatables_query(
        $select = "*", 
        $table, 
        $where_cond = array(), 
        $order = array(), 
        $column_order = array(), 
        $column_search = array(), 
        $join = array() 
    )
    {         
        $this->db->select($select);
        $this->db->from($table); 

        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"INNER");
        }

        $i = 0;     
        foreach ($column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($where_cond)) // here condition on list 
        {
           // $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
    }
    
    function get_datatables($select, $table, $where_cond, $order, $column_order, $column_search, $join)
    {   
        $this->_get_datatables_query($select, $table, $where_cond, $order, $column_order, $column_search, $join);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    
    function count_filtered($select, $table, $where_cond, $order, $column_order, $column_search, $join) // filter data of records
    {   
        $this->_get_datatables_query($select, $table, $where_cond, $order, $column_order, $column_search, $join);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($table) // count all record
    {
        $this->db->from($table);
        return $this->db->count_all_results();        
    }    
}
?>