<?php

/*
 * @author : Kiran.
 * description: manage the school model
 */

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Pagination_school_model extends CI_Model { 
    var $table = TB_STUDENT;
    var $column_order = array(null, 'role_id', 'stud_username', 'stud_age', 'stud_address', 'parent_name', 'parent_phone', 'parent_email'); //set column field database for datatable orderable
    var $column_search = array('role_id', 'stud_username', 'stud_age', 'stud_address', 'parent_name', 'parent_phone', 'parent_email'); //set column field database for datatable searchable 
    var $order = array('role_id' => 'asc'); // default order 
    
    var $where_cond = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query($orgId)
    {
        $this->db->select("role_id, stud_id, stud_username, stud_age, parent_email, parent_name, parent_phone, stud_address");
        $this->db->from($this->table);       
        $this->db->Where("user_id",$orgId);
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }   
    }
 
    function get_datatables($orgId)
    {
        $this->_get_datatables_query($orgId);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();        
        return $query->result();
    }
 
    function count_filtered($orgId) // filter data of records
    {
        $this->_get_datatables_query($orgId);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($orgId) // count all record
    {
        $this->db->from($this->table);
        $this->db->Where("user_id",$orgId);
        return $this->db->count_all_results();
    } 
}