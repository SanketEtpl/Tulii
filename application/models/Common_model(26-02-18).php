<?php
class Common_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function insert($table,$data)
    {
        $this->db->insert($table,$data);
        //echo $this->db->last_query();die;
        return $this->db->insert_id();
    }
    
    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
    }
    
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        //echo $this->db->last_query();//die;
        return $this->db->affected_rows();
    }
    function select($sel,$table,$cond = array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();//die;
		return $query->result_array();
	}
	
	function selectQuery($sel,$table,$cond = array(),$orderBy=array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
    
    function update_batch($table,$where,$data)
    {
		$this->db->_protect_identifiers=true;
        $this->db->update_batch($table,$data,$where);
       // echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    
    public function getRowsFrontPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array(),$group_by="")
    {
        if($this->session->userdata('perpage'))
        {
        $per_page=$this->session->userdata('perpage');
        }else {
        $per_page = "";
        }
        $this->db->protect_identifiers=true;
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
                           if($v !=""){
                                        $this->db->where($k,$v);
                            }
                            else{
                                    $this->db->where($k);
                            }
        }
        
        
                    
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
        $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"LEFT");
        }
         //echo "<pre>";print_r($orderBy);die;
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        if($group_by != ""){
            $this->db->group_by($group_by);
        }
        // $this->db->limit($per_page,($page*$per_page));
        $query = $this->db->get();
        //print_r($query );die;
        // echo $this->db->last_query();die;
        return $query->result_array();
    }


    public function getRowsPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array())
    {
        $per_page=$this->session->userdata('perpage');
        $this->db->protect_identifiers=true;
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val,"LEFT");
        }
       
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
         
        //$this->db->limit($per_page,($page*$per_page));
        $query = $this->db->get();
        //print_r($query );die;
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
}
?>
