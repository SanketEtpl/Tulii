<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class data_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function userListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.user_email, BaseTbl.user_name, BaseTbl.user_phone_number, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.user_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_phone_number  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.user_email, BaseTbl.user_name, BaseTbl.user_phone_number, Role.role');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.user_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_name  LIKE '%".$searchText."%'
                            OR  Role.role  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_phone_number  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    /**
     * This function is used to get the user roles information
     * @return array $result : This is result of the query
     */
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param {string} $email : This is email id
     * @param {number} $userId : This is user id
     * @return {mixed} $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("user_email");
        $this->db->from("tbl_users");
        $this->db->where("user_email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("user_id !=", $userId);
        }
        $query = $this->db->get();

        return $query->result_array();
    }
    
    
    /**
     * This function is used to add new user to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('tbl_users', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    function insert_batch($table,$data)
    {   
        $this->db->protect_identifiers=true;
        $this->db->insert_batch($table,$data);
        //return $this->db->insert_id();
    }
    
    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('user_id, user_name, user_email, user_phone_number, roleId');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted', 0);
		$this->db->where('roleId !=', 1);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        
        return $query->result();
    }
    
    
    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('user_id', $userId);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }


    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('user_id, user_password');
        $this->db->where('user_id', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_users');
        
        $user = $query->result();
     
        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->user_password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('user_id', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', $userInfo);
        
        return $this->db->affected_rows();
    }

     function select($sel,$table,$cond = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();//die;
        return $query->result_array();
    }

    /* function getAllUsers($cond = array())
    {
        $this->db->select("user_id AS id,user_name,user_email,roleId,user_password,user_status,user_phone_number,user_birth_date,user_pic,user_gender,user_address,user_device_token,user_device_id", FALSE);
        // $this->db->select("user_id AS id,user_name,user_email,roleId,user_password,user_status,user_phone_number,user_no_of_kids,user_birth_date,user_age,user_pic,user_gender,user_address,user_category_id,user_sub_category_id,user_device_token,user_device_id", FALSE);
        $this->db->from(TB_USERS);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result_array();
    }*/

    function getAllUsers($cond = array(),$app_id = '')
    {
        if($app_id == 0){
           $this->db->select("service_user_id AS user_id,user_fullname,user_email,user_type_id as roleId,user_password,user_status,user_phone_number,user_birth_date,user_prof_pic,user_gender,user_address,user_device_token,user_device_id", FALSE);
           $this->db->from(TB_SERVICE_USERS);
        }
        else{
           $this->db->select("service_provider_id AS user_id,sp_fullname,sp_email,sp_user_type as roleId,sp_password,sp_status,sp_phone_number,sp_birth_date,sp_prof_pic,sp_gender ,sp_address,sp_device_token,sp_device_id as user_device_id", FALSE);
           $this->db->from(TB_SERVICE_PROVIDER);
        }
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result_array();
    }


    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
        //echo $this->db->last_query();die;
        return $this->db->affected_rows();
    }

    function select_in($cond = array(),$ids)
    {
        $this->db->select("user_device_id", FALSE);
        $this->db->from(TB_USERS);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        if($ids !== NULL) 
       {
        $this->db->where_in('user_device_id',trim($ids));
       }    
       $query = $this->db->get();
       return $query->result_array();
        }
}

  