<?php 

/*

Author : Rajendra pawar 
Page :  terms_model.php
Description : Terms model use for Terms & Condition functionality

*/


if(!defined('BASEPATH')) exit('No direct script access allowed');

class Terms_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */


      function termsData()
    {
        $this->db->select('terms.terms_id, terms.terms_message');
        $this->db->from('tbl_terms as terms');
       
        $this->db->where('terms.isDeleted', 0);
        $this->db->limit(1);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    function updateTermsModel($terms_id, $termsInfo)
    { 
        $this->db->where('terms_id', $terms_id);
        $this->db->update('tbl_terms', $termsInfo);
        return $this->db->affected_rows();
    }
    
}

?>