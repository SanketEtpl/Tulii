<?php 

/*
Author : Rajendra pawar 
Page :  careDrive_model.php
Description : Care Drive model use for care driver managment functionality

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class careDrive_model extends CI_Model
{
    var $table = TB_USERS;
    var $column_order = array(null, 'user_email','user_name','user_phone_number'); //set column field database for datatable orderable
    var $column_search = array('user_email','user_name','user_phone_number'); //set column field database for datatable searchable 
    var $order = array('user_id' => 'desc'); // default order 
    var $where_cond = array("roleId ="=>4,"isDeleted"=>0);
       
function careDriverListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.user_id, BaseTbl.user_email, BaseTbl.user_name, BaseTbl.user_phone_number, BaseTbl.user_status');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.user_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_phone_number  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('Role.role =', 'Care Driver');
        $query = $this->db->get();
        
        return count($query->result());
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function careDriverListing($searchText = '', $page, $segment)
    {
         $this->db->select('BaseTbl.user_id, BaseTbl.user_email, BaseTbl.user_name, BaseTbl.user_phone_number, BaseTbl.user_status');
        $this->db->from('tbl_users as BaseTbl');
        $this->db->join('tbl_roles as Role', 'Role.roleId = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.user_email  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.user_phone_number  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('Role.role =', 'Care Driver');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }    
   function select_exist($table,$user_id,$user_email)
    {
        $this->db->select('*', FALSE);
        $this->db->from($table);
        $this->db->where('user_email',$user_email);
        if($user_id != 0) { $this->db->where('user_id !=', $user_id); }
        
        $query = $this->db->get();
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
    private function _get_datatables_query()
    {         
        $this->db->from($this->table); 
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
     function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
}
?>