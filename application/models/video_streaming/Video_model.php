<?php 

/*
Author : Rajendra pawar 
Page :  Booking_model.php
Description : Booking model use for track booking details in admin
*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Video_model extends CI_Model
{
    var $table = TB_VIDEO_STREAM;
    var $column_order = array(null,'tbl_users.user_name','tbl_bookings.booking_duration','tbl_bookings.booking_start_date','tbl_bookings.booking_end_date','tbl_bookings.booking_pick_up_location','tbl_bookings.booking_drop_off_location'); //set column field database for datatable orderable
    var $column_search = array('tbl_users.user_name','tbl_bookings.booking_duration','tbl_bookings.booking_start_date','tbl_bookings.booking_end_date','tbl_bookings.booking_pick_up_location','tbl_bookings.booking_drop_off_location'); //set column field database for datatable searchable 
    var $order = array('tbl_video_streaming.id' => 'desc'); // default order

     var  $where_cond = array('tbl_users.isDeleted'=>'0','tbl_users.user_status'=>'1','tbl_bookings.isDeleted'=>0,'tbl_video_streaming.isDeleted'=>'0');
   
    private function _get_datatables_query()
    {         
        $this->db->from($this->table); 
        $this->db->join(TB_BOOKINGS, TB_BOOKINGS.".booking_id = ".TB_VIDEO_STREAM.".booking_id");
        $this->db->join(TB_USERS,TB_USERS.".user_id = ".TB_BOOKINGS.".user_id");
        
        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

      if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
    }
    function get_datatables()
    {   
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
       // echo $this->db->last_query();exit();
        return $query->result();
       
    }
     function count_filtered() // filter data of records
    {   
        
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
}
?>