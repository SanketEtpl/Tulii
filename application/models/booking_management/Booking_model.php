<?php 

/*
Author : Kiran n 
Page :  Booking_model.php
Description : Booking model use for track booking details in admin
*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Booking_model extends CI_Model
{
    var $table = TB_USERS;
    var $column_order = array(null,'user_name','kid_name','booking_date','booking_start_date','booking_status','booking_duration','booking_price','payment_status'); //set column field database for datatable orderable
    var $column_search = array('user_name','kid_name','booking_date','booking_start_date','booking_status','booking_duration','booking_price','payment_status'); //set column field database for datatable searchable 
    var $order = array('tbl_bookings.booking_id' => 'desc'); // default order 
    //var $where_cond = array("tbl_users.isDeleted"=>0,"tbl_kids.kid_status ="=>1,
       // "tbl_bookings.isDeleted"=>0,"tbl_categories.category"=>"Rides");
   
    private function _get_datatables_query($where_cond = array(),$subCategery = null)
    {         
        $this->db->from($this->table); 
        $this->db->join(TB_KIDS, 'tbl_users.user_id = tbl_kids.user_id');
        $this->db->join(TB_BOOKINGS, 'tbl_users.user_id = tbl_bookings.user_id');
        $this->db->join(TB_CATEGORIES, 'tbl_bookings.booking_category_id = tbl_categories.category_id');
        if(!empty($subCategery)) {
            $this->db->join(TB_SUBCATEGORIES, 'tbl_bookings.booking_sub_category_id = tbl_subcategories.sub_category_id');
        }
        $this->db->join(TB_KIDS_BOOKING, 'tbl_kids_booking.booking_id = tbl_bookings.booking_id and tbl_kids_booking.kids_id = tbl_kids.kid_id'); 

        $i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($where_cond)) // here condition on list 
        {
           // $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }
    }
    function get_datatables($cond = array(),$subCategery = null)
    {   
        $where_cond = $cond;

        $this->_get_datatables_query($where_cond,$subCategery);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
     function count_filtered($cond = array(),$subCategery = null) // filter data of records
    {   
        $where_cond = $cond;
        $this->_get_datatables_query($where_cond,$subCategery);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
}
?>