<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends CI_Model
{   

	var $table = TB_GROUP_MASTER;
    var $column_order = array('group_id','group_name','no_of_people','user_fullname','role_name','group_status');
    var $column_search = array('group_name','no_of_people','user_fullname','role_name');
    var $order = array('group_name' => 'asc');
    var $where_cond = array("group_master_ut.is_Deleted" => "0");



    public function __construct()
    {
    	parent::__construct();
    	$this->load->database();
    }


    private function _get_datatables_query()
    {         
    	$this->db->select("group_id,group_name,no_of_people,user_fullname,role_name,group_status,group_master_ut.is_Deleted");
        $this->db->from($this->table); 
        $this->db->join(TB_SERVICE_USERS, TB_SERVICE_USERS.'.service_user_id  = '.TB_GROUP_MASTER.'.service_user_id');
        $this->db->join(TB_ROLES, TB_ROLES.'.role_id  = '.TB_SERVICE_USERS.'.user_type_id');

    	$i = 0;     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                	$this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
                }
                $i++;
            }

        if(isset($_POST['order'])) // here order processing
        {
        	$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
        	$order = $this->order;
        	$this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($this->where_cond)) // here condition on list 
        {
        	$where_cond = $this->where_cond;
        	foreach ($where_cond as $key => $value) {
        		$this->db->where($key,$value);
        	}            
        }
    }

    function get_datatables()
    {
    	$this->_get_datatables_query();
    	if($_POST['length'] != -1)
    		$this->db->limit($_POST['length'], $_POST['start']);
    	$query = $this->db->get();
    	return $query->result();
    }

    function count_filtered() // filter data of records
    {
    	$this->_get_datatables_query();
    	$query = $this->db->get();
    	return $query->num_rows();
    }

    public function count_all() // count all record
    {
    	$this->db->from($this->table);
    	return $this->db->count_all_results();
    }
}
?>