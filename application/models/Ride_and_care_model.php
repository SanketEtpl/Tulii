<?php 

/*
Author : Kiran n 
Page :  Ride_and_care_model.php
Description : Ride and Care model use for track booking details in admin
*/

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ride_and_care_model extends CI_Model
{
    var $table = TB_USERS;
    var $column_order = array(null,'user_name','user_email','user_phone_number','user_gender'); //set column field database for datatable orderable
    var $column_search = array('user_name','user_email','user_phone_number','user_gender'); //set column field database for datatable searchable 
    var $order = array('tbl_users.user_id' => 'desc'); // default order
    var $where_cond = array("tbl_categories.status"=>1,"tbl_users.roleId !="=>1,'tbl_users.isDeleted'=>0,"tbl_users.user_category_id"=>RIDE_AND_CARE,"tbl_users.roleId ="=>ROLE_CARE_DRIVER);
    private function _get_datatables_query()
    {         
        $this->db->from($this->table); 
        $this->db->join(TB_CATEGORIES, 'tbl_users.user_category_id = tbl_categories.category_id');
        //$this->db->join(TB_SUBCATEGORIES, 'tbl_users.user_sub_category_id = tbl_subcategories.sub_category_id');
        $this->db->join(TB_ROLES, 'tbl_users.roleId = tbl_roles.roleId');
        $i = 0;
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        if(isset($this->where_cond)) // here condition on list 
        {
            $where_cond = $this->where_cond;
            foreach ($where_cond as $key => $value) {
                $this->db->where($key,$value);
            }            
        }  
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered() // filter data of records
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all() // count all record
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
}
?>