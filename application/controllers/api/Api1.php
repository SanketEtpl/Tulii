<?php 
/*
Author: Ram K
Page: APi
Description: Created Api for App.
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Api1 extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("data_model");
        $this->load->model("login_model");
        $this->load->model("common_model");
        $this->load->library('Common');
        $this->load->library("email"); 
        $this->load->helper("email_template"); 
        $this->load->helper("cias"); 
        $this->load->library('S3');
        $this->load->library("fileupload");
        $this->load->library('form_validation');
        date_default_timezone_set("Asia/Kolkata");
        header('Access-Control-Allow-Origin: *');
        ini_set('display_errors', '0');        
        //error_reporting(E_ALL);        
    }    

   /**
    * Ashwini changes here
     * Title       : sign up  
     * Description : user sign up
     *
     * @param user_type
     * @param full_name
     * @param first_name
     * @param last_name
     * @param user_email
     * @param password
     * @param gender
     * @param contact_no
     * @param user_dob
     * @param user_device_id
     * @param user_device_type
     * @param reg_no
     * @return Array 
     */

    /*public function signUp_post()
    {
        // post data
        $postData = $this->post();
        $dob='';
        $userAge='';
        $reg_no='';
        $gender = '';
        
        if($postData['user_type'] == '')
        { 
            $this->response(array("status" => false, "message" => 'User type field is required.'), 200); exit; 
        }
        if($postData['user_email'] == '')
        { 
            $this->response(array("status" => false, "message" => 'Email field is required.'), 200); exit; 
        }
        else if(is_email_exist(trim($postData['user_email'])))
        {
            $this->response(array("status" => false, "message" => 'Email address already exists.'), 200); exit;
        }
        if($postData['password'] == '')
        { 
            $this->response(array("status" => false, "message" => 'Password field is required.'), 200); exit;
        }
        if($postData['contact_no'] == '')
        { 
            $this->response(array("status" => false, "message" => 'Contact no field is required.'), 200); exit; 
        }
        if($postData['user_device_id'] == '')
        { 
            $this->response(array("status" => false, "message" => 'Device id field is required.'), 200); exit; 
        }   
        if($postData['user_device_type'] == '')
        { 
            $this->response(array("status" => false, "message" => 'Device type field is required.'), 200); exit;
        }

        switch ($postData['user_type'])
        {
            case ROLE_PARENTS:
            $user_type = 'Parent';
            break;

            case SCHOOL:
            $user_type = 'School';
            break;

            case ORGANIZATION:
            $user_type = 'Organization';
            break;

            case ROLE_CARE_DRIVER:
            $user_type = 'Driver';
            break;

            case ROLE_TUTOR:
            $user_type = 'Tutor';
            break;
        }

        if($postData['user_type'] == ROLE_PARENTS || $postData['user_type'] == SCHOOL || $postData['user_type'] == ORGANIZATION || $postData['user_type'] == ROLE_CARE_DRIVER || $postData['user_type'] == ROLE_TUTOR)
        {
            //-----Check Fisrt name and Last name for parent, driver, tutor ------
            if($postData['user_type'] == ROLE_PARENTS || $postData['user_type'] ==ROLE_CARE_DRIVER  || 
                $postData['user_type'] == ROLE_TUTOR)
            {
                //----- first name and last name ----
                if(empty(trim($postData['first_name'])) || empty(trim($postData['last_name'])))
                {
                    $this->response(array("status" => false, "message" => 'First and Last name is required.'), 200); exit;
                }else
                {
                    $full_name = trim($postData['first_name'])." ".trim($postData['last_name']);
                }

                //---- DOB ----
                if(empty(trim($postData['user_dob'])))
                {
                    $this->response(array("status" => false, "message" => 'Date of birth field is required.'),200); exit;
                }else
                {
                    $dob = trim($postData['user_dob']);
                    $userAge = $this->getAge($dob);
                    if ($userAge < 18)
                    {
                        $this->response(array("status" => false, "message" => 'You are below 18 years so you can\'t register.'), 200); exit;
                    }
                }

                // ---- GENDER ----
                if(empty(trim($postData['gender'])))
                {
                    $this->response(array("status" => false, "message" => 'Gender is required.'), 200); exit;
                }else
                {
                    $gender = trim($postData['gender']);
                }
            }

            // ------ Check full name and reg no for school or organization ------
            if($postData['user_type'] == SCHOOL || $postData['user_type'] == ORGANIZATION)
            {
                if(empty(trim($postData['full_name'])))
                {
                    $this->response(array("status" => false, "message" => 'Full name is required.'), 200); exit;
                }else
                {
                    $full_name = trim($postData['full_name']);
                }

                if(empty(trim($postData['reg_no'])))
                {
                    $this->response(array("status" => false, "message" => 'Registration no is required.'), 200); exit;
                }else
                {
                    $reg_no = trim($postData['reg_no']);
                }
            }

            $userArr = array(
                "roleId"=>trim($postData['user_type']),
                "user_name"=>$full_name,
                "user_email"=>trim($postData['user_email']),
                "user_password"=>getHashedPassword(trim($postData['password'])),
                "user_phone_number"=>trim($postData['contact_no']),
                "user_gender"=>$gender,
                "user_birth_date"=>$dob,
                "user_device_id"=>trim($postData['user_device_id']),
                "user_device_type"=>trim($postData['user_device_type']),
                "notification_status"=>"1",
                "new_record_status"=>"1",
                "reg_no"=>$reg_no,
                "user_status"=>"1",
                "is_block"=>"0",
                "isDeleted"=>"0",
                "created_at"=>date("Y-m-d H:i:s"),
                );
            $result = $this->common_model->insert(TB_USERS,$userArr);

            if ($result)
            {
            // Send email & notification
                $message  = "You have registered successfully as a ".$user_type; 
                $deviceId = trim($postData['user_device_id']);
                $myData   = array("message"=>"Hi $full_name,<br/> Tulii admin will review your profile and active your account within 24hr.");
                $this->sendPushNotification($deviceId,$message,$myData,$user_type);
                $send = sendEmail($postData['user_email'],$full_name,$message,"Tulii registration");
                $this->response(array('status' => true, 'message' => 'You have registered successfully as '.$user_type, 'user_id' => $result),200);
            }
            else
            {
                $this->response(array("status" => false, "message" => "Something went wrong. !!"),200);
            }

        }else
        {
            $this->response(array("status" => false, "message" => "Invalid user type."),200);exit;
        }     
    }*/

    public function signUp_post()
    {
        $postData = $this->post(); 
        $org_name='Not Available';
        $reg_no='Not Available';
        if($postData['user_type'] == '')
        { 
            $this->response(array("status"=>false, "message" => 'User type field is required.'), 200); exit; 
        }
        if($postData['user_email'] == '')
        { 
            $this->response(array("status"=>false, "message" => 'Email field is required.'), 200); exit; 
        }
        if($postData['password'] == '')
        { 
            $this->response(array("status"=>false, "message" => 'Password field is required.'), 200); exit;
        }
        if($postData['contact_no'] == '')
        { 
            $this->response(array("status"=>false, "message" => 'Contact no field is required.'), 200); exit; 
        }
        if($postData['device_id'] == '')
        { 
            $this->response(array("status"=>false, "message" => 'Device id field is required.'), 200); exit; 
        }   
        if($postData['device_type'] == '')
        { 
            $this->response(array("status"=>false, "message" => 'Device type field is required.'), 200); exit;
        }
        if(empty(trim($postData['first_name'])) || empty(trim($postData['last_name'])))
        {
            $this->response(array("status"=>false, "message" => 'First and Last name is required.'), 200); exit;
        }else
        {
            $full_name = trim($postData['first_name'])." ".trim($postData['last_name']);
        }

        switch ($postData['user_type'])
        {
            case ROLE_PARENTS:
            $user_type = 'Parent';
            break;

            case SCHOOL:
            $user_type = 'School';
            break;

            case ORGANIZATION:
            $user_type = 'Organization';
            break;

            case ROLE_CARE_DRIVER:
            $user_type = 'Driver';
            break;

            case ROLE_TUTOR:
            $user_type = 'Tutor';
            break;
        }

        if($postData['user_type'] == ROLE_PARENTS || $postData['user_type'] == SCHOOL || $postData['user_type'] == ORGANIZATION || $postData['user_type'] == ROLE_CARE_DRIVER || $postData['user_type'] == ROLE_TUTOR)
        {            
            // ------ Check full name and reg no for school or organization ------
            if($postData['user_type'] == SCHOOL || $postData['user_type'] == ORGANIZATION)
            {
                if(empty(trim($postData['org_name'])))
                {
                    $this->response(array("status"=>false, "message" => 'Organization name is required.'), 200); exit;
                }else
                {
                    $org_name = trim($postData['org_name']);
                }

                if(empty(trim($postData['reg_no'])))
                {
                    $this->response(array("status"=>false, "message" => 'Registration no is required.'), 200); exit;
                }else
                {
                    $reg_no = trim($postData['reg_no']);
                }
            }

            if($postData['user_type'] == ROLE_PARENTS || $postData['user_type'] == SCHOOL || $postData['user_type'] == ORGANIZATION)
            {
                $chkEmail = $this->common_model->select("user_email",TB_SERVICE_USERS,array("user_email"=>trim($postData['user_email'])));
                if(count($chkEmail) > 0)
                {
                   $this->response(array("status"=>false, "message" => 'Account with same Email ID already exists. Please use another.'), 200); exit; 
               }
               else
               {
                $userArr = array(
                    "user_type_id"=>trim($postData['user_type']),
                    "user_fullname"=>$full_name,
                    "organization_name"=>$org_name,
                    "user_email"=>trim($postData['user_email']),
                    "user_password"=>trim($postData['password']),
                    "user_phone_number"=>trim($postData['contact_no']),               
                    "user_device_id"=>trim($postData['user_device_id']),
                    "user_device_type"=>trim($postData['user_device_type']),
                    "notification_status"=>"1",
                    "new_record_status"=>"1",
                    "registration_no"=>$reg_no,
                    "user_status"=>"1",
                    "is_block"=>"0",
                    "is_deleted"=>"0",
                    "created_at"=>date("Y-m-d H:i:s"),
                    );
                //$result = $this->common_model->insert(TB_SERVICE_USERS,$userArr);
                $result = $this->common_model->registerUser($userArr);

            }
        }

        if($postData['user_type'] == ROLE_CARE_DRIVER || $postData['user_type'] == ROLE_TUTOR)
        {
            $chkEmail1 = $this->common_model->select("sp_email",TB_SERVICE_PROVIDER,array("sp_email"=>trim($postData['user_email'])));
            if(count($chkEmail1) > 0)
            {
               $this->response(array("status"=>false, "message" => 'Account with same Email ID already exists. Please use another.'), 200); exit; 
           }else
           {
            $userArr = array(
                "sp_user_type"=>trim($postData['user_type']),
                "sp_fullname"=>$full_name,
                "sp_email"=>trim($postData['user_email']),
                "sp_password"=>trim($postData['password']),
                "sp_phone_number"=>trim($postData['contact_no']),               
                "sp_device_id"=>trim($postData['user_device_id']),
                "sp_device_type"=>trim($postData['user_device_type']),
                "notification_status"=>"1",
                "new_record_status"=>"1",
                "sp_status"=>"1",
                "is_block"=>"0",
                "is_deleted"=>"0",
                "created_at"=>date("Y-m-d H:i:s"),
                );
            //$result = $this->common_model->insert(TB_SERVICE_PROVIDER,$userArr);

            $result = $this->common_model->registerParent($userArr);
        }
    }

    if ($result)
    {
        if($user_type == "School" || $user_type == "Organization")
        {
            $user_full_name = $org_name;
        }else if($user_type == "Parent" || $user_type == "Driver" || $user_type == "Tutor")
        {
            $user_full_name = $full_name;
        }
        $message  = "You have registered successfully as a ".$user_type; 
        $deviceId = trim($postData['user_device_id']);
        $myData   = array("message"=>"Hi $user_full_name,<br/> Tulii admin will review your profile and active your account within 24hr.");
        $this->sendPushNotification($deviceId,$message,$myData,$user_type);
        $send = sendEmail($postData['user_email'],$user_full_name,$message,"Tulii registration");
        $this->response(array('status' => true, 'message' => 'You have registered successfully as '.$user_type, 'user_id' => $result),200);
    }
    else
    {
        $this->response(array("status"=>false, "message" => "Something went wrong. !!"),200);
    }

}else
{
    $this->response(array("status"=>false, "message" => "Invalid user type."),200);exit;
}     
}

    /** 
    * Ashwini chnages here
     * Title       : Login 
     * Description : User login credential
     *
     * @param  String user_email 
     * @param  String password
     * @return Array 
     */

    /*public function login_post ()
    {        
        // Post data
        $postData = $_POST;

        // input validation
        if ( $postData['user_email'] == '' ) {
            $this->response(array("status" => false, "message" => 'Email field is required.')); exit;
        }
        if ( $postData['password'] == '' ) { 
            $this->response(array("status" => false, "message" => 'Password field is required.')); exit; 
        }

        $userArr = $this->login_model->login($postData["user_email"], $postData['password']);
        if ( count($userArr) > 0 ) {            

            // Checked user is active or not
            if ( $userArr[0]['user_status'] == '1' ) {

                // Checked user type              
                $user_token = md5(uniqid(rand(), true));   
                $cond       = array("user_email" => $postData["user_email"]);                    
                $userData   = $this->data_model->getAllUsers($cond);
                $this->session->set_userdata("userLoggedin", $userData[0]);
                $cond  = array("roleId" => $userData[0]['roleId']);
                $table = TB_ROLES;
                $roles = $this->data_model->select("roleId,role",$table,$cond);
                $userArr[0] += ['type' => $roles[0]['role']];

                // if($roles[0]['roleId'] == 3)
                // {
                //     $cond  = array("category_id" => $userData[0]['user_category_id']);
                //     $table = TB_CATEGORIES;
                //     $usercategory = $this->data_model->select("category_id,category",$table,$cond);

                //     if($usercategory[0]['category_id'] == 4 || $usercategory[0]['category_id'] == 1)
                //     {
                //         $cond = array("category_id" => $userData[0]['user_category_id']);
                //         $table = TB_SUBCATEGORIES;
                //         $usersubcategory = $this->data_model->select("sub_category_id,name",$table,$cond);

                //         if ( isset($usercategory[0]['category']) && $usercategory[0]['category'] != '' ) {
                //             $userArr[0] += ['category' => $usercategory[0]['category']];
                //         } else {
                //             $userArr[0] += ['category' => ''];
                //         }
                //         if ( isset($usersubcategory[0]['name']) && $usersubcategory[0]['name']!='' ) {
                //             $userArr[0] +=['sub_category' => $usersubcategory[0]['name']];
                //         } else {
                //             $userArr[0] +=['sub_category' => ''];
                //         }
                //     }
                // }                   
                
                $date = date("Y-m-d H:i:s");
                $user_token = md5(uniqid(rand(), true)); 
                $this->common_model->update(TB_USERS,array("user_id"=>$userArr[0]['user_id']),array('user_device_token'=>$user_token,'updated_at'=>$date));
                
                    // Checked device id                    
                if ( isset($postData["device_id"]) && $postData["device_id"] != "" ) {

                 $ssss= explode(",", $userArr[0]['user_device_id']);
                 if ( in_array($postData['device_id'], $ssss) ) {
                     $sql = $userArr[0]['user_device_id'];
                 } else {
                  $sql = $userArr[0]['user_device_id'].','.$postData["device_id"];
              }                     
              $device_id = $userArr[0]['user_device_id'];
              $this->common_model->update(TB_USERS, array("user_id" => $userArr[0]['user_id']), array('user_device_id' => $sql));
          }
              // array_splice($userArr[0], 7, 1);
          $this->response(array("status" => true, "user_token" => $user_token, "message" => "You have successfully login.", "user" => $userArr[0]), 200); exit;

      } else {                
        $this->response(array("status" => false, "message" => "Your account is inactive. Please contact admin")); exit;
    }            
} else {
    $this->response(array("status" => false, "message" => "Email or password is incorrect.")); exit;
}
}*/



public function login_post ()
{        
        // Post data
    $postData = $_POST;

        // input validation
    if ( $postData['user_email'] == '' ) {
        $this->response(array("status" => false, "message" => 'Email is required.')); exit;
    }
    if ( $postData['password'] == '' ) { 
        $this->response(array("status" => false, "message" => 'Password is required.')); exit; 
    }
    /*if ( $postData['user_type'] == '' ) { 
        $this->response(array("status" => false, "message" => 'User type is required.')); exit; 
    }*/
    if ( $postData['device_id'] == '' ) { 
        $this->response(array("status" => false, "message" => 'Device id is required.')); exit; 
    }
    if ( $postData['device_type'] == '' ) { 
        $this->response(array("status" => false, "message" => 'Device type is required.')); exit; 
    }

    if ( $postData['app_id'] == '' ) { 
        $this->response(array("status" => false, "message" => 'App id is required.')); exit; 
    }

   // $userArr = $this->login_model->login($postData["user_email"], $postData['password'],$postData['user_type']);

    if(1 == $postData['app_id'])
    {

        //$userArr  = $this->common_model->select('*',TB_SERVICE_PROVIDER,array('sp_email' => $postData['user_email'],'sp_status' => '1')); 

        $userArr = $this->common_model->loginSupplier($postData['user_email'],'1',$postData['password'],$postData['device_type'],$postData['device_id'],date('Y-m-d H:i:s'),1);

        if(count($userArr) > 0)
        {
         /*if(verifyHashedPassword($postData['password'],$userArr[0]['sp_password']))
         {
             $data = array(
                 'sp_device_type' => $postData['device_type'],
                 'sp_device_id' => $postData['device_id'],
                 'updated_at' => date('Y-m-d H:i:s')

                 );
             $update = $this->common_model->update(TB_SERVICE_PROVIDER,array('sp_email' => $userArr[0]['sp_email']),$data);*/

            /*if($update)
            {*/
                $userArr[0]['sp_device_type'] = $postData['device_type'];
                $userArr[0]['sp_device_id'] = $postData['device_id'];

                $this->response(array("status" => true, "message" => "Login successfully .", "user_details" => $userArr[0]), 200); exit;
           /* }
            else
            {
                 echo $this->response(array("status" => false,"message" => "Something went wrong"));
            }*/
         /*}
         else
         {
             echo $this->response(array("status" => false,"message" => "Invalid password"));
         }*/


        }
        else
        {
            echo $this->response(array("status" => false , "message" => "Invalid email"));
        }
    }
    if(0 == $postData['app_id'])
    {
        $userArr  = $this->common_model->select('*',TB_SERVICE_USERS,array('user_email' => $postData['user_email'],'user_status' => '1')); 

        if(count($userArr) > 0)
        {
             if(verifyHashedPassword($postData['password'],$userArr[0]['user_password']))
             {
                 $data = array(
                     'user_device_type' => $postData['device_type'],
                     'user_device_id' => $postData['device_id'],
                     'updated_at' => date('Y-m-d H:i:s')

                     );
                 $update = $this->common_model->update(TB_SERVICE_USERS,array('user_email' => $userArr[0]['user_email']),$data);

                 if($update)
                 {
                     $userArr[0]['user_device_type'] = $postData['device_type'];
                     $userArr[0]['user_device_id'] = $postData['device_id'];

                     $this->response(array("status" => true, "message" => "Login successfully .", "user_details" => $userArr[0]), 200); exit;
                 }
                 else
                 {
                     echo $this->response(array("status" => false,"message" => "Something went wrong"));
                 }
             }
             else
             {
                 echo $this->response(array("status" => false,"message" => "Invalid password"));
             }


        }
        else
        {
            echo $this->response(array("status" => false , "message" => "Invalid email"));
        }
    }
    /*exit();
    if($userArr[0]['user_status']){
        $userstatus = $userArr[0]['user_status'];
        $useremail = 'user_email';
        unset($userArr[0]['user_password']);
    }
    else{
        $userstatus = $userArr[0]['sp_status'];
        $useremail = 'sp_email';
        unset($userArr[0]['sp_password']);
    }
    if ( count($userArr) > 0 ) {            
        $user_type = $postData['user_type'];
            // Checked user is active or not
        if ( $userstatus == '1' ) {

                // Checked user type              
            //$user_token = md5(uniqid(rand(),true));   
            $cond       = array($useremail => $postData["user_email"]);                    
            $userData   = $this->data_model->getAllUsers($cond,$postData['user_type']);
            $this->session->set_userdata("userLoggedin", $userData[0]);
            $cond  = array("role_id" => $userData[0]['roleId']);
            $table = TB_ROLES;
            $roles = $this->data_model->select("role_id,role_name",$table,$cond);
            $userArr[0] += ['type' => $roles[0]['role_name']];
            $userArr[0] += ['user_device_id' => $postData['device_id']];
            $userArr[0] += ['user_device_type' => $postData['device_type']];
            $date = date("Y-m-d H:i:s");
            //$user_token = md5(uniqid(rand(),true)); 

            if($user_type == '2' || $user_type == '5' || $user_type == '6'){
             $this->common_model->update(TB_SERVICE_USERS,array("service_user_id"=>$userArr[0]['user_id']),array('updated_at'=>$date));
         }
         else{
             $this->common_model->update(TB_SERVICE_PROVIDER,array("service_provider_id"=>$userArr[0]['user_id']),array('updated_at'=>$date));
         }

                    // Checked device id                    
         if ( isset($postData["device_id"]) && $postData["device_id"] != "" && isset($postData["device_type"]) && $postData["device_type"]) {

            $ssss= explode(",", $userArr[0]['user_device_id']);

            if ( in_array($postData['device_id'], $ssss) ) {
               $sql = $userArr[0]['user_device_id'];
           } else {
              $sql = $userArr[0]['user_device_id'].','.$postData["device_id"];
          }                     
          $device_id = $userArr[0]['user_device_id'];
          $device_type = $postData["device_type"];

          if($user_type == '2' || $user_type == '5' || $user_type == '6'){
            $this->common_model->update(TB_SERVICE_USERS, array("service_user_id" => $userArr[0]['user_id']), array('user_device_id' => $sql,'user_device_type' => $device_type));

        }
        else{
            $this->common_model->update(TB_SERVICE_PROVIDER, array("service_provider_id" => $userArr[0]['user_id']), array('sp_device_id' => $sql,'user_device_type' => $device_type));
        }
    }
    // $this->response(array("status" => true, "user_token" => $user_token, "message" => "You have successfully login.", "user_details" => $userArr[0]), 200); exit;
    $this->response(array("status" => true, "message" => "Login successfully .", "user_details" => $userArr[0]), 200); exit;
} else {                
    $this->response(array("status" => false, "message" => "Your account is inactive. Please contact admin")); exit;
}            
} else {
    $this->response(array("status" => false, "message" => "Email or password is incorrect.")); exit;
}*/
}


    /**
     * Title       : forgot_password
     * Description : Forgot user password 
     * 
     * @param      : String email
     * @return     : Array  
     */

    function forgot_password_post ()
    {
        // Post data
        $postData = $_POST;
        
        // Checked email address empty or not      
        if ( trim($postData["user_email"]) == "" ) {
            $this->response(array("status" => false, "message" => "Please enter email.")); exit;
        } 

        if(trim($postData["app_id"] == ""))
        {
        	$this->response(array("status" => false,"message" => "Invalid app id"));
        }

        else 
        {            

          $email    = $postData['user_email'];
          $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $randstring = '';
          $usrname = '';
          for ( $i = 0; $i < 8; $i++ ) {
            $randstring .= $characters[rand(0, strlen($characters))];
        }
        if($postData["app_id"] == 0)
        {
           $userdata = $this->common_model->select("*", TB_SERVICE_USERS, array("user_email" => trim($email)));
           $tbl = TB_SERVICE_USERS;
           $key = "service_user_id";
           $pass = "user_password";
           $id = $userdata[0]['service_user_id'];
           $usrname  = $userdata[0]['user_fullname'];

           if ($userdata[0]['user_status'] == '0' || $userdata[0]['user_status'] == "") {
             $this->response(array("status" => false, "message" => "Your account is inactive, You need to contact to admin.")); exit;
         }
     }
     if($postData["app_id"] == 1)
     {
       $userdata = $this->common_model->select("*", TB_SERVICE_PROVIDER, array("sp_email" => trim($email)));
       $tbl = TB_SERVICE_PROVIDER;
       $key = "service_provider_id";
       $pass = "sp_password";
       $id = $userdata[0]['service_provider_id'];
       $usrname  = $userdata[0]['sp_fullname'];
       if ($userdata[0]['sp_status'] == '0' || $userdata[0]['sp_status'] == "") {
         $this->response(array("status" => false, "message" => "Your account is inactive, You need to contact to admin.")); exit;
     }
 }

 $update = $this->common_model->update($tbl,array($key => $id), array($pass => getHashedPassword($randstring)));

 if ($update)
 {
    $hostname           = $this->config->item('hostname');
    
    $config['mailtype'] ='html';
    $config['charset']  ='iso-8859-1';
    $this->email->initialize($config);
    $from               = EMAIL_FROM; 
    $this->messageBody  = email_header();
    $this->messageBody  .= "Hello ".$usrname." 
    <br/><br/>You recently requested to reset your password for your tulii account. Please refer this password.  
    <br/>Password: <b>" . $randstring ."</b>
    ";
    
    $this->messageBody  .= email_footer();
    $this->email->from($from, $from);
    $this->email->to("$email");
    $this->email->subject('Your new Password');
    $this->email->message($this->messageBody); 
    $this->email->send();
                /*$user_token = md5(uniqid(rand(), true));
                $data       = array("user_token" => $user_token,"user_id" => $userId);*/
                $this->response(array("status" => true, "message" => "Your password has been sent to your email address. please check your email address."), 200); exit;
            } else {
                $this->response(array("status" => false,  "message" => "Something went wrong !!!")); exit;
            } 



            

            
            
            
            /*if ( !count($userdata) ) {                
                $this->response(array("status" => false, "message" => "Please enter correct email address, this email address does not exist.")); exit;
            } else {

                if ($userdata[0]['user_status'] == '0' || $userdata[0]['user_status'] == "") {
                    $this->response(array("status" => false, "message" => "Your account is inactive, You need to contact to admin.")); exit;
                }
                $userId     =$userdata[0]['user_id'];
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';

                // Generate random value
                for ( $i = 0; $i < 8; $i++ ) {
                    $randstring .= $characters[rand(0, strlen($characters))];
                }
                
                // Updated password & password send to email                
                $update = $this->common_model->update(TB_USERS,array("user_id" => $userdata[0]['user_id']), array('user_password' => getHashedPassword($randstring)));
                if ($update) {
                    $hostname           = $this->config->item('hostname');
                    $usrname            = $userdata[0]['user_name'];
                    $config['mailtype'] ='html';
                    $config['charset']  ='iso-8859-1';
                    $this->email->initialize($config);
                    $from               = EMAIL_FROM; 
                    $this->messageBody  = email_header();
                    $this->messageBody  .= "Hello ".$usrname." 
                    <br/><br/>You recently requested to reset your password for your tulii account. Please refer this password.  
                    <br/>Password: <b>" . $randstring ."</b>
                    ";
                    
                    $this->messageBody  .= email_footer();
                    $this->email->from($from, $from);
                    $this->email->to("$email");
                    $this->email->subject('Your new Password');
                    $this->email->message($this->messageBody); 
                    $this->email->send();
                    $user_token = md5(uniqid(rand(), true));
                    $data       = array("user_token" => $user_token,"user_id" => $userId);
                    $this->response(array("status" => true, "result" => $data, "message" => "Your password has been sent to your email address. please check your email address."), 200); exit;
                } else {
                    $this->response(array("status" => false, "user_token" => $user_token, "message" => "Something went wrong !!!")); exit;
                }    
            }*/
        }
    }

       /**
     * Jyoti Korde
     * Title       : Change password
     * Description : Change user password
     * 
     * @param  Integer user_id
     * @param  Integer app_id
     * @param  String new_pwd
     * @param  String old_pwd
     * @return Array
     */

       public function change_password_post () 
       {
        // Post data
        $postData = $_POST;

        if(empty($postData['user_id']))
        {
            $this->response(array("status"=>false, "message" => 'User id is required'), 200); exit;
        }
        if(empty($postData['old_pwd']))
        {
            $this->response(array("status"=>false, "message" => 'Old password is required'), 200); exit;
        }
        if(empty($postData['new_pwd']))
        {
            $this->response(array("status"=>false, "message" => 'New password is required'), 200); exit;
        }
        if ( $postData['app_id'] == '' ) { 
            $this->response(array("status"=>false, "message" => 'App id is required.')); exit; 
        }

        

        // Checked user exist or not
        if ( $this->user_exists($postData["user_id"],$postData['app_id']) == 0 ) {
            $this->response(array("status"=>false, "message" => 'User id is not exists'), 200); exit;
        }

        $app_id = $postData['app_id'];
        if($app_id == 0){
            $cond_check  = array('service_user_id' => $postData['user_id']);
            $currentUser = $this->login_model->validUser(TB_SERVICE_USERS,'user_password,user_fullname', $cond_check);
            $oldPassword = $currentUser[0]['user_password'];
        }
        else{
            $cond_check  = array('service_provider_id' => $postData['user_id']);
            $currentUser = $this->login_model->validUser(TB_SERVICE_PROVIDER,'sp_password,sp_fullname', $cond_check);
            $oldPassword = $currentUser[0]['sp_password'];
        }
        if( !empty($currentUser) ) {
            if ( verifyHashedPassword(trim($postData['old_pwd']), $oldPassword) ) {
                if( count($currentUser) == 0 ) {
                    $this->response(array("status"=>false, "message" => 'You have entered incorrect old password.'), 200 ); exit;
                }            

                // Updated password
                if($app_id == 0){
                    $cond       = array("service_user_id" => $postData['user_id']);
                    $updateData = array("user_password" => getHashedPassword($postData['new_pwd']));
                    $result     = $this->common_model->update(TB_SERVICE_USERS, $cond, $updateData);
                }
                else{
                    $cond       = array("service_provider_id" => $postData['user_id']);
                    $updateData = array("sp_password" => getHashedPassword($postData['new_pwd']));
                    $result     = $this->common_model->update(TB_SERVICE_PROVIDER, $cond, $updateData);

                     
                }

                // Result 
                if ( $result ) {
                    $this->response(array("status" => true, "message" => 'Password has been updated successfully.'), 200); exit;
                } else {
                    $this->response(array("status"=>false, "message" => 'Error occured during updating Password.'), 200); exit;
                } 
            } else {
                $this->response(array("status"=>false, "message" => 'Incorrect old password.'), 200); exit;
            }
        }        
    }

     /**
     * Title : driver_update
     * Description : update driver details
     * 
     * @param Integer user_id
     * @return Array 
     */

     public function driver_update_post ()
     {
        // Input data
        $postData = $_POST;  

        // Checked user is authenticate or not 

        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/

        if ( $postData['user_id'] != "" && $postData['user_id'] > 0 ) {
            $user_id = $this->common_model->select("user_email, user_password, user_name, user_phone_number, user_gender, user_address, user_birth_date, user_age, is_your_own_car, driving_license_type, license_no, valid_from_date, valid_until_date, car_model, car_register_no, school, degree, specialization, education_from, education_to, certification_name, license_pic, upload_certificate, car_pic", TB_USERS, array('user_id' => $postData['user_id']));
            
            if(empty($user_id)) {
                $this->response(array("status"=>false,"message"=> 'User id does not exists'));exit;
            } else {    
                $upload_car_pics      = "";
                $licenseCopy          = "";
                $certification        = "";
                $car_pics             = "";
                $scan_copy_of_license = "";
                $upload_certification = "";
                $temp                 = "";
                $fileImages           = $_FILES;
                $allFiles             = array();
                
                if ( !empty($fileImages) ) {
                    $j =1;            

                    foreach ($fileImages as $key => $value) {
                        $cpt = count($_FILES[$key]['name']);
                        if ( $j == 1 )
                            $temp = 'car_img';
                        else if ( $j == 2 )
                            $temp = 'license_img';
                        else
                            $temp = 'certification_img';
                        $files_name = array();

                        for ( $i=0; $i < $cpt; $i++ ) {
                            $_FILES[$temp]['name']      = $_FILES[$key]['name'][$i];
                            $_FILES[$temp]['type']      = $_FILES[$key]['type'][$i];
                            $_FILES[$temp]['tmp_name']  = $_FILES[$key]['tmp_name'][$i];
                            $_FILES[$temp]['error']     = $_FILES[$key]['error'][$i];
                            $_FILES[$temp]['size']      = $_FILES[$key]['size'][$i];    
                            $files_name[$i]['name']     = $_FILES[$temp]['name'];
                            $files_name[$i]['type']     = $_FILES[$temp]['type'];
                            $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                            $files_name[$i]['error']    = $_FILES[$temp]['error'];
                            $files_name[$i]['size']     = $_FILES[$temp]['size'];
                        }
                        $allFiles[$temp]=$files_name;
                        $j++;                            
                    }

                    foreach ( $allFiles as $key => $value ) {
                        $i = 0;      

                        for ( $j=0; $j < count($value); $j++ ) { 

                            if ( strcmp($key,'car_img') == 0 ) {                                   
                                $upload_car_pics = multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
                                $car_pics .= "|".$upload_car_pics; 
                            } else if ( strcmp($key,'certification_img') == 0 ) {
                                $upload_certification = multiple_image_upload($value[$j],'uploads/driver/certification/'); 
                                $certification .= "|".$upload_certification; 
                            } else if(strcmp($key,'license_img') == 0 ){
                                $scan_copy_of_license = multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
                                $licenseCopy .= "|".$scan_copy_of_license;                                         
                            } else {}         
                            $i++;                                                     
                        }
                    }    
                } else {
                    $car_pics = $user_id[0]['car_pic'] ? $user_id[0]['car_pic'] :'';
                    $certification = $user_id[0]['upload_certificate'] ? $user_id[0]['upload_certificate'] :'';
                    $licenseCopy = $user_id[0]['license_pic'] ? $user_id[0]['license_pic'] :'';
                }
                
                // update driver details      
                $userdata = array(                    
                    'user_name'            => $this->post('user_name'),                    
                    'user_gender'          => $this->post('gender'),
                    'user_address'         => $this->post('address'),
                    'user_birth_date'      => $this->post('dob'),
                    'user_age'             => $this->post('age'), 
                    'is_your_own_car'      => $this->post('own_car'),
                    'driving_license_type' => $this->post('driver_license'), 
                    'license_no'           => $this->post('license_no'),
                    'valid_from_date'      => $this->post('valid_from'),
                    'valid_until_date'     => $this->post('valid_until'),
                    'car_model'            => $this->post('car_model'),
                    'car_register_no'      => $this->post('car_reg_no'),
                    'school'               => $this->post('school_name'),
                    'degree'               => $this->post('degree'),
                    'specialization'       => $this->post('specialization'),
                    'education_from'       => $this->post('from_date'),
                    'education_to'         => $this->post('to_date'),
                    'certification_name'   => $this->post('certification_name'),
                    'user_device_id'       => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                    'user_device_token'    => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                    'user_device_type'     => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                    'license_pic'          => trim($licenseCopy,"|"),
                    'upload_certificate'   => trim($certification,"|"),
                    'car_pic'              => trim($car_pics,"|")
                    );
                $user = $this->db->update(TB_USERS, $userdata, array('user_id' => $this->post('user_id')));

                if ( $user ) {
                    $this->response(array("status" => true, "message" => 'Driver has been updated successfully'), 200); exit;
                } else {
                    $this->response(array("status" => false,"message" => 'Something went wrong'), 200); exit;
                }
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please enter the user id.')); exit;
        }
    }

    

    /**
     * Title        : save_profilePic
     * Description  : Upload user profile picture
     *
     * @param  Integer user_id
     * @param  Integer user_pic
     * @return Array
     */

    public function save_profilePic_post ()
    { 
        // Post data
        $postData = $_POST;

        // Checked user authenticate
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/

        // Checked user exist or not
        if ( $this->user_exists($postData["user_id"]) == 0 ) {
            $this->response(array("status" => false,"message" => 'User is not exists.')); exit;
        }
        
        $user_id = $postData["user_id"];

        // Upload user picture
        if ( isset($_FILES['user_pic']['name']) ) {   

            if ( isset($_FILES['user_pic']) && !empty($_FILES['user_pic']) ) {
                $result = upload_image($_FILES['user_pic'], './' . USER_PROFILE_IMG);
            }        

            if ( isset($result['status']) == "error" ) {
                $this->response(array("status" => false, "message" => strip_tags($result['message']))); exit;               
            } else {
                $profile_data['user_pic'] = $result['filename'];
            }            
        } else {
            $this->response(array("status" => false,"message" => 'User picture do not empty.')); exit;
        }

        // Update user profile picture
        if ( !empty($postData) ) {    

            $where = array("user_id" => $user_id);            
            $update_profile = $this->common_model->update(TB_USERS, $where, $profile_data);

            if ( $update_profile ) {
                $link = base_url().USER_PROFILE_IMG.$profile_data['user_pic'];                
                $arr_result[] = array(                         
                    'message'   => 'User profile pic uploaded successfully.',
                    'url'       => $link,
                    'user_id'   => $user_id
                    );
                // Result of user profile
                $this->response(array("status" => true,'result' => $arr_result), 200); exit;                
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong !!!')); exit;
            }            
        } else {
            $this->response(array("status" => false, "message"=> 'User data can not empty!!!')); exit;
        }        
    }

    /**
     * Title        : user_exists
     * Description  : Check if user exists
     *   
     * @param  Integer user_id
     * @param  Integer user_type
     * @return Integer  
     */

    public function user_exists ( $user_id ,$app_id = '')
    { 
        // Check user available or not
        if($app_id == 0){
            $cond = array("service_user_id" => $user_id, "user_status" => "1");
        }
        else{
            $cond = array("service_provider_id" => $user_id, "sp_status" => "1");
        }
        $exist_users = $this->data_model->getAllUsers($cond,$app_id); 

        // If user available return 1 otherwise 0
        if ( count($exist_users) > 0 ) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Author : Kiran
     * Title : services_list
     * Description : service list
     *
     * @return Array
     */

    public function services_list_get() 
    {
        // Get all services
        $resultServiceList = $this->Common_model->select("service_id, service_desc, unit_of_measurement", TB_SERVICE_MASTER, array("status" => "1"));
        $serviceArr = array();
        // result
        if (count($resultServiceList) > 0) {            
            $i = 0;
            foreach ($resultServiceList as $key => $value) {
                $serviceArr[$i]['id'] = $value['service_id'];
                $serviceArr[$i]['service_name'] = $value['service_desc'];
                $serviceArr[$i]['unit_of_measurement'] = $value['unit_of_measurement'];                         
                $i++;
            }
            $this->response(array('status' => true, 'message' => 'Data found', 'service_list' => $serviceArr), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
        }    
    }


     /**
     * Author : Sanket
     * Title : services Provider List
     * Description : service list
     *
     * @return Array
     */
    public function providerList_get()
    {
        $serviceProviderData = $this->common_model->select_in("*",TB_ROLES,'role_id',array(3,4,7));

        

        if(count($serviceProviderData) > 0)
        {
            
            $this->response(array('status' => true,'message' => 'Data found','provider_list' => $serviceProviderData),200);
        }
        else
        {
             $this->response(array('status' => false,'message' => 'no data found'),200);
        }
    }
    

    /**
    * Ashwini changes here
     * Edit user
     * @param  user_id
     * @return full_name
     * @return contact_no
     * @return address
     * @return address_latlong
     * @return user_dob : if parent
     * @return gender : if parent
     * @return reg_no : if school or organisation
     * @return Array
     */
   /* public function user_edit_post()
    {
        $postData = $this->post();
        $dob='';
        $userAge='';
        $reg_no='';
        $gender = '';
        // if(is_authorized()) {
        //     $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        // }
        if(empty($postData['user_id']))
        {
            $this->response(array('status' => false, 'message' => 'User id is required'), 200); exit;
        }
        if(empty($postData['full_name']))
        {
            $this->response(array('status' => false, 'message' => 'Full name is required'), 200); exit;
        }
        if(empty($postData['contact_no']))
        { 
            $this->response(array("status" => false, "message" => 'Contact no is required.'), 200); exit; 
        }
        if(empty(trim($postData['address'])))
        {
            $this->response(array("status" => false, "message" => 'Address is required.'), 200); exit;
        }
        if(empty(trim($postData['address_latlong'])))
        {
            $this->response(array("status" => false, "message" => 'Address Lat Long is required.'), 200); exit;
        }


        $checkUser = $this->common_model->select("*",TB_USERS,array("user_id"=>trim($postData['user_id'])));
        if(count($checkUser) > 0)
        {
            $user_type = $checkUser[0]['roleId'];
            if($user_type == ROLE_PARENTS || $user_type == SCHOOL || $user_type == ORGANIZATION)
            {            
                if($user_type == ROLE_PARENTS)
                {
                //---- DOB ----
                    if(empty(trim($postData['user_dob'])))
                    {
                        $this->response(array("status" => false, "message" => 'Date of birth field is required.'),200); exit;
                    }else
                    {
                        $dob = trim($postData['user_dob']);
                        $userAge = $this->getAge($dob);
                        if ($userAge < 18)
                        {
                            $this->response(array("status" => false, "message" => 'You are below 18 years so you can\'t register.'), 200); exit;
                        }
                    }

                // ---- GENDER ----
                    if(empty(trim($postData['gender'])))
                    {
                        $this->response(array("status" => false, "message" => 'Gender is required.'), 200); exit;
                    }else
                    {
                        $gender = trim($postData['gender']);
                    }
                }

                if($user_type == SCHOOL || $user_type == ORGANIZATION)
                {
                 if(empty(trim($postData['reg_no'])))
                 {
                    $this->response(array("status" => false, "message" => 'Registration no is required.'), 200); exit;
                }else
                {
                    $reg_no = trim($postData['reg_no']);
                }
            }

            $updtArr = array(
                "user_name"=>trim($postData['full_name']),
                "user_phone_number"=>trim($postData['contact_no']),
                "user_gender"=>$gender,
                "user_birth_date"=>$dob,
                "user_address"=>trim($postData['address']),
                "user_add_lat_lng"=>trim($postData['address_latlong']),
                "reg_no"=>$reg_no
                );
            $result = $this->common_model->update(TB_USERS,array("user_id"=>$postData['user_id']),$updtArr);
            if($result)
            {
             $this->response(array("status" => true, "message" => 'User updated successfully.'), 200); exit; 
         }else
         {
            $this->response(array("status" => false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
        }
    }else
    {
        $this->response(array("status" => false, "message" => "Invalid user."),200);exit;
    }
}else
{
    $this->response(array("status" => false, "message" => 'This user id does not exists.'), 200); exit; 
} 
}*/


	/**
    * Ashwini changes here
     * Edit user only
     * @param  user_id
     * @return full_name
     * @return contact_no
     * @return address
     * @return address_latlong
     * @return user_dob : if parent
     * @return gender : if parent
     * @return reg_no : if school or organisation
     * @return org_name : if school or organisation
     * @return Array
     */
    public function user_edit_post()
    {
        $postData = $this->post();
        $dob='';
        $gender = '';
        $userAge='';
        $org_name='Not Available';
        $reg_no='Not Available';

        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }*/
        if(empty($postData['user_id']))
        {
            $this->response(array('status' => false, 'message' => 'User id is required'), 200); exit;
        }
        if(empty($postData['full_name']))
        {
            $this->response(array('status' => false, 'message' => 'Full name is required'), 200); exit;
        }
        if(empty($postData['contact_no']))
        { 
            $this->response(array("status"=>false, "message" => 'Contact no is required.'), 200); exit; 
        }
        if(empty(trim($postData['address'])))
        {
            $this->response(array("status"=>false, "message" => 'Address is required.'), 200); exit;
        }
        if(empty(trim($postData['address_latlong'])))
        {
            $this->response(array("status"=>false, "message" => 'Address Lat Long is required.'), 200); exit;
        }


        $checkUser = $this->common_model->select("*",TB_SERVICE_USERS,array("service_user_id"=>trim($postData['user_id'])));
        if(count($checkUser) > 0)
        {
            $user_type = $checkUser[0]['user_type_id'];
            if($user_type == ROLE_PARENTS || $user_type == SCHOOL || $user_type == ORGANIZATION)
            {          
                if($user_type == ROLE_PARENTS)
                {
                //---- DOB ----
                    if(empty(trim($postData['user_dob'])))
                    {
                        $this->response(array("status"=>false, "message" => 'Date of birth field is required.'),200); exit;
                    }else
                    {
                        $dob = trim($postData['user_dob']);
                        $userAge = $this->getAge($dob);
                        if ($userAge < 18)
                        {
                            $this->response(array("status"=>false, "message" => 'You are below 18 years so you can\'t register.'), 200); exit;
                        }
                    }

                // ---- GENDER ----
                    if(empty(trim($postData['gender'])))
                    {
                        $this->response(array("status"=>false, "message" => 'Gender is required.'), 200); exit;
                    }else
                    {
                        $gender = trim($postData['gender']);
                    }
                }

                if($user_type == SCHOOL || $user_type == ORGANIZATION)
                {
                    if(empty(trim($postData['reg_no'])))
                    {
                        $this->response(array("status"=>false, "message" => 'Registration no is required.'), 200); exit;
                    }else
                    {
                        $org_name = trim($postData['org_name']);
                    }
                    if(empty(trim($postData['org_name'])))
                    {
                        $this->response(array("status"=>false, "message" => 'Organization name is required.'), 200); exit;
                    }else
                    {
                        $org_name = trim($postData['org_name']);
                    }
                }

                $updtArr = array(
                    "user_fullname"=>trim($postData['full_name']),
                    "organization_name"=>$org_name,
                    "user_phone_number"=>trim($postData['contact_no']),
                    "user_gender"=>$gender,
                    "user_birth_date"=>$dob,
                    "user_address"=>trim($postData['address']),
                    "user_address_latlong"=>trim($postData['address_latlong']),
                    "registration_no"=>$reg_no
                    );
                $result = $this->common_model->update(TB_SERVICE_USERS,array("service_user_id"=>$postData['user_id']),$updtArr);
                if($result)
                {
                 $this->response(array("status"=>true, "message" => 'User updated successfully.'), 200); exit; 
             }else
             {
                $this->response(array("status"=>false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
            }
        }else
        {
            $this->response(array("status"=>false, "message" => "Invalid user."),200);exit;
        }
    }else
    {
     $this->response(array("status"=>false, "message" => 'This user id does not exists.'), 200); exit; 
 } 
}







    /**
     * Title: kid_edit
     * Description : Add/update kids details
     * 
     * @param String kid_name
     * @param String kid_gender
     * @param Integer user_id
     * @param Date dob
     * @param kid_id (To edit)
     * @return Array 
     */

    public function kid_edit_post ()
    {
        // Input data
        $data = $_POST;
        // Checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/
        
        // Input validation 
        if ( $this->post('kid_name') == "" ) {
            $this->response(array("status" => "error", "message" => 'Child name field is required.')); exit; 
        }    
        if ( $this->post('kid_gender') == "" ) {
            $this->response(array("status" => "error", "message" => 'Gender field is required.')); exit; 
        }    
        if ( $this->post('user_id') == "" ) { 
            $this->response(array("status" => "error", "message" => 'Parent id field is required.')); exit; 
        }    
        if ( $this->post('dob') == "" ) { 
            $this->response(array("status" => "error", "message" => 'Child date of birth field is required.')); exit; 
        }    
        
        $dateOfBirth = $this->post('dob');
        $today       = date("Y-m-d");
        $diff        = date_diff(date_create($dateOfBirth), date_create($today));
        $age         = $diff->format('%y');
        if ( 5 > $age || 15 < $age ) { 
            $this->response(array("status" => false, "message" => 'Child age should be 5 to 15 years allowed.')); exit; 
        }    
        
        // Update kids details
        $kidData = array(
            'kid_name'     => $this->post('kid_name'),
            'kid_gender'   => $this->post('kid_gender'),
            'kid_age'      => $age." Year",
            'user_id'      => $this->post('user_id'),   
            'kid_birthdate'=> $this->post('dob'),     
            'kid_status'   => 1,
            );

        if ( isset($data['user_id']) && $data['user_id'] > 0 ) {            
            $result = $this->common_model->select_join("user_id, role", TB_USERS, array("user_id" => trim($data['user_id'])), array(), array(), array(TB_ROLES => 'tbl_users.roleId = tbl_roles.roleId'), null);
            /*echo $this->db->last_query();die;
            echo "<pre>";print_r($result);die;*/
            if ( !count($result) ) {
                $this->response(array('status' => false, 'message' => 'Invalid user')); exit;
            }
            
            if ( isset($data['kid_id']) && $data['kid_id'] > 0 ) {
                try {
                    $this->common_model->update(TB_KIDS, array("kid_id" => $data['kid_id']), $kidData);
                    $this->response(array('status' => true, 'message' => 'Kid updated successfully.'), 200); exit;
                } catch (Exception $ex) {
                    $this->response(array('status' => false, 'message' => $ex->getCode())); exit;
                }
            } else {
                try {
                    $this->common_model->insert(TB_KIDS, $kidData);
                    $this->response(array('status' => true, 'message' => 'Kid saved successfully.'), 200); exit;
                } catch (Exception $ex) {
                    $this->response(array('status' => false, 'message' => $ex->getCode())); exit;
                }
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Provide user_id')); exit; 
        }
    }

    /**
     * Title : validate_booking
     * Description : Booking a ride validation
     *
     * @param type $post
     * @return Array
     */

    function validate_booking ( $post ) 
    {        
        $bookingdata = [];
        $duration    = array(DAILY,WEEKLY,MONTHLY);
        $today       = date('Y-m-d');
        if ( $today > trim($post['start_date']) ) {
            $this->response(array('status' => false, 'message' => 'You can book today onwards.')); exit;
        } 
        if ( trim($post['start_date']) == "" ) {
            $this->response(array('status' => false, 'message' => 'Please enter duration daily or weekly or monthly')); exit;
        }
        if ( trim($post['duration']) == "" ) {
            $this->response(array('status' => false, 'message' => 'Please enter duration daily or weekly or monthly')); exit;
        }
        if ( !in_array(trim($post['duration']),$duration) ) {
            $this->response(array('status' => false, 'message' => $post['duration'].' Invalid duration')); exit;
        }
        if ( trim($post['booking_service_type']) == MY_PREFERENCE && (!isset($post['booking_my_preference_service_users']) || !is_array($post['booking_my_preference_service_users'])) ) {
            $this->response(array('status' => false, 'message' => 'You must provide an booking_my_preference_service_users in array format.')); exit;
        }
        if ( trim($post['booking_service_type']) == MY_MATCHES && (isset($post['booking_service_user']) && $post['booking_service_user'] < 1) ) {
            $this->response(array('status' => false, 'message' => 'You must provide an booking_service_user.')); exit;
        }
        if ( $post['duration'] == DAILY && strtotime($post['date']) < strtotime(date('Y-m-d')) ) {
            $this->response(array('status' => false, 'message' => 'Please enter date greater than today and date format is Y-m-d(2018-01-31)')); exit;
        }
        if ( ($post['duration'] == WEEKLY || $post['duration'] == MONTHLY) && ((strtotime($post['start_date']) < strtotime(date('Y-m-d'))) || (strtotime($post['start_date']) > strtotime($post['end_date'])) || (strtotime($post['end_date']) < strtotime($post['start_date']))) ) {
            $this->response(array('status' => false, 'message' => 'Please enter valid start and end date and date format is Y-m-d(2018-01-31)'));exit;
        }
        $user = $this->common_model->select("user_status, user_type, roleId", TB_USERS,array('user_id' => $post['user_id']));
        if ( !$user ) {
            $this->response(array('status' => false, 'message' => 'Invalid user')); exit;
        }

        $service_provider = $this->common_model->select("user_id",TB_USERS,array('user_id' => $this->post('service_provider_user_id'), 'roleId' => ROLE_SERVICE_PROVIDER, 'user_status' => '1'));
        
        if ( !$service_provider ) {
            $this->response(array('status' => false, 'message' => 'Invalid service provider, please enter valid service_provider_user_id.')); exit;
        }

        if ( $user[0]['user_status'] != 1 ) {
            $this->response(array('status' => false, 'message' => 'User not active')); exit;
        }
        if ( $user[0]['roleId'] != USER_TYPE_PARENT ) {
            $this->response(array('status' => false, 'message' => 'User type not parent')); exit;
        }

        $catdata = $this->common->validate_category($post);
        if ( trim($post['duration']) == DAILY ) {
            $bookingdata['booking_date']       = date('Y-m-d', strtotime($post['date']));
            $bookingdata['booking_total_days'] = 1;
        } else {
            $bookingdata['booking_start_date'] = date('Y-m-d', strtotime($post['start_date']));
            $bookingdata['booking_end_date']   = date('Y-m-d', strtotime($post['end_date']));
            $start_date                        = $post['start_date'];
            $datetime1                         = new DateTime($post['start_date']);
            $datetime2                         = new DateTime($post['end_date']);
            $interval                          = date_diff($datetime1, $datetime2);
            
            $bookingdata['booking_total_days'] = $interval->days;
            if ($post['duration'] == 'weekly') {
                $bookingdata['booking_total_weeks'] = ceil(($interval->days) / 7);
            }
            if ($post['duration'] == 'monthly') {
                $bookingdata['booking_total_moths'] = ($interval->format('%m')) + 1;
            }
        }
        
        $bookingdata['booking_pick_up_location']  = $post['pick_up_location'];
        $bookingdata['pick_up_location_lat']      = $post['pick_up_location_lat'];
        $bookingdata['pick_up_location_lng']      = $post['pick_up_location_lng'];
        $bookingdata['booking_drop_off_location'] = $post['drop_off_location'];
        $bookingdata['drop_off_location_lat']     = $post['drop_off_location_lat'];
        $bookingdata['drop_off_location_lng']     = $post['drop_off_location_lng'];
        $bookingdata['booking_pick_up_time']      = $post['pick_up_time'];
        $bookingdata['booking_drop_off_time']     = $post['drop_off_time'];
        $bookingdata['booking_category_id']       = $catdata['category_id'];
        $bookingdata['booking_sub_category_id']   = isset($catdata['sub_category_id']) ? $catdata['sub_category_id'] : '';
        // $bookingdata['booking_secret_code']    = $post['secret_code'];
        $bookingdata['booking_secret_code']       = '';
        $bookingdata['booking_duration']          = $post['duration'];
        $bookingdata['user_id']                   = $post['user_id'];
        $booking_service_type                     = json_decode(SERVICE_TYPES, true);
        $bookingdata['booking_service_type']      = $post['booking_service_type'] && in_array($post['booking_service_type'], $booking_service_type) ? $post['booking_service_type'] : PLACE_REQUEST;
        $bookingdata['booking_no_off_kids']       = count($post['child_id']);
        
        return $bookingdata;
    }

     /**
     * Title : booking
     * Description : user book a ride
     *
     * @param Array user_kids['name']
     * @param Array user_kids['gender']
     * @param Array user_kids['age']
     * @param Date start_date
     * @param Date end_date
     * @param String pick_up_location
     * @param String drop_off_location
     * @param Time pick_up_time
     * @param Time drop_off_time
     * @param Integer category
     * @param Integer sub_category
     * @param String secret_code
     * @param String duration
     * @param String booking_service_type
     * @param Integer user_id
     * @param Integer booking_service_user
     * @param Integer booking_my_preference_service_users
     * @param Integer service_provider_user_id
     * @param Date date
     * @param Double pick_up_location_lat
     * @param Double pick_up_location_lng
     * @param Double drop_off_location_lat
     * @param Double drop_off_location_lng
     * @return Array
     */

     function booking_post () 
     {
        // Input data
        $data = $this->post();  

        // checked user is authenticate
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */     

        // booking input field validation
        $bookingdata = $this->validate_booking($data);
        
        if ( $bookingdata ) {

            // Insert ride booking details
            $booking = $this->db->insert(TB_BOOKINGS, $bookingdata);
            $booking_id = $this->db->insert_id();

            if ( $booking_id ) {                
                $booking_service_user = '';
                if ( $this->post('booking_service_type') == MY_PREFERENCE ) {
                    $booking_service_user = $this->post('booking_my_preference_service_users');
                }
                if ( $this->post('booking_service_type') == MY_MATCHES ) {
                    $booking_service_user = $this->post('booking_service_user');
                }

                // kids info details insert
                if ( $this->post('child_id') ) {

                    $user_kids = $this->post('child_id');
                    $kids_all  = array(
                        'kids_id'    => isset($user_kids) ? $user_kids : '',
                        'booking_id' => $booking_id
                        );               
                    $this->db->insert(TB_KIDS_BOOKING, $kids_all);
                }

                $respone['booking_status']['success']    = 'Booking has been saved successfully';
                $respone['booking_status']['booking_id'] = $booking_id;               
                
                // ride request insert
                $service_request = array(
                    "service_booking_id"       => $booking_id,
                    "service_parent_user_id"   => $this->post('user_id'),
                    "service_provider_user_id" => $this->post('service_provider_user_id'),
                    "service_request_status"   => PENDING
                    );
                $result = $this->db->insert(TB_SERVICE_REQUESTS, $service_request);

                if (!$result) {
                    $respone['message'] = 'Request not send to driver, please try again!!!';
                } else {
                    $userData  = $this->common_model->select('user_id,user_name',TB_USERS,array('user_id' => $this->post('user_id')));
                    $userName  = isset($userData[0]['user_name'])?$userData[0]['user_name']:"Someone";
                    $message   = $userName." has booked a ride"; 
                    $service_provider_device_id = $this->common_model->select("user_device_id, roleId", TB_USERS, array("user_id" => $this->post('service_provider_user_id')));
                    $device_id = isset($service_provider_device_id[0]['user_device_id'])?$service_provider_device_id[0]['user_device_id']:"";
                    $myData    = array("message" => "Booking the ride please check it.");
                    $this->sendPushNotification($device_id, $message, $myData, $service_provider_device_id[0]['roleId']);

                    $checkAlreadyMyPref = $this->common_model->select("mp_id, trip_count", TB_MY_PREFERENCE, array("parent_id" => $this->post('user_id'), "driver_id" => $this->post('service_provider_user_id')));
                    if ( count($checkAlreadyMyPref) > 0 ) {
                        $my_preference = array(
                            "trip_count" => $checkAlreadyMyPref[0]['trip_count']+1,
                            "updated_at" => date('Y-m-d H:i:s')
                            );
                        $result = $this->common_model->update(TB_MY_PREFERENCE,array("parent_id"=>$this->post('user_id'),"driver_id"=>$this->post('service_provider_user_id')),$my_preference);
                    } else {
                        $my_preference = array(
                            "parent_id"  => $this->post('user_id'),
                            "driver_id"  => $this->post('service_provider_user_id'),
                            "trip_count" => 1,
                            "created_at" => date('Y-m-d H:i:s')
                            );
                        $result = $this->db->insert(TB_MY_PREFERENCE, $my_preference);
                    }                    
                    $respone['message'] = 'Request has been sent successfully';
                } 
                $this->response(array('status' => true, 'message' => $respone), 200); exit;
            }
        }
        $this->response(array('status' => false, 'message' => 'Some error has been occurred!!!')); exit;
    }

    /**
     * Title : my_bookings
     * Description : booking list show
     *
     * @param  Integer user_id
     * @return Array 
     */

    function my_bookings_post() 
    {
        // Checked user is authenticate or not
        /*if(is_authorized()) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/   

        // Input data
        $user_id = $_POST['user_id'];
        
        if ( $user_id > 0 ) {
            $post    = $this->post();       
            $catdata = $this->common->validate_category($this->post());
            $cond    = ['bookings.booking_category_id' => $catdata['category_id']];
            if ( isset($catdata['sub_category_id']) && $catdata['sub_category_id'] > 0 ) {
                $cond['bookings.booking_sub_category_id'] = $catdata['sub_category_id'];
            }
            $user = $this->common_model->select("user_status, user_type, roleId", TB_USERS, array('user_id' => $post['user_id']));
            
            if ( count($user) > 0 ) {
                if ( $user[0]['roleId'] == USER_TYPE_SERVICE_PROVIDER ) {
                    $like       = array();
                    $conds      = array(TB_BOOKINGS.".user_id" => $user_id,"booking_status" => $this->post('booking_status'));
                    $select     = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id, kid_id, kid_name, kid_gender, kid_birthdate,".TB_CATEGORIES.".category,".TB_SUBCATEGORIES.".name,kid_age,kid_status";
                    $table_name = TB_BOOKINGS;
                    $join = array(
                        TB_CATEGORIES    => TB_CATEGORIES . '.category_id=' . TB_BOOKINGS . '.booking_category_id',
                        TB_SUBCATEGORIES => TB_SUBCATEGORIES . '.sub_category_id=' . TB_BOOKINGS . '.booking_sub_category_id',
                        TB_KIDS          => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING  => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id'
                        );
                    $default_order_column = "created_at";
                    $service_requests     = $this->common_model->getRowsPerPage($select, $table_name, $conds, $like, array(), '', $join);
                    
                    // Show list of booking details
                    foreach ( $service_requests as $key => $service_request ) {
                        $data[$key]['booking_id']                = $service_request['booking_id'];
                        $data[$key]['booking_duration']          = $service_request['booking_duration'];
                        $data[$key]['booking_date']              = $service_request['booking_date'];
                        $data[$key]['booking_total_days']        = $service_request['booking_total_days'];
                        $data[$key]['booking_pick_up_location']  = $service_request['booking_pick_up_location'];
                        $data[$key]['pick_up_location_lat']      = $service_request['pick_up_location_lat'];
                        $data[$key]['pick_up_location_lng']      = $service_request['pick_up_location_lng'];
                        $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                        $data[$key]['drop_off_location_lat']     = $service_request['drop_off_location_lat'];
                        $data[$key]['drop_off_location_lng']     = $service_request['drop_off_location_lng'];
                        $data[$key]['booking_pick_up_time']      = $service_request['booking_pick_up_time'];
                        $data[$key]['booking_drop_off_time']     = $service_request['booking_drop_off_time'];
                        $data[$key]['booking_category_id']       = $service_request['booking_category_id'];
                        $data[$key]['booking_sub_category_id']   = $service_request['booking_sub_category_id'];
                        $data[$key]['booking_no_off_kids']       = $service_request['booking_no_off_kids'];
                        $data[$key]['booking_price']             = $service_request['booking_price'];
                        $data[$key]['booking_total_weeks']       = $service_request['booking_total_weeks'];
                        $data[$key]['booking_total_moths']       = $service_request['booking_total_moths'];
                        $data[$key]['booking_status']            = $service_request['booking_status'];
                        $data[$key]['user_id']                   = $service_request['user_id'];
                        $data[$key]['booking_service_type']      = $service_request['booking_service_type'];
                        $data[$key]['created_at']                = $service_request['created_at'];
                        $data[$key]['category']                  = $service_request['category'];
                        $data[$key]['sub_category']              = $service_request['name'];
                        $data[$key]['kid_id']                    = $service_request['kid_id'];
                        $data[$key]['kid_name']                  = $service_request['kid_name'];
                        $data[$key]['kid_gender']                = $service_request['kid_gender'];
                        $data[$key]['kid_birthdate']             = $service_request['kid_birthdate'];
                        $data[$key]['kid_age']                   = $service_request['kid_age'];
                    }
                } else {
                    $cond['user_id']                 = $user_id;
                    $cond['bookings.booking_status'] = $this->post('booking_status');
                    
                    $like       = array();
                    $conds      = array(TB_BOOKINGS.".user_id" => $user_id, "booking_status" => $this->post('booking_status'));
                    $select     = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id,kid_id,kid_name,kid_gender,kid_birthdate,kid_age,kid_status";
                    $table_name = TB_BOOKINGS;
                    $join = array(
                        TB_KIDS         => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id'
                        );
                    
                    // Fetch records
                    $default_order_column = "created_at";
                    $service_requests = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array(),'',$join);
                    
                    // show list of data 
                    $data = array();
                    foreach ($service_requests as $key => $service_request) {
                        $data[$key]['booking_id']                = $service_request['booking_id'];
                        $data[$key]['booking_duration']          = $service_request['booking_duration'];
                        $data[$key]['booking_date']              = $service_request['booking_date'];
                        $data[$key]['booking_total_days']        = $service_request['booking_total_days'];
                        $data[$key]['booking_pick_up_location']  = $service_request['booking_pick_up_location'];
                        $data[$key]['pick_up_location_lat']      = $service_request['pick_up_location_lat'];
                        $data[$key]['pick_up_location_lng']      = $service_request['pick_up_location_lng'];
                        $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                        $data[$key]['drop_off_location_lat']     = $service_request['drop_off_location_lat'];
                        $data[$key]['drop_off_location_lng']     = $service_request['drop_off_location_lng'];
                        $data[$key]['booking_pick_up_time']      = $service_request['booking_pick_up_time'];
                        $data[$key]['booking_drop_off_time']     = $service_request['booking_drop_off_time'];
                        $data[$key]['booking_category_id']       = $service_request['booking_category_id'];
                        $data[$key]['booking_sub_category_id']   = $service_request['booking_sub_category_id'];
                        $data[$key]['booking_no_off_kids']       = $service_request['booking_no_off_kids'];
                        $data[$key]['booking_price']             = $service_request['booking_price'];
                        $data[$key]['booking_total_weeks']       = $service_request['booking_total_weeks'];
                        $data[$key]['booking_total_moths']       = $service_request['booking_total_moths'];
                        $data[$key]['booking_status']            = $service_request['booking_status'];
                        $data[$key]['user_id']                   = $service_request['user_id'];
                        $data[$key]['booking_service_type']      = $service_request['booking_service_type'];
                        $data[$key]['created_at']                = $service_request['created_at'];
                        $data[$key]['kid_id']                    = $service_request['kid_id'];
                        $data[$key]['kid_name']                  = $service_request['kid_name'];
                        $data[$key]['kid_gender']                = $service_request['kid_gender'];
                        $data[$key]['kid_birthdate']             = $service_request['kid_birthdate'];
                        $data[$key]['kid_age']                   = $service_request['kid_age'];
                    }
                }

                $result['booking_details'] = $data;
                $this->response(array('status' => true, 'result' => $result),200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid user_id')); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide user_id')); exit;
        }
    }
    
    /*
     * Title : booking_status
     * Description : booking list of status
     * 
     * @param Integer service_request_id
     * @param String status
     * @return Array 
     */
    function booking_status_post () 
    {
        // checked user is authenticate or not 
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        $service_request_id = $_POST['service_request_id'];

        if ( $service_request_id > 0 ) {
            // Check service request is exist or not             
            $service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id));
            
            if ( !$service_requests ) {
                $this->response(array('status' => false, 'message' => 'Service request not available')); exit;
            }

            //Accept service request
            if ( $_POST['status'] == ACCEPTED ) {
                //Check that booking already started by anouther one 
                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>IN_PROCESS));

                //Check another service not started in that duration
                //hold for the moment
                $validbooking = 1;
                
                if ( $validbooking ) {

                    // update records
                    if ( count($is_service_requests) < 1 ) {
                        //Start that request                        
                        $accept_service =  $this->common_model->update(TB_SERVICE_REQUESTS, array("service_request_id" => $service_request_id, "service_request_status" => PENDING), array('service_request_status' => IN_PROCESS));
                        $accept_booking =  $this->common_model->update(TB_BOOKINGS, array("booking_id" => $service_request_id, "booking_status" => PENDING), array('booking_status' => IN_PROCESS));
                        
                        if ( $accept_service && $accept_booking ) {
                            $this->response(array('status' => true, 'message' => 'Service has been accepted successfully'),200); exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!')); exit;
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Service already accepted')); exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'You can\'n able to start this request, because already another booking is in process in that duration')); exit;
                }
            }

            //cancel service request
            if ( $_POST['status'] == CANCELLED ) {
                $is_service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_booking_id' => $service_request_id, 'service_request_status' => CANCELLED));
                
                // update records                
                if ( count($is_service_requests) < 1 ) {                    
                    $cancel_service =  $this->common_model->update(TB_SERVICE_REQUESTS, array("service_request_id" => $service_request_id), array('service_request_status' => CANCELLED));
                    $cancel_booking =  $this->common_model->update(TB_BOOKINGS, array('booking_id' => $service_request_id), array('booking_status' => CANCELLED));
                    
                    if ( $cancel_service & $cancel_booking ) {
                        $this->response(array('status' => true, 'message' => 'Service has been cancelled successfully'),200); exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!')); exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service already cancelled')); exit;
                }
            }

            // Start service request
            if ( $_POST['status'] == START ) {
                $is_service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id, 'service_request_status' => START));
                
                // update records
                if ( count($is_service_requests) > 0 ) {
                    $this->response(array('status' => false, 'message' => 'Service already started.')); exit;
                } else {                                  
                    $start_service = $this->common_model->update(TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id), array('service_request_status' => START));           
                    if ( $start_service ) {
                        $this->response(array('status' => true, 'message' => 'Service has been started successfully'), 200); exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!')); exit;
                    }
                }
            }

            // End service request
            if ( $_POST['status'] == END ) {
                $is_service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id, 'service_request_status' => END));
                
                // update records
                if ( count($is_service_requests) > 0 ) {
                    $this->response(array('status' => false, 'message' => 'Service already end.')); exit;
                } else{                                  
                    $end_service = $this->common_model->update(TB_SERVICE_REQUESTS, array('service_request_id' => $service_request_id), array('service_request_status' => END));  
                    $end_booking = $this->common_model->update(TB_BOOKINGS, array('booking_id' => $service_request_id), array('booking_status' => COMPLETED));        
                    if ( $end_service && $end_booking ) {
                      $this->response(array('status' => true, 'message' => 'Service has been ended successfully'),200); exit;
                  } else {
                    $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!')); exit;
                }
            }
        }
    } else {
        $this->response(array('status' => false, 'message' => 'Please provide service_request_id')); exit;
    }
}

    /**
     * Title : slider_list 
     * Descriptin : show the list of slider images
     *
     * @return Array
     */
    function slider_list_get ()
    {
        // checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'),200); exit;
        } */

        $sliderList = $this->common_model->select("id, title, banner_img", TB_BANNER, array("banner_status" => 1));
        $data = array();

        // list of slider 
        if ( count($sliderList) > 0 ) {

            foreach ( $sliderList as $key => $slider ) {
                $data[$key]['id']         = $slider['id'];
                $data[$key]['title']      = $slider['title'];
                $data[$key]['banner_img'] = base_url().$slider['banner_img'];                    
            }
            $this->response(array('status' => true, 'slider_list' => $data),200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'no slider images are available ')); exit;
        }
    }

    /**
    Currently not in use, may be change according to new scope
     * Title : start_or_end_service
     * Description : booking service start or end
     *
     * @param Integer service_request_id
     * @param Integer status
     * @return Array
     */
    function start_or_end_service_post () 
    {
        // Checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        // Input data
        $postData = $_POST;
        if ( $postData['service_request_id'] > 0 ) {
            // Check service request is exist or not 
            $service_requests = $this->common_model->select("*", TB_SERVICE_REQUESTS, array('service_request_id' => $postData['service_request_id'], 'service_request_status' => IN_PROCESS));
            if ( !$service_requests ) {
                $this->response(array('status' => false, 'message' => 'Service request not available')); exit;
            }

            //Start service request
            if ( $postData['status'] == START || $postData['status'] == END ) {
                $checkstatus = AVAILABLE;
                $statuname   = 'ended';

                if ($postData['status'] == START) {
                    $checkstatus = NOT_AVAILABLE;
                    $statuname   = 'started';
                }

                //Check that sp already started service
                $is_service_requests = $this->common_model->select("*",TB_USERS,array('user_id' => $service_requests[0]['service_provider_user_id'], 'is_user_available' => $checkstatus));
                if ( count($is_service_requests) < 1 ) {
                    //Update that service provider to not available
                    $update = $this->common_model->update(TB_USERS, array('user_id' => $service_requests[0]['service_provider_user_id']), array('is_user_available' => $checkstatus));
                    
                    if ( $update ) {

                        if ( $postData['status'] == END ) {
                            //if end service complete that booking
                            $this->common_model->update(TB_SERVICE_REQUESTS, array('service_request_id' => $postData['service_request_id']), array('service_request_status' => COMPLETED));
                            $this->common_model->update(TB_BOOKINGS, array('booking_id' => $service_requests[0]['service_booking_id']), array('booking_status' => COMPLETED));
                        }
                        $this->response(array('status' => true, 'message' => 'Service ' . $statuname . ' successfully'),200); exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!')); exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service ' . $statuname . ' already')); exit;
                }
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid status')); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id')); exit;
        }
    }

    

    /**
     * Title : read_notification
     * Description : List of read notification 
     *
     * @param Integer userId
     * @param Integer notifId
     * @return Array 
     */

/*    function read_notification_post() 
    {


        // Input data
        $postData = $_POST;

        // Get list of data
        if ( $postData['userId'] != "" && $postData['notifId'] != "" ) {
            $checkNotif = $this->common_model->select("n_id", TB_NOTIFICATION, array('user_id' => $postData['userId'], 'n_id' => $postData['notifId']));
            
            if ( count($checkNotif) > 0 ) {
             $updateNotif = $this->common_model->update(TB_NOTIFICATION, array('user_id' => $postData['userId'], 'n_id' => $postData['notifId']), array('n_read' => "0"));
             
             if($updateNotif) {
                $this->response(array('status' => true, 'message' => 'The notification has been read successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'You have already read notification.')); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.')); exit;
        }           
    } else {
        $this->response(array('status' => false, 'message' => 'All fields are required. ')); exit;
    }        
}*/

    /**
     * Title       : notification_list
     * Description : Notification list
     *
     * @param  Integer user_id
     * @param  Integer notifId
     * @return Array
     */
/*
    function notification_listing_post() 
    {
      

        // post data
        $postData = $_POST;
        //---- Ashwini changes here ----
        if(empty($postData['user_id']))
        {
            $this->response(array('status' => false, 'message' => 'User id is required.'), 200); exit;
        }


        if ( $postData['user_id'] != "" && $postData['notifId'] != "" ) {
            // Get specific record 
            $checkNotif = $this->common_model->select("n_content, user_id, n_id", TB_NOTIFICATION, array('user_id' => $postData['user_id'], 'n_id' => $postData['notifId'], 'n_read' => "1"));
            
            // result
            if ( $checkNotif ) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.', 'notification_list' => $checkNotif), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
            }                 
        } else {
            // Get all notification list
            $checkNotif = $this->common_model->select("n_content,user_id,n_id",TB_NOTIFICATION,array('n_read' => "1","user_id"=>$postData['user_id']));
            
            // result
            if (count($checkNotif) > 0) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.','notification_list' => $checkNotif), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
            }            
        }     
    }*/
    /**
     * Title : faq_list
     * Description : FAQ list
     * 
     * @return Array
     */
    function faq_list_get() 
    {
        // Checked user is authenticate or not
        /*if( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        } */
        
        // Get all record of FAQ list
        $cond     = array(TB_CATEGORIES.'.status' => 1, TB_SUBCATEGORIES.'.status' => 1);
        $jointype = array(TB_CATEGORIES => "LEFT",TB_SUBCATEGORIES => "LEFT");
        $join     = array(TB_CATEGORIES => TB_CATEGORIES.".category_id = ".TB_FAQ.".category_id", TB_SUBCATEGORIES => TB_SUBCATEGORIES.".sub_category_id = ".TB_FAQ.".subcategory_id");
        $faqList  = $this->Common_model->selectJoin("id, name, category, faq_description, faq_title", TB_FAQ, $cond, array(), $join, $jointype);
        $data = array();           
        
        if ( count($faqList) > 0 ) {
            foreach ($faqList as $key => $faq) {
                $data[$key]['id']              = $faq['id'];
                $data[$key]['categoryName']    = $faq['category'];
                $data[$key]['subCategoryName'] = $faq['name'];
                $data[$key]['description']     = $faq['faq_description'];
                $data[$key]['title']           = $faq['faq_title'];
            }
            // result
            $this->response(array('status' => true, 'message' => 'FAQ list.','faq_list' => $data), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Faq data not available.')); exit;
        }       
    }

    /**
    * Jyoti k
     * Title       :  faq list user wise 
     * @param Int user_id
     * @param Int app_id
     * @return Array 
     */
    public function faqlist_userwise_post()
    {
        $postData = $this->post();

        if(trim(empty($postData['user_id'])))
        {
            $this->response(array("status"=>false,"message"=>"User id is required."));
            exit;
        }
        if($postData['user_id'] == ''){
             $this->response(array("status"=>false,"message"=>"App id is required."));
            exit;
        }
        if($app_id == '0'){
            $role_id = $this->common_model->select("user_type_id",TB_SERVICE_USERS,array("service_user_id"=>trim($postData['user_id'])));
            if($role_id){
                $faqlist=$this->common_model->select("*",TB_FAQ,array("role_id"=>$role_id[0]['user_type_id']));
            }
            else{
               $this->response(array("status"=>false,"message"=>"User id not exist."));
               exit;
            }
        }
        else{
            $role_id = $this->common_model->select("sp_user_type",TB_SERVICE_PROVIDER,array("service_provider_id"=>trim($postData['user_id'])));
            if($role_id){
                $faqlist=$this->common_model->select("*",TB_FAQ,array("role_id"=>$role_id[0]['sp_user_type']));
            }
            else{
               $this->response(array("status"=>false,"message"=>"User id not exist."));
               exit;
            }
        }
            if($faqlist){
              $this->response(array("status"=>true,"message"=>"Data found","faq_list"=>$faqlist));
            }
            else{
              $this->response(array("status"=>false,"message"=>"Data not found"));
            }
    }
        
   


    /**
     * Title : tutoring_list
     * Description : tutoring list details
     * 
     * @return Array
     */

    function tutoring_list_get () 
    {
        // Checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        // Get all data
        $cond    = array();       
        $join    = array(TB_TEACHERS => TB_TEACHERS.".id = ".TB_SUBJECT_MASTER.".tutor_id");
        $subList = $this->Common_model->select_join(TB_TEACHERS.".id, sub_id, teacher_name", TB_SUBJECT_MASTER, $cond, array(), array(), $join);
        $data    = array();

        if ( count($subList) > 0 ) {

            foreach ( $subList as $key => $sub ) {
                $subArray = explode(',', $sub['sub_id']);
                $subjects = $this->Common_model->select_where_in("id,sub_name",TB_SUBJECTS,array(),$subArray);
                $data[$sub['teacher_name']]=$subjects;
            }
            $this->response(array('status' => true, 'teacher_list' => $data),200);exit;
        } else {
            $this->response(array('status' => false, 'message' => 'No data are available '));exit;
        }      
    }

    /**
    Currently not in use, may be change according to new scope
     * Title : service_list
     * Description : service list
     * 
     * @param Integer category_id
     * @param Integer sub_category_id
     * @param Integer status
     * @return Array
     */

    public function getServiceList_post () 
    {
        // checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        // input data
        $postData = $_POST;

        if ( ($postData['category_id'] > 0) && ($this->input->post('status') != "") && ($postData['sub_category_id'] > 0) ) {
            // Check category request is exist or not 
            $category_requests = $this->common_model->select("category_id",TB_CATEGORIES,array('category_id' => $postData['category_id'], 'status' => '1'));
            
            if ( !$category_requests ) {
                $this->response(array('status' => false, 'message' => 'Category request not available')); exit;
            } else {

                // Check sub category request is exist or not 
                $sub_category_requests = $this->common_model->select("sub_category_id", TB_SUBCATEGORIES, array('sub_category_id' => $postData['sub_category_id'], 'status' => '1'));
                if ( !$sub_category_requests ) {
                    $this->response(array('status' => false, 'message' => 'Sub category request not available')); exit;
                } else {

                    if( $postData['status'] != "" ) {
                        // Check status request is exist or not 
                        $status_requests = $this->common_model->select("booking_id", TB_BOOKINGS, array('booking_status' => $postData['status']));
                        if ( !$status_requests ) {
                            $this->response(array('status' => false, 'message' => 'Status request not available')); exit;
                        } else {

                            $result = $this->common_model->selectJoin(TB_USERS.".user_id, booking_id, user_name, user_pic", TB_USERS, array("booking_status" => trim($postData['status']), "booking_category_id" => trim($postData['category_id']), "booking_sub_category_id" => trim($postData['sub_category_id'])), array(), array(TB_BOOKINGS => 'tbl_users.user_id = tbl_bookings.user_id'));
                            if( count($result) > 0 ) {
                                $this->response(array('status' => true, 'service_list' => $result), 200); exit;
                            } else {
                                $this->response(array('status' => false, 'message' => 'Data not found')); exit;
                            }
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Status is required')); exit;
                    }
                }
            }            
        } else {
           $this->response(array('status' => false, 'message' => 'Please provide category id, sub category id & status')); exit;
       }        
   }


    /**
     * Title : getServiceDetail     
     * Description : service details
     * 
     * @param Integer user_id
     * @param Integer booking_id
     * @return Array
     */

    public function getServiceDetail_post () 
    {
        // Input data
        $postData = $_POST;

        // Checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/ 

        // Checke input not empty
        if ( $postData['booking_id'] != ""   && $postData['user_id'] != "" ) {

            $result = $this->common_model->selectJoin(TB_USERS.".user_id, user_name, user_phone_number, booking_pick_up_location, booking_drop_off_location, booking_pick_up_time, booking_drop_off_time, booking_duration, booking_id", TB_USERS, array(TB_USERS.".user_id" => trim($postData['user_id']), "booking_id" => $postData['booking_id']), array(), array(TB_BOOKINGS => 'tbl_users.user_id = tbl_bookings.user_id'));
            if ( count($result) > 0 ) {
                $user_id        = $result[0]['user_id']                   ? $result[0]['user_id']                   :'';
                $parent_name    = $result[0]['user_name']                 ? $result[0]['user_name']                 :'';
                $parent_no      = $result[0]['user_phone_number']         ? $result[0]['user_phone_number']         :'';
                $pick_up_point  = $result[0]['booking_pick_up_location']  ? $result[0]['booking_pick_up_location']  :'';
                $drop_off_point = $result[0]['booking_drop_off_location'] ? $result[0]['booking_drop_off_location'] :'';
                $pick_up_time   = $result[0]['booking_pick_up_time']      ? $result[0]['booking_pick_up_time']      :'';
                $drop_off_time  = $result[0]['booking_drop_off_time']     ? $result[0]['booking_drop_off_time']     :'';
                $service_type   = $result[0]['booking_duration']          ? $result[0]['booking_duration']          :'';
                $serviceResult  = $this->common_model->select("booking_date, booking_start_date, booking_end_date, booking_total_days, booking_secret_code, booking_price, booking_total_weeks, booking_total_moths", TB_BOOKINGS, array("booking_duration" => $service_type, "booking_id" => $postData['booking_id']));
                $duration       = 0;

                if ( $pick_up_time !='' && $drop_off_time !='' ) {
                    $total      = strtotime($drop_off_time) - strtotime($pick_up_time);
                    $hours      = floor($total / 60 / 60);
                    $minutes    = round(($total - ($hours * 60 * 60)) / 60);
                    $duration   = $hours.'.'.$minutes;                    
                }

                if(count($serviceResult) > 0) {

                    if($service_type == "daily") {

                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['days']        = $value['booking_total_days'];                             
                            $serviceList[$key]['secret_code'] = ''; 
                            $serviceList[$key]['price']       = $value['booking_price'];                             
                        }                        
                    } else if( $service_type == "weekly" ) {

                        foreach ( $serviceResult as $key => $value ) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']    = $value['booking_end_date'];
                            $serviceList[$key]['weekly']      = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = ''; 
                            $serviceList[$key]['price']       = $value['booking_price'];                             
                        }
                    } else {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']    = $value['booking_end_date']; 
                            $serviceList[$key]['monthly']     = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code'] = '';
                            $serviceList[$key]['price']       = $value['booking_price'];                             
                        }
                    }
                } else {
                    $serviceList = array(); 
                }     

                $subQuery='';           
                $kidIdResult =  $this->common_model->select("kids_id",TB_KIDS_BOOKING,array("booking_id"=>$postData['booking_id'])); 
                
                if ( count($kidIdResult) > 0 ) {

                    foreach ($kidIdResult as $key => $value) {
                        $subQuery = $value['kids_id'];    
                    }
                }

                $kidResult = $this->common_model->select_where_in_with_no_quote("kid_id, kid_gender, kid_age, kid_name", TB_KIDS, "kid_id", $subQuery);
                if(count($kidResult) > 0) {

                    foreach ( $kidResult as $key => $value ) {
                        $kidsList[$key]['id']     = $value['kid_id'];
                        $kidsList[$key]['name']   = $value['kid_name'];
                        $kidsList[$key]['gender'] = $value['kid_gender'];
                        $kidsList[$key]['age']    = $value['kid_age'];                    
                    }
                } else {
                    $kidsList = array();
                }
                $this->response(array('status' => true, 'user_id' => $user_id, 'pick_up_point' => $pick_up_point, 'drop_off_point' => $drop_off_point, 'pick_up_time' => $pick_up_time, 'drop_off_time' => $drop_off_time, 'trip_duration' => $duration, 'service_type' => $service_type, 'parent_no' => $parent_no, 'parent_name' => $parent_name, 'service_view' => $serviceList, 'kids_list' => $kidsList), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Data not found')); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters')); exit;
        }
    }

    /**
    Currently not in use, may be change according to new scope
     * Title : serviceAction
     * Description : service details 
     *
     * @param Integer booking_id
     * @param Integer user_id
     * @return Array
     */

    public function serviceAction_post () 
    {
        // Input data
        $postData = $_POST;

        // checked user is authenticate or not 
       /* if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/ 

        if ( $postData['booking_id'] != ""   && $postData['user_id'] != "") {

            $result = $this->common_model->selectJoin(TB_USERS.".user_id, user_name, user_phone_number, booking_pick_up_location, booking_drop_off_location, booking_pick_up_time, booking_drop_off_time, booking_duration, booking_id", TB_USERS, array(TB_USERS.".user_id" => trim($postData['user_id']), "booking_id" => $postData['booking_id']), array(), array(TB_BOOKINGS => 'tbl_users.user_id = tbl_bookings.user_id'));
            if ( count($result) > 0 ) {
                $user_id       = $result[0]['user_id']               ? $result[0]['user_id']               :'';
                $pick_up_time  = $result[0]['booking_pick_up_time']  ? $result[0]['booking_pick_up_time']  :'';
                $drop_off_time = $result[0]['booking_drop_off_time'] ? $result[0]['booking_drop_off_time'] :'';
                $service_type  = $result[0]['booking_duration']      ? $result[0]['booking_duration']      :'';
                $serviceResult = $this->common_model->select("booking_date, booking_start_date, booking_end_date, booking_total_days, booking_secret_code, booking_price, booking_total_weeks, booking_total_moths", TB_BOOKINGS, array("booking_duration" => $service_type,"booking_id" => $postData['booking_id']));
                
                if ( count($serviceResult) > 0 ) {

                    if ( $service_type == "daily" ) {

                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['total_days']  = $value['booking_total_days']; 
                            $serviceList[$key]['secret_code'] = ''; 
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }                        
                    } else if( $service_type == "weekly" ) {

                        foreach ( $serviceResult as $key => $value ) {
                            $serviceList[$key]['start_date']  = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']    = $value['booking_end_date'];
                            $serviceList[$key]['total_weeks'] = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = '';
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }
                    } else {

                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date']   = $value['booking_start_date']; 
                            $serviceList[$key]['end_date']     = $value['booking_end_date']; 
                            $serviceList[$key]['total_months'] = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code']  = '';
                            $serviceList[$key]['total_price']  = $value['booking_price'];                             
                        }
                    }
                } else {
                    $serviceList = array(); 
                }           

                $this->response(array('status' => true, 'user_id' => $user_id, 'booking_id' => $postData['booking_id'], 'service_type' => $service_type, 'service_view' => $serviceList), 200); exit;
            } else { 
                $this->response(array('status' => false, 'message' => 'Data not found')); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters')); exit;
        }    
    }

    /**
    Currently not in use, may be change according to new scope
     * Title : current_lat_and_lng
     * Description : update current latitude and longitude
     *
     * @param Integer booking_id
     * @param Integer current_lat
     * @param Integer current_lng
     * @return Array
     */

    function current_lat_and_lng_post () 
    {
        // Input data
        $postData = $_POST;

        // checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/ 

        if ( $postData['booking_id'] != "" && $postData['current_location_lat'] != "" && $postData['current_location_lng'] != "" ) {     
            $result = $this->db->update(TB_BOOKINGS, array('current_location_lat' => $postData['current_location_lat'], 'current_location_lng' => $postData['current_location_lng']), array('booking_id' => $postData['booking_id']));   
            
            if ( !$result ) {
                $this->response(array('status' => false, 'message' => 'something went wrong ,please try again!!!')); exit;
            } else {
                $this->response(array('status' => true, 'booking_id' => $postData['booking_id'], 'Current latitude' => $postData['current_location_lat'], 'Current longitude' => $postData['current_location_lng'], 'message' => 'Current latitude and longitude has been updated successfully'), 200); exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter booking id , current latitude and longitude.')); exit;
        }
    } 
    
    /**
     * Title : sendPushNotification
     * Description : send the push notification
     */

    public function sendPushNotification( $device_id, $message, $data, $user_type)
    {     
        $content = array(
            "en" => $message
            );

        if($user_type == ROLE_PARENTS || $user_type == SCHOOL || $user_type == ORGANIZATION)
        {
            $app_id = ONSIGNALKEYPARENT;
        }else{
            $app_id = ONSIGNALKEYSP;
        }

        $fields = array(
            'app_id'             => $app_id,
            'include_player_ids' =>(array)$device_id,
            'data'               => $data,
            'contents'           => $content
            );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
       // print_r($response);exit;            
        curl_close($ch);
    }

    /**
     * Title :driver_location
     * Description : update of driver current location get lat & long
     * 
     * @param  Integer user_id
     * @param  Double loc_lat
     * @param  Double loc_lng
     * @return Array
     */

    public function driver_location_post()
    {
        // post data
        $postData = $this->post();
        // checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/ 

        // Checked input is not empty
        if ( $postData['user_id'] != "" && $postData['loc_lat'] != "" && $postData['loc_lng'] != "" ) {

            if ( $postData['user_id'] > 0 ) {

                // Checked user is present or not
                $driverIdChecked = $this->common_model->select("user_id", TB_USERS, array("user_id" => $postData['user_id'], "roleId" => ROLE_CARE_DRIVER));
                
                if ( count($driverIdChecked) > 0 ) {

                    // Get user data                               
                    $getDriverId = $this->common_model->select("user_id", TB_DRIVER_LOCATION, array("user_id" => $postData['user_id']));
                    
                    if ( count($getDriverId) > 0 ) {
                        $userdata = array("loc_lat" => $postData['loc_lat'], "loc_long" => $postData['loc_lng'], "created_at" => date("Y-m-d H:i:s"));
                        $user     = $this->db->update(TB_DRIVER_LOCATION, $userdata, array('user_id' => $this->post('user_id')));
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Care driver location has been updated successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
                        }
                    } else {
                        $userdata = array("user_id" => $postData['user_id'], "loc_lat" => $postData['loc_lat'], "loc_long" => $postData['loc_lng'], "created_at" => date("Y-m-d H:i:s"));
                        $user     = $this->db->insert(TB_DRIVER_LOCATION, $userdata);
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Care driver location has been inserted successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
                        }
                    }
                } else {
                    $this->response(array("status" => false, "message" => 'User id is not driver or exists.')); exit;
                } 
            } else {
                $this->response(array("status" => false, "message" => 'Provide proper user id')); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide userid, lattitude & loggitude.')); exit;
        }
    }


    /**
     * Title :parent_current_location
     * Description : add & update of parent current lat & long location
     * 
     * @param  Integer user_id
     * @param  Double current_lat
     * @param  Double loc_lng
     * @return Array
     */

    public function parent_current_location_post()
    {
        // post data
        $postData = $this->post();
        // checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        // Checked input is not empty
        if ( $postData['user_id'] != "" && $postData['current_lat'] != "" && $postData['current_lng'] != "" ) {

            if ( $postData['user_id'] > 0 ) {

                // Checked user is present or not
                $driverIdChecked = $this->common_model->select("user_id", TB_USERS, array("user_id" => $postData['user_id'], "roleId" => ROLE_PARENTS));
                
                if ( count($driverIdChecked) > 0 ) {

                    // Get user data                               
                    $getDriverId = $this->common_model->select("user_id", TB_PARENT_LOCATION, array("user_id" => $postData['user_id']));

                    
                    if ( count($getDriverId) > 0 ) {
                        $userdata = array("current_lat" => $postData['current_lat'], "current_lng" => $postData['current_lng'], "updated_at" => date("Y-m-d"));
                        $user     = $this->db->update(TB_PARENT_LOCATION, $userdata, array('user_id' => $this->post('user_id')));
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Parent location has been updated successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
                        }
                    } else {
                        $userdata = array("user_id" => $postData['user_id'], "current_lat" => $postData['current_lat'], "current_lng" => $postData['current_lng'], "created_at" => date("Y-m-d H:i:s"), "updated_at" => date("Y-m-d"));
                        $user     = $this->db->insert(TB_PARENT_LOCATION, $userdata);
                        if ( $user ) {
                            $this->response(array("status" => true, "message" => 'Parent location has been inserted successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
                        }
                    }
                } else {
                    $this->response(array("status" => false, "message" => 'User id is not parent or exists.')); exit;
                } 
            } else {
                $this->response(array("status" => false, "message" => 'Provide proper user id')); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide userid, lattitude & loggitude.')); exit;
        }
    }

    /**
     * Title       : get_parent_current_loc
     * Description : get parent current location
     * 
     * @param Integer user_id
     * @return Array
     */
    public function get_parent_current_loc_post()
    {
        // post data
        $postData = $this->post();
        // checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

         // Validation
        if ( trim($postData['user_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'User id field is required.'), 200); exit; 
        } 

        $getparentData = $this->common_model->select("id,user_id,current_lat,current_lng,updated_at", 
            TB_PARENT_LOCATION, array("user_id" => $postData['user_id'],"updated_at" => date("Y-m-d")));
        $parentList = array();
        $i = 0;
        if(count($getparentData) > 0) {
            foreach ($getparentData as $key => $value) {
                $parentList[$i]['id'] = $value['id'];
                $parentList[$i]['user_id'] = $value['user_id'];
                $parentList[$i]['current_lat'] = $value['current_lat'];
                $parentList[$i]['current_lng'] = $value['current_lng'];
                $parentList[$i]['updated_at'] = $value['updated_at'];
                $i++;
            }
            $this->response(array("status" => true, "result" => $parentList), 200); exit;
        } else {
            $this->response(array("status" => false, "message" => 'No records found.')); exit;
        }
    }
    
    
    /**
     Author:Priya
     * Title       : video_streaming
     * Description : Ride Video streaming
     * 
     * @param Integer ride_id
     * @param Integer service_provider_id
     * @param File video_stream
     * @return Array
     */

    public function video_streaming_post ()
    {       
        // post data
        $postData = $this->post();

        if($postData['service_provider_id'] == '') {

            $this->response(array("status" => false,"message" => 'Service provider id is required.'), 200); exit;
        } else if($postData['ride_id'] == '') {

            $this->response(array("status" => false,"message" => 'Ride id is required.'), 200); exit;
        } else {

            // Validation
            $streaming_file = "";

            $resultUpload = upload_video_on_amazon($_FILES['video_stream']);
            if ( $resultUpload['success'] == 1 ) {
                $video_data      = array("service_provider_id" => $postData['service_provider_id'], "ride_id" => $postData['ride_id'], "file_path" => $resultUpload['fullpath'], "status" => 1, "created_date" => date('Y-m-d H:i:s'));
                $video_streaming = $this->db->insert(TB_VIDEO_STREAM, $video_data);

                $getBookId = $this->common_model->select("*", TB_RIDE, array("ride_id" => trim($postData['ride_id'])));
                
                if(count($getBookId) != 0) {

                $bookId = $getBookId[0]['booking_id']; 

                    // Get all data trip & parent details
                $cond    = array("booking_id" => $bookId);       
                $join    = array(TB_USERS => TB_USERS.".user_id = ".TB_BOOKINGS.".user_id");
                $resultUserDetails = $this->Common_model->select_join(TB_BOOKINGS.".booking_id,booking_start_date,booking_pick_up_location, booking_drop_off_location,booking_pick_up_time,booking_drop_off_time,booking_status, user_email, user_name", TB_BOOKINGS, $cond, array(), array(), $join);
                $email = !empty($resultUserDetails[0]['user_email'])?$resultUserDetails[0]['user_email']:"";
                $name = !empty($resultUserDetails[0]['user_name'])?$resultUserDetails[0]['user_name']:"";
                $subject = "Tulii trip details on ".date("Y-m-d H:i:s");
                $message = '<tr> 
                <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Hello '.$name.',</p>
                    <p>
                        <h4>Tulii trip details are following.<h4><br/>
                            <b>Trip date : '.$resultUserDetails[0]['booking_start_date'].'</b><br/>
                            <b>Pick up location : '.$resultUserDetails[0]['booking_pick_up_location'].'</b><br/>
                            <b>Drop off location : '.$resultUserDetails[0]['booking_drop_off_location'].'</b><br/>
                            <b>Pick up time : '.$resultUserDetails[0]['booking_pick_up_time'].'</b><br/>
                            <b>Drop up time : '.$resultUserDetails[0]['booking_drop_off_time'].'</b><br/>
                            <b>Status : '.$resultUserDetails[0]['booking_status'].'</b><br/>
                            <b>Video link  : '.$resultUpload['fullpath'].'</b><br/>
                            <b>Download video link  : <a href="'.$resultUpload['fullpath'].'" download="tulii_trip_on_'.date("Y_m_d_H_i_s").'">Download</a></b><br/>
                            
                            Thanks and Regards.<br/>            
                            Tulii Admin         
                        </p>                    
                    </td>
                </tr>
                ';  
                $send = sendEmailTemplate($email, $message, $subject);

                }
                
                
                //result
                if ( $video_streaming ) {
                    $this->response(array("status" => true, "message" => "Video has been uploaded successfully.", "data" =>$resultUpload), 200); exit;     
                } else {
                    $this->response(array("status" => false, "message" => "Some error has been occured, please try again!!!")); exit; 
                }               
            } else {
                $this->response(array("status" => false, "message" => $resultUpload['msg'])); exit;                                                
            }
        }
    } 

    /**
     * Title       : video_trash
     * Description : trash Video streaming of rides
     * 
     * @return Array
     */

    public function video_trash_get ()
    {
        // Get 15 days ago date
        $daysAgo = date('Y-m-d', strtotime('-15 days', strtotime(date('Y-m-d'))));
        $resultVideosData = $this->Common_model->select("id,file_path",TB_VIDEO_STREAM,array('created_at <= ' => $daysAgo)); 
        
        // trash videos
        foreach ($resultVideosData as $key => $value) {
            if ( file_exists("./video/".$value['file_path']) ) {
                unlink("./video/".$value['file_path']);
            }            
        }

        // Delete records
        $resultDeleteVideos = $this->Common_model->delete(TB_VIDEO_STREAM,array('created_at <= ' => $daysAgo)); 
        if( $resultDeleteVideos ) {
            $this->response(array("status" => true, "message" => 'Video file are deleted successfully.'), 200); exit; 
        } else {
            $this->response(array("status" => false, "message" => 'Video file are not deleted successfully.')); exit; 
        }
    }

    /**
     * Title :find_driver_loc 
     * Description : find the current location of the driver within 5 miles 
     *
     * @param Double lattitude
     * @param Double loggitude     
     * @return Array
     */

    public function find_driver_loc_post () 
    {
        // Input data
        $postData = $this->post();

        // checked user is authenticate or not 
       /* if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        if ( $postData['lattitude'] != "" && $postData['loggitude'] != "" ) {            
            $cond         = array(TB_DRIVER_LOCATION.'.created_at' => date("Y-m-d"),TB_USERS.'.roleId' => 3);
            $jointype     = array(TB_DRIVER_LOCATION => "LEFT");
            $join         = array(TB_DRIVER_LOCATION => TB_DRIVER_LOCATION.".user_id = ".TB_USERS.".user_id");
            $allDriverLoc = $this->common_model->selectJoin(TB_DRIVER_LOCATION.".user_id, loc_long, loc_lat, user_email, user_name, user_phone_number, user_gender, user_address, user_birth_date, user_age, is_your_own_car, user_category_id, user_sub_category_id, driving_license_type, license_no, valid_from_date, valid_until_date, car_model, car_register_no, car_seating_capacity, car_manufacturing_year, car_age, car_pic, license_pic, school, degree, specialization, education_from, education_to, certification_name, upload_certificate, user_pic", TB_USERS, $cond, array(), $join, $jointype);

            if ( count($allDriverLoc) > 0 ) {
                $data = array();
                $i    = 0; 

                foreach ( $allDriverLoc as $key => $value ) {
                    $carpics  = $license = $certification = array();
                    $milesLoc = round($this->distance($postData['lattitude'], $postData['loggitude'], $value['loc_lat'], $value['loc_long'], "M"));           
                    
                    if ( $milesLoc <= 25 && $milesLoc >= 0 ) {                       
                        $data[$i]["driver_id"]              = $value['user_id'];
                        $data[$i]["email"]                  = $value['user_email'];
                        $data[$i]["name"]                   = $value['user_name'];
                        $data[$i]["mobile_no"]              = $value['user_phone_number'];                        
                        $data[$i]["gender"]                 = $value['user_gender'];
                        $data[$i]["address"]                = $value['user_address'];
                        $data[$i]["dob"]                    = $value['user_birth_date'];
                        $data[$i]["age"]                    = $value['user_age'];
                        $data[$i]["is_your_own_car"]        = $value['is_your_own_car'];
                        $data[$i]["driving_license_type"]   = $value['driving_license_type'];
                        $data[$i]["license_no"]             = $value['license_no'];
                        $data[$i]["valid_from_date"]        = $value['valid_from_date'];
                        $data[$i]["valid_until_date"]       = $value['valid_until_date'];
                        $data[$i]["car_model"]              = $value['car_model'];
                        $data[$i]["car_register_no"]        = $value['car_register_no'];
                        $data[$i]["car_seating_capacity"]   = $value['car_seating_capacity'];
                        $data[$i]["car_manufacturing_year"] = $value['car_manufacturing_year'];
                        $data[$i]["car_age"]                = $value['car_age'];
                        $data[$i]["school"]                 = $value['school'];
                        $data[$i]["degree"]                 = $value['degree'];
                        $data[$i]["specialization"]         = $value['specialization'];
                        $data[$i]["education_from"]         = $value['education_from'];
                        $data[$i]["education_to"]           = $value['education_to'];
                        $data[$i]["certification_name"]     = $value['certification_name'];

                        $licenseArr = explode("|", $value['license_pic']);
                        
                        if ( !empty($licenseArr[0]) ) {

                            foreach ( $licenseArr as $lkey => $lvalue ) {
                                $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                            }
                        }

                        $carArr = explode("|", $value['car_pic']);
                        if ( !empty($carArr[0]) ) {

                            foreach ( $carArr as $ckey => $carValue ) {
                                $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                            }
                        }

                        $certificateArr = explode("|", $value['upload_certificate']);
                        if ( !empty($certificateArr[0]) ) {

                            foreach ( $certificateArr as $cetkey => $certifValue ) {
                                $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                            }
                        }
                        
                        $data[$i]["license_pics"]     = $license; 
                        $data[$i]["car_pics"]         = $carpics;                                               
                        $data[$i]["certificate_pics"] = $certification;
                        $data[$i]["user_pics"]        = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: "";                       
                        $i++;
                    } 
                }   
                
                if ( empty($data) ) {
                    $this->response(array("status" => false, "message" => 'No drivers found within 5 miles.')); exit;        
                } else {
                    $this->response(array("status" => true, "drivers_list" => $data), 200); exit;        
                }                
            } else {
                $this->response(array("status" => false, "message" => 'Today no drivers available.')); exit;    
            }
        } else {
            $this->response(array("status" => false,"message" => 'Please enter lattitude & loggitude.')); exit;
        }
    }

    /**
     * Title : find_all_drivers_loc
     * Description : find the current location of the driver within 25 miles 
     *
     * @param : attitude
     * @param : loggitude     
     * @return Array
     */

    public function find_all_drivers_loc_post () 
    {
        // Input data
        $postData = $this->post();
        // Checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        } */

        // Validation
        if ( trim($postData['lattitude']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Lattitude field is required.'), 200); exit; 
        }    
        if ( trim($postData['loggitude']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Loggitude field is required.'), 200); exit; 
        } 

        // Get all driver details           
        $cond         = array(TB_DRIVER_LOCATION.'.created_at' => date("Y-m-d"), TB_USERS.'.roleId' => 3);
        $jointype     = array(TB_DRIVER_LOCATION => "LEFT");
        $join         = array(TB_DRIVER_LOCATION => TB_DRIVER_LOCATION.".user_id = ".TB_USERS.".user_id");
        $allDriverLoc = $this->common_model->selectJoin(TB_DRIVER_LOCATION.".user_id, loc_long, loc_lat, user_name, user_phone_number, user_pic", TB_USERS, $cond, array(), $join, $jointype);
        
        // create json formate
        if(count($allDriverLoc) > 0) {
            $driverListArr = array();
            $i = 0;

            foreach ($allDriverLoc as $key => $value) {
                $milesLoc      = round($this->distance($postData['lattitude'], $postData['loggitude'], $value['loc_lat'], $value['loc_long'], "M"));           
                
                if ( $milesLoc <= 25 && $milesLoc >= 0 ) {                       
                    $driverListArr[$i]["driver_id"] = $value['user_id'];
                    $driverListArr[$i]["name"] = $value['user_name'];
                    $driverListArr[$i]["mobile_no"] = $value['user_phone_number']; 
                    $driverListArr[$i]["lattitude"] = $value['loc_lat']; 
                    $driverListArr[$i]["loggitude"] = $value['loc_long']; 
                    $driverListArr[$i]["driver_range"] = $milesLoc." Miles";                                           
                    $driverListArr[$i]["user_pics"] = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: base_url().'uploads/blank-profile-picture.png';                       
                    $i++;
                }
            }   
            // result
            if ( empty($driverListArr) ) {
                $this->response(array("status" => false, "message" => 'No drivers found within 5 miles.')); exit;        
            } else {
                $this->response(array("status" => true, "drivers_list" => $driverListArr), 200); exit;        
            }                
        } else {
            $this->response(array("status" => false, "message" => 'Today no drivers available.')); exit;    
        }        
    }

    /**
     * Title : distance
     * Description :calculate distance miles , kilometers and Nautical miles
     *
     * @return Integer 
     */

    function distance($lat1, $lon1, $lat2, $lon2, $unit) 
    {
        $theta = $lon1 - $lon2;
        $dist  = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist  = acos($dist);
        $dist  = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit  = strtoupper($unit);

        if ( $unit == "K" ) {
            return ($miles * 1.609344);
        } else if ( $unit == "N" ) {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * Title : driver_details
     * Description: get specifi driver details 
     *
     * @param Integer user_id
     * @return Array
     */

    public function driver_details_post () 
    {
        // Input data
        $postData = $this->post();

        // Checked user is authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        }*/ 

        if ( $postData['user_id'] != "" ) {
            $allDrivers = $this->common_model->select("*", TB_USERS, array("user_id" => $postData['user_id']));
            
            if ( count($allDrivers) > 0 ) { 

                $status = $allDrivers ? $allDrivers[0]['roleId']:"";
                if ( $status == ROLE_SERVICE_PROVIDER && $status != "" ) {
                    $driverRatingArr = $driverArr = $perInfo = $driverDetail = $education = array(); 
                    $resultDriverRating = $this->common_model->select_join("user_name, rating_id, rating, rating_comment, user_pic", TB_RATING, array("tbl_rating.user_id" => trim($postData['user_id']) ,"tbl_rating.isDeleted" => "0"), array(), array(), array(TB_USERS => 'tbl_users.user_id = tbl_rating.ratedByUserId'),null);
                    
                    if ( count($resultDriverRating) > 0 ) {
                        $j = 0; 
                        foreach ($resultDriverRating as $key => $value) {
                            $driverRatingArr[$j]['rating_id']      = $value['rating_id'];    
                            $driverRatingArr[$j]['user_name']      = $value['user_name'];
                            $driverRatingArr[$j]['rating']         = $value['rating'];
                            $driverRatingArr[$j]['user_picture']   = !empty($value['user_pic']) ? base_url().USER_PROFILE_IMG.$value['user_pic'] : base_url()."uploads/blank-profile-picture.png";
                            $driverRatingArr[$j]['rating_comment'] = $value['rating_comment'];     
                            $j++;
                        }
                    } 

                    foreach ( $allDrivers as $key => $value ) {
                        $driverArr[$key]["user_pics"]               = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: "";
                        $driverArr[$key]["category_id"]             = $value["user_category_id"];
                        $driverArr[$key]["sub_category_id"]         = $value["user_sub_category_id"];                        
                        $driverArr[$key]["car_seating_capacity"]    = $value['car_seating_capacity'];
                        $driverArr[$key]["car_manufacturing_year"]  = $value['car_manufacturing_year'];
                        $driverArr[$key]["car_age"]                 = $value['car_age'];
                        $driverArr[$key]["total_experience"]        = $value['total_experience'];
                        $driverArr[$key]["car_number_plate"]        = $value['car_number_plate'];
                        $perInfo[$key]["driver_id"]                 = $value['user_id'];
                        $perInfo[$key]["email"]                     = $value['user_email'];
                        $perInfo[$key]["name"]                      = $value['user_name'];
                        $perInfo[$key]["mobile_no"]                 = $value['user_phone_number'];                        
                        $perInfo[$key]["gender"]                    = $value['user_gender'];
                        $perInfo[$key]["address"]                   = $value['user_address'];                                
                        $perInfo[$key]["dob"]                       = $value['user_birth_date'];                                
                        $perInfo[$key]["age"]                       = $value['user_age'];                      
                        $driverDetail[$key]["is_your_own_car"]      = $value['is_your_own_car'];
                        $driverDetail[$key]["driving_license_type"] = $value['driving_license_type'];
                        $driverDetail[$key]["license_no"]           = $value['license_no'];
                        $driverDetail[$key]["valid_from_date"]      = $value['valid_from_date'];
                        $driverDetail[$key]["valid_until_date"]     = $value['valid_until_date'];
                        $driverDetail[$key]["car_model"]            = $value['car_model'];
                        $driverDetail[$key]["car_register_no"]      = $value['car_register_no'];

                        $licenseArr = explode("|", $value['license_pic']);
                        
                        if ( !empty($licenseArr[0]) ) {

                            foreach ( $licenseArr as $lkey => $lvalue ) {
                                $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                            }
                        } else {
                            $license = array(); 
                        }

                        $carArr = explode("|", $value['car_pic']);
                        if ( !empty($carArr[0]) ) {

                            foreach ( $carArr as $ckey => $carValue ) {
                                $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                            }
                        } else {
                            $carpics = array();
                        }
                        
                        $education[$key]["school"]             = $value['school'];
                        $education[$key]["degree"]             = $value['degree'];
                        $education[$key]["specialization"]     = $value['specialization'];
                        $education[$key]["education_from"]     = $value['education_from'];
                        $education[$key]["education_to"]       = $value['education_to'];
                        $education[$key]["certification_name"] = $value['certification_name'];
                        $certificateArr = explode("|", $value['upload_certificate']);

                        if ( !empty($certificateArr[0]) ) {

                            foreach ( $certificateArr as $cetkey => $certifValue ) {
                                $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                            }
                        } else {
                            $certification = array();
                        }

                        $driverDetail[$key]["license_pics"]  = $license;
                        $driverDetail[$key]["car_pics"]      = $carpics;    
                        $education[$key]["certificate_pics"] = $certification;
                        $driverArr[$key]["personalInfo"]     = $perInfo;       
                        $driverArr[$key]["driverDetails"]    = $driverDetail;
                        $driverArr[$key]["education"]        = $education;
                        $driverArr[$key]["rating"]           = $driverRatingArr;   
                        
                    }
                    $this->response(array("status" => true, "driver_list" => $driverArr), 200); exit;
                } else {
                    $this->response(array("status" => false ,"message" => 'User is not driver or blank.')); exit;
                }
            } else {
                $this->response(array("status" => false,"message" => 'No drivers has been exists.')); exit;
            }
            
        } else {
            $this->response(array("status" => false,"message" => 'Please enter the user id')); exit;
        }
    }
    
     /**
     Currently not in use, may be change according to new scope
     * Title : video list
     * Description : video list
     *  
     * @param  Integer user_id
     * @return Array
     */

     public function video_list_post()
     {   
        // Checked user is authenticate or not
       /* if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        // checked inputs are not emtpy
        if ( $this->post('user_id') != '' ) {

            // Get video data
            $cond      = array(TB_USERS.'.isDeleted' => 0, TB_USERS.'.user_status' => '1', TB_BOOKINGS.'.isDeleted' => 0, TB_VIDEO_STREAM.'.isDeleted' => 0, TB_USERS.'.user_id' => $this->post('user_id'));
            $jointype  = array(TB_USERS => "LEFT", TB_BOOKINGS => "LEFT"); 
            $join      = array(TB_BOOKINGS => TB_BOOKINGS.".booking_id = ".TB_VIDEO_STREAM.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_BOOKINGS.".user_id");       
            $videoList = $this->Common_model->selectJoin("user_name, user_phone_number, user_gender, user_address, file_path", TB_VIDEO_STREAM, $cond, array(), $join, $jointype);
            
            $arrayList = array();
            $i = 0;
            // result
            if ( count($videoList) > 0 ) {

                foreach ($videoList as $key => $value) {
                    $arrayList[$i]['user_name']         = $value['user_name'];
                    $arrayList[$i]['user_phone_number'] = $value['user_phone_number']; 
                    $arrayList[$i]['user_gender']       =  $value['user_gender'];
                    $arrayList[$i]['user_address']      = $value['user_address'];
                    $arrayList[$i]['file_path']         = !empty($value['file_path']) ? base_url()."video/".$value['file_path']:"N/A";

                    $i++;
                }
                $this->response(array("status" => true, "result" => $arrayList), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Video not found.')); exit;
            } 
        } else {
            $this->response(array("status" => false,"message" => 'Please provide user id.')); exit; 
        }
    }    

    /**
     * Title       : upload_pictures
     * Description : Upload multiple pictures
     * 
     * @param  Integer user_id
     * @param  String certificate_pics
     * @param  String license_pics
     * @param  String car_pics
     * @param  String cover_pics     
     * @return Array
     */

    public function upload_pictures_post ()
    {       
        // Post data
        $postData = $this->post();      
        
        // Checked user is authenticate
        /*if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */ 
        
        // Checked user not empty
        if ( $this->post('user_id') != "" ) {               
            $user_id = $this->common_model->select("*", TB_USERS, array("user_id" => $postData['user_id']));
            
            // Checked user is exist or not
            if ( count($user_id) > 0 ) {

                $flag = 0;
                $filename = "";
                $filePath = "";
                if ( isset($_FILES['certificate_pics']['tmp_name']) != "" ) {
                    $filename = "Certificate";
                    $clmnName = "upload_certificate";
                    $filePath = 'uploads/driver/certification/';
                    $flag     = 1;
                }   
                if ( isset($_FILES['license_pics']['tmp_name']) != "" ) {
                    $filename = 'License';
                    $clmnName = "license_pic";
                    $filePath = 'uploads/driver/licenseCopy/';
                    $flag     = 1;
                } 
                if ( isset($_FILES['car_pics']['tmp_name']) != "" ) {
                    $filename = 'Car';
                    $clmnName = "car_pic";
                    $filePath = 'uploads/driver/carPics/';
                    $flag     = 1;
                } 
                if ( isset($_FILES['cover_pics']['tmp_name']) != "" ) {                   
                    $filename = 'Cover';
                    $clmnName = "cover_pics";
                    $filePath = 'uploads/driver/coverPics/';
                    $flag     = 1;
                } 
                if ( $flag == 0 ) {
                    $this->response(array("status" => false,"message" => "Please upload images.")); exit;  
                }

                // Upload pictures
                if ( $flag == 1 ) 
                {
                    $uploadFileName       = "";
                    $dynamicUploadedFile  = "";                   
                    $temp                 = "";
                    $fileImages           = $_FILES;
                    $allFiles             = array();

                    if ( !empty($fileImages) ) {

                        foreach ( $fileImages as $key => $value ) {
                            $files_name   = array();
                            $countAllFile = count($_FILES[$key]['name']);                           
                            $temp         = $filename;                           
                            
                            for ( $i=0; $i < $countAllFile; $i++ ) {
                                $_FILES[$temp]['name']      = $_FILES[$key]['name'][$i];
                                $_FILES[$temp]['type']      = $_FILES[$key]['type'][$i];
                                $_FILES[$temp]['tmp_name']  = $_FILES[$key]['tmp_name'][$i];
                                $_FILES[$temp]['error']     = $_FILES[$key]['error'][$i];
                                $_FILES[$temp]['size']      = $_FILES[$key]['size'][$i];    
                                $files_name[$i]['name']     = $_FILES[$temp]['name'];
                                $files_name[$i]['type']     = $_FILES[$temp]['type'];
                                $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                $files_name[$i]['error']    = $_FILES[$temp]['error'];
                                $files_name[$i]['size']     = $_FILES[$temp]['size'];
                            }
                            $allFiles[$temp]=$files_name;                                                         
                        }

                        // uploaded multiple images
                        foreach ( $allFiles as $key => $value ) {

                            for ( $i=0, $j=0; $j < count($value); $j++ ) {                                     
                                if ( strcmp($key,$filename) == 0 ) {
                                    $dynamicUploadedFile = multiple_image_upload( $value[$j], $filePath ); 
                                    if ( is_array($dynamicUploadedFile) ) {
                                        $this->response($dynamicUploadedFile); exit;
                                    } else {
                                        $uploadFileName .= "|".$dynamicUploadedFile;    
                                    }
                                } 
                                $i++;                                                     
                            }
                        }

                        // update table
                        $userdata = array(
                            $clmnName =>trim($uploadFileName,"|")                         
                            );                            
                        $user = $this->db->update(TB_USERS, $userdata, array("user_id" => $postData['user_id']));
                        
                        // result                       
                        if ($user) {
                            $this->response(array('status' => true, 'message' => "${filename} picture has been uploaded sucessfully."),200); exit;
                        } else {
                            $this->response(array("status" => false, "message" => "Something went wrong. !!")); exit;
                        }
                    } else {
                        $this->response(array("status" => false, "message" => "Please upload ${filename} pic.")); exit;  
                    }
                }    
            } else {
                $this->response(array("status" => false, "message" => 'User id not exist.')); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide user id.')); exit;
        }        
    }   

    /**
     * demo_post
     * @param lat,lng
     * @return  json
     */
    function lat_lng_post() 
    {
        $params = (array) json_decode(file_get_contents('php://input'), TRUE);
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        } */ 
        if(empty($params['location']['coords']['latitude']) || empty($params['location']['coords']['longitude'])) {
           $this->response(array("status"=>fasle, "message"=> "Please provide lattitude & longitude."));exit; 
       }

       $arrData = array("current_lat"=>$params['location']['coords']['latitude'],"current_lng"=>$params['location']['coords']['longitude']);
       $resultData = $this->Common_model->insert(TB_DEMO, $arrData);
       if($resultData) {
        $this->response(array("status"=>true,"message"=> '1 record has been inserted successfully'), 200);exit;
    } else {
        $this->response(array("status"=>false,"message"=> 'Something went wrong'));exit;
    }        
}

    /**
     * latest_lat_lng
     */
    public function latestLatLng_get()
    {     
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        } */    
        $latestLatLng = $this->db->order_by('id',"desc")
        ->limit(1)
        ->get(TB_DEMO)
        ->row();      
        $data = array();

        if(count($latestLatLng) > 0) {
            $latLng['id']=$latestLatLng->id;
            $latLng['lattitude']=$latestLatLng->current_lat;
            $latLng['longitude']=$latestLatLng->current_lng;
            $data[] = $latLng;
            $this->response(array("status"=>true,"result"=>$data), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'No lattitude & longitude are exists.'));exit;
        }
    }  

    /**
    Ashwini Changes
     * Driver rating
     *
     * @param driver_id
     * @param user_id
     * @param rating
     * @param comment
     * @param desc
     * @return array
     **/  
    public function rating_post()
    {
        $postData = $this->post();
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }*/  
        if($postData['driver_id'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'Driver id field is required.'), 200);exit;
        }
        if($postData['user_id'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'User id field is required.'), 200);exit;
        }    
        if($postData['rating'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'Rating field is required.'), 200);exit;
        }    
        if($postData['comment'] == "")
        { 
            $this->response(array("status"=>"error","message"=> 'Comment field is required.'), 200);exit;
        }

        if(!empty($postData['rating']) && $postData['rating'] <= 3)
        {
            if(empty($postData['description']))
            {
                $this->response(array("status"=>"error","message"=> 'Description field is required.'), 200);exit;
            }else
            {
                $desc = $postData['description'];
            }
        }else
        {
            $desc = '';
        }
        $arrData = array(
            "user_id"=>$postData['driver_id'],
            "rating"=>$postData['rating'],
            "rating_comment"=>$postData['comment'],
            "rating_desc"=>$desc,
            "ratedByUserId"=>$postData['user_id'],
            "isDeleted"=>0);
        $resultData = $this->Common_model->insert(TB_RATING, $arrData);
        if($resultData) {
            //--- Block driver if rating <= 3 for 2 times ----
            $cond = array("user_id"=>$postData['driver_id'],"rating <= "=>3);
            $checkDriver = $this->common_model->select("COUNT(*) as rate_cnt",TB_RATING,$cond);
            if($checkDriver[0]['rate_cnt'] >= 2)
            {
                $this->common_model->update(TB_USERS,array("user_id"=>$postData['driver_id']),array("is_block"=>"1"));
            }

            $this->response(array("status"=>true,"message"=> 'Rating has been inserted successfully'), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'Something went wrong'));exit;
        }
    }

    
    /**
     * Title : rating_list
     * Description : show the list of driver rating 
     *
     * @return Array     
     */ 

    public function rating_list_get ()
    {
        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  
        
        $ratingList = array();
        $i = 0;
        $join     = array(TB_USERS => TB_USERS.".user_id=".TB_RATING.".user_id");
        $resultOfRatingData = $this->Common_model->select_join(TB_RATING.".user_id, ROUND(AVG(rating),1) as rating,user_pic,user_name as driver_name",TB_RATING,array(TB_RATING.".isDeleted" => "0"),array(),array(),$join,"tbl_rating.user_id");
        
        if ( count($resultOfRatingData) > 0 ) {

            foreach ($resultOfRatingData as $key => $value) {
                $ratingList[$i]['user_id']     = $value['user_id'];
                $ratingList[$i]['driver_name'] = $value['driver_name']; 
                $ratingList[$i]['user_pic']    =  !empty($value['user_pic']) ? base_url()."uploads/driver/driverImg/".$value['user_pic'] : base_url()."uploads/blank-profile-picture.png";
                $ratingList[$i]['rating']      = $value['rating'];
                $i++;
            }
            $this->response(array("status" => true, "result" => $ratingList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'No driver ratings found.')); exit;
        }       
    }   

    /**
     * get driver location
     * driver_id
     */
    public function get_driver_location_post()
    {
        $postData = $this->post();
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        } */ 
        if($postData['driver_id'] == "")
            { $this->response(array("status"=>"error","message"=> 'driver id field is required.'), 200);exit; }    
        $getDriverLatLng = $this->common_model->select("user_id,loc_lat,loc_long ",TB_DRIVER_LOCATION,array("user_id"=>$postData["driver_id"]));
        if($getDriverLatLng) {
            $this->response(array("status"=>true,"result"=> $getDriverLatLng), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'No records available'));exit;
        }        
    }

    /** As per current requirement this method not in use
     * Title : driver_edit_profile
     * Description : edit driver profile
     *
     * @param Integer user_id
     * @param  user_email
     * @param  dob
     * @param  tutor_status
     * @param  user_name
     * @param  gender
     * @param  address
     * @param  user_add_lat_lng
     * @param  subject
     * @return Array
     */

    public function driver_edit_profile_post()
    {
        $data = $_POST;
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }  */
        if (isset($data['user_id']) && $data['user_id'] > 0) {        
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);            
            if(!count($result)) {
                $this->response(array('status' => false, 'message' => 'Invalid user'));exit;
            } else {                   
                    //check email 
                $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null); 

                if ($emailExist) {
                    $this->response(array("status" => false, "message" => "Duplicate email!!!"));exit;
                }           
                $dateOfBirth =$this->post('dob');
                $today = date("Y-m-d");
                $diff = date_diff(date_create($dateOfBirth), date_create($today));
                $age = $diff->format('%y');

                    //----- Ashwini's code here --------
                if($result[0]['role'] == "Tutor")
                {
                    if(empty($this->post('tutor_status')))
                    {
                        $this->response(array('status' => "error", 'message' => 'Tutor status is required.'), 200);exit;
                    }else
                    {
                        $tutor_status = $this->post('tutor_status');
                    }
                }else
                {
                    $tutor_status = '';
                }


                $userdata = array(
                    'user_name' => $this->post('user_name'),
                    'user_gender' => $this->post('gender'),
                    'user_address' => $this->post('address'),
                    'user_add_lat_lng' => $this->post('user_add_lat_lng'),
                    'tutor_status' => $tutor_status,
                        // 'user_email' => $this->post('user_email'),                
                    'user_phone_number' => $this->post('contact_no'),
                    'user_birth_date' => $dateOfBirth, 
                    'tutoring_subject' => $this->post('subject'),
                    'user_age' => $age
                    );
                try {
                   $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
               } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()));exit;
            }                
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
    } else {
        $this->response(array('status' => false, 'message' => 'Provide user_id'));exit;
    }
}


    /** Author: Ram K
     * Title       : logout
     * Description : User logout
     *   
     * @param Integer user_id
     * @param Integer device_id
     * @return Array
     */

    /*function logout_post ()
    {
        // Input data
    	$postData = $_POST;

    
        if ( isset($postData['user_id']) && $postData['user_id'] > 0 ) {

           if ( isset($postData["device_id"]) && $postData["device_id"] != "" ) {
              $cond     = array("user_id"=>$postData['user_id']);
              $userData = $this->data_model->getAllUsers($cond);
              $ssss     = explode(",", $userData[0]['user_device_id']);

              if ( in_array($postData['device_id'], $ssss) ) {
               unset($ssss[array_search($postData['device_id'],$ssss)]);
               $test = implode(',', $ssss);
               $this->common_model->update(TB_USERS, array("user_id" => $postData['user_id']), array('user_device_id' => $test));
           }
           $this->response(array('status' => true, 'message' => 'You have logout successfully.'), 200); exit;        		
       } else {
         $this->response(array('status' => false, 'message' => 'Provide device_id')); exit;
     }
 } else {
     $this->response(array('status' => false, 'message' => 'Provide user_id')); exit;
 }
}*/

    /** Author: Jyoti Korde
     * Title       : logout
     * Description : User logout
     *   
     * @param Integer user_id
     * @param Integer  app_id
     * @param Integer device_id
     * @return Array
     */

    public function logout_post ()
    {
        // Input data
        $postData = $_POST;

        if ( isset($postData['user_id']) && $postData['user_id'] > 0 ) {

            if ( isset($postData["device_id"]) && $postData["device_id"] != "" ) {

                if($postData["app_id"] == '0'){
                  $cond     = array("service_user_id"=>$postData['user_id']);
                  $userArr  = $this->common_model->select('user_device_id',TB_SERVICE_USERS,array('service_user_id' => $postData['user_id'])); 
                  if ($userArr[0]['user_device_id']) {
                   $this->common_model->update(TB_SERVICE_USERS, array("service_user_id" => $postData['user_id']), array('user_device_id' => ''));
                   $this->response(array('status' => true, 'message' => 'You have logout successfully.'), 200); exit;               
               }
               else{
                   $this->response(array('status' => true, 'message' => 'User does not exist.'), 200); exit;               
               }
           }
           else {
            $cond     = array("service_provider_id"=>$postData['user_id']);
            $userArr  = $this->common_model->select('sp_device_id',TB_SERVICE_PROVIDER,array('service_provider_id' => $postData['user_id'])); 

            if ($userArr[0]['sp_device_id']) {
               $this->common_model->update(TB_SERVICE_PROVIDER, array("service_provider_id" => $postData['user_id']), array('sp_device_id' => ''));
               $this->response(array('status' => true, 'message' => 'You have logout successfully.'), 200); exit;               
           }
           else{
               $this->response(array('status' => true, 'message' => 'User does not exist.'), 200); exit;               
           }
       }
   } 
   else {
     $this->response(array('status' => false, 'message' => 'Device id is required.')); exit;
 }
} 
else {
 $this->response(array('status' => false, 'message' => 'User id is required.')); exit;
}
}

    /**
     * Title       : resolution_issues
     * Description : Resolution center of service provider issues
     *   
     * @return Array
     */

    public function resolution_issues_get () 
    {
        // Checked user is authenticate or not
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }*/

        // Get Issues list 
        $resultResolutionIssues = $this->Common_model->select("id, parent_name, parent_number, service_provider, issue, issue_date, CASE WHEN issue_status = 1 THEN 'Issue is pending' ELSE 'Issue is resolved' END as issue_status", TB_RESOLUTION_CENTER, array('isDelete' => 1));
        if ( count($resultResolutionIssues) > 0 ) {
            $this->response(array('status' => true, 'result' => $resultResolutionIssues), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'No resolution issues are available.')); exit;       
        }      
    }

    /**
     * Title       : contact_list
     * Description : Contact us list
     *   
     * @return Array
     */

    public function contact_list_get () 
    {
        // Checked user is authenticate or not
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }*/
        
        // Get contact list 
        $resultContactUs = $this->Common_model->select("contact_id as id, contact_name, contact_email as email, contact_phone_number as mobile_no, contact_subject as subject, contact_message as message", TB_CONTACT, array('isDeleted' => 0));
        if ( count($resultContactUs) > 0 ) {
            $this->response(array('status' => true, 'result' => $resultContactUs), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'No contact details are available.')); exit;       
        }      
    }

    /** 
     * Author : Kiran
     * Title : contact_us
     * Description : User contact us
     *   
     * @param String name 
     * @param String email 
     * @param Integer contact_no 
     * @param String message 
     * @return Array
     */

    public function contact_us_post () 
    {
        // Input data
        $postData = $this->post();
        // Input validation
        if ( empty(trim($postData['name'])) ) { 
            $this->response(array("status" => false, "message" => 'Name is required.'), 200); exit; 
        }    
        if ( empty(trim($postData['email'])) ) { 
            $this->response(array("status" => false, "message" => 'Email is required.'), 200); exit; 
        } 
        if ( empty(trim($postData['contact_no'])) ) { 
            $this->response(array("status" => false, "message" => 'Contact no is required.'), 200); exit; 
        }
        if ( empty(trim($postData['message'])) ) { 
            $this->response(array("status" => false, "message" => 'Message is required.'), 200); exit; 
        }

        $contactArr = array( 
            "contact_name" => ucfirst(trim($postData['name'])),
            "contact_email" => trim($postData['email']),
            "contact_phone_number" => trim($postData['contact_no']),
            "contact_message" => trim($postData['message']),
            "status" => '1',
            "created_date" => date('Y-m-d H:i:s')        
            );
        
        // insert contact 
        $resultContactUs = $this->Common_model->insert(TB_CONTACT, $contactArr);
        if ( $resultContactUs ) {
            $username = ucfirst(trim($postData['name'])); 
            $subject =  'Contact us of tulii'; 
            $messageBody = "Hello ".$username." 
            <br/><br/>Thank you for contact us, as soon as possible we will contact us.
            ";    
            $email = trim($postData['email']);
            $send = sendEmailTemplate($email, $messageBody, $subject);
            $this->response(array('status' => true, 'message' => "Contact us has been added successfully."), 200); exit;              
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.')); exit;       
        }      
    }

 // Store card details on Stripe payment gateway
    
    function save_card_post()
    {
        $postData = $_POST;
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }*/

        if(empty($postData['user_id']))
        {
            $this->response(array("status"=>false,"message"=> 'User id is required.'), 200);exit;
        }

        require_once APPPATH."third_party/stripe/init.php";
        \Stripe\Stripe::setApiKey("sk_test_MPxQB3aPE11o9EDJP1Oa0i1z");
        $t = \Stripe\Token::create(array( "card" => array(
            "number" => "4242424242424242",
            "exp_month" => 11,
            "exp_year" => 2018,
            "cvc" => "314"  )));

        $token  = $t->id;
        $name = 'Ashwini';
        $email = 'ashwini@excepptionaire.co';
        $card_num = "4242424242424242";
        $card_cvc = "314";
        $card_exp_month = 11;
        $card_exp_year = 2018;

        $stripe = array(
          "secret_key"      => "sk_test_MPxQB3aPE11o9EDJP1Oa0i1z",
          "publishable_key" => "pk_test_XNBjyrFUKPMx5pszIZuC8563"
          );
        
        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        if($token !='')
        {
            //add customer to stripe
            $customer = \Stripe\Customer::create(array(
                'email' => $email,
                'source'  => $token
                ));

            $itemName = "Stripe Donation";
            $itemNumber = "PS123456";
            $itemPrice = 999;
            $currency = "usd";
            $orderID = "SKA92712382139";

            $charge = \Stripe\Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $itemPrice,
                'currency' => $currency,
                'description' => $itemNumber,
                'metadata' => array(
                    'item_id' => $itemNumber
                    )
                ));
            echo "<pre>";print_r($charge);die;
            if($customer->id !='')
            {

                $userdata = array(                    
                    'customer_id'            => $customer->id
                    );
                $user = $this->db->update(TB_USERS, $userdata, array('user_id' =>$postData['user_id']));

                $this->response(array('status' => true, 'customer_id'=>$customer->id, 'message' => 'You have successfully created customer.'), 200);exit;
            }else{
                $this->response(array('status' => true, 'message' => 'Something went wrong!!!.'), 200);exit;
            }

        }else{
            $this->response(array('status' => true, 'message' => 'You have added invalide card details.'), 200);exit;
        }            
    }

    //Create Account on stripe for Canada

    public function CreateAcc_For_Canada_Strip_post(){

        require_once APPPATH.'third_party/stripe/init.php';
        \Stripe\Stripe::setApiKey('sk_test_MPxQB3aPE11o9EDJP1Oa0i1z');

        try {
        // Use Stripe's library to make requests...
            //Creating connect account
            $account = \Stripe\Account::create(array(
                "type" => "custom",
                "email" => "ramk@exceptionaire.com",
                "country"=>"CA",
                "product_description"=>"mytest",
                "business_url"=>"http://rkinfosoft.com",
                "legal_entity" => array(
                 'type'=>'sole_prop',
                 'address' => array(
                    'city' => 'test',
                    'country' => 'CA',
                    "line1" => 'test',
                    "postal_code" =>'T0E 2K0',
                    "state" => 'AB'
                    ),
                 'first_name'=>'Ram',
                 'last_name'=>'Kedar',
                 'dob' => array(
                    'month' => '05',
                    'day' => '04',
                    "year" => '1997'
                    ),
                 'personal_address' => array(
                    'city' => 'test',
                    'country' => 'CA',
                    "line1" => 'test',
                    "postal_code" =>'T0E 2K0',
                    "state" => 'Alberta',
                    )),
                'statement_descriptor'=>'test',
                'support_phone'=>'(555) 678-1313',
                'external_account' => array(
                    "object" => "bank_account",
                    "country" => 'CA',
                    'currency' => 'cad',
                    'bank_code' => '01234',
                    "branch_code" => '001',
                    "account_number" =>'000123456789',
                    "routing_number" => '01234001'
                    ),
                ));


            //print_r($account);die;
            

            
            // update details of user account
            $acc_details = \Stripe\Account::retrieve($account['id']);
            $acc_details->tos_acceptance->date = time();
            // Assumes you're not using a proxy
            $acc_details->tos_acceptance->ip = $_SERVER['REMOTE_ADDR'];


            $acc_details->legal_entity['first_name'] = 'Ram';
            $acc_details->legal_entity['last_name'] = 'Kedar';
            $acc_details->legal_entity['dob']['day'] = '04';
            $acc_details->legal_entity['dob']['month'] = '05';
            $acc_details->legal_entity['dob']['year'] = '1997';
            $acc_details->legal_entity['type'] = 'individual';//Either “individual” or “company” 
            $acc_details->legal_entity['personal_address']['line1'] = 'test';
            $acc_details->legal_entity['personal_address']['postal_code'] = 'T0E 2K0';
            $acc_details->legal_entity['personal_address']['city'] = 'test';
            $acc_details->legal_entity['personal_address']['state'] = 'Alberta';
            $acc_details->legal_entity['address']['city'] = 'test';
            $acc_details->legal_entity['address']['country'] = 'CA';
            $acc_details->legal_entity['address']['line1'] = 'test';
            $acc_details->legal_entity['address']['postal_code'] = 'T0E 2K0';
            //$acc_details->legal_entity['address']['locality'] = 'test';
            $acc_details->legal_entity['address']['state'] = 'AB';
            //$acc_details->legal_entity['address']['local_timezone'] = 'IST - Calcutta';
           // $acc_details->legal_entity['address']['postal_code'] = '123456';
            //$acc_details->legal_entity['address']['state'] = 'Alberta';
            // $acc_details->legal_entity['support_phone'] = '(555) 678 1212';



            $save_details = $acc_details->save();

            print_r($save_details);
            
            
            /*
            // transfer amount to user account
            $account = \Stripe\Account::retrieve('acct_1ChDYLHm8BuZjOIm');
            // transfer amount to service provider account
            $transfer = \Stripe\Transfer::create(array(
                      "amount" => 10,
                      "currency" => "usd",
                      "destination" => $account['id']
                    ));
            print_r($transfer);
            */

        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        }
        
        
    }

    // Create Account on Stripe for US with Usd

    public function CreateAcc_for_Us_Strip_post(){

        require_once APPPATH.'third_party/stripe/init.php';
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

        try {
        // Use Stripe's library to make requests...
            //Creating connect account
            $account = \Stripe\Account::create(array(
              "type" => "custom",
              "country" => "US",
              "email" => "alfia123@exceptionaire.com",
              "external_account" => array(
                "object" => "bank_account",
                "country" => "US",
                "currency" => "usd",
                "account_holder_name" => 'Alfia Sorte',
                "account_holder_type" => 'individual',
                "routing_number" => "110000000",
                "account_number" => "000123456789"
                )
              ));
            //print_r($account);
            

            
            // update details of user account
            $acc_details = \Stripe\Account::retrieve($account['id']);
            $acc_details->tos_acceptance->date = time();
            // Assumes you're not using a proxy
            $acc_details->tos_acceptance->ip = $_SERVER['REMOTE_ADDR'];


            $acc_details->legal_entity['first_name'] = 'Alfia';
            $acc_details->legal_entity['last_name'] = 'Sorte';
            $acc_details->legal_entity['dob']['day'] = '01';
            $acc_details->legal_entity['dob']['month'] = '01';
            $acc_details->legal_entity['dob']['year'] = '1900';
            $acc_details->legal_entity['type'] = 'individual';//Either “individual” or “company”
            $acc_details->legal_entity['address']['city'] = 'AZ';
            $acc_details->legal_entity['address']['country'] = 'US';
            $acc_details->legal_entity['address']['line1'] = 'MEGASYSTEMS INC';
            $acc_details->legal_entity['address']['line2'] = '799 E DRAGRAM SUITE 5A';
            $acc_details->legal_entity['address']['postal_code'] = '85705';
            $acc_details->legal_entity['address']['state'] = 'TUCSON AZ';
            $acc_details->legal_entity['personal_id_number'] = '000000000';


            $save_details = $acc_details->save();

            // print_r($save_details);
            
            
            /*
            // transfer amount to user account
            $account = \Stripe\Account::retrieve('acct_1ChDYLHm8BuZjOIm');
            // transfer amount to service provider account
            $transfer = \Stripe\Transfer::create(array(
                      "amount" => 10,
                      "currency" => "usd",
                      "destination" => $account['id']
                    ));
            print_r($transfer);
            */

        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        }
        
        
    }


    /**
     * Title       : makePayment_post
     * Description : To make payment
     *   
     * @param user_id 
     * @param booking_id
     * @param amount 
     * @param customer_id 
     * @return Array
     */
    public function makePayment_post()
    {
      $postData = $_POST;
         // echo "<pre>";print_r($_POST);die;
      /*if(is_authorized()) {
        $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
    }*/

    if("" == $postData['user_id'])
    {
      $this->response(array('status' => 'error','message' => 'please enter user id'));
  }

  if("" == $postData['amount'])
  {
      $this->response(array('status' => 'error','message' => 'please enter amount'));
  }

  if("" == $postData['booking_id'])
  {
      $this->response(array('status' => 'error','message' => 'please enter booking id'));
  }



  $getUserData = $this->common_model->select('*',TB_USERS,array('user_id' => $postData['user_id']));
              // echo "<pre>";print_r($getUserData);die;
  if(count($getUserData) > 0)
  {
      if('2' != $getUserData[0]['roleId'])
      {
          $this->response(array('status' => 'error','message' => 'Invalid User Type'));
          die();
      }
  }
  else
  {
      $this->response(array('status' => 'error','message' => 'No User Available'));
      die();
  }

  try
  {
    require_once APPPATH."third_party/stripe/init.php";
                    // \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);


    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 14; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

                /*$itemName = "Stripe Donation dmo";
                $itemNumber = "PS123456";
                $itemPrice = $postData['amount'];*/
                //$currency = "usd";
                $orderID = $randomString;
               // echo "<pre>";print_r($orderID);
                $charge = \Stripe\Charge::create(array(
                 'customer' => $getUserData[0]['customer_id'],
                 'amount'   => ($postData['amount'] * 100), // cents to dollar conversion
                 'currency' => "usd",
                 'description' => "Ride booking",
                 'metadata' => array(
                   'item_id' => $postData['booking_id']
                   )
                 ));

                // echo "<pre>"; print_r($charge);die;

                if(null!= $charge)
                {
                    $data = array(

                        'user_id' => $postData['user_id'],
                        'booking_id' => $postData['booking_id'],
                        'order_id' => $orderID,
                        'status' => 'paid',
                        'sp_status' => 'pending',
                        'amount' => $postData['amount'],
                        'created_at' => date('Y-m-d h:i:s'),
                        'on_hold_reason' => ''
                        );

                    $insertData  = $this->common_model->insert(TB_PAYMENT,$data);

                    if($insertData)
                    {
                        $this->response(array('status' => 'success','message' => 'Payment completed'));
                    }
                    else
                    {
                       $this->response(array('status' => 'error','message' => 'Something went wrong !!'));
                   }
               }
           }
           catch (\Stripe\Error\Card $e)
           {
                // Card was declined.

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\ApiConnection $e)
        {
                // Network problem, perhaps try again.

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\InvalidRequest $e)
        {
                // You screwed up in your programming. Shouldn't happen!

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\Api $e)
        {
                // Stripe's servers are down!

            $this->response(array('status' => 'error','message' => $e->getJsonBody()));

        }
        catch (\Stripe\Error\Base $e)
        {
                // Something else that's not the customer's fault.

         $this->response(array('status' => 'error','message' => $e->getJsonBody()));
     }
 }

    /**
     * Title : payment_onhold
     * Description : payment onhold of service provider
     *
     * @param Integer payment_id
     * @param String reason
     * @return Array
     */

    public function payment_onhold_post ()
    {
        // Input data
        $postData = $this->post();

        // Checked user are authenticate or not
       /* if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  

        // Validation
        if ( trim($postData['payment_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'payment id field is required.'), 200); exit; 
        }    
        if ( trim($postData['reason']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Reason field is required.'), 200); exit; 
        }        
        
        // payment onhold of service provider 
        $resultUpdate = $this->Common_model->update(TB_PAYMENT, array('id' => $postData['payment_id']), array("on_hold_reason" => $postData['reason'], "status" => ONHOLD));
        if ( $resultUpdate ) {
            $this->response(array('status' => true, 'message' => 'Payment has been onhold successfully .'), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'));exit;
        }  
        
    }   


    /**
     * Title : payment_pending_list
     * Description : show the list of payment pending list
     *
     * @return Array     
     */ 

    public function payment_pending_list_get ()
    {
        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  
        $payPendingList = $payList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => PENDING);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {

            foreach ($paymentData as $key => $value) {
                $payPendingList[$i]['payment_id']                = $value['payment_id'];
                $payPendingList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payPendingList[$i]['Driver_name']               = $value['user_name'];
                $payPendingList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payPendingList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payPendingList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payPendingList[$i]['booking_price']             = $value['booking_price']; 
                $payPendingList[$i]['status']                    = $value['status']; 
                $payPendingList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            $payList['pending_payment_list'] =  $payPendingList; 
            
            $this->response(array("status" => true, "result" => $payList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'Payment pending records not found.')); exit;
        }
    }   

    /**
     * Title : payment_paid_list
     * Description : show the list of payment paid list
     *
     * @return Array     
     */ 

    public function payment_paid_list_get ()
    {
        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  

        $payPaidList = $payList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => PAID);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {

            foreach ($paymentData as $key => $value) {
                $payPaidList[$i]['payment_id']                = $value['payment_id'];
                $payPaidList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payPaidList[$i]['Driver_name']               = $value['user_name'];
                $payPaidList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payPaidList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payPaidList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payPaidList[$i]['booking_price']             = $value['booking_price']; 
                $payPaidList[$i]['status']                    = $value['status']; 
                $payPaidList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            $payList['paid_payment_list'] =  $payPaidList; 
            
            $this->response(array("status" => true, "result" => $payList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'Payment paid records not found.')); exit;
        }
    } 

    /**
     * Title : payment_onhold_list
     * Description : show the list of payment on-hold list
     *
     * @return Array     
     */ 

    public function payment_onhold_list_get ()
    {
        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  

        $payOnholdList = $payList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".status" => ONHOLD);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {

            foreach ($paymentData as $key => $value) {
                $payOnholdList[$i]['payment_id']                = $value['payment_id'];
                $payOnholdList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payOnholdList[$i]['Driver_name']               = $value['user_name'];
                $payOnholdList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payOnholdList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payOnholdList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payOnholdList[$i]['booking_price']             = $value['booking_price']; 
                $payOnholdList[$i]['status']                    = $value['status']; 
                $payOnholdList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            $payList['onhold_payment_list'] =  $payOnholdList; 
            
            $this->response(array("status" => true, "result" => $payList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'Payment on-hold records not found.')); exit;
        }
    } 

    /**
    * Ashwini changes here
     * Title : add_group
     * Description : create new groups
     *
     * @param Integer user_id
     * @param Integer group_id
     * @param String group_name
     * @param String group_flag
     * @param String created by
     * @return Array
     */

    public function add_group_post ()
    {
        // Input data
        $postData = $this->post();

        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        } */ 

        if(empty(trim($postData['group_flag'])))
        {
            $this->response(array("status" => "error", "message" => 'group flag field is required.'), 200); exit; 
        }

        // Validation
        if(trim($postData['group_flag']) != "parent_parent")
        {
            if ( trim($postData['user_id']) == "" ) { 
                $this->response(array("status" => "error", "message" => 'User id field is required.'), 200); exit; 
            }
        }    
        if ( trim($postData['group_name']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'group name field is required.'), 200); exit; 
        } else if ( is_group_exist(trim($postData['group_name'])) && empty($postData['group_id']) ) {
            $this->response(array("status" => "error", "message" => 'group name are already exist.'), 200); exit; 
        }

        $count = count(explode(',',$postData['user_id']));    

        if ( !empty($postData['group_id']) ) {

            // Group member data update 
            $resultUpdate = $this->Common_model->update(TB_GROUP, array('group_id' => $postData['group_id']), array("user_id" => $postData['user_id'], "group_name" => $postData['group_name'], "no_of_people" => $count, 'updated_date' => date("Y-m-d H:i:s"), "updated_by" => $postData['modify_by']));
            if ( $resultUpdate ) {
                $this->response(array('status' => true, 'message' => 'Group has been Updated successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'));exit;
            }  
        } else {
            // insert group details
            if(trim($postData['group_flag']) == "parent_parent")
            {
                $all_user = '';
                $total_people = 0;
            }else
            {
                $all_user = trim($postData['user_id']);
                $total_people = $count;
            }

            $arrData    = array("user_id" => $all_user, "group_name" => $postData['group_name'], "no_of_people" => $total_people, "created_date" => date("Y-m-d H:i:s"), "created_by" => $postData['created_by']);
            $resultGrouData = $this->Common_model->insert(TB_GROUP, $arrData);
            if ($resultGrouData) {
                if(trim($postData['group_flag']) == "parent_parent")
                {
                    $each_user = explode(",",trim($postData['user_id']));
                    foreach ($each_user as $keyu => $valueu) 
                    {
                     $insertArr[] = array(
                        "group_id"=>$resultGrouData,
                        "sender_id"=>trim($postData['created_by']),
                        "receiver_id"=>$valueu,
                        "created_at"=>date("Y-m-d")
                        );
                 }
                 $result = $this->common_model->insert_batch(TB_TEMPORARY_GROUP,$insertArr);

             }



             $this->response(array("status" => true, "message" => 'Group has been created successfully.'), 200); exit;
         } else {
            $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
        }
    }
}

    /**
     * Title : group_list
     * Description : show the list of group name
     *
     * @return Array     
     */ 

    public function group_list_get ()
    {
        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  
        $groupList = $groupMemers = array();
        $i = $j = 0;
        $cond     = array(TB_GROUP.'.isDeleted'=>1);
        $jointype = array(TB_USERS => "LEFT");
        $join     = array(TB_USERS => TB_USERS.".user_id=".TB_GROUP.".created_by");
        $resultOfGroupData = $this->Common_model->selectJoin(TB_GROUP.".user_id, group_id, group_name, no_of_people, created_by,user_name as createdBy",TB_GROUP,$cond,array(),$join,$jointype);
        
        if( count($resultOfGroupData) > 0 ) {
            foreach ($resultOfGroupData as $key => $value) {
                $groupMemers = array();
                $resultUsersName = $this->Common_model->select_where_in_with_no_quote("user_id, user_name, user_email", TB_USERS,"user_id",(!empty($value['user_id'])?$value['user_id']:0));    
                $groupList[$i]['group_id']     = $value['group_id'];
                $groupList[$i]['group_name']   = $value['group_name'];
                $groupList[$i]['no_of_people'] = $value['no_of_people'];
                $groupList[$i]['group_id']     = $value['group_id']; 
                $groupList[$i]['created_by']   = $value['createdBy'];
                
                foreach ($resultUsersName as $key1 => $value1) {
                    $groupMemers[$j]['id']           = $value1['user_id'];
                    $groupMemers[$j]['member_name']  = $value1['user_name'];
                    $groupMemers[$j]['member_email'] = $value1['user_email'];
                    $j++;           
                }   
                $groupList[$i]['group_members'] = $groupMemers; 
                $groupMemers = array();
                $j = 0;
                $i++;
            }
            
            if ( $resultOfGroupData ) {
                $this->response(array("status" => true, "result" => $groupList), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
            }
        } else {
            $this->response(array("status" => false, "message" => 'No groups found.')); exit;
        }
    }   

    /**
     * Title : delete_group
     * Description : delete group  
     * 
     * @param Integer group_id
     * @return Array 
     */

    function delete_group_post() 
    {
        // Input data
        $postData = $_POST; 

        // Validation
        if ( trim($postData['group_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Group id field is required.'), 200); exit; 
        }  
        // Group data is Soft delete 
        $resultDelete = $this->common_model->update(TB_GROUP_MASTER,array('group_id' => $postData['group_id']),array('is_Deleted' => "1", 'updated_date' => date("Y-m-d H:i:s")));
        if($resultDelete) {
            $this->response(array('status' => true, 'message' => 'Group has been deleted successfully .'), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'));exit;
        }                                   
    }

     /**
     * Author : Priya
     * Title : delete_group_member
     * Description : delete group member
     * 
     * @param multiple comma separted passenger_id
     * @param Integer group_id
     * @return Array
     */

     public function delete_group_member_post()
     {
        $postData = $this->post();
        if($postData['passenger_id'] == '')
        {
            $this->response(array("status"=>false, "message" => 'passenger id is required.'), 200);
            exit;
        }
        if(empty(trim($postData['group_id'])))
        {
            $this->response(array("status"=>false, "message" => 'Group id is required.'), 200);
            exit;
        }

        $groupId = trim($postData['group_id']);
        $passangerArray = $postData['passenger_id'];
        $result = $this->common_model->deleteMultiPass($passangerArray, $groupId) ;

        if ($result) {
            $this->response(array("status" => true, "message" => "Group member has been deleted successfully"), 200); exit; 
        } else {
            $this->response(array("status" => false, "message" => 'passenger id or group id not exists'), 200); exit;
        }
        
    }
    
    //Priya
    public function deleteMultiPass($whereArray, $groupId) {
        $this->db->query("UPDATE ".TB_GROUP_MEMBER." SET status = '0' WHERE passenger_id IN (".$whereArray.") AND group_id = ".$groupId." ");
        return $this->db->affected_rows();
    }
    /**
      Jyoti korde
     * Title : vehicle_details
     * Description : add vehicle
     *
     * @param Integer user_id
     * @param Integer car_model
     * @param Integer vehicle_category_id
     * @param String car_reg_no
     * @param String car_manufacturing_year
     * @param String car_pic
     * @param String car_number_plate_pic
     * @param String car_number_plate
     * @param String licence_scan_copy
     * @return Array
     */

      public function vehicle_details_post ()
      {
        // Input data
        $postData = $this->post();

        
        // Input Validation
        if ( trim($postData['user_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'User id is required.'), 200); exit; 
        }
        //Check user exist
        if ( $this->service_provider_exists($postData["user_id"]) == 0 ) {
            $this->response(array("status" => false,"message" => 'User id is not exists.')); exit;
        }    
        if ( trim($postData['car_model']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car model is required.'), 200); exit; 
        } 
        if ( trim($postData['vehicle_category_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Vehicle category id is required.'), 200); exit; 
        } 
        if ( trim($postData['car_reg_no']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car register no is required.'), 200); exit; 
        }    
        // if ( trim($postData['car_seating_capacity']) == "" ) { 
        //     $this->response(array("status" => "error", "message" => 'Car seating capacity is required.'), 200); exit; 
        // } 
        if ( trim($postData['car_manufacturing_year']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Car manufacturing year is required.'), 200); exit; 
        } 
        if ( trim($postData['car_number_plate']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'car number plate is required.'), 200); exit; 
        } 

        if(!isset($_FILES['car_pic']))
        {
            $this->response(array("status" => "error", "message" => 'Car pictures is required.'), 200); exit;
        }
        else if(isset($_FILES['car_pic']) && count($_FILES['car_pic']['error']) == 1 && $_FILES['car_pic']['error'][0] > 0 ) {
            $this->response(array("status" => "error", "message" => 'Car pictures is required.'), 200); exit; 
        } 

        if(!isset($_FILES['car_number_plate_pic']))
        {
            $this->response(array("status" => "error", "message" => 'Car number plate picture is required.'), 200); exit; 
        }else if(isset($_FILES['car_number_plate_pic']) && count($_FILES['car_number_plate_pic']['error']) == 1 && $_FILES['car_number_plate_pic']['error'][0] > 0)
        { 
            $this->response(array("status" => "error", "message" => 'Car number plate picture is required.'), 200); exit; 
        }

        if(!isset($_FILES['licence_scan_copy']))
        {
            $this->response(array("status" => "error", "message" => 'licence scan copy is required.'), 200); exit;
        }else if(isset($_FILES['licence_scan_copy']) && count($_FILES['licence_scan_copy']['error']) == 1 && $_FILES['licence_scan_copy']['error'][0] > 0)
        {
            $this->response(array("status" => "error", "message" => 'licence scan copy is required.'), 200); exit; 
        } 
        

        $fileUploaded = multiple_pics_uploads($_FILES);
        $certificate  = $fileUploaded['certificate'];
        $license      = $fileUploaded['license'];
        $car          = $fileUploaded['car'];
        $car_number   = $fileUploaded['car_number']; 

        // insert vehicle details
        $arrData = array(
            "service_provider_id"    => $postData['user_id'],
            "vehicle_category_id"    => $postData['vehicle_category_id'],
            "car_model"              => $postData['car_model'],
            "car_reg_no"             => $postData['car_reg_no'],
            // "car_seating_capacity"   => $postData['car_seating_capacity'],
            "car_manufacturing_year" => $postData['car_manufacturing_year'],
            "car_number_plate"       => $postData['car_number_plate']                        
            ); 
            
        if ( !empty($postData['vehicle_id']) ) {    
            $resultOfVehicleDetails = $this->common_model->select("", TB_SERVICE_PROVIDER_VEHICLE, array("vehicle_id" => $postData['vehicle_id']));
            $arrData["car_pic"]              = !empty($car)         ? $car         : $resultOfVehicleDetails[0]['car_pic'];
            $arrData["car_number_plate_pic"] = !empty($car_number)  ? $car_number  : $resultOfVehicleDetails[0]['car_number_plate_pic'];
            $arrData["licence_scan_copy"]    = !empty($license)     ? $license     : $resultOfVehicleDetails[0]['licence_scan_copy'];
            $arrData["certification_pics"]   = !empty($certificate) ? $certificate : $resultOfVehicleDetails[0]['certification_pics'];
            $arrData["updated_date"]         = date("Y-m-d H:i:s");     

            // Vehicle data update 
            $resultUpdate = $this->Common_model->update(TB_SERVICE_PROVIDER_VEHICLE, array('vehicle_id' => $postData['vehicle_id']), $arrData);
            if ( $resultUpdate ) {
                $this->response(array('status' => true, 'message' => 'Vehicle details has been Updated successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'));exit;
            }  
        } else {
            $arrData["car_pic"]              = $car;
            $arrData["car_number_plate_pic"] = $car_number;
            $arrData["licence_scan_copy"]    = $license;
            $arrData["certification_pics"]   = $certificate;          
            $arrData["is_default"]           = "0";
            $arrData["status"]               = "0";
            $arrData["created_date"]          = date("Y-m-d H:i:s");
           //echo "<pre>";print_r($arrData);die;

            $resultVehicleData = $this->Common_model->insert(TB_SERVICE_PROVIDER_VEHICLE, $arrData);
            if ( $resultVehicleData ) {
                $this->response(array("status" => true, "message" => 'Vehicle details has been inserted successfully.'), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong')); exit;
            }
        }
    }


     /** Author: Jyoti Korde
     * Title : set_default_vehicle
     * Description : Set default vehicle
     *
     * @param Integer user_id
     * @param Integer vehicle_id
     * @return Array
     */

     public function set_default_vehicle_post ()
     {
        $postData = $this->post();
        
        if(empty($postData['user_id']))
        {
            $this->response(array('status' => false, 'message' => 'User id is required.'), 200);exit;
        }
        if(empty($postData['vehicle_id']))
        {
            $this->response(array('status' => false, 'message' => 'Vehicle id is required.'), 200);exit;
        }

        $arrData = array("is_default"=>'1');
        $cond = array('vehicle_id'=>$postData['vehicle_id'],'service_provider_id'=>$postData['user_id']);
        // Vehicle data update 

        $exist = $this->common_model->select('vehicle_id',TB_SERVICE_PROVIDER_VEHICLE,array('service_provider_id' => $postData['user_id'],'is_default' => '1'));

        if(count($exist[0]) > 0)
        {
            $this->response(array('status' => false,'message' =>'Default vehicle is already set '));
            exit();
        }

        $resultUpdate = $this->Common_model->update(TB_SERVICE_PROVIDER_VEHICLE, $cond, $arrData);
        if($resultUpdate)
        {
            $this->response(array('status' => true, 'message' => 'Vehicle successfully set as default.'), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Something went wrong.'), 200);exit;
        }  

    }



    /**
     * Title : get_payment_details
     * Description : show the details of payment 
     *
     * @param Integer payment_id
     * @param Integer booking_id
     * @return Array     
     */ 

    public function get_payment_details_post ()
    {
        // Checked user are authenticate or not
        /*if ( is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }*/  

        //Input data
        $postData = $this->post();

         // Validation
        if ( trim($postData['payment_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Payment id field is required.'), 200); exit; 
        }    
        if ( trim($postData['booking_id']) == "" ) { 
            $this->response(array("status" => "error", "message" => 'Booking id field is required.'), 200); exit; 
        }

        $payPendingList = array();
        $i = 0;

        $cond        = array(TB_BOOKINGS.'.booking_status' => COMPLETED, TB_USERS.".isDeleted" => 0, TB_PAYMENT.".id" => $postData['payment_id'], TB_PAYMENT.".booking_id" => $postData['booking_id']);
        $jointype    = array(TB_BOOKINGS => "LEFT", TB_USERS => "LEFT", TB_SERVICE_REQUESTS => "LEFT");
        $join        = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_PAYMENT.".booking_id", TB_USERS => TB_USERS.".user_id = ".TB_PAYMENT.".user_id", TB_SERVICE_REQUESTS => TB_SERVICE_REQUESTS.'.service_booking_id = '.TB_BOOKINGS.'.booking_id');
        $paymentData = $this->Common_model->selectJoin("id as payment_id, service_provider_user_id, user_name, booking_pick_up_location, booking_drop_off_location, booking_start_date, booking_price, status, user_rating",TB_PAYMENT,$cond,array(),$join,$jointype);
        
        if( count($paymentData) > 0 ) {

            foreach ($paymentData as $key => $value) {
                $payPendingList[$i]['payment_id']                = $value['payment_id'];
                $payPendingList[$i]['service_provider_user_id']  = $value['service_provider_user_id'];
                $payPendingList[$i]['Driver_name']               = $value['user_name'];
                $payPendingList[$i]['booking_pick_up_location']  = $value['booking_pick_up_location']; 
                $payPendingList[$i]['booking_drop_off_location'] = $value['booking_drop_off_location']; 
                $payPendingList[$i]['booking_start_date']        = $value['booking_start_date']; 
                $payPendingList[$i]['booking_price']             = $value['booking_price']; 
                $payPendingList[$i]['status']                    = $value['status']; 
                $payPendingList[$i]['user_rating']               = $value['user_rating'];                

                $i++;
            }
            
            $this->response(array("status" => true, "result" => $payPendingList), 200); exit;
            
        } else {
            $this->response(array("status" => false, "message" => 'NO records found.')); exit;
        }
    }

    /**
     * Title : upload_students
     * Description : To upload multiple students by csv
     *
     * @param Integer user_id
     * @param File csv_file
     * @return Array
     */
    public function upload_students_post()
    {
        $postData = $this->post();
        /*if(is_authorized()) {
            $this->response(array("status"=>false,"message"=> 'You are not authorized to access'), 401);exit;
        }*/

        $user_id = $postData['user_id'];

        // check file is set
        if(!isset($_FILES['csv_file']['tmp_name'])) {
            $this->response(array("status"=>false,"message"=> 'Please upload csv file.'));exit;
        } else {
            $allowed =  array('csv');
            $filename = $_FILES['csv_file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $this->response(array("status"=>false,"message"=> 'Invalid file format, Please upload CSV file.'));exit;
            }else{
                $count=0;
                $total_records= 0;
                $fp = fopen($_FILES['csv_file']['tmp_name'],'r') or die("can't open file");
                $err = 0;
                while($csv_line = fgetcsv($fp,1024))
                {
                    $count++;
                    if($count == 1)
                    {
                        continue;
                    }//keep this if condition if you want to remove the first row
                   // echo "<pre>";print_r($csv_line);die;
                    for($i = 0, $j = count($csv_line); $i < $j; $i++)
                    {
                        $insert_csv = array();
                        $insert_csv['stud_name'] = $csv_line[0];
                        $insert_csv['stud_username'] = $csv_line[1];
                        $insert_csv['stud_age'] = $csv_line[2];
                        $insert_csv['stud_address'] = $csv_line[3];
                        $insert_csv['parent_name'] = $csv_line[4];
                        $insert_csv['parent_phone'] = $csv_line[5];
                        $insert_csv['parent_email'] = $csv_line[6];

                    }
                    $i++;

                    if((!empty($insert_csv['stud_name'])) && (!empty($insert_csv['stud_username'])) && (!empty($insert_csv['stud_age'])) && (!empty($insert_csv['stud_address'])) && (!empty($insert_csv['parent_name'])) && (!empty($insert_csv['parent_phone'])) && (!empty($insert_csv['parent_email'])) ){

                        $current_time = date( 'Y-m-d H:i:s', time());
                        $data_new[] = array(
                            $user_id,
                            "'".$insert_csv['stud_name']."'",
                            "'".$insert_csv['stud_username']."'",
                            "'".$insert_csv['stud_age']."'",
                            "'".$insert_csv['stud_address']."'",
                            "'".$insert_csv['parent_name']."'",
                            "'".$insert_csv['parent_phone']."'",
                            "'".$insert_csv['parent_email']."'",
                            "'".$current_time."'");
                        $total_records++;
                    }
                    else
                    {
                        $err = 1;
                    }
                }

                if(1 == $err)
                {
                    echo $this->response(array('status' => false,'message' => 'Please enter mandatory information'),200);

                    exit();
                }

                $key_new = "user_id,stud_name,stud_username,stud_age,stud_address,parent_name,parent_phone,parent_email,created_date";

                if(count($data_new) > 0)
                {
                    $result = $this->common_model->insert_ignore(TB_STUDENT,$key_new,$data_new);
                     // echo $this->db->last_query();die;
                    fclose($fp) or die("can't close file");

                    if(!empty($result)){
                        $this->response(array("status"=>true,"result"=> array('total_records_inserted'=>$result)), 200);exit;
                    }
                }
                else
                {
                     echo $this->response(array('status' => false,'message' => 'Something went wrong.'),200);
                }
                

            }

        }

    } 

    /**
     * Title : passenger_listing
     * Description : listing of passeger 
     *
     * @param Integer service_user_id
     * @return Array
     */

   /* public function passenger_listing_post ()
    {
        //Input data
        $postData = $this->post();
        
        // Validation to check user id not empty
        if (trim($postData['service_user_id']) == "") { 
            $this->response(array("status" => false, "message" => 'User id is required.'), 200); exit; 
        } else {

            // Fetch all records according of user id
            $cond = array("service_user_id" => trim($postData['service_user_id']));
            $ord = array("pass_fullname" => "ASC");
            $resultPassenger = $this->common_model->selectQuery("*", TB_PASSENGER, $cond, $ord);

            if (count($resultPassenger) > 0) {
                $this->response(array("status" => true, "result" => $resultPassenger), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Data not found.'), 200); exit; 
            }
        }        
    }*/
    public function passenger_listing_post ()
    {
        //Input data
        $postData = $this->post();
        
        // Validation to check user id not empty
        if (trim($postData['user_id']) == "") { 
            $this->response(array("status" => false, "message" => 'User id is required.'), 200); exit; 
        } else {

            // Fetch all records according of user id
            $checkUserType = $this->common_model->select("user_type_id",TB_SERVICE_USERS,array("service_user_id"=>$postData['user_id']));
            if(count($checkUserType) == '0'){
               $this->response(array("status" => false, "message" => 'User id is not available'), 200); exit;
            }
            else{
                $user_type = $checkUserType[0]['user_type_id'];
                if($user_type == 2){
                    $select='passenger_id,service_user_id,pass_fullname,pass_dob,pass_age,pass_standard';
                }
                else if($user_type == 5 || $user_type == 6) {
                    $select='passenger_id,service_user_id,pass_username,pass_fullname,pass_parent_name,pass_gender,pass_contact_no,pass_address';
                }
                $cond = array("service_user_id" => trim($postData['user_id']),'is_deleted' => '0') ;
                $ord = array("pass_fullname" => "ASC");
                $resultPassenger = $this->common_model->selectQuery($select, TB_PASSENGER, $cond, $ord);
                if (count($resultPassenger) > 0) {
                    $this->response(array("status" =>true ,"message" => 'Data found.',"Passenger_listing" => $resultPassenger), 200); exit;
                } else {
                    $this->response(array("status" => false, "message" => 'Data not found.'), 200); exit; 
                }
            }
        }        
    }

    /**
     * Title : add_passenger
     * Description : To add new passenger
     * 
     * @param Integer service_user_id
     * @param Varchar user_name
     * @param Varchar full_name
     * @param Varchar gender
     * @param Date dob
     * @param Varchar parent_name
     * @param Integer parent_phone
     * @param Varchar address
     * @return Array 
     */

    /*public function add_passenger_post ()
    {
        // Input data
        $postData = $this->post();
        
        $service_user_id = $postData['service_user_id'];
        $user_name = $postData['user_name'];
        $full_name = $postData['full_name'];
        $gender = $postData['gender'];
        $dob = $postData['dob'];        
        $parent_name = $postData['parent_name'];
        $parent_phone = $postData['parent_phone'];
        $address = $postData['address'];
        $current_time = date( 'Y-m-d H:i:s', time());
        
        // Validation to check all inputs are required
        if ((!empty($service_user_id)) && 
           (!empty($user_name)) && 
           (!empty($full_name)) && 
           (!empty($gender)) && 
           (!empty($address)) && 
           (!empty($parent_name)) && 
           (!empty($parent_phone)) && 
           (!empty($dob)))
        {
            // Check user name already exist
            $checkExists = $this->common_model->select("pass_username", TB_PASSENGER, array("pass_username"=>$user_name));
            if (count($checkExists) > 0) {
                $this->response(array("status" => false,"message" => 'Passenger already exists with same username.'), 200); exit; 
            } else {

                $data = array(
                    'service_user_id' => $service_user_id,                    
                    'pass_username' => $user_name,
                    'pass_fullname' => $full_name,
                    'pass_gender' => $gender,
                    'pass_address' => $address,
                    'pass_parent_name' => $parent_name,
                    'pass_contact_no' => $parent_phone,
                    'pass_dob' => $dob,
                    'created_at' => $current_time,
                    'pass_status' => "1"
                );
                // Add data
                $last_id = $this->common_model->insert(TB_PASSENGER, $data);

                if ($last_id) {
                    $msg = "Dear $parent_name, $full_name is successfully registered on Tulii as student.";
                    // Send message to parent phone
                    $resMessage = "";
                    $from = 'TFCTOR';
                    $SendAt = date("Y-m-d");
                    $response = sendOTP($msg, trim($parent_phone),$from);
                    if ($response) {
                        $resMessage = "Message to parent sent successfully.";
                    } else {
                        $resMessage = $response;
                    }
                    
                    $this->response(array("status" => true, "result" => array('msg' => 'Passenger added successfully.', 'student_id' => $last_id, 'sms_message' => $resMessage)), 200);exit;
                } else {
                    $this->response(array("status" => false, "message" => 'Something went wrong.')); exit;
                }
            }
        } else {
            $this->response(array("status" => false, "message" => 'Please provide all required fields.')); exit;
        }
    }*/

    /**
     * Title : update_passenger
     * Description : To edit existing passenger
     * 
     * @param Integer passenger_id
     * @param Integer service_user_id
     * @param Varchar user_name
     * @param Varchar full_name
     * @param Varchar gender
     * @param Date dob
     * @param Varchar parent_name
     * @param Integer parent_phone
     * @param Varchar address
     * @return Array 
     */
    /*public function update_passenger_post()
    {
        // Input data
        $postData = $this->post();
        
        $passenger_id = $postData['passenger_id'];
        $service_user_id = $postData['service_user_id'];
        $user_name = $postData['user_name'];
        $full_name = $postData['full_name'];
        $gender = $postData['gender'];
        $dob = $postData['dob'];        
        $parent_name = $postData['parent_name'];
        $parent_phone = $postData['parent_phone'];
        $address = $postData['address'];
        $current_time = date('Y-m-d H:i:s', time());        
            
        // Validation to check all inputs are required
        if ((!empty($service_user_id)) && 
           (!empty($user_name)) && 
           (!empty($full_name)) && 
           (!empty($gender)) && 
           (!empty($address)) && 
           (!empty($parent_name)) && 
           (!empty($parent_phone)) && 
           (!empty($dob)) && 
           (!empty($passenger_id))) 
        {
            // check passenger exist
            $resultPassengerExistOrNot = $this->common_model->select("pass_id", TB_PASSENGER, array("pass_id" => $postData['passenger_id']));
            if (count($resultPassengerExistOrNot) > 0) {
                
                // Check user name already exist
                $checkExists = $this->common_model->select("pass_username", TB_PASSENGER, array("pass_username" => $user_name, "pass_id !=" => $postData['passenger_id']));
                if (count($checkExists) > 0) {
                    $this->response(array("status" => false,"message" => 'Passenger already exists with same username.'), 200); exit; 
                } else {
                    $update_data = array(
                        'service_user_id' => $service_user_id,                    
                        'pass_username' => $user_name,
                        'pass_fullname' => $full_name,
                        'pass_gender' => $gender,
                        'pass_address' => $address,
                        'pass_parent_name' => $parent_name,
                        'pass_contact_no' => $parent_phone,
                        'pass_dob' => $dob,
                        'updated_at' => $current_time
                    );

                    // update passenger
                    try {
                        $this->common_model->update(TB_PASSENGER, array("pass_id" => $passenger_id), $update_data);
                    } catch (Exception $ex) {
                        $this->response(array('status' => false, 'message' => $ex->getCode())); exit;
                    }                
                    $this->response(array('status' => true, 'message' => 'Passenger updated successfully.'), 200); exit;
                    }
                } else {
                // Passenger dose not exist
                $this->response(array('status' => false, 'message' => "Passenger does not exist"));exit;
            }
        } else {
            $this->response(array("status" => false,"message" => 'Please provide all required fields.')); exit;
        }       
    }*/


    /**
     * Title : delete_passenger
     * Description : delete passenger  
     * 
     * @param Integer passenger_id
     * @return Array 
     */

    function delete_passenger_post() 
    {
        // Input data
        $postData = $this->post(); 

        // Validation
        if (trim($postData['passenger_id']) == "") { 
            $this->response(array("status" => "error", "message" => 'Passenger id field is required.')); exit; 
        }  

        // check passenger exist
        $resultPassExistOrNot = $this->common_model->select("pass_id", TB_PASSENGER, array("pass_id" => $postData['passenger_id']));
        if (count($resultPassExistOrNot) > 0) {

            // Passenger delete 
            $resultPassDelete = $this->common_model->delete(TB_PASSENGER, array('pass_id' => $postData['passenger_id']));
            if ($resultPassDelete) {
                $this->response(array('status' => true, 'message' => 'Passenger has been deleted successfully .'), 200); exit;
            } else {
                $this->response(array('status' => false, 'message' => 'Something went wrong.'));exit;
            }   
        } else {
            // Passenger dose not exist
            $this->response(array('status' => false, 'message' => "Passenger does not exist"));exit;
        }                                
    }

    /**
     * Title : add student to existing group
     * Description : Api for add new students in the existing group
     * 
     * @param Integer group_id
     * @param Integer student_id
     * @return Array 
     */
    public function student_add_group_post()
    {
        // Checked user are authenticate or not
       /* if ( is_authorized() ) {
            $this->response(array("status" => false, "message" => 'You are not authorized to access'), 401); exit;
        } */

        // Input data
        $postData = $_POST; 

        $student_id = $postData['student_id'];
        $group_id = $postData['group_id'];

        // Validation
        if ( (trim($postData['student_id']) == "") || (trim($postData['group_id']) == "") ) { 
            $this->response(array("status" => "error", "message" => 'Please provide required parameters.'), 200); exit; 
        }

        // check student exist
        $db_student_id = $this->common_model->select("stud_id,stud_name,  parent_name,parent_phone",TB_STUDENT,array("stud_id"=>$postData['student_id']));
        if(count($db_student_id) > 0) {
            //check group exist

            $group_details = $this->common_model->select("*",TB_GROUP,array("group_id"=>$postData['group_id']));
            if(count($group_details) > 0) {
                foreach ($group_details as $key => $value) {

                    $groupMembers = $value['user_id'];
                    $no_of_people = $value['no_of_people'];

                    if($groupMembers!=""){
                        $members_arr = explode(',',$groupMembers);
                        if (in_array($student_id, $members_arr))
                        {
                            $this->response(array('status' => false, 'message' => "Student is member of this group"));exit;
                        }
                        else
                        {
                                //add student to member string
                            $members_str = $groupMembers.','.$student_id;
                        }
                    }else{
                        $members_str = $student_id;
                    }

                    $total_ppl = $no_of_people + 1;
                        // update student to group
                    $update_data = array(
                        'user_id' => $members_str,
                        'no_of_people' => $total_ppl
                        );

                    try {
                     $this->common_model->update(TB_GROUP,array("group_id"=>$group_id),$update_data);
                     $msg = 'Dear '.$db_student_id[0]['parent_name'].', Your child'.$db_student_id[0]['stud_name'].' is added in '.$group_details [0]['group_name'];

                     $to = $db_student_id[0]['parent_phone'];
                            //$to = '9766792917';
                     $responce = sendOTP($msg, trim($to));


                 } catch (Exception $ex) {
                    $this->response(array('status' => false, 'message' => $ex->getCode()));exit;
                }                
                $this->response(array('status' => true, 'message' => 'Student added in group successfully.'), 200);exit;


            }
        }else{
                    // group dose not exist
            $this->response(array('status' => false, 'message' => "Group does not exist"));exit;
        }
    }else{
                // student dose not exist
        $this->response(array('status' => false, 'message' => "Student does not exist"));exit;
    }   
}

    //----------- Ashwini Code start ---------------

    /**
    * Ashwini code
     * Title : group listing
     * @param Integer user_id
     * @return Array
     */
    public function group_listing_post()
    {
        $postData = $this->post();
        if(empty(trim($postData['user_id'])))
        {
            $this->response(array("status"=>false, "message" => 'User id is required.'), 200);
            exit;
        }    
        $cond = array("service_user_id"=>trim($postData['user_id']),"is_Deleted" => '0');
        $result = $this->common_model->select("*",TB_GROUP_MASTER,$cond);
        if(!empty($result))
        {
            $this->response(array("status"=>true, "data" => $result), 200);
            exit;
        }else
        {
            $this->response(array("status"=>false, "message" => 'Record not found.'), 200);
            exit;
        }    
    }


    /**
     * Title : Display Group members
     * @param Integer group_id
     * @return Array
     */
    public function list_group_members_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(trim($postData['group_id']) == "" ) 
            { 
                $this->response(array("status" => "error", "message" => 'Group id is required.'), 200); exit; 
            }else
            {
                $cond=array("group_id"=>trim($postData['group_id']));
                $getUser = $this->common_model->select("user_id",TB_GROUP,$cond);
                $stud_ids = $getUser[0]['user_id'];
                if(!empty($stud_ids))
                {
                    $result = $this->common_model->select_where_in_with_no_quote("stud_id,stud_name",TB_STUDENT,"stud_id",$stud_ids);
                    if(!(empty($result)))
                    {
                        $this->response(array("status" => "success", "data" => $result), 200); exit;
                    }else
                    {
                        $this->response(array("status" => "error", "message" => 'Data not found.'), 200); exit;        
                    }
                }else
                {
                 $this->response(array("status" => "error", "message" => 'Data not found.'), 200); exit; 
             }
         }
    // }
     }


    /**
     * Author : kiran
     * Title : Group shifting
     * Desc : Shift members from one group to another group
     *
     * @param Integer from_group_id
     * @param Integer to_group_id
     * @param Integer student_id
     * @param Integer status
     * @param Integer update_by
     * @param Date temp_date
     * @return Array
     */

    public function shift_group_post()
    {
        // Input data 
        $postData = $this->post();
        $from_group_id = trim($postData['from_group_id']);
        $to_group_id = trim($postData['to_group_id']);
        $student_id = trim($postData['student_id']);
        $status = trim($postData['status']);
        $temp_date = trim($postData['temp_date']);
        $update_by = trim($postData['update_by']);

        //Input validation
        if($from_group_id == "" ) {
            $this->response(array("status" => false, "message" => 'From Group id is required.'), 200); exit;
        }
        if($to_group_id == "" ) {
            $this->response(array("status" => false, "message" => 'To Group id is required.'), 200); exit;
        }
        if($student_id == "" ) {
            $this->response(array("status" => false, "message" => 'Student id is required.'), 200); exit;
        }
        if($status == "" ) {
            $this->response(array("status" => false, "message" => 'Status is required.'), 200); exit;
        }
        if($update_by == "" ) {
            $this->response(array("status" => false, "message" => 'Update by is required.'), 200); exit;
        }
        if($status == TEMPORARY && $temp_date == "") {
            $this->response(array("status" => false, "message" => 'Temporary Date is required.'), 200); exit;
        }

        $student_ids = explode(",",$student_id);
        foreach ($student_ids as $student) 
        {
            $student_id=$student;
            $cond = array("passenger_id" => $student_id ,"group_id" => $to_group_id);
            $resultFromStud = $this->common_model->select("group_mem_id", TB_GROUP_MEMBER, $cond);

            //------ To check if student already in To-group ------
            if(count($resultFromStud) > 0) {
                $this->response(array("status" => false, "message" => 'Student id '.$student_id.' already exists in this group.'), 200); exit;
            } 
            else 
            {
                $temp_date_new = ($status == TEMPORARY) ? $temp_date : "";
                
                $insertArr = array(
                    "from_group_id" => $from_group_id,
                    "to_group_id" => $to_group_id,
                    "pass_id" => $student_id,
                    "shift_status" => $status,
                    "temp_date" => $temp_date_new,
                    "user_id" => $update_by,
                    "created_at" => date("Y-m-d H:i:s")
                    );
                $result = $this->common_model->insert(TB_GROUP_SHIFT,$insertArr);
                if($result) {
                    // update group id  
                    $cond1 = array("group_id" => $from_group_id, "passenger_id" => $student_id);
                    $upArr1 = array("group_id" => $to_group_id); 
                    $upfromGrp = $this->common_model->update(TB_GROUP_MEMBER,$cond1,$upArr1);
                    
                    if(empty($upfromGrp)){
                       $this->response(array("status" => false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
                    }
                    // Get first group no of peoples
                    $resultNoOfPeoples = $this->common_model->select("no_of_people", TB_GROUP_MASTER, array("group_id" => $from_group_id));

                    //---- update group first no of people --
                    $uptoGrp = $this->common_model->update(TB_GROUP_MASTER,array("group_id" => $from_group_id),array("no_of_people"=>($resultNoOfPeoples[0]['no_of_people'] - 1)));
                    if(empty($uptoGrp)){
                       $this->response(array("status" => false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
                    }
                    // Get second group no of peoples
                    $resultSecondGroupNOP = $this->common_model->select("no_of_people", TB_GROUP_MASTER, array("group_id" => $to_group_id));

                    //---- update second group no of people --
                    $uptoScdGrp = $this->common_model->update(TB_GROUP_MASTER,array("group_id" => $to_group_id),array("no_of_people"=>($resultSecondGroupNOP[0]['no_of_people'] + 1)));
                    if(empty($uptoScdGrp)){
                       $this->response(array("status" => false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
                    }
                } else {
                    $this->response(array("status" => false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
                }
            }       
        }
        $this->response(array("status" => true, "message" => 'Student shifted successfully.'), 200); exit;

        /*if($upfromGrp && $uptoGrp && $uptoScdGrp) {
            $this->response(array("status" => true, "message" => 'Student shifted successfully.'), 200); exit;
        } else {
            $this->response(array("status" => false, "message" => 'Oops!!! Something went wrong.'), 200); exit;
        }*/
    }

    /**
     * Title : Cron Group shifting
     * Desc : Automatically shift students to old group after temporary date
     */

    public function cron_shift_group_get()
    {
        $postData = $this->post();
        $today = date("Y-m-d");
        $getStudent = $this->common_model->select("*",TB_GROUP_SHIFT,array("temp_date"=>$today,"shift_status"=>"1"));
        if(!empty($getStudent))
        {
            foreach ($getStudent as $key => $value)
            {
                $cond1 = array("group_id"=>$value['to_group_id']);
                $fromStud = $this->common_model->select("*",TB_GROUP,$cond1);
                $from_students=$fromStud[0]['user_id'];
                $from_stud_arr = explode(',',$from_students);

                $cond2 = array("group_id"=>$value['from_group_id']);
                $toStud = $this->common_model->select("*",TB_GROUP,$cond2);
                $to_students=$toStud[0]['user_id'];
                $to_stud_arr = explode(',',$to_students);

            //---- Remove student from to group & update no of people --
                if (($key = array_search($value['student_id'], $from_stud_arr)) !== false) {
                    unset($from_stud_arr[$key]);
                }
                $new_from_people = count($from_stud_arr);
                $new_from_stud = implode(',',$from_stud_arr);
                $upArr1 = array("user_id"=>$new_from_stud,"no_of_people"=>$new_from_people);
                $upfromGrp = $this->common_model->update(TB_GROUP,$cond1,$upArr1);
                    // echo $this->db->last_query();die;

                    //---- Add student to 2nd group & update no of people --
                array_push($to_stud_arr,$value['student_id']);
                $new_to_people = count($to_stud_arr);
                $new_to_stud = implode(',',$to_stud_arr);
                $upArr2 = array("user_id"=>$new_to_stud,"no_of_people"=>$new_to_people);
                $uptoGrp = $this->common_model->update(TB_GROUP,$cond2,$upArr2);

                if($upfromGrp && $uptoGrp)
                {
                    $this->common_model->delete(TB_GROUP_SHIFT,array("shift_id"=>$value['shift_id']));

                }
            }
        }
    }


    /**
     * Title : Accept / Reject group request
     * @param Integer user_id
     * @param Integer group_id
     * @param String status
     * @return Array
     */

    public function change_request_status_post()
    {
        $postData = $this->post();
        if(empty(trim($postData['user_id'])))
        {
            $this->response(array("status" => "error", "message" => 'User id is required.'), 200); exit;
        }
        if(empty(trim($postData['group_id'])))
        {
            $this->response(array("status" => "error", "message" => 'Group id is required.'), 200); exit;
        }
        if(empty(trim($postData['status'])))
        {
            $this->response(array("status" => "error", "message" => 'Status is required.'), 200); exit;
        }

        $status = trim($postData['status']);

        if($status == "accept")
        {
            $cond1 = array("group_id"=>trim($postData['group_id']),"receiver_id"=>trim($postData['user_id']));
            $upArr = array("status"=>'accept');
            $uptoGrp = $this->common_model->update(TB_TEMPORARY_GROUP,$cond1,$upArr);
            if($uptoGrp)
            {
                $insertArr = array(
                    "group_id"=>$postData['group_id'],
                    "passenger_id"=>$postData['user_id'],
                    "status"=>"1",
                    "created_at"=>date("Y-m-d H:i:s")
                    );
                $ins_std =$this->common_model->insert(TB_GROUP_MEMBER,$insertArr); 
                if($ins_std){
                  $this->response(array("status" => "success", "message" => "You are added in group successfully."), 200); exit;
              }
              else{
                  $this->response(array("status" => "error", "message" => "You are not added in group."), 200); exit;
              }
          }
      }
      else if($status == "reject")
      {
        $cond2= array("group_id"=>trim($postData['group_id']),"receiver_id"=>trim($postData['user_id']));
        $upArr = array("status"=>'reject');
        $this->common_model->update(TB_TEMPORARY_GROUP,$cond2,$upArr);
        $this->response(array("status" => "success", "message" => "You have rejected group."), 200); exit;
    }else
    {
       $this->response(array("status" => "error", "message" => "Oops!!! Something went wrong."), 200); exit;
   }
}

    /**
     * Title : Standard listing
     * @return Array
     */

    public function standard_listing_get()
    {
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            $all_standard = unserialize(STANDARD);
            if(!empty($all_standard))
            {
                $this->response(array("status" => "success","data" => $all_standard)); exit;
            }else
            {
                $this->response(array("status" => "error","message" => 'Standards are not defined.')); exit;
            }
        //} 
        }

    /**
     * Title : Save tutor standard
     * @param Integer tutor_id
     * @param Integer standard_id
     * @return Array
     */
    public function save_tutor_standard_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(empty(trim($postData['tutor_id'])))
            {
                $this->response(array("status" => "error", "message" => 'Tutor id is required.'), 200); exit;
            }
            if(empty(trim($postData['standard_id'])))
            {
                $this->response(array("status" => "error", "message" => 'Standard id is required.'), 200); exit;
            }

            $all_std = explode(",",trim($postData['standard_id']));
            foreach ($all_std as $keys => $values) 
            {
                $chkQuery = $this->common_model->select("*",TB_TUTOR_STANDARD,
                    array("tutor_id"=>trim($postData['tutor_id']),"std_id"=>$values));
                if(count($chkQuery) > 0)
                {

                }else
                {
                    $insertArr = array(
                        "tutor_id"=>trim($postData['tutor_id']),
                        "std_id"=>$values,
                        "is_deleted"=>"0",
                        "created_at"=>date("Y-m-d H:i:s")
                        );
                    $ins_std =$this->common_model->insert(TB_TUTOR_STANDARD,$insertArr);               
                }
            }
            $this->response(array("status" => "success", "message" => 'Standard added successfully.'), 200); exit;
        //}
        }

    /**
     * Title : Delete tutor standard
     * @param Integer tutor_id
     * @param Integer ts_id
     * @return Array
     */

    public function delete_tutor_standard_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(empty($postData['tutor_id']))
            {
                $this->response(array("status" => "error","message" => 'Tutor id is required.')); exit;
            }
            if(empty($postData['ts_id']))
            {
                $this->response(array("status" => "error","message" => 'Tutor standard id is required.')); exit;
            }

            $updtArr = array("is_deleted"=>"1");
            $cond = array("tutor_id"=>trim($postData['tutor_id']),"ts_id"=>trim($postData['ts_id']));
            $updtQuery = $this->common_model->update(TB_TUTOR_STANDARD,$cond,$updtArr);
            if($updtQuery)
            {
                $this->response(array("status" => "success", "message" => 'Standard deleted successfully.'), 200); exit;
            }else
            {
                $this->response(array("status" => "error","message" => 'Oops!!! Something went wrong.')); exit;
            }
        //}
        }

    // -------- Chatting between parent and care user ------
    // ****** Check care driver is providing service to same parent to whom he is chatting ******
    /**
     * Title : Send messages
     * @param Integer sender_id
     * @param Integer sender_app_id
     * @param Integer receiver_id
     * @param Integer receiver_app_id
     * @param String message
     * @return Array
     */
    public function sendMessage_post()
    {
        $postData = $this->post();    
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(empty($postData['sender_id']))
            {
                $this->response(array("status" => false,"message" => 'Sender id is required.'), 200); exit;
            }
            if(empty($postData['receiver_id']))
            {
                $this->response(array("status" => false,"message" => 'Receiver id is required.'), 200); exit;
            }
            if ($postData['sender_app_id'] == "") { 
                $this->response(array("status" => false, "message" => 'Sender App id is required.')); exit; 
            }
            if($postData['receiver_app_id'] == '')
            {
                $this->response(array("status" => false,"message" => 'Receiver app id is required.'), 200); exit;
            }
            if(trim(empty($postData['message'])))
            {
                $this->response(array("status" => false,"message" => 'Message is required.'), 200); exit;
            }
            if(($postData['sender_app_id'] == 0 && $postData['receiver_app_id'] == 0) || ($postData['sender_app_id'] == 1 && $postData['receiver_app_id'] == 1))
            {
                $this->response(array("status" => false,"message" => 'Sender id or receiver id is incorrect.'), 200); exit;
            }
            /*$headers = apache_request_headers();
            $auth_token = $headers['Authorizationtoken'];
            $checkToken = $this->common_model->select("user_id",TB_USERS,array("user_device_token"=>$auth_token));
            $user_id = $checkToken[0]['user_id'];

            if($user_id == $postData['sender_id'] || $user_id == $postData['receiver_id'])
            {*/

                $insertArr = array(
                    "sender_id"=>$postData['sender_id'],
                    "receiver_id"=>$postData['receiver_id'],
                    "sender_app_id"=>$postData['sender_app_id'],
                    "receiver_app_id"=>$postData['receiver_app_id'],
                    "message"=>trim($postData['message']),
                    "message_seen"=>"0",
                    "created_date"=>date("Y-m-d H:i:s")
                    );
                $result = $this->common_model->insert(TB_CHAT,$insertArr);
                if($result)
                {
                    $all_ids = $postData['sender_id'].",".$postData['receiver_id'];

                    $this->db->select("*");
                    $this->db->from(TB_CHAT);
                    $this->db->where("sender_id IN($all_ids) AND receiver_id IN($all_ids)");
                    $this->db->order_by('created_date','ASC');
                    $query = $this->db->get();
                    $result = $query->result_array();

                    if(!empty($result))
                    {
                        //echo "<pre>";print_r($result);die;
                      foreach($result as $key => $value)
                      {
                          $sender_data = $this->getUserName($result[$key]['sender_id'],$result[$key]['sender_app_id']);
                          $receiver_data = $this->getUserName($result[$key]['receiver_id'],$result[$key]['receiver_app_id']);
                          $result[$key]['sender_name'] = $sender_data[0]['fullname'];
                          $result[$key]['receiver_name'] = $receiver_data[0]['fullname'];
                          $result[$key]['sender_profile_pic'] = $sender_data[0]['prof_pic'];
                          $result[$key]['receiver_profile_pic'] = $receiver_data[0]['prof_pic'];
                          
                          $clear_by = explode(",",$result[$key]['clear_by']);
                          if(in_array($postData['sender_id'],$clear_by))
                          {
                            $result[$key]['message'] = "Message is deleted";
                          }
                      }
                    $this->response(array("status" => true,"message" => "Message send successfully", "message_list" => $result), 200); exit;
                }else
                {
                    $this->response(array("status" => false, "message" => 'Messages not found.'), 200); exit;
                }
                   // $this->response(array("status" => true, "message" => 'Message send successfully'), 200); exit;
                    // $getDevice = $this->common_model->select("user_device_id",TB_USERS,array("user_id"=>$postData['receiver_id']));
                    // $device_id = $getDevice[0]['user_device_id'];
                // $message  = "You have new message"; 
                // $myData   = array("message"=>trim($postData['message']));
                // $this->sendPushNotification($device_id,$message,$myData);
            }
            /*}else
            {
                $this->response(array("status" => "error", "message" => 'Bad user to access request.'), 200); exit;
            }*/
        //}
        }

    /**
     * Title : Get messages
     * @param Integer sender_id
     * @param Integer receiver_id
     * @param Integer sender_app_id
     * @param Integer receiver_app_id
     * @return Array
     */
    public function getMessages_post()
    {
        $postData = $this->post();
       
            if(empty($postData['sender_id']))
            {
                $this->response(array("status" => false,"message" => 'Sender id is required.'), 200); exit;
            }
            if(empty($postData['receiver_id']))
            {
                $this->response(array("status" => false,"message" => 'Receiver id is required.'), 200); exit;
            }
            if($postData['sender_app_id'] == "")
            {
                $this->response(array("status" => false,"message" => 'Sender app id is required.'), 200); exit;
            }
            if($postData['receiver_app_id'] == "")
            {
                $this->response(array("status" => false,"message" => 'Receiver app id is required.'), 200); exit;
            }
            if(($postData['sender_app_id'] == 0 && $postData['receiver_app_id'] == 0) || ($postData['sender_app_id'] == 1 && $postData['receiver_app_id'] == 1))
            {
                $this->response(array("status" => false,"message" => 'Sender id or receiver id is incorrect.'), 200); exit;
            }
           
            $all_ids = $postData['sender_id'].",".$postData['receiver_id'];

            $this->db->select("*");
            $this->db->from(TB_CHAT);
            $this->db->where("sender_id IN(".$all_ids.") AND receiver_id IN(".$all_ids.")");
            $this->db->order_by('created_date','ASC');
            $query = $this->db->get();
            $result = $query->result_array();
            
            if(!empty($result))
            {
                
            foreach($result as $key => $value) {
                $sender_data = $this->getUserName($value['sender_id'],$value['sender_app_id']);
                $receiver_data = $this->getUserName($value['receiver_id'],$value['receiver_app_id']);
                  
                  $result[$key]['sender_name'] = $sender_data[0]['fullname'];
                  $result[$key]['receiver_name'] = $receiver_data[0]['fullname'];
                  $result[$key]['sender_profile_pic'] = $sender_data[0]['prof_pic'];
                  $result[$key]['receiver_profile_pic'] = $receiver_data[0]['prof_pic'];
                  $result[$key]['message_seen'] = 1;
                  $clear_by = explode(",",$result[$key]['clear_by']);
                  if(in_array($postData['sender_id'],$clear_by))
                  {
                    $result[$key]['message'] = "Message is deleted";
                }
            }
            $this->response(array("status" => true, "message_list" => $result), 200); exit;
        }else
        {
            $this->response(array("status" => false, "message" => 'Messages not found.'), 200); exit;
        }
        
    }

/*public function getUserName($user_id)
{
    $getSender = $this->common_model->select("user_name",TB_USERS,array("user_id"=>$user_id));
    return $getSender[0]['user_name'];
}*/

public function getUserName($user_id,$app_id='')
{
    if($app_id==0){
        $getSender = $this->common_model->select("service_user_id as user_id,user_fullname as fullname,user_prof_pic as prof_pics",TB_SERVICE_USERS,array("service_user_id"=>$user_id));
        //$getSender = $this->common_model->select("user_fullname",TB_SERVICE_USERS,array("service_user_id"=>$user_id));
        if($getSender[0]['prof_pics']){
          $getSender[0]['prof_pic'] = base_url().'uploads/serviceUser/'.$getSender[0]['user_id'].'/'.$getSender[0]['prof_pics'];
        }
        else{
          $getSender[0]['prof_pic'] = '';
        }
        return $getSender;
    }
    else if($app_id==1){
   
        $getReceiver = $this->common_model->select("service_provider_id as user_id,sp_user_type,sp_fullname as fullname,sp_prof_pic as prof_pics",TB_SERVICE_PROVIDER,array("service_provider_id"=>$user_id));
        //$getReceiver = $this->common_model->select("sp_fullname",TB_SERVICE_PROVIDER,array("service_provider_id"=>$user_id));
        if($getReceiver[0]['sp_user_type'] == '4' && $getReceiver[0]['prof_pics']){
          $getReceiver[0]['prof_pic'] = base_url().'uploads/tutor/'.$getReceiver[0]['user_id'].'/'.$getReceiver[0]['prof_pics'];
        }
        else if($getReceiver[0]['sp_user_type'] == '3' && $getReceiver[0]['prof_pics']){
          $getReceiver[0]['prof_pic'] = base_url().'uploads/driver/'.$getReceiver[0]['user_id'].'/'.$getReceiver[0]['prof_pics'];
        }
        else{
           $getReceiver[0]['prof_pic'] = '';
        }
        return $getReceiver;
    }
}

    /** Author : Priya
     * Title : Update Chat messages
     * @param Integer chat_id
     * @param String message
     * @return message
     */
    public function updateChatMessage_post()
    {
        $postData = $this->post();

        if($postData['chat_id'] == '') {

            $this->response(array("status" => false,"message" => 'Please enter chat id.'), 200); exit;
        } else if($postData['message'] == '') {

            $this->response(array("status" => false,"message" => 'Please enter message.'), 200); exit;
        } else {

            $updtArr = array("message"=>trim($postData['message']));
            $cond = array("chat_id"=>$postData['chat_id']);
            $result = $this->common_model->update(TB_CHAT,$cond,$updtArr);

            if($result)  {
                $this->response(array("status" => true,"message" => 'Message is updated successfully'), 200); exit;
            }else {
                $this->response(array("status" => false,"message" => 'Oops!!! Something went wrong.'), 200); exit;
            }
        }
    }

    /** Author : Priya
     * Title : Delete Chat messages
     * @param Integer chat_id
     * @param Integer sender_id
     * @return message
     */
    public function deleteChatMessage_post()
    {
        $postData = $this->post();

        if($postData['chat_id'] == '') {

            $this->response(array("status" => false,"message" => 'Please enter chat id.'), 200); exit;
        } else if($postData['sender_id'] == '') {

            $this->response(array("status" => false,"message" => 'Please enter sender id.'), 200); exit;
        } else {

            $getData = $this->common_model->select("*", TB_CHAT, array("chat_id" => $postData['chat_id']));

            if(count($getData) != 0) {

                if($getData[0]['clear_by'] ==  '') {

                    $Arr = array("clear_by"=>$postData['sender_id']);
                    $cond = array("chat_id"=>$postData['chat_id']);
                    $result = $this->common_model->update(TB_CHAT,$cond,$Arr);

                    if($result)  {
                        $this->response(array("status" => true,"message" => 'Message deleted successfully'), 200); exit;
                    }else {
                        $this->response(array("status" => false,"message" => 'Oops!!! Something went wrong.'), 200); exit;
                    }

                } else {

                    $clearId = $getData[0]['clear_by'];
                    $newClearIds = $clearId.','.$postData['sender_id'];

                    $Arr = array("clear_by"=>$newClearIds);
                    $cond = array("chat_id"=>$postData['chat_id']);
                    $result = $this->common_model->update(TB_CHAT,$cond,$Arr);

                    if($result)  {
                        $this->response(array("status" => true,"message" => 'Message deleted successfully'), 200); exit;
                    }else {
                        $this->response(array("status" => false,"message" => 'Oops!!! Something went wrong.'), 200); exit;
                    }

                }
            } else {

                $this->response(array("status" => false,"message" => 'No message available.'), 200); exit;
            }
        }
    } 
    /**
    * Ashwini code
     * Title : create_group
      Description : create new groups 
     * @param Integer user_id
     * @param String group_name
     * @param String group_desc
     * @param String group_flag
     * @param String group_limit_date
     * @param String group_members
     * @return Array
     */

      public function create_group_post()
      {
        $postData = $this->post();
        $group_limit_date='';             
        if (empty(trim($postData['user_id'])))
        { 
            $this->response(array("status"=>false, "message" => 'User id is required.'), 200); exit; 
        }
        if (empty(trim($postData['group_name'])))
        { 
            $this->response(array("status"=>false, "message" => 'Group name is required.'), 200); exit; 
        }
        if (empty(trim($postData['group_desc'])))
        { 
            $this->response(array("status"=>false, "message" => 'Group description is required.'), 200); exit; 
        }
        if (empty(trim($postData['group_members'])))
        { 
            $this->response(array("status"=>false, "message" => 'Group members is required.'), 200); exit; 
        }
        if (empty(trim($postData['group_flag'])))
        { 
            $this->response(array("status"=>false, "message" => 'Group flag is required.'), 200); exit; 
        }else if(trim($postData['group_flag']) == "parent")
        {
            if(empty(trim($postData['group_limit_date'])))
            {
                $this->response(array("status"=>false, "message" => 'Limit date is required.'), 200);
                exit;
            }else
            {
                $group_limit_date = trim($postData['group_limit_date']);
            }
        }

        $chkGroup = $this->common_model->select("*",TB_GROUP_MASTER,array("service_user_id"=>trim($postData['user_id']),"group_name"=>trim($postData['group_name'])));
        if(count($chkGroup) > 0)
        {
            $this->response(array("status"=>false, "message" => 'Group already exists with same name.'), 200); exit; 
        }else
        {
            $all_members = explode(",",trim($postData['group_members']));
            if(trim($postData['group_flag']) == "parent")
            {
                $no_of_people = 0;
            }else
            {
                $no_of_people = count($all_members);
            }
            
            $grpArr = array(
                "service_user_id"=>trim($postData['user_id']),
                "group_name"=>trim($postData['group_name']),
                "description"=>trim($postData['group_desc']),
                "no_of_people"=>$no_of_people,
                "group_limit_date"=>$group_limit_date,
                "group_status"=>"1",
                "created_date"=>date("Y-m-d H:i:s")
                );
            $result = $this->common_model->insert(TB_GROUP_MASTER,$grpArr);
            if($result)
            {
                if(trim($postData['group_flag']) == "parent")
                {
                    foreach ($all_members as $keym => $valuem)
                    {
                       $grpMembArr[] = array(
                        "group_id"=>$result,
                        "sender_id"=>trim($postData['user_id']),
                        "receiver_id"=>$valuem,
                        "status"=>"request sent",
                        "created_at"=>date("Y-m-d H:i:s")
                        );
                   }
                   $result1 = $this->db->insert_batch(TB_TEMPORARY_GROUP,$grpMembArr);
               }else
               {
                foreach ($all_members as $keym => $valuem)
                {
                   $grpMembArr[] = array(
                    "group_id"=>$result,
                    "passenger_id"=>$valuem,
                    "status"=>"1",
                    "created_at"=>date("Y-m-d H:i:s")
                    );
               }
               $result1 = $this->db->insert_batch(TB_GROUP_MEMBER,$grpMembArr);
           }
       }
       if($result1)
       {
        $this->response(array("status"=>true, "message" => 'Group is created successfully.'), 200);
        exit; 
    }else
    {
        $this->response(array("status"=>false, "message" => 'Oops!!! Something went wrong.'), 200);
        exit; 
    }
}
}


    /**
     * Title : Delete messages
     * @param Integer user_id
     * @param Integer chat_id
     * @return Array
     */
    public function deleteMessages_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(empty($postData['user_id']))
            {
                $this->response(array("status" => "error","message" => 'User id is required.'), 200); exit;
            }
            if(empty($postData['chat_id']))
            {
                $this->response(array("status" => "error","message" => 'Chat id is required.'), 200); exit;
            }

            $headers = apache_request_headers();
            $auth_token = $headers['Authorizationtoken'];
            $checkToken = $this->common_model->select("user_id",TB_USERS,array("user_device_token"=>$auth_token));
            $user_id = $checkToken[0]['user_id'];

            if($user_id == $postData['user_id'])
            {
                $all_chat_ids = explode(",",trim($postData['chat_id']));
                foreach ($all_chat_ids as $key => $value)
                {
                    $getClear = $this->common_model->select("clear_by",TB_CHAT,array("chat_id"=>$value));
                    if(!empty($getClear[0]['clear_by']))
                    {
                        $new_clear = $getClear[0]['clear_by'].",".$postData['user_id'];
                    }else
                    {
                        $new_clear = $postData['user_id'];
                    }

                    $updtArr = array("clear_by"=>$new_clear);
                    $cond = array("chat_id"=>$value);
                    $updateq = $this->common_model->update(TB_CHAT,$cond,$updtArr);
                }
                if($updateq)
                {
                    $this->response(array("status" => "success", "message" => "Message deleted successfully."), 200); exit;
                }else
                {
                    $this->response(array("status" => "error", "message" => "Oops!!! Something went wrong."), 200); exit;
                }
            }else
            {
                $this->response(array("status" => "error", "message" => 'Bad user to access request.'), 200); exit;
            }

        //}
        }

    /**
     * Title : Update messages
     * @param Integer chat_id
     * @param String message
     * @return Array
     */
    public function updateMessage_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(empty($postData['chat_id']))
            {
                $this->response(array("status" => "error","message" => 'Chat id is required'), 200); exit;
            }
            if(empty(trim($postData['chat_id'])))
            {
                $this->response(array("status" => "error","message" => 'Message is required'), 200); exit;
            }

            $updtArr = array("message"=>trim($postData['message']));
            $cond = array("chat_id"=>$postData['chat_id']);
            $result = $this->common_model->update(TB_CHAT,$cond,$updtArr);
            if($result)
            {
                $this->response(array("status" => "success","message" => 'Message is updated successfully'), 200); exit;
            }else
            {
                $this->response(array("status" => "error","message" => 'Oops!!! Something went wrong.'), 200); exit;
            }
       // }
        }

    //---------- Cronjob function to delete messages in chat after 7 days -------
        public function cron_delete_messages_get()
        {
         $postData = $this->post();
         $today = date("Y-m-d");
         $getData = $this->common_model->select("DATEDIFF('$today',date(created_at)) AS days,chat_id",TB_CHAT);
         foreach ($getData as $key => $value)
         {
            if($value['days'] == "7")    
            {
                $this->common_model->delete(TB_CHAT,array("chat_id"=>$value['chat_id']));
            }
        }
    }

    /**
     * Title : Update user profile only driver and tutor (service providers)
     * @param Integer user_id
     * @param String full_name
     * @param String contact_no
     * @param String user_dob
     * @param String gender
     * @param String address
     * @param String address_latlong
     * @param String school
     * @param String degree
     * @param String specialization
     * @param String education_from
     * @param String education_to
     * @param String certification_name
     * @param String user_type
     * @param String own_car
     * @param String dr_licence_type
     * @param String license_no
     * @param String valid_from
     * @param String valid_until
     * @return Array
     */

    public function update_service_provider_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => "error","message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if(empty($postData['user_type']))
            {
                $this->response(array("status"=>"error","message"=>'User type is required.'),200);
                exit;
            }
            if(empty($postData['user_id']))
            {
                $this->response(array("status"=>"error","message"=>'User id is required.'),200);
                exit;
            }

            if($postData['user_type'] == ROLE_CARE_DRIVER || $postData['user_type'] == ROLE_TUTOR)
            {
                $checkUser = $this->common_model->select("*",TB_USERS,array("user_id"=>trim($postData['user_id'])));
                if(count($checkUser) > 0)
                {
                    if(empty($postData['full_name']))
                    {
                        $this->response(array("status"=>"error","message"=>'Full name is required.'),200);
                        exit;
                    }
                    if(empty($postData['contact_no']))
                    {
                        $this->response(array("status"=>"error","message"=>'Contact no is required.'),200);
                        exit;
                    }
                    if(empty(trim($postData['address'])))
                    {
                        $this->response(array("status" => false, "message" => 'Address is required.'), 200); exit;
                    }
                    if(empty(trim($postData['address_latlong'])))
                    {
                        $this->response(array("status" => false, "message" => 'Address Lat Long is required.'), 200); exit;
                    }
                    if(empty($postData['gender']))
                    {
                        $this->response(array("status"=>"error","message"=>'Gender is required.'),200); exit;
                    } 
                    if(empty(trim($postData['user_dob'])))
                    {
                        $this->response(array("status" => false, "message" => 'Date of birth field is required.'),200); exit;
                    }else
                    {
                        $dob = trim($postData['user_dob']);
                        $userAge = $this->getAge($dob);
                        if ($userAge < 18)
                        {
                            $this->response(array("status" => false, "message" => 'Age greater than 18 is required.'), 200); exit;
                        }
                    }
                        //------- CARE DRIVER PROFILE ----------
                    if($postData['user_type'] == ROLE_CARE_DRIVER)
                    {

                        if(empty($postData['own_car']))
                        {
                            $this->response(array('status' => "error", 'message' => 'Own car yes/no is required.'), 200);exit;
                        }else
                        {
                            $own_car = trim($postData['own_car']);
                        }

                        if(empty($postData['dr_licence_type']))
                        {
                            $this->response(array('status' => "error", 'message' => 'Driving license type is required.'), 200);exit;
                        }else
                        {
                            $dr_licence_type = trim($postData['dr_licence_type']);
                        }

                        if(empty($postData['license_no']))
                        {
                            $this->response(array('status' => "error", 'message' => 'License number is required.'), 200);exit;
                        }else
                        {
                            $license_no = trim($postData['license_no']);
                        }

                        if(empty($postData['valid_from']))
                        {
                            $this->response(array('status' => "error", 'message' => 'Valid from date is required.'), 200);exit;
                        }else
                        {
                            $valid_from = trim($postData['valid_from']);
                        }

                        if(empty($postData['valid_until']))
                        {
                            $this->response(array('status' => "error", 'message' => 'Valid until date is required.'), 200);exit;
                        }else
                        {
                            $valid_until = trim($postData['valid_until']);
                        }

                        if(isset($postData['is_care']) && !(empty($postData['is_care'])))
                        {
                            $is_care = '1';
                            $care_admin_approved = '1';
                            // --- school---
                            if(empty($postData['school']))
                            {
                                $this->response(array("status"=>"error","message"=>'School/College/Institute name is required.'),200);
                                exit;
                            }else
                            {
                                $school = trim($postData['school']);
                            }

                            // --- degree ---
                            if(empty($postData['degree']))
                            {
                                $this->response(array("status"=>"error","message"=>'Degree is required.'),200);
                                exit;
                            }else
                            {
                                $degree = $postData['degree'];
                            }

                            // --- specialization ---
                            if(empty($postData['specialization']))
                            {
                                $this->response(array("status"=>"error","message"=>'Specialization is required.'),200);
                                exit;
                            }else
                            {
                                $specialization = $postData['specialization'];
                            }

                            // --- education from ---
                            if(empty($postData['education_from']))
                            {
                                $this->response(array("status"=>"error","message"=>'Education From date is required.'),200);
                                exit;
                            }else
                            {
                                $education_from = $postData['education_from'];
                            }

                            // --- education to ---
                            if(empty($postData['education_to']))
                            {
                                $this->response(array("status"=>"error","message"=>'Education End date is required.'),200);
                                exit;
                            }else
                            {
                                $education_to = $postData['education_to'];
                            }

                            // --- certification ---
                            if(empty($postData['certification_name']))
                            {
                                $this->response(array("status"=>"error","message"=>'Certification name is required.'),200);
                                exit;
                            }else
                            {
                                $certification = $postData['certification_name'];
                            }

                            //---  certification_pics ----
                            if(isset($_FILES['certification_pics']) && !(empty($_FILES['certification_pics'])))
                            {
                                $files_name = array();
                                $cnt = count($_FILES['certification_pics']['name']);
                                for($i=0;$i<$cnt;$i++)
                                {
                                    $files_name[$i]['name']     = $_FILES['certification_pics']['name'][$i];
                                    $files_name[$i]['type']     = $_FILES['certification_pics']['type'][$i];
                                    $files_name[$i]['tmp_name'] = $_FILES['certification_pics']['tmp_name'][$i];
                                    $files_name[$i]['error']    = $_FILES['certification_pics']['error'][$i];
                                    $files_name[$i]['size']     = $_FILES['certification_pics']['size'][$i];
                                }

                                foreach ($files_name as $key => $value)
                                {
                                    $upload_certification = multiple_image_upload($value,'uploads/driver/certification/'); 
                                    if(is_array($upload_certification) && $upload_certification['status'] == "false")
                                    {
                                        echo $this->response($upload_certification);
                                        exit;
                                    }else
                                    {
                                        $certification_doc .= "|".$upload_certification;
                                    }
                                }
                            }else
                            {
                                $certification_doc = '';
                            }
                        }else
                        {
                            $is_care = '0';
                            $care_admin_approved = '0';
                            $school = '';
                            $degree = '';
                            $specialization = '';
                            $education_from = '';
                            $education_to = '';
                            $certification = '';
                        }


                        $updtArr = array(
                            "user_name"=>trim($postData['full_name']),
                            "user_phone_number"=>trim($postData['contact_no']),
                            "user_birth_date"=>trim($postData['user_dob']),
                            "user_gender"=>trim($postData['gender']),
                            "user_address"=>trim($postData['address']),
                            "user_add_lat_lng"=>trim($postData['address_latlong'])
                            );
                        $result = $this->common_model->update(TB_USERS,array("user_id"=>$postData['user_id']),$updtArr);

                        $drvArr = array(
                            "user_id"=>trim($postData['user_id']),
                            "is_care"=>$is_care,
                            "care_admin_approved"=>$care_admin_approved,
                            "is_your_own_car"=>$own_car,
                            "driving_license_type"=>$dr_licence_type,
                            "license_no"=>$license_no,
                            "valid_from_date"=>$valid_from,
                            "valid_until_date"=>$valid_until,
                            "school"=>$school,
                            "degree"=>$degree,
                            "specialization"=>$specialization,
                            "education_from"=>$education_from,
                            "education_to"=>$education_to,
                            "certification_name"=>$certification,
                            "upload_certificate"=>$certification_doc,
                            "created_at"=>date("Y-m-d H:i:s")
                            );
                        $checkDriver = $this->common_model->select("*",TB_DRIVER,array("user_id"=>trim($postData['user_id'])));
                        if(count($checkDriver) > 0)
                        {
                            $result_final = $this->common_model->update(TB_DRIVER,array("user_id"=>$postData['user_id']),$drvArr);
                        }else
                        {
                            $result_final = $this->common_model->insert(TB_DRIVER,$drvArr);
                        }


                    }else if($postData['user_type'] == ROLE_TUTOR)
                    {
                            //------- TUTOR PROFILE ----------
                        if(empty($postData['school']))
                        {
                            $this->response(array("status"=>"error","message"=>'School/College/Institute name is required.'),200);
                            exit;
                        }
                        if(empty($postData['degree']))
                        {
                            $this->response(array("status"=>"error","message"=>'Degree is required.'),200);
                            exit;
                        }
                        if(empty($postData['address_type']))
                        {
                            $this->response(array("status"=>"error","message"=>'Address type is required.'),200);
                            exit;
                        }else
                        {
                            $all_address = explode(",",$postData['address_type']);
                            if(in_array('2',$all_address) && empty($postData['tutor_address']))
                            {
                                $this->response(array("status"=>"error","message"=>'Tutor address is required.'),200);
                                exit;
                            }else if($postData['address_type'] == '1')
                            {
                                $tutor_address = '';
                            }else
                            {
                                $tutor_address = trim($postData['tutor_address']);
                            }
                        }
                        if(empty($postData['subject_data']))
                        {
                            $this->response(array("status"=>"error","message"=>'Subject data is required.'),200);
                            exit;
                        }
                        //---  certification_pics ----
                        if(isset($_FILES['certification_pics']) && !(empty($_FILES['certification_pics'])))
                        {
                            $files_name = array();
                            $cnt = count($_FILES['certification_pics']['name']);
                            for($i=0;$i<$cnt;$i++)
                            {
                                $files_name[$i]['name']     = $_FILES['certification_pics']['name'][$i];
                                $files_name[$i]['type']     = $_FILES['certification_pics']['type'][$i];
                                $files_name[$i]['tmp_name'] = $_FILES['certification_pics']['tmp_name'][$i];
                                $files_name[$i]['error']    = $_FILES['certification_pics']['error'][$i];
                                $files_name[$i]['size']     = $_FILES['certification_pics']['size'][$i];
                            }

                            foreach ($files_name as $key => $value)
                            {
                                $upload_certification = multiple_image_upload($value,'uploads/tutor/certification/'); 
                                if(is_array($upload_certification) && $upload_certification['status'] == "false")
                                {
                                    echo $this->response($upload_certification);
                                    exit;
                                }else
                                {
                                    $certification_doc .= "|".$upload_certification;
                                }
                            }
                        }else
                        {
                            $certification_doc = '';
                        }

                        //--- USER BASIC INFO ---
                        $updtArr = array(
                            "user_name"=>trim($postData['full_name']),
                            "user_phone_number"=>trim($postData['contact_no']),
                            "user_birth_date"=>trim($postData['user_dob']),
                            "user_gender"=>trim($postData['gender']),
                            "user_address"=>trim($postData['address']),
                            "user_add_lat_lng"=>trim($postData['address_latlong'])
                            );
                        $result = $this->common_model->update(TB_USERS,array("user_id"=>$postData['user_id']),$updtArr);

                        //--- TUTOR INFO ---
                        $tutArr = array(
                            "user_id"=>trim($postData['user_id']),
                            "tutor_organization"=>trim($postData['school']),
                            "degree"=>trim($postData['degree']),
                            "address_type"=>trim($postData['address_type']),
                            "tutor_address"=>$tutor_address,
                            "certificate_doc"=>$certification_doc,
                            "created_at"=>date("Y-m-d H:i:s")
                            );
                        $checkTutor = $this->common_model->select("*",TB_TUTOR,array("user_id"=>trim($postData['user_id'])));
                        if(count($checkTutor) > 0)
                        {
                            $result = $this->common_model->update(TB_TUTOR,array("user_id"=>$postData['user_id']),$tutArr);
                        }else
                        {
                            $result = $this->common_model->insert(TB_TUTOR,$tutArr);
                        }

                        //---- TUTOR SUBJECT INFO -----
                        $all_subject = json_decode($postData['subject_data'],true);
                        foreach ($all_subject['subject_data'] as $key1 => $value1)
                        {
                            foreach ($value1['cat_details'] as $key2 => $value2)
                            {
                                $subjArr[]=array(
                                    "user_id"=>trim($postData['user_id']),
                                    "cat_id"=>$value1['cat_id'],
                                    "sub_id"=>$value2['sub_id'],
                                    "rate_per_hour"=>$value2['rate'],
                                    "created_at"=>date("Y-m-d H:i:s")
                                    );
                            }
                        }

                        $checkSub = $this->common_model->select("*",TB_TUTOR_SUBJECTS,array("user_id"=>$postData['user_id']));
                        if(count($checkSub) > 0)
                        {
                            $deleteSub = $this->common_model->delete(TB_TUTOR_SUBJECTS,array("user_id"=>$postData['user_id']));
                        }
                        $result_final = $this->db->insert_batch(TB_TUTOR_SUBJECTS,$subjArr);
                    }

                    if($result_final)
                    {
                        $this->response(array("status" => true, "message" => "User updated successfully."),200);exit;
                    }else
                    {
                        $this->response(array("status" => false, "message" => "Oops!!! Something went wrong."),200);exit;
                    }

                }else
                {
                    $this->response(array("status" => false, "message" => "User does not exists."),200);exit;
                }
            }else
            {
                $this->response(array("status" => false, "message" => "Invalid user."),200);exit;
            }


       //}
        }

        
    //---------- Ashwini Code end here ----------




    /** Ram K
     * Title : Service provider unavailability add/update functionality API
     * @param Integer sp_id(User_id)
     * @param Varchar date
     * @param Integer unavail_id
     * @return Array
     */  

    public function add_unavilability_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            // ---- Ashwini changes here ----
            if(empty($postData['sp_id']))
            {
                $this->response(array("status" => false,"message" => 'Service Provider id is required.'), 200); exit;
            }
            if(empty($postData['date']))
            {
                $this->response(array("status" => false,"message" => 'Date is required.'), 200); exit;
            }

            $allundate = explode(",",trim($postData['date']));
            $count = count($allundate);

            foreach ($allundate as $keydt => $valuedt)
            {
                $data_new[] = array(
                    "'".trim($postData['sp_id'])."'",
                    "'1'",
                    "'".$valuedt."'",
                    "'".date("Y-m-d H:i:s")."'");
            }

            $key_new = "sp_id,status,date,created_date";
            $result = $this->common_model->insert_ignore(TB_UNAVAILABILITY,$key_new,$data_new);
            if($result)
            {
                $this->response(array("status" => "success","message" => 'Tulii accepted your unavailability for "'.$count.'" days.'), 200); exit;
            }else
            {
                $this->response(array("status" => false,"message" => 'Your unavailability is already set.'), 200); exit;
            }
        //}
        }

    /** Ram K
     * Title : Service provider list of unavailable users
     * @param Integer sp_id(User_id)
     * @return Array
     */

    public function view_unavilability_post()
    {
        $postData = $this->post();
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            //---- Ashwini changes here ---
            if(empty($postData['sp_id']))
            {
                $this->response(array("status" => false,"message" => 'Service Provider id is required.'), 200); exit;   
            }
            $cond1 = array("sp_id"=>$postData['sp_id'],'date>='=>date("Y-m-d"));
            $result = $this->common_model->select("*",TB_UNAVAILABILITY,$cond1);            
            if ($result) {
               $this->response(array("status" => "success", "data" => $result), 200); exit; 
           }else{
            $this->response(array("status" => "error", "message" => 'No records found.'), 200); exit;
        }
     // }
    }

    /** Ram k
     * Title : Service provider delete unavailable users
     * @param Integer sp_id(User_id)
     * @return Array
     * Need to set cron job for delete unavailble users.
     */


    public function delete_unavilability_post()
    {
        $postData = $this->post();
        $resultDelete = $this->common_model->delete(TB_UNAVAILABILITY,array('sp_id' =>$postData['sp_id'],'date<'=>date("Y-m-d")));                       
        if ($resultDelete) {
           $this->response(array("status" => "success"), 200); exit; 
       }else{
        $this->response(array("status" => "error", "message" => 'Something went wrong!!!.'), 200); exit;
        
    }
}

public function sendSMS($to,$msg)
{
    $from = 'TFCTOR';
    $SendAt = date("Y-m-d");
    $response = sendOTP($msg, trim($to),$from);
}

    /** Author: Ram k
     * Description: Get listing of car category
     */ 

    public function car_category_get()
    {
    /*if(is_authorized())
    {
        $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
    }else
    {*/
        $cond1 = array("is_deleted"=>'0');
        $result = $this->common_model->select("*",TB_CAR_CATEGORY,$cond1);            
        if ($result) {
         $this->response(array("status" => "success", "data" => $result), 200); exit; 
     }else{
        $this->response(array("status" => "error", "message" => 'No records found.'), 200); exit;
    }
  //}
}


    /** Author: Ram k
     * Title : Tutor subject rate (add/update) functionality API
     * @param Integer tutor_id(User id)
     * @param Integer cat_id(Tutor category id)
     * @param Integer sub_id(Subject id)
     * @param Float rate
     * @param Integer tutor_sub_id(For update info)
     * @return Array
     */  

    public function subject_rate_post()
    {
        $postData = $this->post();
        $subject_rate = json_decode($postData['subject_rate'],true);
        /*if(is_authorized())
        {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }else
        {*/
            if($postData['tutor_sub_id'] =='')
            {
                foreach ($subject_rate['request_data'] as $key => $catrate) {
                 $catid = $catrate['cat_id'];
                 foreach ($catrate['cat_details'] as $key => $sub_rate) {
                    $insertArr=array(
                        "cat_id"=>$catid,
                        "sub_id"=>$sub_rate['sub_id'],
                        "tutor_id"=>$postData['tutor_id'],
                        "rate_per_hour"=>$sub_rate['rate'],
                        "created_at"=>date("Y-m-d H:i:s")
                        );
                    $result = $this->common_model->insert(TB_SUBJECT_MASTER,$insertArr);
                }
            }
            if ($result) {
             $this->response(array("status" => "success", "message" => 'Subject rate added successfully.'), 200); exit; 
         }
     }else{
        foreach ($subject_rate['request_data'] as $key => $catrate) {
            $catid = $catrate['cat_id'];
            foreach ($catrate['cat_details'] as $key => $sub_rate) {
             $updateArr=array(
                "cat_id"=>$catid,
                "sub_id"=>$sub_rate['sub_id'],
                "tutor_id"=>$postData['tutor_id'],
                "rate_per_hour"=>$sub_rate['rate'],
                "created_at"=>date("Y-m-d H:i:s")
                );

             $result = $this->common_model->update(TB_SUBJECT_MASTER,array("id"=>$postData['tutor_sub_id']),$updateArr);
         }   

     }
     if ($result) {
         $this->response(array("status" => "success", "message" => 'Subject rate updated successfully.'), 200); exit; 
     }
 }            
    //}
}

    /** Author: Ram k
     * Title : tutor_list
     * Description : tutor list
     *  
     * @param  Integer tutor_id
     * @return Array
     */

 /*   public function tutor_list_post()
    {   
        $postData = $this->post();
        if(empty($postData['tutor_id']))
        {
            $this->response(array("status" => false,"message" => 'Tutory id required.'), 200); exit;
        }

        
        // checked inputs are not emtpy
        if ( !empty($postData['tutor_id']) ) {
            $cond = array("tbl_tutor_subjects.status" => 1 ,"tbl_tutor_subjects.is_deleted" => "0","tbl_tutor_subjects.id" => $postData['tutor_id']);
            $join = array(TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".id = ".TB_TUTOR_SUBJECTS.".cat_id",TB_USERS=>TB_TUTOR_SUBJECTS.".tutor_id  = ".TB_USERS.".user_id");
            $tutorData = $this->Common_model->select_join("tbl_tutor_subjects.id,user_name,category_name,tutor_id", TB_TUTOR_SUBJECTS, $cond, array(), array(), $join,"tutor_id");            
        } else {
            $cond = array("tbl_tutor_subjects.status" => 1 ,"tbl_tutor_subjects.is_deleted" => "0");
            $join = array(TB_TUTOR_CATEGORY => TB_TUTOR_CATEGORY.".id = ".TB_TUTOR_SUBJECTS.".cat_id",TB_USERS=>TB_TUTOR_SUBJECTS.".tutor_id  = ".TB_USERS.".user_id");
            $tutorData = $this->Common_model->select_join("tbl_tutor_subjects.id,user_name,category_name,tutor_id", TB_TUTOR_SUBJECTS, $cond, array(), array(), $join,"tutor_id");            
        }
        
        $arrayList = array();
        $i = 0;
        // result
        if ( count($tutorData) > 0 ) {
            foreach ($tutorData as $key => $value) {
                $cond1    = array("tbl_tutor_subjects.tutor_id"=>$value['tutor_id']);       
                $join1    = array(TB_TUTOR_SUBCATEGORY => TB_TUTOR_SUBJECTS.".sub_id  = ".TB_TUTOR_SUBCATEGORY.".id");
                $subjPriceData = $this->Common_model->select_join("rate_per_hour,subcategory_name", TB_TUTOR_SUBJECTS, $cond1, array(), array(), $join1);
                $arrayList[$i]['id']            = $value['id'];
                $arrayList[$i]['tutor_name']    = $value['user_name'];
                $arrayList[$i]['category_name'] = $value['category_name'];                
                $arrayList[$i]['subject_price_list'] = $subjPriceData;
                $i++;
            }
            $this->response(array("status" => true, "result" => $arrayList), 200); exit;
        } else {
            $this->response(array("status" => false, "message" => 'Tutor data not found.'), 200); exit;
        }
    }*/

    /**
     * Author : Ram K
     * Title : isProfileActive
     * Objective : Set Cron
     * Description : Check if user completed his profile or not
     */
    public function isProfileActive_post()
    {
        $cond = array("police_verify_status"=>'0');
        $role = ROLE_CARE_DRIVER.','.ROLE_TUTOR.','.SCHOOL.','.ORGANIZATION;
        $wherein = explode(',', $role);
        
        $result = $this->common_model->select_where_in1("user_id,roleId,police_verify_status,user_status,created_at",TB_USERS,$cond,$wherein);
        foreach ($result as $key => $value) {
            $dt = $value['created_at'];
            $date = new DateTime($dt);
            $now = new DateTime();
            $diff = $now->diff($date);
                  //die;
            if($diff->days > 7) {
               $updateArr=array(
                "user_status"=>'1'
                );
               $result = $this->common_model->update(TB_USERS,array("user_id"=>$value['user_id']),$updateArr);
           }
       }        
   }

   /**     
     * Title        : getProfile
     * Description  : user details
     * Author       : jyoti k
     * @param  Integer user_id
     * @return Array
     */

    /*public function getProfile_post () 
    {      
           // Post data
        $postData = $_POST;

        if ( $this->user_exists($postData["user_id"]) == 0 ) {
            $this->response(array("status" => false,"message" => 'User id is not exists.')); exit;
        }
            // User details
        if ( $postData["user_id"] != '' ) {
            $cond                       = array("user_id" => $postData["user_id"]);
            $userData                   = $this->common_model->select("user_pic,roleId,user_id,user_name,user_gender,user_device_id,user_address,user_add_lat_lng,user_birth_date,user_email,user_phone_number",TB_USERS,$cond);
            $link                       = base_url().USER_PROFILE_IMG.$userData[0]['user_pic'];
            $data['user_id']            = $userData[0]['user_id']           ? $userData[0]['user_id']           : 'N/A';
            $data['role_id']            = $userData[0]['roleId']            ? $userData[0]['roleId']            : 'N/A';
            $data['user_name']          = $userData[0]['user_name']         ? $userData[0]['user_name']         : 'N/A';
            $data['user_email']         = $userData[0]['user_email']        ? $userData[0]['user_email']        : 'N/A';
            $data['user_phone_number']  = $userData[0]['user_phone_number'] ? $userData[0]['user_phone_number'] : 'N/A';
            $data['user_birth_date']    = $userData[0]['user_birth_date']   ? $userData[0]['user_birth_date']   : 'N/A';
            $data['user_address']       = $userData[0]['user_address']      ? $userData[0]['user_address']      : 'N/A';
            $data['user_device_id']     = $userData[0]['user_device_id']    ? $userData[0]['user_device_id']    : 'N/A';
            $data['user_pic']           = $link;
            
            //Seprate the lat and long parameter
            $address_lat_long=explode(',', $userData[0]['user_add_lat_lng']);

            $data['user_address_lat']     = $address_lat_long[0]    ? $address_lat_long[0]    : 'N/A';
            $data['user_address_long']     = $address_lat_long[1]    ? $address_lat_long[1]   : 'N/A';

            //Age calculation
             $dob = trim($userData[0]['user_birth_date']);
             $userAge = $this->getAge($dob);
             $data['user_age']           = $userAge          ? $userAge          : 'N/A';

            //Gender Details
            if ( $userData[0]['roleId'] == ROLE_PARENTS || $userData[0]['roleId'] == ROLE_CARE_DRIVER || $userData[0]['roleId'] == ROLE_TUTOR) {
               $data['user_gender']        = $userData[0]['user_gender']       ? $userData[0]['user_gender']       : 'N/A';
            }

            // Parent details
            if ( $userData[0]['roleId'] == ROLE_PARENTS ) {
                $data['user_no_of_kids'] = $userData[0]['user_no_of_kids'] ? $userData[0]['user_no_of_kids'] : '';
                $kidData                 = $this->common_model->select("kid_id,kid_name,standard,kid_gender,kid_birthdate, YEAR( CURDATE( ) ) - YEAR( kid_birthdate ) AS kid_age",TB_KIDS,array("user_id" => $postData["user_id"]));
                
                // Children detailsd

                if (count($kidData)>0) {

                    foreach ( $kidData as $key => $value ) {
                        $kidInfo[] = array(
                            'kid_id'        => $value['kid_id'],
                            'kid_name'      => $value['kid_name'],
                            'kid_standard'      => $value['standard'],
                            'kid_gender'    => $value['kid_gender'], 
                            'kid_birthdate' => $value['kid_birthdate'],                 
                            'kid_age'       => $value['kid_age']                               
                            ); 
                    }
                    $data['kidsDetails'] = $kidInfo;
                } else {
                    $data['kidsDetails'] = array();
                }

            } 
            elseif( $userData[0]['roleId'] == ROLE_CARE_DRIVER ) { 
                // Driver details
                $cond                       = array("user_id" => $postData["user_id"]);
                $userData                   = $this->common_model->select("*",TB_DRIVER,$cond);
               

                $data["is_your_own_car"]        = $userData[0]['is_your_own_car']        ? $userData[0]['is_your_own_car']        : 'N/A';
                $data["driving_license_type"]   = $userData[0]['driving_license_type']   ? $userData[0]['driving_license_type']   : 'N/A';
                $data["license_no"]             = $userData[0]['license_no']             ? $userData[0]['license_no']             : 'N/A';
                $data["valid_from_date"]        = $userData[0]['valid_from_date']        ? $userData[0]['valid_from_date']        : 'N/A';
                $data["valid_until_date"]       = $userData[0]['valid_until_date']       ? $userData[0]['valid_until_date']       : 'N/A';
                
                $data["school"]                 = $userData[0]['school']                 ? $userData[0]['school']                 : 'N/A';
                $data["degree"]                 = $userData[0]['degree']                 ? $userData[0]['degree']                 : 'N/A';
                $data["specialization"]         = $userData[0]['specialization']         ? $userData[0]['specialization']         : 'N/A';
                $data["education_from"]         = $userData[0]['education_from']         ? $userData[0]['education_from']         : 'N/A';
                $data["education_to"]           = $userData[0]['education_to']           ? $userData[0]['education_to']           : 'N/A';
                $data["certification_name"]     = $userData[0]['certification_name']     ? $userData[0]['certification_name']     : 'N/A';
                $data["police_verify_status"]     = $userData[0]['police_verify_status']     ? $userData[0]['police_verify_status']     : 'N/A';
                $data["verification_no"]     = $userData[0]['verification_no']     ? $userData[0]['verification_no']     : 'N/A';

                // Certificate images
                $certificateArr = explode("|", $userData[0]['upload_certificate']);
                if( !empty($certificateArr[0]) ) {
                    foreach ( $certificateArr as $cetkey => $certifValue ) {
                        $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                    }
                } else {
                    $certification = array();
                }

                //Police Varification Document
                $docArr = explode("|", $userData[0]['police_verify_doc']);
                if( !empty($docArr[0]) ) {
                    foreach ( $docArr as $cetkey => $certifValue ) {
                        $document[$cetkey]['verification_document'] = base_url().'uploads/driver/licenseCopy/'.$certifValue;
                    }
                } else {
                    $document = array();
                }
                                                         
                $data["verification_document"] = $document;
                $data["certificate_pics"] = $certification;
            } 
            else if($userData[0]['roleId'] == ROLE_TUTOR)
            { 
                //Tutor Information                
                $cond                       = array("user_id" => $postData["user_id"]);
                $userData                   = $this->common_model->select("*",TB_TUTOR,$cond);
                
                //Tutor Subject Information
                $cond                       = array("user_id" => $postData["user_id"]);
                $subjectData                   = $this->common_model->select("*",TB_TUTOR_SUBJECTS,$cond);

                $data["tutor_organization"]     = $userData[0]['tutor_organization']      ? $userData[0]['tutor_organization']     : 'N/A';
                $data["degree"]                 = $userData[0]['degree']                  ? $userData[0]['degree']                 : 'N/A';
                $data["address_type"]           = $userData[0]['address_type']            ? $userData[0]['address_type']           : 'N/A';
                $data["tutor_address"]          = $userData[0]['tutor_address']           ? $userData[0]['tutor_address']          : 'N/A';
                
                // Certificate images
                $certificateArr = explode("|", $userData[0]['certificate_doc']);
                if( !empty($certificateArr[0]) ) {
                    foreach ( $certificateArr as $cetkey => $certifValue ) {
                        $certification[$cetkey]['certification_images'] = base_url().'uploads/tutor/certification/'.$certifValue;
                    }
                } else {
                    $certification = array();
                }

                $data["certificate_pics"] = $certification;

                // Subject Details
                if (count($subjectData)>0) {
                    foreach ( $subjectData as $key => $value ) {
                        $subjectInfo[] = array(
                            'tut_subj_id'        => $value['tut_subj_id'],
                            'cat_id'      => $value['cat_id'],
                            'sub_id'    => $value['sub_id'], 
                            'rate_per_hour' => $value['rate_per_hour'],                 
                            'status'       => $value['status']                               
                            ); 
                    }
                    $data['subjectDetails'] = $subjectInfo;
                } 
                else {
                    $data['subjectDetails'] = array();
                }
            }

            // Result of user detail
            $this->response(array("status" => true, "user_details" => $data), 200); exit;
        }
    }*/


    /**
     * Author : Sanket
     * Title : subject_rate
     * Objective : Add subject rate
     * Description : Add subject rate for tutor.
     */
    // public function subject_rate_post()
    // {

    //     $data = file_get_contents('php://input');
    //     $newData = json_decode($data,true);

    //     $subject_rate = $newData['subject_rate'];


    //     //$postData = $this->post();
    //     if(is_authorized())
    //     {
    //         $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
    //     }else
    //     {

    //         /*echo "wwwww";

    //         exit();*/
    //         $tutor_id = $subject_rate['tutor_id'];
    

    //         foreach ($subject_rate['request_data'] as $key => $catrate)
    //         {

    //             $catid = $catrate['cat_id'];
    //             foreach ($catrate['cat_details'] as $key => $sub_rate) 
    //             {
    //                 $insertData[] = array(

    //                     'cat_id' => $catid,
    //                     'sub_id' => $sub_rate['sub_id'],
    //                     'tutor_id' => $tutor_id,
    //                     'rate_per_hour' => $sub_rate['rate'],
    //                     'created_at' => date('Y-m-d h:i:s')
    //                 );
    //             }   

    //         }

    //         if($insertData) 
    //         {

    //             $tutorSubject = $this->db->select('*',TB_TUTOR_SUBJECTS,array('tutor_id' => $tutor_id));

    //             if(count($tutorSubject) > 0)
    //             {

    //                 $delete = $this->db->delete(TB_TUTOR_SUBJECTS,array('tutor_id' => $tutor_id));
    //             }

    
    //             $result = $this->db->insert_batch(TB_TUTOR_SUBJECTS,$insertData);    
    
    //             if($result)
    //             {
    //                 $this->response(array("status" => "success", "message" => 'Data inserted Successfully.'), 200); exit;     
    //             }
    //             else
    //             {
    //                 $this->response(array("status" => "error", "message" => 'Data insertion problem.'), 200); exit;   
    //             }
    
    //         }
    
    //     }
    // }


    /**
     * Author : Sanket
     * Title : Create profile
     * Objective : Create profile
     * Description : Create profile for user.
     */
    public function create_profile_post()
    {
        /*if (is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }
        else
        {*/
            if("" == $this->input->post('user_id'))
            {
                echo $this->response(array('status' => 'error' , 'message' => " Please enter valid user id"),200);
            }
            if("" == $this->input->post('degree'))
            {
                echo $this->response(array('status' => 'error' , 'message' => " Please enter valid degree"),200);
            }
            /*if("" == $this->input->post('std'))
            {
                echo $this->response(array('status' => 'error' , 'message' => " Please enter valid standard"),200);
            }*/
            if("" == $this->input->post('address_type'))
            {
                echo $this->response(array('status' => 'error' , 'message' => " Please enter valid address type"),200);   
            }
            if("" == $this->input->post('address'))
            {
                echo $this->response(array('status' => 'error' , 'message' => " Please enter valid address"),200);   
            }
            if(isset($_FILES['certificate']['name']) && "" == $_FILES['certificate']['name'])
            {
                echo $this->response(array('status' => 'error' , 'message' => " Please enter valid Certificate"),200); 
            }
            else
            {

                $config = array(
                    'upload_path' => './uploads/tutor/',
                    'allowed_types' => 'pdf|png|jpeg|jpg',
                    'max_width' => '6000',
                    'max_height' => '5000',
                    'encrypt_name' => TRUE
                    );
                $this->load->library('upload', $config);
                $this->upload->initialize($config); 
                $check_upload = $this->upload->do_upload('certificate');

                if($check_upload)
                {
                    $data =array(
                        'user_id' => $this->input->post('user_id'),
                        'degree' => $this->input->post('degree'),
                        'tutor_organization' => $this->input->post('tutor_organization'),
                        //'standard' => $this->input->post('std'),
                        'address_type' => $this->input->post('address_type'),
                        'tutor_address' => $this->input->post('address'),
                        'certificate' => $this->upload->data('file_name'),
                        'created_date' => $this->input->post('created_date')

                        );

                    $insert = $this->common_model->insert(TB_TUTOR,$data);

                    if($insert)
                    {
                        echo $this->response(array('status' => 'success','message' => 'Profile created successfully'),200);
                    }
                    else
                    {
                        echo $this->response(array('status' => 'success','message' => 'error in profile creation'),200);   
                    }
                }
                else
                {
                    $error = $this->upload->display_errors(); 
                    echo $this->response(array("status" => "error", "message" => strip_tags($error)), 200);
                }
            }
            /* }*/
        }

    /**
     * Author : Sanket
     * Title :  service_type
     * Objective : Get service type
     * Description : Get service type.
     */
    public function service_type_post()
    {
        /*if (is_authorized() ) {
            $this->response(array("status" => false,"message" => 'You are not authorized to access'), 401); exit;
        }
        else
        {*/
            $serviceType = $this->common_model->select('*',TB_SERVICE_TYPE);

            if(count($serviceType) > 0)
            {
                echo $this->response(array("status" => "success","data" => $service_type),200);
                exit();
            }
            else
            {
                echo $this->response(array("status" => "error","message" => "no services available"),200);
                exit();   
            }
        //}
        }

        public function getTime_get()
        {
            $t = date('Y-m-d H:i:s');
            echo "<pre>";print_r($t);die;
        }


        public function testStrip_post(){

            require_once APPPATH.'third_party/stripe/init.php';
            \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

            try {
        // Use Stripe's library to make requests...
            //Creating connect account
                $account = \Stripe\Account::create(array(
                  "type" => "custom",
                  "country" => "CA",
                  "email" => "ramk@exceptionaire.com",
                  "statement_descriptor"=>"test",
                  "business_url" => "http://rkinfosoft.com",
                  "product_description" => "test",
                  "external_account" => array(
                    "object" => "bank_account",
                    "country" => "CA",
                    "currency" => "cad",
                    "account_holder_name" => 'Ram Kedar',
                    "account_holder_type" => 'individual',
                    "routing_number" => "110000000",
                    "account_number" => "12341234567",
                    "bank_code" => "01234",
                    "branch_code" => "001"
                    )
                  ));
            //print_r($account);
                

                
            // update details of user account
                $acc_details = \Stripe\Account::retrieve($account['id']);
                $acc_details->tos_acceptance->date = time();
            // Assumes you're not using a proxy
                $acc_details->tos_acceptance->ip = $_SERVER['REMOTE_ADDR'];


                $acc_details->legal_entity['first_name'] = 'Ram';
                $acc_details->legal_entity['last_name'] = 'Kedar';
                $acc_details->legal_entity['dob']['day'] = '01';
                $acc_details->legal_entity['dob']['month'] = '01';
                $acc_details->legal_entity['dob']['year'] = '1900';
            $acc_details->legal_entity['type'] = 'individual';//Either “individual” or “company” 
            $acc_details->legal_entity['personal_address']['line1'] = 'test';
            $acc_details->legal_entity['personal_address']['postal_code'] = '123456';
            $acc_details->legal_entity['personal_address']['city'] = 'test';
            $acc_details->legal_entity['personal_address']['state'] = 'Alberta';
            $acc_details->legal_entity['address']['city'] = 'test';
            $acc_details->legal_entity['address']['country'] = 'CA';
            $acc_details->legal_entity['address']['address'] = 'test';
            $acc_details->legal_entity['address']['address-line2'] = 'test';
            $acc_details->legal_entity['address']['locality'] = 'test';
            $acc_details->legal_entity['address']['subregion'] = 'Alberta';
            $acc_details->legal_entity['address']['local_timezone'] = 'IST - Calcutta';
            $acc_details->legal_entity['address']['postal_code'] = '123456';
            $acc_details->legal_entity['address']['state'] = 'Alberta';
            $acc_details->legal_entity['support_phone'] = '(555) 678 1212';



            $save_details = $acc_details->save();

            print_r($save_details);
            
            
            /*
            // transfer amount to user account
            $account = \Stripe\Account::retrieve('acct_1ChDYLHm8BuZjOIm');
            // transfer amount to service provider account
            $transfer = \Stripe\Transfer::create(array(
                      "amount" => 10,
                      "currency" => "usd",
                      "destination" => $account['id']
                    ));
            print_r($transfer);
            */

        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\InvalidRequest $e) {
          // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\Authentication $e) {
          // Authentication with Stripe's API failed
          // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\ApiConnection $e) {
          // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (\Stripe\Error\Base $e) {
          // Display a very generic error to the user, and maybe send
          // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        } catch (Exception $e) {
          // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];

            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");
        }
        
        
    }

    public function getAge($dob)
    {
        $today    = date('Y-m-d');
        $ageDiff  = date_diff(date_create($dob), date_create($today));
        $userAge  = $ageDiff->format('%y');
        return $userAge;
    }

    /** Author: Kiran
     * Title : user_feedback_list
     * Description : user feedbacks
     *  
     * @return Array
     */

    public function user_feedback_list_get()
    {
        $cond      = array();
        $jointype  = array(TB_ROLES => "LEFT");
        $join      = array(TB_ROLES => TB_ROLES.".roleId = ".TB_USER_FEEDBACK.".role_id");
        $resultUserFeedbackList = $this->Common_model->selectJoin("fb_id, user_feedback, roleId", TB_USER_FEEDBACK, $cond, array(), $join, $jointype);
        $parentFeedbackArray = $careDriverFeedbackArray = $tutorFeedbackArray = $schoolFeedbackArray = $orgFeedbackArray = array();
        $p = $c = $t = $s = $o = 0;

        if(count($resultUserFeedbackList) > 0) {

            foreach ($resultUserFeedbackList as $key => $value) {

                if($value['roleId'] == ROLE_PARENTS){
                    $parentFeedbackArray[$p]['id'] = $value['fb_id'];
                    $parentFeedbackArray[$p]['feedback'] = $value['user_feedback'];    
                    $p++;                
                } else if( $value['roleId'] == ROLE_CARE_DRIVER) {
                    $careDriverFeedbackArray[$c]['id'] = $value['fb_id'];
                    $careDriverFeedbackArray[$c]['feedback'] = $value['user_feedback'];
                    $c++;
                } else if( $value['roleId'] == ROLE_TUTOR) {
                    $tutorFeedbackArray[$t]['id'] = $value['fb_id'];
                    $tutorFeedbackArray[$t]['feedback'] = $value['user_feedback'];
                    $t++;
                } else if( $value['roleId'] == SCHOOL) {
                    $schoolFeedbackArray[$s]['id'] = $value['fb_id'];
                    $schoolFeedbackArray[$s]['feedback'] = $value['user_feedback'];
                    $s++;
                } else if( $value['roleId'] == ORGANIZATION) {
                    $orgFeedbackArray[$o]['id'] = $value['fb_id'];
                    $orgFeedbackArray[$o]['feedback'] = $value['user_feedback'];                    
                    $o++;
                }
            }

             // Assign array value
            $data['parent_list'] = $parentFeedbackArray;
            $data['caredriver_list'] = $careDriverFeedbackArray;
            $data['tutor_list'] = $tutorFeedbackArray; 
            $data['school_list'] = $schoolFeedbackArray; 
            $data['organization_list'] = $orgFeedbackArray; 

            $this->response(array('status' => true, 'user_feedback_list' => $data),200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'No feedback data are available ')); exit;
        }        
    }

    /**
    * Author : Priya
     * Title        : Attribute Data
     * Description  : Attribute Data according to service users
     * @param  Integer service_id
     * @return Array
     */

    public function attributeList_post ()
    {
        // Post data
        $postData = $_POST;
        
        // Checked id empty or not      
        if ($postData["service_id"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter valid service id.")); exit;
        } else {            
            $service_id    = $postData['service_id'];

            $cond      = array("".TB_ATTRIBUTE_MASTER.".status" => 1, "".TB_ATTRIBUTE_MAPPING.".status" => 1, "".TB_ATTRIBUTE_MAPPING.".service_id" => $service_id);
            $jointype  = array(TB_ATTRIBUTE_MAPPING => "LEFT");
            $join      = array(TB_ATTRIBUTE_MAPPING => TB_ATTRIBUTE_MAPPING.".attribute_id = ".TB_ATTRIBUTE_MASTER.".attribute_id");

            $attributedata = $this->Common_model->selectJoin(" ".TB_ATTRIBUTE_MASTER.".attribute_id, ".TB_ATTRIBUTE_MASTER.".attribute_description", TB_ATTRIBUTE_MASTER, $cond, array(), $join, $jointype);

            if ( !count($attributedata) ) {                
                $this->response(array("status" => false, "message" => "No attribute assign for this service.")); exit;
            } else {

                $this->response(array("status" => true, "message" => "Attribute data", "attributes" => $attributedata)); exit;
            }
        }

    }  


    public function getProfile_post ()   // for parent,school,organization
    {      
           // Post data
        $postData = $_POST;
        if(!empty($postData["user_id"])){
            if ( $this->service_user_exists($postData["user_id"]) == 0 ) {
                $this->response(array("status" => false,"message" => 'User id is not exists.')); exit;
            }
                // User details
            if ( $postData["user_id"] != '' ) {
                $cond                       = array("service_user_id" => $postData["user_id"]);
                $userData                   = $this->common_model->select("user_prof_pic,user_type_id,service_user_id,user_fullname,user_gender,user_device_id,user_address,user_address_latlong,user_birth_date,user_email,user_phone_number",TB_SERVICE_USERS,$cond);
                
                $data['user_id']            = $userData[0]['service_user_id']           ? $userData[0]['service_user_id']           : 'N/A';
                $data['role_id']            = $userData[0]['user_type_id']            ? $userData[0]['user_type_id']            : 'N/A';
                $data['user_name']          = $userData[0]['user_fullname']         ? $userData[0]['user_fullname']         : 'N/A';
                $data['user_email']         = $userData[0]['user_email']        ? $userData[0]['user_email']        : 'N/A';
                $data['user_phone_number']  = $userData[0]['user_phone_number'] ? $userData[0]['user_phone_number'] : 'N/A';
                $data['user_birth_date']    = $userData[0]['user_birth_date']   ? $userData[0]['user_birth_date']   : 'N/A';
                $data['user_address']       = $userData[0]['user_address']      ? $userData[0]['user_address']      : 'N/A';
                $data['user_device_id']     = $userData[0]['user_device_id']    ? $userData[0]['user_device_id']    : 'N/A';
                $data['user_pic']           = base_url('uploads/serviceUser/'.$userData[0]['service_user_id'].'/'.$userData[0]['user_prof_pic'].'');
                
                //Seprate the lat and long parameter
                if($userData[0]['user_address_latlong']){
                   $address_lat_long=explode(',', $userData[0]['user_address_latlong']);
                   $data['user_address_lat']     = $address_lat_long[0];
                   $data['user_address_long']     = $address_lat_long[1];
               }
               else{
                $data['user_address_lat']     = 'N/A';
                $data['user_address_long']     = 'N/A';
            }


                //Age calculation
            $dob = trim($userData[0]['user_birth_date']);
            $userAge = $this->getAge($dob);
            $data['user_age']           = $userAge          ? $userAge          : 'N/A';

                //Gender Details
            if ( $userData[0]['user_type_id'] == ROLE_PARENTS ) {
               $data['user_gender']        = $userData[0]['user_gender']       ? $userData[0]['user_gender']       : 'N/A';
           }

           $passengerData   = $this->common_model->select("*",TB_PASSENGER,array("service_user_id" => $postData["user_id"]));
                // Parent details
           if ($userData[0]['user_type_id'] == ROLE_PARENTS ) {

            $total_no_of_kids=count($passengerData);
            $data['user_no_of_kids'] = $total_no_of_kids ? $total_no_of_kids : '';
            
                    // Children detailsd
            if (count($passengerData)>0) {

                foreach ( $passengerData as $key => $value ) {
                    $kidInfo[] = array(
                        'kid_id'        => $value['passenger_id'],
                        'kid_name'      => $value['pass_fullname'],
                        'kid_gender'    => $value['pass_gender'], 
                        'kid_birthdate' => $value['pass_dob'],                 
                        'kid_age'       => $value['pass_age'],                               
                        'kid_standard'  => $value['pass_standard']                               
                        ); 
                }
                $data['kidsDetails'] = $kidInfo;
            } else {
                $data['kidsDetails'] = array();
            }
            $this->response(array("status" => true,"message"=>'Data found',"profile_details" => $data), 200); exit;
        } 
                else{   //student details
                    if (count($passengerData)>0) {
                        foreach ($passengerData as $key => $value ) {
                            $studentInfo[] = array(
                                'student_id'        => $value['passenger_id'],
                                'student_fullname'      => $value['pass_fullname'],
                                'student_username'      => $value['pass_username'],
                                'student_parent_name'    => $value['pass_parent_name'], 
                                'student_contact_no' => $value['pass_contact_no'],                 
                                'student_address'       => $value['pass_address']                               
                                //'student_standard'  => $value['pass_standard']                               
                                ); 
                        }
                        $data['studentDetails'] = $studentInfo;
                    } else {
                        $data['studentDetails'] = array();
                    }
                    $this->response(array("status" => true,"message"=>'Data found',"profile_details" => $data), 200); exit;
                }
            }
        }
        else{
           $this->response(array("status" => true,"message"=>'User id is required'), 200); exit;
       }
   }

   public function service_user_exists ($user_id)
   { 
        // Check user available or not
    $cond = array("service_user_id" => $user_id, "user_status" => "1");
    $exist_users = $this->common_model->select('service_user_id',TB_SERVICE_USERS,$cond); 
        // If user available return 1 otherwise 0
    if ( count($exist_users) > 0 ) {
        return 1;
    } else {
        return 0;
    }
}



     /**
     * Jyoti Korde
     * Title : add_passenger
     * Description : To add new passenger
     * 
     * @param Integer service_user_id
     * for user type = 2
     * @param Varchar pass_fullname
     * @param Varchar pass_gender
     * @param Date    pass_dob
     * for user type = 5 and 6
     * @param Varchar pass_username
     * @param Varchar pass_fullname
     * @param Varchar pass_parent_name
     * @param Integer pass_contact_no
     * @param Varchar pass_address
     * @param Varchar pass_standard
     * @return Array 
     */

     public function add_passenger_post ()
     {
        // Input data
        $postData = $this->post();

        //Check user role
        if(!empty($postData['user_id'])){
           $checkUserType = $this->common_model->select("user_type_id",TB_SERVICE_USERS,array("service_user_id"=>$postData['user_id']));
           if(count($checkUserType) == '0'){
             $this->response(array("status" => false, "message" => 'User id is not available'), 200); exit;
         }
     }
     else{
        $this->response(array("status" => false, "message" => 'User id is required'), 200); exit;
    }

        //Input Validation
    $user_type = $checkUserType[0]['user_type_id'];

    if($user_type == '2'){

        if(empty($postData['pass_fullname'])){
           $this->response(array("status" => false, "message" => 'Fullname is required'), 200); exit;
       }
       if(empty($postData['pass_gender'])){
           $this->response(array("status" => false, "message" => 'Gender is required'), 200); exit;
       }
       if(empty($postData['pass_dob'])){
           $this->response(array("status" => false, "message" => 'Birth date is required'), 200); exit;
       }
       if(empty($postData['pass_standard'])){
           $this->response(array("status" => false, "message" => 'Standard is required'), 200); exit;
       }
       $data = array(
        'service_user_id' => $postData['user_id'],                    
        'pass_fullname' => $postData['pass_fullname'],
        'pass_gender' => $postData['pass_gender'],
        'pass_dob' => $postData['pass_dob'],
        'pass_standard' => $postData['pass_standard'],
        'pass_age' => $this->getAge($postData['pass_dob']),
        'created_date' => date( 'Y-m-d H:i:s', time()),
        'pass_status' => "1"
        );

            // Add data
       $lastInsertId = $this->common_model->insert(TB_PASSENGER, $data);

       if($lastInsertId){
           $this->response(array("status" => true, "message" => 'Kid added successfully.'), 200);exit;
       }
       else
       {
           $this->response(array("status" => false, "message" => 'Somthing went wrong'), 200);exit;
       }

   }
   else if ($user_type == '5' || $user_type == '6') {
    if(empty($postData['pass_username'])){
       $this->response(array("status" => 'false', "message" => 'Username is required'), 200); exit;
   }
   if(empty($postData['pass_fullname'])){
       $this->response(array("status" => false, "message" => 'Fullname is required'), 200); exit;
   }
   if(empty($postData['pass_parent_name'])){
       $this->response(array("status" => false, "message" => 'Parent Name is required'), 200); exit;
   }
   if(empty($postData['pass_contact_no'])){
       $this->response(array("status" => false, "message" => 'Contact number is required'), 200); exit;
   }
   if(empty($postData['pass_address'])){
       $this->response(array("status" => false, "message" => 'Address is required'), 200); exit;
   }
   $checkExists = $this->common_model->select("pass_username", TB_PASSENGER, array("pass_username"=>$postData['pass_username']));
   if (count($checkExists) > 0) {
    $this->response(array("status" => false,"message" => 'Student already exists with same username.'), 200); exit; 
} 
else{
    $data = array(
        'service_user_id' => $postData['user_id'],                    
        'pass_fullname' => $postData['pass_fullname'],
        'pass_username' => $postData['pass_username'],
        'pass_parent_name' => $postData['pass_parent_name'],
        'pass_contact_no' => $postData['pass_contact_no'],
        'pass_address' => $postData['pass_address'],
        'created_date' => date( 'Y-m-d H:i:s', time()),
        'pass_status' => "1"
        );
                // Add data
    $lastInsertId = $this->common_model->insert(TB_PASSENGER, $data);
    if($lastInsertId){
       $this->response(array("status" => true, "message" => 'Student added successfully.'), 200);exit;
   }
   else
   {
       $this->response(array("status" => false, "message" => 'Somthing went wrong'), 200);exit;
   }
}
}
}

    /**
     * Title : update_passenger
     * Description : To edit existing passenger
     * 
     * @param passenger_id
     * @param Integer user_id
     * for user type = 2
     * @param Varchar pass_fullname
     * @param Varchar pass_gender
     * @param Date    pass_dob
     * for user type = 5 and 6
     * @param Varchar pass_username
     * @param Varchar pass_fullname
     * @param Varchar pass_parent_name
     * @param Integer pass_contact_no
     * @param Varchar pass_address
     * @return Array 
     */
    public function update_passenger_post()
    {
        // Input data
        $postData = $this->post();
        $passenger_id = $postData['passenger_id'];
        if(!empty($postData['passenger_id'])){
            //Check user role
            if(!empty($postData['user_id'])){
               $checkUserType = $this->common_model->select("user_type_id",TB_SERVICE_USERS,array("service_user_id"=>$postData['user_id']));
               if(count($checkUserType) == '0'){
                 $this->response(array("status" => false, "message" => 'User id is not available'), 200); exit;
             }
         }
         else{
            $this->response(array("status" => false, "message" => 'User id is required'), 200); exit;
        }

            //Input Validation
        $user_type = $checkUserType[0]['user_type_id'];
        if($user_type == '2'){
                   // Validation to check all inputs are required
            if(empty($postData['pass_fullname'])){
               $this->response(array("status" => false, "message" => 'Fullname is required'), 200); exit;
           }
           if(empty($postData['pass_gender'])){
               $this->response(array("status" => false, "message" => 'Gender is required'), 200); exit;
           }
           if(empty($postData['pass_dob'])){
               $this->response(array("status" => false, "message" => 'Birth date is required'), 200); exit;
           }
           if(empty($postData['pass_standard'])){
               $this->response(array("status" => false, "message" => 'Standard is required'), 200); exit;
           }
           $data = array(
            'service_user_id' => $postData['user_id'],                    
            'pass_fullname' => $postData['pass_fullname'],
            'pass_gender' => $postData['pass_gender'],
            'pass_dob' => $postData['pass_dob'],
            'pass_standard' => $postData['pass_standard'],
            'pass_age' => $this->getAge($postData['pass_dob']),
            'pass_status' => "1",
            'updated_date' => date('Y-m-d H:i:s', time())
            );
                    // Update data
           $updateRows=$this->common_model->update(TB_PASSENGER, array("passenger_id" => $passenger_id), $data);
           if($updateRows){
               $this->response(array("status" => true, "message" => 'Kid updated successfully.'), 200);exit;
           }
           else
           {
               $this->response(array("status" => false, "message" => 'Somthing went wrong'), 200);exit;
           }
       }
       else if ($user_type == '5' || $user_type == '6') {

                   // Validation to check all inputs are required
        if(empty($postData['pass_username'])){
           $this->response(array("status" => false, "message" => 'Username is required'), 200); exit;
       }
       if(empty($postData['pass_fullname'])){
           $this->response(array("status" => false, "message" => 'Name is required'), 200); exit;
       }
       if(empty($postData['pass_parent_name'])){
           $this->response(array("status" => false, "message" => 'Parent Name is required'), 200); exit;
       }
       if(empty($postData['pass_contact_no'])){
           $this->response(array("status" => false, "message" => 'Contact number is required'), 200); exit;
       }
       if(empty($postData['pass_address'])){
           $this->response(array("status" => false, "message" => 'Address is required'), 200); exit;
       }
       $checkExists = $this->common_model->select("pass_username", TB_PASSENGER, array("pass_username"=>$postData['pass_username'],"passenger_id!="=> $passenger_id));

       if (count($checkExists) > 0) {
        $this->response(array("status" => false,"message" => 'Student already exists with same username.'), 200); exit; 
    } 
    else{
        $data = array(
            'service_user_id' => $postData['user_id'],                    
            'pass_fullname' => $postData['pass_fullname'],
            'pass_username' => $postData['pass_username'],
            'pass_parent_name' => $postData['pass_parent_name'],
            'pass_contact_no' => $postData['pass_contact_no'],
            'pass_address' => $postData['pass_address'],
            'pass_status' => "1",
            'updated_date' => date('Y-m-d H:i:s', time())
            );
                        // Update data
        $updateRows=$this->common_model->update(TB_PASSENGER, array("passenger_id" => $passenger_id), $data);

        if($updateRows){
           $this->response(array("status" => true, "message" => 'Student updated successfully.'), 200);exit;
       }
       else
       {
           $this->response(array("status" => false, "message" => 'Somthing went wrong'), 200);exit;
       }
   }
}
}
else{
    $this->response(array("status" => false,"message" => 'Passenger id is required.')); exit;
}
}


    /**
    * Author : Priya
     * Title        : Get profile data for driver and tutor
     * Description  : Driver and Tutor profile information
     *   
     * @return Array
     */

    public function serviceProviderProfile_post ()
    {
        // Post data
        $postData = $_POST;
        
        // Checked email address empty or not      
        if ($postData["provider_id"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter service provider id.")); exit;
        } else {            
            $service_provider_id    = $postData['provider_id'];

            $tutordata = $this->common_model->select("*", TB_SERVICE_PROVIDER, array("service_provider_id" => $service_provider_id));
            $tutordata[0]['sp_age']=$this->getAge($tutordata[0]['sp_birth_date']);

            if ( !count($tutordata) ) {                
                $this->response(array("status" => false, "message" => "Please enter correct tutor id, this tutor does not exist.")); exit;
            } else {


                if($tutordata[0]['sp_status'] == 1) {

                    $cond      = array("".TB_ATTRIBUTE_VALUES.".service_provider_id" => $service_provider_id, "".TB_ATTRIBUTE_MASTER.".status" => 1 );
                    $jointype  = array(TB_ATTRIBUTE_MASTER => "LEFT");
                    $join      = array(TB_ATTRIBUTE_MASTER => TB_ATTRIBUTE_MASTER.".attribute_id = ".TB_ATTRIBUTE_VALUES.".attribute_id");

                    $attriData = $this->Common_model->selectJoin(" ".TB_ATTRIBUTE_MASTER.".attribute_id, ".TB_ATTRIBUTE_MASTER.".attribute_description, ".TB_ATTRIBUTE_VALUES.".attribute_value, ".TB_ATTRIBUTE_VALUES.".attribute_type", TB_ATTRIBUTE_VALUES, $cond, array(), $join, $jointype);


                    if ($tutordata[0]['sp_user_type'] == 4) {

                        $catData = $this->common_model->getPriceWithCat($service_provider_id);


                        foreach($attriData as $key => $allAttri ) {

                            if($allAttri['attribute_type'] == 1) {

                                $attriData[$key]['attribute_value'] = base_url('uploads/tutor/'.$service_provider_id.'/'.$allAttri['attribute_value'].' ');
                            } 
                        }

                        if($tutordata[0]['sp_prof_pic'] != '') {
                            $tutordata[0]['sp_prof_pic'] = base_url('uploads/tutor/'.$service_provider_id.'/'.$tutordata[0]['sp_prof_pic'].' ');
                        }

                        if($tutordata[0]['sp_cover_pic'] != '') {
                            $tutordata[0]['sp_cover_pic'] = base_url('uploads/tutor/'.$service_provider_id.'/'.$tutordata[0]['sp_cover_pic'].' ');
                        }

                        $this->response(array("status" => true, "message" => "Your profile data", "basic_profile" => $tutordata[0], "edu_details" => $attriData, "catSub_details" => $catData)); exit;

                    } else if ($tutordata[0]['sp_user_type'] == 3) {

                        foreach($attriData as $key => $allAttri ) {

                            if($allAttri['attribute_type'] == 1) {

                                $attriData[$key]['attribute_value'] = base_url('uploads/driver/'.$service_provider_id.'/'.$allAttri['attribute_value'].' ');

                            } 
                        }

                        if($tutordata[0]['sp_prof_pic'] != '') {
                            $tutordata[0]['sp_prof_pic'] = base_url('uploads/driver/'.$service_provider_id.'/'.$tutordata[0]['sp_prof_pic'].' ');
                        }

                        if($tutordata[0]['sp_cover_pic'] != '') {
                            $tutordata[0]['sp_cover_pic'] = base_url('uploads/driver/'.$service_provider_id.'/'.$tutordata[0]['sp_cover_pic'].' ');
                        }

                        //vehicle details
                        $vehicledata = $this->common_model->select("*", TB_SERVICE_PROVIDER_VEHICLE, array("service_provider_id" => $service_provider_id,"is_default" =>'1'));
                        $vehicledata[0]['car_pic'] = base_url('uploads/driver/carPics/'.$vehicledata[0]['car_pic'].' ');
                        $vehicledata[0]['car_number_plate_pic'] = base_url('uploads/driver/carNumberPlate/'.$vehicledata[0]['car_number_plate_pic'].' ');
                        $vehicledata[0]['licence_scan_copy'] = base_url('uploads/driver/licenseCopy/'.$vehicledata[0]['licence_scan_copy'].' ');
                        $this->response(array("status" => true, "message" => "Your profile data", "basic_profile" => $tutordata[0], "edu_details" => $attriData, "car_details" => $vehicledata)); exit;

                    } else {

                        $this->response(array("status" => false, "message" => "Invalid user type.")); exit;
                    }

                    
                } else {
                    $this->response(array("status" => false, "message" => "Your account is inactive, You need to contact to admin.")); exit;
                }
                
                
            }
        }

    }

        /**
     * Author : Kiran
     * Title : service_user_profile_details
     * 
     * @param Integer s_id
     * @return Array
     */

        public function service_user_profile_details_post()
        {
        // Input validation
            $postData = $this->post();
            if(empty(trim($postData['s_id'])))
            {
                $this->response(array("status"=>false, "message" => 'Sevice user id is required.'), 200);
                exit;
            }    

        // Fetch data from multiple tables
            $cond = array(TB_SERVICE_USERS.'.service_user_id' => $postData['s_id']);
            $jointype = array();
            $join = array(TB_PASSENGER => TB_PASSENGER.".service_user_id = ".TB_SERVICE_USERS.".service_user_id");
            $resultSPDetails = $this->Common_model->selectJoin(TB_SERVICE_USERS.".service_user_id, organization_name, user_email,user_fullname, user_phone_number, user_address, passenger_id, pass_username, pass_fullname, pass_gender, YEAR( CURDATE( ) ) - YEAR( pass_dob ) AS kid_age",TB_SERVICE_USERS,$cond,array(),$join,$jointype);
            if(count($resultSPDetails) > 0)
            {
                $serviceProviderData = $passengerData = array();
                $i = 0;
                $serviceProviderData['service_user_id'] = $resultSPDetails[0]['service_user_id'];
                $serviceProviderData['full_name'] = $resultSPDetails[0]['user_fullname'];
                $serviceProviderData['organization_name'] = $resultSPDetails[0]['organization_name'];
                $serviceProviderData['user_email'] = $resultSPDetails[0]['user_email'];
                $serviceProviderData['user_phone_number'] = $resultSPDetails[0]['user_phone_number'];
                $serviceProviderData['user_address'] = $resultSPDetails[0]['user_address'];
                
            //Kids info
                foreach ($resultSPDetails as $key => $value) {
                    $passengerData[$i]['passenger_id'] = $value['passenger_id'];
                    $passengerData[$i]['username'] = $value['pass_username'];
                    $passengerData[$i]['fullname'] = ucfirst($value['pass_fullname']);
                    $passengerData[$i]['gender'] = ucfirst($value['pass_gender']);
                    $passengerData[$i]['age'] = $value['kid_age'];                
                    $i++;
                }

                $this->response(array("status"=>true, "serviceData" => $serviceProviderData,"studentInfo"=>$passengerData), 200);
                exit;
            } else {
                $this->response(array("status"=>false, "message" => 'Record not found.'), 200);
                exit;
            }    
        }

    /**
     * Author : Kiran
     * Title : add_group_member
     * Description : add group member
     * 
     * @param Integer group_id
     * @param Integer stud_id
     * @return Array
     */

    public function add_group_member_post()
    {
        $postData = $this->post();
        // Input validation
        if(empty(trim($postData['group_id'])))
        {
            $this->response(array("status"=>false, "message" => 'Group id is required.'), 200);
            exit;
        }
        if(empty(trim($postData['stud_id'])))
        {
            $this->response(array("status"=>false, "message" => 'student id is required.'), 200);
            exit;
        }
        
        // check group member is exist or not
        $cond = array("group_id"=>trim($postData['group_id']), "passenger_id" => $postData['stud_id']);
        $resultUserExistCheck = $this->common_model->select("*",TB_GROUP_MEMBER,$cond);
        if(count($resultUserExistCheck) > 0){

            if($resultUserExistCheck[0]['status'] == 0) {

                // update group no of peoples
                $condS = array("group_id"=>$postData['group_id'], "passenger_id"=>$postData['stud_id']);
                $dataS = array("status"=>"1");
                $upStatus = $this->common_model->update(TB_GROUP_MEMBER,$condS,$dataS);

                    if($upStatus) {
                        $this->response(array("status" => false, "message" => 'Group member has been added successfully.'), 200);exit;
                    } else {
                        $this->response(array("status" => false, "message" => 'Failed to add member in group'), 200);exit;
                    }

            } else {
                $this->response(array("status" => false, "message" => 'Passenger id already exists in group'), 200);exit;
            }
            
        } else {
            // add group member
            $data = array(
                "group_id" => $postData['group_id'],
                "passenger_id" => $postData['stud_id'],
                "status" => "1",
                "created_at" => date("Y-m-d H:i:s")
                );
            $resultAddMember = $this->common_model->insert(TB_GROUP_MEMBER,$data);

            // Get group no of peoples
            $resultNoOfPeoples = $this->common_model->select("no_of_people", TB_GROUP_MASTER, array("group_id" => $postData['group_id']));

            // update group no of peoples
            $cond1 = array("group_id"=>$postData['group_id']);
            $data1 = array("no_of_people"=>($resultNoOfPeoples[0]['no_of_people'] + 1),"updated_date" => date( 'Y-m-d H:i:s'));
            $resultUpdateGroupNOP = $this->common_model->update(TB_GROUP_MASTER,$cond1,$data1);            
            
            if($resultAddMember && $resultUpdateGroupNOP){
                $this->response(array("status" => true, "message" => 'Group member has been added successfully.'), 200);exit;
            } else {
                $this->response(array("status" => false, "message" => 'Somthing went wrong'), 200);exit;
            }
        }
    }

    /**
     * Author : Kiran
     * Title : notification_list
     * Description : Notification list
     *
     * @param  Integer user_id
     * @param  Integer count
     * @return Array
     */

    public function notification_listing_post() 
    {
        // post data
        $postData = $_POST;
        
        // input validation
        if(empty($postData['user_id'])) {
            $this->response(array('status' => false, 'message' => 'User id is required.'), 200); exit;
        }
        if(empty($postData['count'])) {
            $this->response(array('status' => false, 'message' => 'count is required.'), 200); exit;
        }

        $count = $postData['count'];

        // Get all notification list by user id
        $resultNotifList = $this->Common_model->fetch_no_of_records("notification_id,title,type,type_id,message,read_status,noti_image",TB_NOTIFICATION,array("user_id"=>$postData['user_id']),$count,0);
        
        $notificationArray = array();
        // result
        if (count($resultNotifList) > 0) {            
            $i = $read_count = 0;
            foreach ($resultNotifList as $key => $value) {
                if( $value['read_status'] == '1')
                    $read_count++; 
                $notificationArray[$i]['notification_id'] = $value['notification_id'];
                $notificationArray[$i]['title'] = $value['title'];
                $notificationArray[$i]['type'] = $value['type'];
                $notificationArray[$i]['type_id'] = $value['type_id'];
                $notificationArray[$i]['message'] = $value['message'];
                $notificationArray[$i]['read_status'] = $value['read_status'];
                $notificationArray[$i]['image_url'] = $value['noti_image'];
                $i++;
            }
            $this->response(array('status' => true, 'read_count' => $read_count, 'total_notification' => $i, 'data' => $notificationArray), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Notification data not found.'), 200); exit;
        }    
    }

        /**
     * Author : Kiran
     * Title : read_notification
     * Description : read notification change status
     * 
     * @param Integer notification_id
     * @return Array
     */

        public function read_notification_post()
        {
        // Input data
            $postData = $this->post();
        // Input validation
            if(empty(trim($postData['notification_id'])))
            {
                $this->response(array("status"=>false, "message" => 'Notification id is required.'), 200);
                exit;
            }
            if(empty(trim($postData['user_id'])))
            {
                $this->response(array("status"=>false, "message" => 'User id is required.'), 200);
                exit;
            }
            
        // read notification status change
            $cond = array("notification_id"=>$postData['notification_id'],"user_id" => $postData['user_id']);
            $data = array("read_status" => "1", "update_date"=> date("Y-m-d H:i:s"));
            $resultReadNotif = $this->common_model->update(TB_NOTIFICATION,$cond,$data);            
            if($resultReadNotif) {
                $this->response(array("status" => true, "message" => "Notification has been read successfully"), 200); exit; 
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong'), 200); exit;
            }                
        }


     /**
    * Author : Priya
     * Title        : Update user profile image
     * Description  : Update user profile image
     *   @para : app_id  = 0 (user) 1 = (provider)
     *   @para : user_id
     *   @para : profile_image 
     *   @para : cover_image
     *   return : success message 
     */
     public function updateUserImage_post ()
     {
        // Post data
        $postData = $_POST;
        
        // Checked tutor id empty or not      
        if ($postData["app_id"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter app id.")); exit;
        } else if($postData["user_id"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter user id.")); exit;
        } else {

            $appId = $postData["app_id"];
            $userId = $postData["user_id"];

            if($appId == 0) {

                $checkExists = $this->common_model->select("*", TB_SERVICE_USERS, array("service_user_id" => $userId));

                if(count($checkExists) != 0) {

                    if (!file_exists('./uploads/serviceUser/'.$userId.'/')) {
                        mkdir('./uploads/serviceUser/'.$userId.'/');
                    }

                    if ( isset($_FILES['profile_image']['name']) &&  isset($_FILES['cover_image']['name']) ) {

                        $config =array('upload_path' => './uploads/serviceUser/'.$userId.'/',
                            'allowed_types' => 'png|jpeg|jpg',
                            'max_width' => '6000',
                            'max_height' => '5000'
                            );

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config); 

                        $check_uploadp = $this->upload->do_upload('profile_image');
                        $check_uploadc = $this->upload->do_upload('cover_image');

                        if($check_uploadp && $check_uploadc) {

                            $Arr = array("user_prof_pic"=>$_FILES['profile_image']['name'], "user_cover_pic"=>$_FILES['cover_image']['name']);

                            $cond = array("service_user_id"=>$userId);
                            $result = $this->common_model->update(TB_SERVICE_USERS,$cond,$Arr);

                            if($result) {

                                $this->response(array("status" => true,"message" => 'Your profile image and cover image updated successfully'), 200); exit;
                            } else {

                                $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                            }
                        } else if($check_uploadp){
                            $Arr = array("user_prof_pic"=>$_FILES['profile_image']['name']);

                            $cond = array("service_user_id"=>$userId);
                            $result = $this->common_model->update(TB_SERVICE_USERS,$cond,$Arr);

                            if($result) {

                                $this->response(array("status" => true,"message" => 'Your profile image updated successfully'), 200); exit;
                            } else {

                                $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                            }
                        } else if($check_uploadc) {
                            $Arr = array("user_cover_pic"=>$_FILES['cover_image']['name']);

                            $cond = array("service_user_id"=>$userId);
                            $result = $this->common_model->update(TB_SERVICE_USERS,$cond,$Arr);

                            if($result) {

                                $this->response(array("status" => true,"message" => 'Your cover image updated successfully'), 200); exit;
                            } else {

                                $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                            }
                        } else {
                            $this->response(array("status" => false,"message" => 'Failed to update your profile image, invalid file format check file size.'), 200); exit;
                        }

                    } else if( isset($_FILES['profile_image']['name'])) {
                        $config =array('upload_path' => './uploads/serviceUser/'.$userId.'/',
                            'allowed_types' => 'png|jpeg|jpg',
                            'max_width' => '6000',
                            'max_height' => '5000'
                            );

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config); 

                        $check_uploadp = $this->upload->do_upload('profile_image');

                        if($check_uploadp){
                            $Arr = array("user_prof_pic"=>$_FILES['profile_image']['name']);
                            $cond = array("service_user_id"=>$userId);
                            $result = $this->common_model->update(TB_SERVICE_USERS,$cond,$Arr);

                            if($result) {
                                $this->response(array("status" =>true,"message" => 'Your profile image updated successfully'), 200); exit;
                            } else {
                                $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                            }
                        } else {
                         $this->response(array("status" => false, "message" => "Failed to upload profile imgae, invalid file format or size.")); exit;  
                     }

                 } else if( isset($_FILES['cover_image']['name'])) {

                    $config =array('upload_path' => './uploads/serviceUser/'.$userId.'/',
                        'allowed_types' => 'png|jpeg|jpg',
                        'max_width' => '6000',
                        'max_height' => '5000'
                        );

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config); 

                    $check_uploadp = $this->upload->do_upload('cover_image');

                    if($check_uploadp){
                        $Arr = array("user_cover_pic"=>$_FILES['cover_image']['name']);
                        $cond = array("service_user_id"=>$userId);
                        $result = $this->common_model->update(TB_SERVICE_USERS,$cond,$Arr);

                        if($result) {
                            $this->response(array("status" =>true,"message" => 'Your cover image updated successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false,"message" => 'This image already as cover image.'), 200); exit;
                        }
                    } else {
                     $this->response(array("status" => false, "message" => "Failed to upload cover imgae, invalid file format or size.")); exit;  
                 }

             } else {
                 $this->response(array("status" => false, "message" => "Please upload profile or cover pic.")); exit; 
             }

         } else {
            $this->response(array("status" => false,"message" => 'User not exists.'), 200); exit;
        }

    } else {

        $checkExists = $this->common_model->select("*", TB_SERVICE_PROVIDER, array("service_provider_id" => $userId));

        if(count($checkExists) != 0) {

            $roleId = $checkExists[0]['sp_user_type'];

            if($roleId == 3) {

                if (!file_exists('./uploads/driver/'.$userId.'/')) {
                    mkdir('./uploads/driver/'.$userId.'/');
                }

                if ( isset($_FILES['profile_image']['name']) &&  isset($_FILES['cover_image']['name']) ) {

                    $config=array('upload_path'=>'./uploads/driver/'.$userId.'/',
                        'allowed_types' => 'png|jpeg|jpg',
                        'max_width' => '6000',
                        'max_height' => '5000'
                        );

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config); 

                    $check_uploadp = $this->upload->do_upload('profile_image');
                    $check_uploadc = $this->upload->do_upload('cover_image');

                    if($check_uploadp && $check_uploadc) {

                        $Arr = array("sp_prof_pic"=>$_FILES['profile_image']['name'], "sp_cover_pic"=>$_FILES['cover_image']['name']);

                        $cond = array("service_provider_id"=>$userId);
                        $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                        if($result) {

                            $this->response(array("status" => true,"message" => 'Your profile image and cover image updated successfully'), 200); exit;
                        } else {

                            $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                        }
                    } else if($check_uploadp){
                        $Arr = array("sp_prof_pic"=>$_FILES['profile_image']['name']);

                        $cond = array("service_provider_id"=>$userId);
                        $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                        if($result) {

                            $this->response(array("status" => true,"message" => 'Your profile image updated successfully'), 200); exit;
                        } else {

                            $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                        }
                    } else if($check_uploadc) {
                        $Arr = array("sp_cover_pic"=>$_FILES['cover_image']['name']);

                        $cond = array("service_provider_id"=>$userId);
                        $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                        if($result) {

                            $this->response(array("status" => true,"message" => 'Your cover image updated successfully'), 200); exit;
                        } else {

                            $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                        }
                    } else {
                        $this->response(array("status" => false,"message" => 'Failed to update your profile image, invalid file format check file size.'), 200); exit;
                    }

                } else if( isset($_FILES['profile_image']['name'])) {
                    $config=array('upload_path'=>'./uploads/driver/'.$userId.'/',
                        'allowed_types' => 'png|jpeg|jpg',
                        'max_width' => '6000',
                        'max_height' => '5000'
                        );

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config); 

                    $check_uploadp = $this->upload->do_upload('profile_image');

                    if($check_uploadp){
                        $Arr = array("sp_prof_pic"=>$_FILES['profile_image']['name']);
                        $cond = array("service_provider_id"=>$userId);
                        $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                        if($result) {
                            $this->response(array("status" =>true,"message" => 'Your profile image updated successfully'), 200); exit;
                        } else {
                            $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                        }
                    } else {
                     $this->response(array("status" => false, "message" => "Failed to upload profile imgae, invalid file format or size.")); exit;  
                 }

             } else if( isset($_FILES['cover_image']['name'])) {

                $config=array('upload_path'=>'./uploads/driver/'.$userId.'/',
                    'allowed_types' => 'png|jpeg|jpg',
                    'max_width' => '6000',
                    'max_height' => '5000'
                    );

                $this->load->library('upload', $config);
                $this->upload->initialize($config); 

                $check_uploadp = $this->upload->do_upload('cover_image');

                if($check_uploadp){
                    $Arr = array("sp_cover_pic"=>$_FILES['cover_image']['name']);
                    $cond = array("service_provider_id"=>$userId);
                    $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                    if($result) {
                        $this->response(array("status" =>true,"message" => 'Your cover image updated successfully'), 200); exit;
                    } else {
                        $this->response(array("status" => false,"message" => 'This image already as cover image.'), 200); exit;
                    }
                } else {
                 $this->response(array("status" => false, "message" => "Failed to upload cover imgae, invalid file format or size.")); exit;  
             }

         } else {
             $this->response(array("status" => false, "message" => "Please upload profile or cover pic.")); exit; 
         }

     }  else if($roleId == 4) {

        if (!file_exists('./uploads/tutor/'.$userId.'/')) {
            mkdir('./uploads/tutor/'.$userId.'/');
        }

        if ( isset($_FILES['profile_image']['name']) &&  isset($_FILES['cover_image']['name']) ) {

            $config=array('upload_path'=>'./uploads/tutor/'.$userId.'/',
                'allowed_types' => 'png|jpeg|jpg',
                'max_width' => '6000',
                'max_height' => '5000'
                );

            $this->load->library('upload', $config);
            $this->upload->initialize($config); 

            $check_uploadp = $this->upload->do_upload('profile_image');
            $check_uploadc = $this->upload->do_upload('cover_image');

            if($check_uploadp && $check_uploadc) {

                $Arr = array("sp_prof_pic"=>$_FILES['profile_image']['name'], "sp_cover_pic"=>$_FILES['cover_image']['name']);

                $cond = array("service_provider_id"=>$userId);
                $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                if($result) {

                    $this->response(array("status" => true,"message" => 'Your profile image and cover image updated successfully'), 200); exit;
                } else {

                    $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                }
            } else if($check_uploadp){
                $Arr = array("sp_prof_pic"=>$_FILES['profile_image']['name']);

                $cond = array("service_provider_id"=>$userId);
                $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                if($result) {

                    $this->response(array("status" => true,"message" => 'Your profile image updated successfully'), 200); exit;
                } else {

                    $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                }
            } else if($check_uploadc) {
                $Arr = array("sp_cover_pic"=>$_FILES['cover_image']['name']);

                $cond = array("service_provider_id"=>$userId);
                $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                if($result) {

                    $this->response(array("status" => true,"message" => 'Your cover image updated successfully'), 200); exit;
                } else {

                    $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                }
            } else {
                $this->response(array("status" => false,"message" => 'Failed to update your profile image, invalid file format check file size.'), 200); exit;
            }

        } else if( isset($_FILES['profile_image']['name'])) {
            $config=array('upload_path'=>'./uploads/tutor/'.$userId.'/',
                'allowed_types' => 'png|jpeg|jpg',
                'max_width' => '6000',
                'max_height' => '5000'
                );

            $this->load->library('upload', $config);
            $this->upload->initialize($config); 

            $check_uploadp = $this->upload->do_upload('profile_image');

            if($check_uploadp){
                $Arr = array("sp_prof_pic"=>$_FILES['profile_image']['name']);
                $cond = array("service_provider_id"=>$userId);
                $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

                if($result) {
                    $this->response(array("status" =>true,"message" => 'Your profile image updated successfully'), 200); exit;
                } else {
                    $this->response(array("status" => false,"message" => 'This image already as profile image.'), 200); exit;
                }
            } else {
             $this->response(array("status" => false, "message" => "Failed to upload profile imgae, invalid file format or size.")); exit;  
         }

     } else if( isset($_FILES['cover_image']['name'])) {

        $config=array('upload_path'=>'./uploads/tutor/'.$userId.'/',
            'allowed_types' => 'png|jpeg|jpg',
            'max_width' => '6000',
            'max_height' => '5000'
            );

        $this->load->library('upload', $config);
        $this->upload->initialize($config); 

        $check_uploadp = $this->upload->do_upload('cover_image');

        if($check_uploadp){
            $Arr = array("sp_cover_pic"=>$_FILES['cover_image']['name']);
            $cond = array("service_provider_id"=>$userId);
            $result = $this->common_model->update(TB_SERVICE_PROVIDER,$cond,$Arr);

            if($result) {
                $this->response(array("status" =>true,"message" => 'Your cover image updated successfully'), 200); exit;
            } else {
                $this->response(array("status" => false,"message" => 'This image already as cover image.'), 200); exit;
            }
        } else {
         $this->response(array("status" => false, "message" => "Failed to upload cover imgae, invalid file format or size.")); exit;  
     }

 } else {
     $this->response(array("status" => false, "message" => "Please upload profile or cover pic.")); exit; 
 }

} else {

    $this->response(array("status" => false,"message" => 'Please check your role id.'), 200); exit;
}

} else {
    $this->response(array("status" => false,"message" => 'User not exists.'), 200); exit;
}
}

}

}

    /**
     * Author : Kiran
     * Title : contact_sync
     * Description : parent contact no exist or not checked
     *
     * @param Integer contact_no
     * @return Array
     */

    public function contact_sync_post() 
    {
        $postData = $this->post();

        if(empty($postData['contact_no'][0])) {
            $this->response(array('status' => false, 'message' => 'Contact no is required.'), 200); exit;
        }
        $data = $postData['contact_no'];
        $contactNo = "";
        foreach ($data as $key => $value) {
         $contactNo .= $value.",";
     }
     $contactData = trim($contactNo,",");
        // Check contact no exist or not
     $resultContactList = $this->Common_model->select_in_cond("service_user_id, user_fullname, user_phone_number", TB_SERVICE_USERS, "user_phone_number", $contactData, array("user_status" => "1", "is_deleted" =>"0", "is_block" => "0"));        
     if(count($resultContactList) > 0) {
        $contactArr = array();
        $i=0;
        foreach ($resultContactList as $key => $value) {
            $contactArr[$i]['id'] = $value['service_user_id'];
            $contactArr[$i]['fullname'] = $value['user_fullname'];
            $contactArr[$i]['mobile_no'] = $value['user_phone_number'];                       
            $i++;
        } 
        $this->response(array('status' => true, 'message' => 'Contact sync found', "contact_list" => $contactArr ), 200); exit;
    } else {
        $this->response(array('status' => false, 'message' => 'Contact sync not found' ), 200); exit;    
    }            
}


    /**
    * Author : Priya
     * Title        : Edit driver or tutor Provider profile
     * Description  : Edit Provider profile
     * @param Integer provider_id
     * @param String fullname
     * @param String phone_number
     * @param String address
     * @param String gender
     * @param Date birth_date
     * @param Enum profile_status
     * @return message
     *   
     */
    public function editProviderProfile_post ()
    {
        // Post data
        $postData = $_POST;
        
        // Checked tutor id empty or not      
        if ($postData["provider_id"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter provider id.")); exit;
        } else if($postData["fullname"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter provider full name.")); exit;
        } else if($postData["phone_number"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter provider phone number.")); exit;
        } else if($postData["address"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter provider address.")); exit;
        } else if($postData["gender"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter provider gender.")); exit;
        } else if($postData["birth_date"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter provider birth date.")); exit;
        } else if($postData["profile_status"] == "" ) {            
            $this->response(array("status" => false, "message" => "Please enter provider profile status.")); exit;
        } else {
            $service_provider_id = $postData["provider_id"];

            $checkExists = $this->common_model->select("*", TB_SERVICE_PROVIDER, array("service_provider_id" => $service_provider_id));

            if ( !count($checkExists) ) {

                if($checkExists[0]['sp_user_type'] == 3) {

                    $this->response(array("status" => false, "message" => "Please enter correct driver id, this driver does not exist.")); exit;

                }else if($checkExists[0]['sp_user_type'] == 4) {
                    $this->response(array("status" => false, "message" => "Please enter correct tutor id, this tutor does not exist.")); exit;
                } else {
                    $this->response(array("status" => false, "message" => "Please enter correct provider id, this provider does not exist.")); exit;
                }                  

            } else {

                $profileUpdate = array( 'sp_fullname' => trim($postData["fullname"]),
                    'sp_phone_number' => trim($postData["phone_number"]),
                    'sp_address' => trim($postData["address"]),
                    'sp_gender' => trim($postData["gender"]),
                    'sp_birth_date' => trim($postData["birth_date"]),
                    'profile_status'=> trim($postData["profile_status"])
                    );

                $result = $this->common_model->update(TB_SERVICE_PROVIDER,array("service_provider_id"=>$service_provider_id),$profileUpdate);

                /*if ($result) {*/
                    $this->response(array("status" => true, "message" => 'Service provider profile updated successfully.'), 200); exit; 
                /*} else {
                    $this->response(array("status" => false, "message" => 'Failed to update service provider profile.'), 200); exit; 
                }*/
            }    
        }

    } 

    /**
     * Author : Kiran
     * Title : tutor_category_list
     * Description : tutor category list
     *
     * @return Array
     */

    public function tutor_category_list_get() 
    {
        // Get all tutor category
        $resultTutorCatList = $this->Common_model->select("tut_cat_id, category_name", TB_TUTOR_CATEGORY, array("status" => "1"));
        $tutorArr = array();
        // result
        if (count($resultTutorCatList) > 0) {            
            $i = 0;
            foreach ($resultTutorCatList as $key => $value) {
                $tutorArr[$i]['id'] = $value['tut_cat_id'];
                $tutorArr[$i]['category_name'] = $value['category_name'];                
                $i++;
            }
            $this->response(array('status' => true, 'message' => 'Data found', 'tutor_cat_list' => $tutorArr), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
        }    
    }

    /**
     * Author : Kiran
     * Title : tutor_subcategory_list
     * Description : tutor subcategory list
     *
     * @param Integer category_id
     * @return Array
     */

    public function tutor_subcategory_list_post() 
    {
        // Input data
        $postData = $this->post();

        // input validation
        if(empty(trim($postData['category_id']))) {
            $this->response(array('status' => false, 'message' => 'Category id is required.'), 200); exit;    
        }

        // Get all tutor subcategory
        $resultTutorSubCatList = $this->Common_model->select("tut_sub_cat_id, subcategory_name", TB_TUTOR_SUBCATEGORY, array("status" => "1", "tut_cat_id" => $postData['category_id']));
        $tutorArr = array();
        // result
        if (count($resultTutorSubCatList) > 0) {            
            $i = 0;
            foreach ($resultTutorSubCatList as $key => $value) {
                $tutorArr[$i]['id'] = $value['tut_sub_cat_id'];
                $tutorArr[$i]['subcategory_name'] = $value['subcategory_name'];                
                $i++;
            }
            $this->response(array('status' => true, 'message' => 'Data found', 'tutor_subcat_list' => $tutorArr), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
        }    
    }

    /**
     * Author : Kiran
     * Title : terms_list
     * Description : terms & condition list
     *
     * @param Integer role_id
     * @return Array
     */

    public function terms_list_post() 
    {
        // Input data
        $postData = $this->post();

        // input validation
        if(empty(trim($postData['role_id']))) {
            $this->response(array('status' => false, 'message' => 'Role id is required.'), 200); exit;    
        }

        // Get terms & condition information
        $resultTermsData = $this->Common_model->select("term_id as id, title, description", TB_TERMS, array("status" => "1", "role_id" => $postData['role_id']));
        if (count($resultTermsData) > 0) {                       
            $this->response(array('status' => true, 'message' => 'Data found', 'terms_list' => $resultTermsData), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
        }    
    }


    /**
     * Author : Kiran
     * Title : about_us_list
     * Description : about us list
     *
     * @return Array
     */

    public function about_us_list_get() 
    {
        // Get about us information
        $resultAboutUs = $this->Common_model->select("about_id, message", TB_ABOUT, array("status" => "1"));
        if (count($resultAboutUs) > 0) {                       
            $this->response(array('status' => true, 'message' => 'Data found', 'about_us_list' => $resultAboutUs), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
        }    
    }


/**
    * Author: Priya
     * Title : group members listing
     * @param Integer group_id
     * @return Array
     */
public function group_members_listing_post()
{
    $postData = $this->post();
    $all_members = '';
    if(empty(trim($postData['group_id'])))
    {
        $this->response(array("status"=>false, "message" => 'Group id is required.'), 200);
        exit;
    }

    $condG = array("group_id"=>trim($postData['group_id']),"group_status"=>"1");
    $getGrpAdmin = $this->common_model->select("*",TB_GROUP_MASTER,$condG);
    
    if(count($getGrpAdmin) != 0) {
        $serviceUserId = $getGrpAdmin[0]['service_user_id'];

        $condR = array("service_user_id"=>trim($serviceUserId));
        $getRole = $this->common_model->select("*",TB_SERVICE_USERS,$condR);

        if(count($getRole) != 0) {

            $userType = $getRole[0]['user_type_id'];

            if($userType == 2) {         // parent

                $acceptGroupMemData = [];
                $pendingGroupMemData = [];
                $rejectGroupMemData = [];
                
                $cond = array("group_id"=>trim($postData['group_id']),"status"=>"1");
                $getAllmemb = $this->common_model->select("*",TB_GROUP_MEMBER,$cond);
                

                if(!empty($getAllmemb))
                {
                    foreach ($getAllmemb as $keym => $valuem)
                    {
                        $all_members.= $valuem['passenger_id'].",";
                    }
                    $grpMembers = trim($all_members,",");

                    if(!empty($grpMembers))
                    {
                        //$acceptGroupMemData = $this->common_model->select_where_in_with_no_quote("passenger_id,pass_fullname",TB_PASSENGER,"passenger_id",$grpMembers);
                        $acceptGroupMemData = $this->common_model->select_where_in_with_no_quote("service_user_id,user_fullname",TB_SERVICE_USERS,"service_user_id",$grpMembers);
                        
                        //kid data
                        foreach ($acceptGroupMemData as $key=>$val) {
                           $condkid = array("service_user_id"=>trim($val['service_user_id']));
                           $kidData = $this->common_model->select("pass_fullname,passenger_id",TB_PASSENGER,$condkid);
                           $acceptGroupMemData[$key]['kid_data']=$kidData;
                        }
                    }
                }

                $condP = array("group_id"=>trim($postData['group_id']), "status"=>trim('request sent'));
                $pendingMem = $this->common_model->select("*",TB_TEMPORARY_GROUP,$condP);

                if(!empty($pendingMem)) {

                    foreach ($pendingMem as $keym => $valuem)
                    {
                        $pend_members.= $valuem['receiver_id'].",";
                    }
                    $pendMembers = trim($pend_members,",");

                    if(!empty($pendMembers))
                    {
                        $pendingGroupMemData = $this->common_model->select_where_in_with_no_quote("service_user_id,user_fullname",TB_SERVICE_USERS,"service_user_id",$pendMembers);
                    }
                }

                $condR = array("group_id"=>trim($postData['group_id']), "status"=>trim('reject'));
                $rejectMem = $this->common_model->select("*",TB_TEMPORARY_GROUP,$condR);

                if(!empty($rejectMem)) {

                    foreach ($rejectMem as $keyr => $valuer)
                    {
                        $reject_members.= $valuer['receiver_id'].",";
                    }
                    $rejeMembers = trim($reject_members,",");

                    if(!empty($rejeMembers))
                    {
                        $rejectGroupMemData = $this->common_model->select_where_in_with_no_quote("service_user_id,user_fullname",TB_SERVICE_USERS,"service_user_id",$rejeMembers);
                    }
                }
                

                $this->response(array("status"=>true, "grpMember" => $acceptGroupMemData, "pendMember" => $pendingGroupMemData, "rejectMember" => $rejectGroupMemData), 200);
                exit;


            } else if($userType == 5) {  // school


                //change code by kiran 
                //add code "status" => "1"
                $cond = array("group_id"=>trim($postData['group_id']), "status" => "1");
                $getAllmemb = $this->common_model->select("*",TB_GROUP_MEMBER,$cond);
                if(!empty($getAllmemb))
                {
                    foreach ($getAllmemb as $keym => $valuem)
                    {
                        $all_members.= $valuem['passenger_id'].",";
                    }
                    $grpMembers = trim($all_members,",");
                    if(!empty($grpMembers))
                    {
                        $result = $this->common_model->select_where_in_with_no_quote("passenger_id,pass_fullname",TB_PASSENGER,"passenger_id",$grpMembers);
                    }
                    $this->response(array("status"=>true, "grpMember" => $result), 200);
                    exit;
                }else
                {
                    $this->response(array("status"=>false, "message" => 'No members for this school group'), 200);
                    exit;
                }

            } else if($userType == 6) {  // organization
                //change code by kiran 
                //add code "status" => "1"
                $cond = array("group_id"=>trim($postData['group_id']), "status" => "1");
                $getAllmemb = $this->common_model->select("*",TB_GROUP_MEMBER,$cond);
                if(!empty($getAllmemb))
                {
                    foreach ($getAllmemb as $keym => $valuem)
                    {
                        $all_members.= $valuem['passenger_id'].",";
                    }
                    $grpMembers = trim($all_members,",");
                    if(!empty($grpMembers))
                    {
                        $result = $this->common_model->select_where_in_with_no_quote("passenger_id,pass_fullname",TB_PASSENGER,"passenger_id",$grpMembers);
                    }
                    $this->response(array("status"=>true, "grpMember" => $result), 200);
                    exit;
                }else
                {
                    $this->response(array("status"=>false, "message" => 'No members for this organization group'), 200);
                    exit;
                }

            } else {
                $this->response(array("status"=>false, "message" => 'Invalid user type'), 200);
                exit;
            }

        } else {
            $this->response(array("status"=>false, "message" => 'User not exists.'), 200);
            exit;
        }

    } else {
        $this->response(array("status"=>false, "message" => 'No group found.'), 200);
        exit;
    }

}


    /**
    * Author : Priya
     * Title        : Edit Tutor Education details
     * Description  : Edit Tutor Education details
     *   
     */
    public function editEduDetailsTut_post ()
    {
        // Post data
        $postData = $_POST;

        if ($postData["provider_id"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter provider id.")); exit;
        } else if ($postData["edu_details"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter education details.")); exit;
        } else if ($postData["cat_details"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter category details.")); exit;
        } else {

            // $someJSON = '[{"id":"3","value":"rtyyryr","type":"0"},{"id":"11","value":"2012-08-07","type":"0"}]';
            // education details
            // $eduDetails = json_decode($someJSON, true);
            $eduDetails = json_decode($postData["edu_details"], true);

            foreach ($eduDetails as $key1 => $value1) {
                $attriArr[]=array("service_provider_id"=>trim($postData['provider_id']),
                    "attribute_id"=>$value1['id'],
                    "attribute_value"=>$value1['value'],
                    "attribute_type"=>$value1['type'],
                    "created_at"=>date("Y-m-d H:i:s"),
                    "updated_at"=>date("Y-m-d H:i:s")
                    );
            }
            
            $resultEdu = $this->common_model->checkAttriAndInsert($attriArr);
            
            // $someCJSON = '[{"cat_id":"1","sub_id":"2","rate":"100"},{"cat_id":"1","sub_id":"1","rate":"120"}]';
            // category details
            // $catDetails = json_decode($someCJSON, true);
            $eduDetails = json_decode($postData["cat_details"], true);

            foreach ($catDetails as $key2 => $value2) {
                $catArr[]=array("service_provider_id"=>trim($postData['provider_id']),
                    "cat_id"=>$value2['cat_id'],
                    "sub_cat_id"=>$value2['sub_id'],
                    "created_at"=>date("Y-m-d H:i:s")
                    );
            }
            
            $resultCat = $this->common_model->checkCatAndInsert($catArr);

            if($resultEdu != 0 && $resultCat != 0) {

                $this->response(array("status" => true, "message" => 'Your details updated successfully.'), 200); exit; 
            } else {
                $this->response(array("status" => false, "message" => 'Failed to update your details.'), 200); exit; 
            }

        }
    }  

    /**
    * Author : Priya
     * Title        : Add/Edit Driver details
     * Description  : Add/Edit Driver details
     *   
     */
    public function editDetailsDriver_post ()
    {
        // Post data
        $postData = $_POST;

        if ($postData["provider_id"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter provider id.")); exit;
        } else if ($postData["edu_details"] == "" ) {
            $this->response(array("status" => false, "message" => "Please enter details.")); exit;
        } else {

            // $someJSON = '[{"id":"3","value":"rtyyryr","type":"0"},{"id":"11","value":"2012-08-07","type":"0"}]';
            // education details
            // $eduDetails = json_decode($someJSON, true);
            $eduDetails = json_decode($postData["edu_details"], true);

            foreach ($eduDetails as $key1 => $value1) {
                $attriArr[]=array("service_provider_id"=>trim($postData['provider_id']),
                    "attribute_id"=>$value1['id'],
                    "attribute_value"=>$value1['value'],
                    "attribute_type"=>$value1['type'],
                    "created_at"=>date("Y-m-d H:i:s"),
                    "updated_at"=>date("Y-m-d H:i:s")
                    );
            }
            
            $resultEdu = $this->common_model->checkAttriAndInsert($attriArr);
            
            if($resultEdu != 0) {

                $this->response(array("status" => true, "message" => 'Your details updated successfully.'), 200); exit; 
            } else {
                $this->response(array("status" => false, "message" => 'Failed to update your details.'), 200); exit; 
            }

        }
    }   


    /** Author : Priya
     * Title : GET CATEGORY LISTING
     * @return Array
     */
    public function catSubCatList_get()
    {

        $catData = $this->common_model->select("tut_cat_id, category_name", TB_TUTOR_CATEGORY, array("status" => 1));

        if(count($catData) != 0) {

            foreach ($catData as $key => $value) {

                $catData[$key]['subList'] = $this->common_model->select("tut_sub_cat_id, subcategory_name, rate_per_hour, tut_cat_id", TB_TUTOR_SUBCATEGORY, array("status" => 1, "tut_cat_id" => $value['tut_cat_id']));
            }

            $this->response(array("status" => true,"message" => 'Category list', "catList" => $catData), 200); exit;
        }else {
            $this->response(array("status" => false,"message" => 'No category found.'), 200); exit;
        }
    }


    /** Author : Priya
     * Title : GET DRIVER CAR LIST
     * @return Array
     */
/*public function driver_chart_list_get()
{

    $vehicleData = $this->common_model->select("vehicle_category_id, vehicle_category_name, description, seating_capacity, rate_per_km", TB_VEHICLE_CAT, array("status" => 1));

    if(count($vehicleData) != 0) {

        $this->response(array("status" => true,"message" => 'vehicles', "vehicleList" => $vehicleData), 200); exit;
    }else {
        $this->response(array("status" => false,"message" => 'No vehicles found.'), 200); exit;
    }
}*/


// -------- Ashwini code start --------------------
/**
    * Ashwini code
     * Title : Save booking
     * @param json data
     * @return Array
     */

public function save_booking_post()
{
    $data = file_get_contents('php://input');
    $new_data = json_decode($data,true);
    $ride_flag = 0;
    $care_flag = 0;
    $tutor_flag = 0;
    $tutor_req_flag=0;
    
    if(!isset($new_data) || empty($new_data))
    {
        $this->response(array("status"=>false,"message"=>"Please give booking data."),200);
        exit;
    }
    foreach ($new_data as $keyb => $valueb)
    {
        //------ validations ---------
        if(empty($valueb['parent_id']))
        {
            $this->response(array("status"=>false,"message"=>"Please give parent id."),200);
            exit;  
        }

        $getParentLoc = $this->common_model->select("user_address_latlong",TB_SERVICE_USERS,array("service_user_id"=>$valueb['parent_id']));
        $parent_latlong = explode(",",$getParentLoc[0]['user_address_latlong']);
        $parent_lat = $parent_latlong[0];
        $parent_long = $parent_latlong[1];

        //----- validation for care data -----
        if(!empty($valueb['care_data']))
        {

        }
        //----- validation for tutor data -----
        if(!empty($valueb['tutor_data']))
        {
            $total_cost = 0;
            foreach ($valueb['tutor_data'] as $keyt => $valuet)
            {
                if(empty($valuet['child_data']))
                {
                    $this->response(array("status"=>false,"message"=>"Please give child data."),200);
                    exit;
                }
                if(empty($valuet['subject_data']))
                {
                    $this->response(array("status"=>false,"message"=>"Please give subject data."),200);
                    exit;
                }
                if(empty($valuet['start_date']))
                {
                    $this->response(array("status"=>false,"message"=>"Please give Start date for tutor."),200);
                    exit;
                }
                if(empty($valuet['end_date']))
                {
                    $this->response(array("status"=>false,"message"=>"Please give End date for tutor."),200);
                    exit;
                }
                if($valuet['parent_location'] == "")
                {
                    $this->response(array("status"=>false,"message"=>"Please give parent location."),200);
                    exit;
                }
                if($valuet['tutor_location'] == "")
                {
                    $this->response(array("status"=>false,"message"=>"Please give tutor location."),200);
                    exit;
                }
            }
            $tutor_flag = 1;
        }//tutor validation

        //---- actual insertion for booking -----
        $book_arr = array(
         "service_user_id"=>$valueb['parent_id'],
         "booking_number"=>mt_rand(100000, 999999),
         "booking_date"=>date("Y-m-d"),
         "booking_status" => "1",
         "created_date"=>date("Y-m-d H:i:s")
         );
        $book_result = $this->common_model->insert(TB_BOOKING,$book_arr);
        if($book_result)
        {
            //---- Tutor data insert ----
            if($tutor_flag == 1)
            {                
                foreach ($valueb['tutor_data'] as $keyt => $valuet)
                {
                    for($j=0;$j<count($valuet['subject_data']);$j++)
                    {
                        //----  get subject rate -- 
                        $sub_rate = $this->getSubjectRate($valuet['subject_data'][$j]['subject_id']);

                        $datetime1 = new DateTime($valuet['subject_data'][$j]['start_time']);
                        $datetime2 = new DateTime($valuet['subject_data'][$j]['end_time']);
                        $interval = $datetime1->diff($datetime2);
                        $time_diff = $interval->format('%H').":".$interval->format('%i');

                        $minutes_int = ($interval->format('%i'))/60;
                        $total_hrs = $interval->format('%H')+$minutes_int;
                        $est_cost = round($total_hrs * $sub_rate,2);

                        $datediff = strtotime($valuet['end_date']) - strtotime($valuet['start_date']);
                        $no_of_days = (round($datediff / (60 * 60 * 24)))+1;

                        $total_est_cost = $no_of_days*$est_cost;

                        $child_cnt = count($valuet['child_data']);
                        $final_est_cost = $total_est_cost*$child_cnt;

                        $tut_arr = array(
                            "booking_id"=>$book_result,
                            "cat_id"=>$valuet['subject_data'][$j]['cat_id'],
                            "subcat_id"=>$valuet['subject_data'][$j]['subject_id'],
                            "tut_start_date"=>$valuet['start_date'],
                            "tut_end_date"=>$valuet['end_date'],
                            "parent_location"=>$valuet['parent_location'],
                            "tutor_location"=>$valuet['tutor_location'],
                            "start_time"=>$valuet['subject_data'][$j]['start_time'],
                            "end_time"=>$valuet['subject_data'][$j]['end_time'],
                            "estimated_time"=>$time_diff,
                            "estimated_cost"=>$final_est_cost,
                            "tution_comment"=>$valuet['comment'],
                            "created_date"=>date("Y-m-d H:i:s")
                            );
                        $tutRes = $this->common_model->insert(TB_TUTOR_BOOKING,$tut_arr);

                        $sub_booking_arr[] = array(
                            "service_id"=>$tutRes,
                            "child_data"=>$valuet['child_data'],
                            "start_date"=>$valuet['start_date'],
                            "end_date"=>$valuet['end_date']
                            );

                        $tut_loc_arr[] = array(
                            "booking_id"=>$book_result,
                            "tutor_booking_id"=>$tutRes,
                            "tut_start_date"=>$valuet['start_date'],
                            "tut_end_date"=>$valuet['end_date'],
                            "parent_location"=>$valuet['parent_location'],
                            "tutor_location"=>$valuet['tutor_location'],
                            "cat_id"=>$valuet['subject_data'][$j]['cat_id'],
                            "subcat_id"=>$valuet['subject_data'][$j]['subject_id'],
                            );
                        $total_cost+=$final_est_cost;
                    }
                }
                foreach ($sub_booking_arr as $keysb => $valuesb)
                {
                    for($dt=$valuesb['start_date'];$dt<=$valuesb['end_date'];$dt++)
                    {
                        for($ch=0;$ch<count($valuesb['child_data']);$ch++)
                        {
                            $sub_arr[] = array(
                                "booking_id"=>$book_result,
                                "service_id"=>"3",
                                "service_booking_id"=>$valuesb['service_id'],
                                "pass_id"=>$valuesb['child_data'][$ch]['child_id'],
                                "sub_booking_date"=>$dt,
                                "sub_book_status"=>"1",
                                "created_at"=>date("Y-m-d H:i:s")
                                );  
                        }
                        
                    }
                }

                if(!empty($sub_arr))
                {
                    $result = $this->common_model->insert_batch(TB_BOOKING_DETAILS,$sub_arr);
                }                

            }//tutor flag

        }//book result

    }//foreach    

    //----- send booking request to matching tutor -----
    if(!empty($tut_loc_arr))
    {
        foreach ($tut_loc_arr as $key1 => $value1)
        {
            //-------- get available tutors --------
            $cond = "unavail_date BETWEEN '".$value1['tut_start_date']."' AND '".$value1['tut_end_date']."'";
            $getUnavailsp = $this->common_model->select_simple("GROUP_CONCAT(DISTINCT(service_provider_id)) as sp_id",TB_SP_UNAVAILABILITY,$cond);
            $unavail_sp = $getUnavailsp[0]['sp_id'];
            
            if(!empty($unavail_sp))
            {
                $sp_cond = " AND ".TB_PROVIDER_PRICE.".service_provider_id NOT IN (".$unavail_sp.")";
            }else
            {
                $sp_cond = "";
            }

            // ----- check location wise -----
            if($value1['parent_location'] == "1" && $value1['tutor_location'] == "0")
            {
                $attr_value = "1";
            }
            else if($value1['tutor_location'] == "1" && $value1['parent_location'] == "0")
            {
                $attr_value = "2";
            }
            else if($value1['parent_location'] == "1" && $value1['tutor_location'] == "1")
            {
                $attr_value = "1,2";
            }

            if($attr_value != "")
            {
                $loc_cond = " AND ".TB_ATTRIBUTE_VALUES.".attribute_id= '18' AND attribute_value IN (".$attr_value.")";
            }else
            {
                $loc_cond = "";
            }

            // ----- get matching tutors -------
            $cond = TB_SERVICE_PROVIDER.".sp_user_type='4' AND ".
            TB_PROVIDER_PRICE.".cat_id = '".$value1['cat_id'] ."' AND ".
            TB_PROVIDER_PRICE.".sub_cat_id = '".$value1['subcat_id']."' AND ".
            TB_PROVIDER_PRICE.".status='1'". $sp_cond. $loc_cond;

            $jointype  = array(TB_PROVIDER_PRICE => "LEFT",TB_ATTRIBUTE_VALUES=>"LEFT");

            $join = array(TB_PROVIDER_PRICE => TB_PROVIDER_PRICE.".service_provider_id = ".TB_SERVICE_PROVIDER.".service_provider_id",
                TB_ATTRIBUTE_VALUES=>TB_ATTRIBUTE_VALUES.".service_provider_id=".TB_SERVICE_PROVIDER.".service_provider_id");

            $matching_tutor = $this->common_model->getMaster("DISTINCT(".TB_SERVICE_PROVIDER.".service_provider_id),sp_address_latlong", TB_SERVICE_PROVIDER, $cond, array(), $join, $jointype);
             // echo $this->db->last_query()."<br><br>";
            if(!empty($matching_tutor))
            {

                foreach ($matching_tutor as $keymt => $valuemt)
                {
                    $tut_latlong = explode(",",$valuemt['sp_address_latlong']);
                    $tut_lat = $tut_latlong[0];
                    $tut_long = $tut_latlong[1];
                    $milesLoc = round($this->distance($parent_lat, $parent_long, $tut_lat, $tut_long,"M"));
                    if($milesLoc <= 25 && $milesLoc >= 0)
                    {
                        $tut_book_req_arr[] = array(
                            "booking_id"=>$value1['booking_id'],
                            "service_id"=>"3",
                            "service_booking_id"=>$value1['tutor_booking_id'],
                            "service_provider_id"=>$valuemt['service_provider_id'],
                            "book_request_status"=>"request sent",
                            "created_date"=>date("Y-m-d H:i:s")
                            );
                    }
                }                
            }
        }
        if(!empty($tut_book_req_arr))
        {
            $tutor_booking_res = $this->common_model->insert_batch(TB_BOOKING_REQUEST,$tut_book_req_arr);       
        }       
    }// tut_loc_arr end

    $this->response(array("status"=>true,"message"=>"Ride booked successfully. Total estimated cost is $$total_cost."));
    exit;
}

public function getSubjectRate($subject_id)
{
    $getRate = $this->common_model->select("rate_per_hour",TB_TUTOR_SUBCATEGORY,array("tut_sub_cat_id"=>$subject_id));
    return $getRate[0]['rate_per_hour'];
}


/**
    * Ashwini code
     * Title       : get new booking requests for tutor  
     * @param user_id
     * @return Array 
     */
public function getServiceProvRequest_post()
{
    $postData = $this->post();
    if(empty(trim($postData['user_id'])))
    {
        $this->response(array("status"=>false,"message"=>"Please give user id."));
        exit;
    }

    $getType = $this->common_model->select("sp_user_type",TB_SERVICE_PROVIDER,array("service_provider_id"=>trim($postData['user_id'])));
    $user_type = $getType[0]['sp_user_type'];
    $new_data = array();
    if($user_type == "4") //Tutor
    {        
        $booking_request = $this->commonParentData(trim($postData['user_id']));
        
        foreach($booking_request as $keybr => $request)
        {
            $booking_id = $request['booking_id'];
            $booking_details=$this->getBookingData($postData['user_id'],$booking_id);
            if(!empty($booking_details))
            {
                foreach ($booking_details as $keybd => $valuebd)
                {
                    $request['booking_data'][$keybd]['booking_request_id'] = $valuebd['req_id'];
                    $request['booking_data'][$keybd]['tutor_booking_id'] = $valuebd['tutor_booking_id'];
                    $request['booking_data'][$keybd]['category'] = $valuebd['category_name'];
                    $request['booking_data'][$keybd]['subcategory'] = $valuebd['subcategory_name'];
                    $request['booking_data'][$keybd]['start_time'] = $valuebd['start_time'];
                    $request['booking_data'][$keybd]['end_time'] = $valuebd['end_time'];
                    $request['booking_data'][$keybd]['duration'] = $valuebd['estimated_time'];
                    $request['booking_data'][$keybd]['earnings'] = "$".$valuebd['estimated_cost'];
                    $getPass = $this->getBookingPassenger($valuebd['tutor_booking_id']);
                    $request['booking_data'][$keybd]['passengerData'] = $getPass;
                }
                $new_data[]=$request;
            }
        }
    }else if($user_type == "3") //Care Driver
    {

    }
    if(!empty($new_data))
    {
        $this->response(array("status"=>true,"message"=>"Booking request found.","booking_request"=>$new_data));
        exit;
    }else
    {
        $this->response(array("status"=>false,"message"=>"No data found."));
        exit;
    }

}


// ------ Subfunction : common function to fetch booking parent data --------
public function commonParentData($user_id,$booking_id="")
{
    if(!empty($booking_id))
    {
        $cond = array("service_provider_id"=>$user_id,"book_request_status"=>"request sent",TB_BOOKING_REQUEST.".booking_id"=>$booking_id);
    }else
    {
        $cond = array("service_provider_id"=>$user_id,"book_request_status"=>"request sent");
    }    
    $jointype = array(TB_BOOKING => "LEFT",TB_SERVICE_USERS=>"LEFT");
    $join = array(
        TB_BOOKING => TB_BOOKING.".booking_id = ".TB_BOOKING_REQUEST.".booking_id",
        TB_SERVICE_USERS => TB_SERVICE_USERS.".service_user_id = ".TB_BOOKING.".service_user_id",
        TB_TUTOR_BOOKING => TB_TUTOR_BOOKING.".booking_id = ".TB_BOOKING.".booking_id",
        );
    $group_by = TB_BOOKING.".booking_id";
    $booking_request = $this->Common_model->selectJoin(TB_SERVICE_USERS.".service_user_id,user_fullname,user_prof_pic,".TB_BOOKING.".booking_id,booking_number,".TB_BOOKING.".created_date as booking_created_date,tut_start_date as start_date,tut_end_date as end_date", TB_BOOKING_REQUEST, $cond, array(), $join, $jointype,$group_by);
    /*echo $this->db->last_query();die;
    echo "<pre>";print_r($booking_request);die;*/
    foreach ($booking_request as $keybr => $valuebr)
    {

        $booking_request[$keybr]['profile_pic'] = base_url().'uploads/serviceUser/'.$valuebr['service_user_id'].'/'.$valuebr['user_prof_pic'];
    }
    return $booking_request;
}


/**
    * Ashwini code
     * Title       : view booking details for new request in tutor 
     * @param booking_id
     * @param user_id
     * @return Array 
     */

public function getSpBookingDetails_post()
{
    $postData = $this->post();
    $book_data = array();
    if(empty(trim($postData['booking_id'])))
    {
        $this->response(array("status"=>false,"message"=>"Please give booking id."));
        exit;
    }
    if(empty(trim($postData['user_id'])))
    {
        $this->response(array("status"=>false,"message"=>"Please give user id."));
        exit;
    }

    $getType = $this->common_model->select("sp_user_type",TB_SERVICE_PROVIDER,array("service_provider_id"=>trim($postData['user_id'])));
    $user_type = $getType[0]['sp_user_type'];

    if($user_type == "4") //Tutor
    {
        $parent_data = $this->commonParentData(trim($postData['user_id']),trim($postData['booking_id']));
        $book_data['parent_data'] = $parent_data[0];

        /*$cond = array(
            TB_BOOKING_REQUEST.".service_provider_id"=>trim($postData['user_id']),
            TB_BOOKING_REQUEST.".booking_id"=>trim($postData['booking_id']),
            TB_BOOKING_REQUEST.".service_id"=>"3");
        $jointype = array(TB_TUTOR_BOOKING=>"INNER",TB_TUTOR_CATEGORY=>"LEFT",TB_TUTOR_SUBCATEGORY=>"LEFT");
        $join = array(
            TB_TUTOR_BOOKING=>TB_TUTOR_BOOKING.".tutor_booking_id=".TB_BOOKING_REQUEST.".service_booking_id",
            TB_TUTOR_CATEGORY=>TB_TUTOR_CATEGORY.".tut_cat_id=".TB_TUTOR_BOOKING.".cat_id",
            TB_TUTOR_SUBCATEGORY=>TB_TUTOR_SUBCATEGORY.".tut_sub_cat_id=".TB_TUTOR_BOOKING.".subcat_id",
            );
        $group_by = "";
        $booking_details = $this->Common_model->selectJoin("*", TB_BOOKING_REQUEST, $cond, array(), $join, $jointype,$group_by);*/
        $booking_details=$this->getBookingData($postData['user_id'],$postData['booking_id']);
        if(!empty($booking_details))
        {
            foreach ($booking_details as $keybd => $valuebd)
            {
                $book_data['booking_data'][$keybd]['booking_request_id'] = $valuebd['req_id'];
                $book_data['booking_data'][$keybd]['tutor_booking_id'] = $valuebd['tutor_booking_id'];
                $book_data['booking_data'][$keybd]['category'] = $valuebd['category_name'];
                $book_data['booking_data'][$keybd]['subcategory'] = $valuebd['subcategory_name'];
                $book_data['booking_data'][$keybd]['start_time'] = $valuebd['start_time'];
                $book_data['booking_data'][$keybd]['end_time'] = $valuebd['end_time'];
                $book_data['booking_data'][$keybd]['duration'] = $valuebd['estimated_time'];
                $book_data['booking_data'][$keybd]['earnings'] = "$".$valuebd['estimated_cost'];
                $getPass = $this->getBookingPassenger($valuebd['tutor_booking_id']);
                $book_data['booking_data'][$keybd]['passengerData'] = $getPass;
            }
        }

    }else if($user_type == "3") //Care Driver
    {

    }

    if(!empty($book_data))
    {
       $this->response(array("status"=>true,"message"=>"Booking details found.","booking_details"=>$book_data));
       exit; 
   }else
   {
    $this->response(array("status"=>false,"message"=>"Booking details not found."));
    exit;
}
}

//----- Subfunction : get passenger data to show in booking details in provider -----
public function getBookingPassenger($tutor_booking_id)
{
    $group_by = TB_BOOKING_DETAILS.".service_booking_id";
    $getPass = $this->common_model->selectJoin("GROUP_CONCAT(DISTINCT(pass_id)) as passenger_id",TB_BOOKING_DETAILS,array("service_booking_id"=>$tutor_booking_id),$order_by=array(),$join=array(),$joinType=array(),$group_by);

    $getPassData = $this->common_model->select_where_in_with_no_quote("passenger_id,pass_fullname,pass_standard",TB_PASSENGER,"passenger_id",$getPass[0]['passenger_id']);
    return $getPassData;
}


public function getBookingData($user_id,$booking_id){
   $cond = array(
        TB_BOOKING_REQUEST.".service_provider_id"=>trim($user_id),
        TB_BOOKING_REQUEST.".booking_id"=>trim($booking_id),
        TB_BOOKING_REQUEST.".service_id"=>"3");
    $jointype = array(TB_TUTOR_BOOKING=>"INNER",TB_TUTOR_CATEGORY=>"LEFT",TB_TUTOR_SUBCATEGORY=>"LEFT");
    $join = array(
        TB_TUTOR_BOOKING=>TB_TUTOR_BOOKING.".tutor_booking_id=".TB_BOOKING_REQUEST.".service_booking_id",
        TB_TUTOR_CATEGORY=>TB_TUTOR_CATEGORY.".tut_cat_id=".TB_TUTOR_BOOKING.".cat_id",
        TB_TUTOR_SUBCATEGORY=>TB_TUTOR_SUBCATEGORY.".tut_sub_cat_id=".TB_TUTOR_BOOKING.".subcat_id",
        );
    $group_by = "";
    $booking_details = $this->Common_model->selectJoin("*", TB_BOOKING_REQUEST, $cond, array(), $join, $jointype,$group_by);
    return $booking_details;
}

//----- accept or reject booking request in provider -----
public function changeBookingReqStatus_post()
{
    $postData = $this->post();
    if(empty(trim($postData['user_id'])))
    {
        $this->response(array("status"=>false,"message"=>"Please enter user id."));
        exit;
    }
    if(empty(trim($postData['booking_request_id'])))
    {
        $this->response(array("status"=>false,"message"=>"Please enter booking request id."));
        exit;
    }
}

// -------- Ashwini code end --------------------


    /** Author : Priya
     * Title : GET DRIVER CAR LIST
     * @return Array
     */
    public function driver_chart_list_get()
    {

        $vehicleData = $this->common_model->select("vehicle_category_id, vehicle_category_name, description, seating_capacity, rate_per_km", TB_VEHICLE_CAT, array("status" => 1));

        if(count($vehicleData) != 0) {

            $this->response(array("status" => true,"message" => 'vehicles', "vehicleList" => $vehicleData), 200); exit;
        }else {
            $this->response(array("status" => false,"message" => 'No vehicles found.'), 200); exit;
        }
    }


    /** Author : Priya
     * Title : SET SERVICE PROVIDERS UNAVAILABILITY / AVAILABILITY 
     * @param : service_provider_id
     * @param : unavail_date
     * @param : flag (0 - make sp available, 1 - make sp unavailable)
     * @return message
     */
    public function set_provider_availability_post()
    {

        $postData = $this->post();
        if(empty($postData['service_provider_id']))
        {
            $this->response(array("status" => false,"message" => 'Service provider id is required.'), 200); exit;
        } else if(empty($postData['unavail_date']))
        {
            $this->response(array("status" => false,"message" => 'date is required.'), 200); exit;
        } else if($postData['flag'] == '')
        {
            $this->response(array("status" => false,"message" => 'flag is required.'), 200); exit;
        } else 
        {
            $spId = $postData['service_provider_id'];
            $allDates = $postData['unavail_date'];
            $flag = $postData['flag'];
            $dateArray = explode(',', $allDates);

            if($flag == 0) { // make available

                foreach ($dateArray as $key => $valueA) {

                    $checkExistsA = $this->common_model->select("*", TB_SP_UNAVAILABILITY, array("unavail_date" => $valueA, "service_provider_id" => $spId));

                    if(count($checkExistsA) != 0) {

                        $cond = array("unavail_date" => $valueA,"service_provider_id" => $spId);
                        $result = $this->common_model->delete(TB_SP_UNAVAILABILITY,$cond);
                    }
                }

                $this->response(array("status" => true,"message" => 'Your availibility updated successfully '), 200); exit;

            } else {   // make unavailable

                foreach ($dateArray as $key => $value) {

                    $dateData = array('service_provider_id' => $spId,
                      'unavail_date' => $value,
                      'created_date' => date("Y-m-d H:i:s")
                                      // 'updated_date' => date("Y-m-d H:i:s")
                      );

                    $checkExists = $this->common_model->select("*", TB_SP_UNAVAILABILITY, array("unavail_date" => $value, "service_provider_id" => $spId));

                    if(count($checkExists) == 0) {

                        $resultData = $this->common_model->insert(TB_SP_UNAVAILABILITY,$dateData);
                    }
                }

                $this->response(array("status" => true,"message" => 'Your unavailibility updated successfully '), 200); exit;
            }

        }

    }


    /** Author : Priya
     * Title : GET SERVICE PROVIDERS CALENDER DATES
     * @param : service_provider_id
     * @return array
     */
    /*public function get_provider_calender_post()
    {

        $postData = $this->post();
        if(empty($postData['service_provider_id']))
        {
            $this->response(array("status" => false,"message" => 'Service provider id is required.'), 200); exit;
        } else {

            $spId = $postData['service_provider_id'];
            
            $checkExists = $this->common_model->select("unavail_date as dateVal", TB_SP_UNAVAILABILITY, array("service_provider_id" => $spId));

            if(count($checkExists) != 0) {

                $this->response(array("status" => true,"message" => 'unavailble data.', "unavailble" => $checkExists ), 200); exit;
            } else {

                $this->response(array("status" => false,"message" => 'No unavailble date data found.'), 200); exit;
            }
        }
    }*/

    /**
      Author:Priya
     * Title       : All ridong videos list
     * Description : Ride Videos
     * @param Integer service_provider_id
     * @return Array
     */

    public function allVideos_post ()
    {       
        $postData = $this->post();
        if(empty($postData['service_provider_id']))
        {
            $this->response(array("status" => false,"message" => 'Service provider id is required.'), 200); exit;
        } else {

            $spId = $postData['service_provider_id'];
            
            $getAll = $this->common_model->select("*", TB_VIDEO_STREAM, array("service_provider_id" => $spId, "status" => 1, "is_deleted" => 0));

            if(count($getAll) != 0) {

                $this->response(array("status" => true,"message" => 'video list.', "allvideos" => $getAll ), 200); exit;
            } else {

                $this->response(array("status" => false,"message" => 'No video found.'), 200); exit;
            }
        }      
    }     

    /**
      Author:Priya
     * Title       : GET RATING LIST 
     * Description : GET RATING LIST ACCORDING TO ROLE ID
     * @param Integer role_id
     * @return Array
     */

    public function getRating_post ()
    {       
        $postData = $this->post();
        if(empty($postData['role_id']))
        {
            $this->response(array("status" => false,"message" => 'Role id is required.'), 200); exit;
        } else {

            $roleId = $postData['role_id'];
            
            $getAll = $this->common_model->select("rtype_id, rating_type", TB_RATE_TYPE, array("role_id" => $roleId, "status" => 1));

            if(count($getAll) != 0) {

                $this->response(array("status" => true,"message" => 'rating type list', "rateTypeList" => $getAll ), 200); exit;
            } else {

                $this->response(array("status" => false,"message" => 'No rate type found for this role.'), 200); exit;
            }
        }      
    } 


    /**
      Author:Priya
     * Title       : GIVE RATING
     * Description : GIVE RATING STAR
     * @param Integer rate_from
     * @param Integer rate_to
     * @param Integer book_detailId
     * @param Integer rate_type_id (comma separte rate type id)
     * @param Integer rating (comma separte rating star)
     * @return Array
     */

    public function setUserRating_post ()
    {       
        $postData = $this->post();

        if(empty($postData['rate_from'])) {
            $this->response(array("status" => false,"message" => 'Rate from id is required.'), 200); exit;
        } else if(empty($postData['rate_to'])) {
            $this->response(array("status" => false,"message" => 'Rate to id is required.'), 200); exit;
        } else if(empty($postData['book_detailId'])) {
            $this->response(array("status" => false,"message" => 'Book detail id is required.'), 200); exit;
        } else if($postData['rate_type_id'] == '') {
            $this->response(array("status" => false,"message" => 'Rate type id is required.'), 200); exit;
        } else if($postData['rating'] == '') {
            $this->response(array("status" => false,"message" => 'Rating is required.'), 200); exit;
        }
         else {

            $checkExists = $this->common_model->select("*", TB_RATE_MASTER, array("rating_from" => $postData['rate_from'], "rating_to" => $postData['rate_to'], "book_det_id" => $postData['book_detailId']));

            if(count($checkExists) == 0) {

                $rateFrom = $postData['rate_from'];
                $rateTo   = $postData['rate_to'];
                $detailId = $postData['book_detailId'];
                $rateIdArray   = explode(",",$postData['rate_type_id']);
                $ratingArray   = explode(",",$postData['rating']);

                $totalLength = count($rateIdArray);
                $sum = array_sum($ratingArray);
                $avgRating = round($sum / $totalLength, 2);

                $mainBookRate = array('book_det_id' => $detailId,
                                      'rating_from' => $rateFrom,
                                      'rating_to'   => $rateTo,
                                      'total_rating' => $avgRating,
                                      'status'      => 1,
                                      'created_date' => date('Y-m-d H:i:s')
                                );
                
                $insertMainRate = $this->common_model->insert(TB_RATE_MASTER,$mainBookRate);

                if($insertMainRate) {

                    foreach (array_combine($rateIdArray, $ratingArray) as $id => $star) {

                        $rateDetails = array('rating_id' => $insertMainRate,
                                                'rtype_id' => $id,
                                                'rating' => $star,
                                                'status' => 1,
                                                'created_at' => date('Y-m-d H:i:s')
                                            );

                        $addDetails = $this->common_model->insert(TB_RATE_DETAILS,$rateDetails);
                    }

                    $this->response(array("status" => true,"message" => 'Rate successfully.'), 200); exit;

                } else {
                    $this->response(array("status" => false,"message" => 'Failed to give average rating.'), 200); exit;
                }
  
            } else {
                $this->response(array("status" => false,"message" => 'Already give rating.'), 200); exit;
            }

            
        }      
    }

    /**
      Author:Priya
     * Title       : GET USER TYPE
     * Description : GET USER TYPE LIST 
     * @param Integer app_id
     * @return Array
     */

    public function getuserType_post ()
    {       
        $postData = $this->post();
        if($postData['app_id'] == '')
        {
            $this->response(array("status" => false,"message" => 'App id is required.'), 200); exit;
        } else {

            $appId = $postData['app_id'];
            if($appId == 0) {

                $getAll = $this->common_model->select_in("*", TB_ROLES, 'role_id', array(2,5,6));
                if(count($getAll) != 0) {

                    $this->response(array("status" => true,"message" => 'user type list', "userType" => $getAll ), 200); exit;
                } else {

                    $this->response(array("status" => false,"message" => 'No user type list found.'), 200); exit;
                }

            } else if($appId == 1) {
                $getAll = $this->common_model->select_in("*", TB_ROLES, 'role_id', array(3,4));

                if(count($getAll) != 0) {

                    $this->response(array("status" => true,"message" => 'user type list', "userType" => $getAll ), 200); exit;
                } else {

                    $this->response(array("status" => false,"message" => 'No user type list found.'), 200); exit;
                }
            } else {
                $this->response(array("status" => false,"message" => 'Enter valid app id.'), 200); exit;
            } 
        }
    }   

   /**
      Author:Priya
     * Title       : GET NOTIFICATION LIST
     * Description : GET NOTIFICATION LIST BY USER ID 
     * @param Integer user_id
     * @param Integer app_id
     * @param Integer pageCount
     * @return Array
     */

    public function getNotifications_post ()
    {    
        $postData = $this->post();
        if(empty($postData['user_id']))
        {
            $this->response(array("status" => false,"message" => 'User id is required.'), 200); exit;
        } else if($postData['app_id'] == '')
        {
            $this->response(array("status" => false,"message" => 'App is required.'), 200); exit;
        } else if(empty($postData['pageCount']))
        {
            $this->response(array("status" => false,"message" => 'Page count is required.'), 200); exit;
        } else {

            $userId = $postData['user_id'];
            $appId = $postData['app_id'];
            $pageCount = $postData['pageCount'];
            $getAll = $this->common_model->fetch_no_of_records_order_by("*", TB_NOTIFICATION, array('user_id' => $userId, 'app_id' => $appId ),$pageCount,'0','notification_id','DESC' );
            
            if($getAll) {
                $count = count($getAll);

                $this->response(array("status" => true,"message" => 'notification list', "data" => $getAll, "total_notificationCount" => $count ), 200); exit;
            } else {

                $this->response(array("status" => false,"message" => 'No notification list found.'), 200); exit;
            }
        }
    } 

    /**
     * Author : Priya
     * Title : delete passenger
     * Description : delete multiple passenger
     * @param multiple comma separted passenger_id
     * @return Array
     */

    public function deletePassenger_post()
    {
        $postData = $this->post();
        if($postData['passenger_id'] == '')
        {
            $this->response(array("status"=>false, "message" => 'passenger id is required.'), 200);
            exit;
        } else {

            $passangerArray = $postData['passenger_id'];
            $result = $this->db->query("UPDATE ".TB_PASSENGER." SET is_deleted = '1' WHERE passenger_id IN (".$passangerArray.") ");

            if ($result) {
                $this->response(array("status" => true, "message" => "Passenger has been deleted successfully."), 200); exit; 
            } else {
                $this->response(array("status" => false, "message" => 'passenger id not exists'), 200); exit;
            } 

        }
    }


   /**
     * Author : Priya
     * Title : CREATE TICKET
     * Description : CREATE TICKET TO RAISE ISSUE
     * @param booking_id
     * @param book_det_id
     * @param ticket_from
     * @param ticket_to
     * @param subject
     * @param service
     * @param attach_file
     * @param comment
     * @return message
     */

    public function createTicket_post()
    {
        $postData = $this->post();
        if($postData['ticket_from'] == '') {

            $this->response(array("status"=>false, "message" => 'ticket from is required.'), 200);
            exit;
        } else if($postData['ticket_to'] == '') {

            $this->response(array("status"=>false, "message" => 'ticket to is required.'), 200);
            exit;
        } else if($postData['booking_id'] == '') {

            $this->response(array("status"=>false, "message" => 'booking id is required.'), 200);
            exit;
        } else if($postData['book_det_id'] == '') {

            $this->response(array("status"=>false, "message" => 'booking detail id is required.'), 200);
            exit;
        } else if($postData['subject'] == '') {

            $this->response(array("status"=>false, "message" => 'subject is required.'), 200);
            exit;
        } else if($postData['service'] == '') {

            $this->response(array("status"=>false, "message" => 'service is required.'), 200);
            exit;
        } else if($postData['comment'] == '') {

            $this->response(array("status"=>false, "message" => 'comment is required.'), 200);
            exit;
        }  else {

            $checkExists = $this->common_model->select("*", TB_TICKET, array("booking_id" => $postData['booking_id'], "book_det_id" => $postData['book_det_id'], "ticket_from" => $postData['ticket_from'], "ticket_to" => $postData['ticket_to']));
            

            if(count($checkExists) == 0) {

                if (isset($_FILES['attach_file']['name']) && !empty($_FILES['attach_file']) ) {

                    $config =array('upload_path' => './uploads/ticket/',
                                'allowed_types' => 'png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                               );

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config); 
                    $check_uploadp = $this->upload->do_upload('attach_file');

                    if($check_uploadp){

                    $inData = array("booking_id" => $postData['booking_id'],
                                    "book_det_id" => $postData['book_det_id'],
                                    "ticket_from" => $postData['ticket_from'],
                                    "ticket_to" => $postData['ticket_to'],
                                    "ticket_service" => $postData['service'],
                                    "ticket_subject" => $postData['subject'],
                                    "ticket_comment" => $postData['comment'],
                                    "ticket_attachment" => $_FILES['attach_file']['name'],
                                    "created_date" => date('Y-m-d H:i:s'),
                                    "update_date" => date('Y-m-d H:i:s')
                                );

                    $addTicket = $this->common_model->insert(TB_TICKET,$inData);    
                    if ($addTicket) {
                        $this->response(array("status" => true, "message" => "Ticket has been added successfully."), 200); exit; 
                    } else {
                        $this->response(array("status" => false, "message" => "Failed to add ticket."), 200); exit;
                    } 

                    } else {
                        $this->response(array("status" => false, "message" => "Failed to upload attachment, invalid file format or size.")); exit;
                    }

                } else {

                    $inData = array("booking_id" => $postData['booking_id'],
                                    "book_det_id" => $postData['book_det_id'],
                                    "ticket_from" => $postData['ticket_from'],
                                    "ticket_to" => $postData['ticket_to'],
                                    "ticket_service" => $postData['service'],
                                    "ticket_subject" => $postData['subject'],
                                    "ticket_comment" => $postData['comment'],
                                    "created_date" => date('Y-m-d H:i:s'),
                                    "update_date" => date('Y-m-d H:i:s')
                                );

                    $addTicket = $this->common_model->insert(TB_TICKET,$inData);    
                    if ($addTicket) {
                        $this->response(array("status" => true, "message" => "Ticket has been added successfully."), 200); exit; 
                    } else {
                        $this->response(array("status" => false, "message" => "Failed to add ticket."), 200); exit;
                    } 
                }

                                

            } else {

                if($checkExists[0]['is_deleted'] == 0) {

                    $this->response(array("status" => false, "message" => 'Already raise ticket for this booking.'), 200); exit;
                } else {

                    if (isset($_FILES['attach_file']['name']) && !empty($_FILES['attach_file']) ) {

                        $config =array('upload_path' => './uploads/ticket/',
                                'allowed_types' => 'png|jpeg|jpg',
                                'max_width' => '6000',
                                'max_height' => '5000'
                               );

                        $this->load->library('upload', $config);
                        $this->upload->initialize($config); 
                        $check_uploadu = $this->upload->do_upload('attach_file');

                        if($check_uploadu){

                            $updateData = array("ticket_subject" => $postData['subject'],
                                                "ticket_comment" => $postData['comment'],
                                                "ticket_attachment" =>$_FILES['attach_file']['name'],
                                                "is_deleted" => 0,
                                                "update_date" => date('Y-m-d H:i:s')
                                                );
                            
                            $cond = array("ticket_id" => $checkExists[0]['ticket_id']);
                            $result = $this->Common_model->update(TB_TICKET, $cond, $updateData);

                            if ($result) {
                                $this->response(array("status" => true, "message" => "Ticket has been added successfully."), 200); exit; 
                            } else {
                                $this->response(array("status" => false, "message" => 'Failed to add ticket.'), 200); exit;
                            } 

                        } else {
                            $this->response(array("status" => false, "message" => "Failed to upload attachment, invalid file format or size.")); exit;
                        }
                    } else {

                        $updateData = array("ticket_subject" => $postData['subject'],
                                            "ticket_comment"=>$postData['comment'],
                                            "is_deleted" => 0,
                                            "update_date" => date('Y-m-d H:i:s')
                                        );

                        $cond = array("ticket_id" => $checkExists[0]['ticket_id']);
                        $result = $this->Common_model->update(TB_TICKET, $cond, $updateData);

                        if ($result) {
                            $this->response(array("status" => true, "message" => "Ticket has been added successfully."), 200); exit; 
                        } else {
                            $this->response(array("status" => false, "message" => 'Failed to add ticket.'), 200); exit;
                        } 

                    }
                }
            }

        }
    }



    /**
      Author:Priya
     * Title       : DELETE TICKET
     * Description : DELETE TICKET
     * @param Integer user_id
     * @param Integer ticket_id
     * @return Array
     */

    public function deleteTicket_post ()
    {    
        $postData = $this->post();
        if(empty($postData['user_id']))
        {
            $this->response(array("status" => false,"message" => 'User id is required.'), 200); exit;
        } else if(empty($postData['ticket_id']))
        {
            $this->response(array("status" => false,"message" => 'Ticket id is required.'), 200); exit;
        } else {

            $userId = $postData['user_id'];
            $ticketId = $postData['ticket_id'];

            $checkExists = $this->common_model->select("*", TB_TICKET, array("ticket_from" => $userId, "ticket_id" => $ticketId));
            
            if(count($checkExists) != 0) {
                
                $updateData = array("is_deleted" => 1);
                $cond = array("ticket_id" => $ticketId, "ticket_from" => $userId);
                $result = $this->Common_model->update(TB_TICKET, $cond, $updateData);

                if ($result) {
                    $this->response(array("status" => true, "message" => "Ticket has been deleted successfully."), 200); exit; 
                } else {
                    $this->response(array("status" => false, "message" => 'Failed to delete ticket.'), 200); exit;
                } 
            } else {

                $this->response(array("status" => false,"message" => 'No ticket found.'), 200); exit;
            }
        }
    } 


    /** Author : Jyoti
     * Title : Ticket List
     * @ param Integer user_id
     * @return Array
     */
    public function ticket_list_post()
    {
        $postData = $this->post();

        if(trim(empty($postData['user_id'])))
        {
            $this->response(array("status"=>false,"message"=>"User id is required."));
            exit;
        }
        $filePath = base_url()."uploads/ticket/";
        $listData = $this->common_model->select_or("ticket_id, booking_id, book_det_id, ticket_from, ticket_to, ticket_subject, ticket_service, concat('".$filePath."',ticket_attachment) as ticket_attachment, ticket_comment, status",TB_TICKET, array("is_deleted" => 0,"ticket_from" => $postData['user_id']),array("ticket_to" => $postData['user_id']));
        if(count($listData) != 0) {

            $this->response(array("status" => true,"message" => 'Data found', "ticketList" => $listData), 200); exit;
        }else {
            $this->response(array("status" => false,"message" => 'No data found.'), 200); exit;
        }
    }    

    /**
     * Author : Kiran
     * Title : upload_students_with_csv
     * Description : To upload multiple students by csv
     *
     * @param Integer user_id
     * @param File csv_file
     * @return Array
     */

    public function upload_students_with_csv_post()
    {
        $postData = $this->post();
        // Input validation
        if(empty(trim($postData['user_id'])))
        {
            $this->response(array("status"=>false, "message" => 'User id is required.'), 200);
            exit;
        }
        // check file is set
        if(!isset($_FILES['csv_file']['tmp_name'])) {
            $this->response(array("status"=>false,"message"=> 'Please upload csv file.'));exit;
        } else {
            $allowed =  array('csv');
            $filename = $_FILES['csv_file']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                $this->response(array("status"=>false,"message"=> 'Invalid file format, Please upload CSV file.'));exit;
            }else{
                $count=0;
                $user_id = $postData['user_id'];
                $total_records = $totol_skip_records = $k = 0;
                $skipRecords = array();
                $fp = fopen($_FILES['csv_file']['tmp_name'],'r') or die("can't open file");
                while($csv_line = fgetcsv($fp,1024))
                {
                    $count++;
                    if($count == 1)
                    {
                        continue;
                    }//keep this if condition if you want to remove the first row
                    // check username is unique or not 
                    $cond1 = array("pass_username"=>trim($csv_line[0]));
                    $resultCheckUserNameUnique = $this->common_model->select("pass_username",TB_PASSENGER,$cond1);   
                    
                    if(count($resultCheckUserNameUnique) > 0) {
                        $skipRecords[$k]['username'] = $csv_line[0];
                        $skipRecords[$k]['full name'] = $csv_line[1];
                        $totol_skip_records++;
                        $k++;
                    } else {
                        for($i = 0, $j = count($csv_line); $i < $j; $i++)
                        {
                            $insert_csv = array();
                            $insert_csv['pass_username'] = $csv_line[0];
                            $insert_csv['pass_fullname'] = $csv_line[1];
                            $insert_csv['pass_parent_name'] = $csv_line[2];
                            $insert_csv['pass_contact_no'] = $csv_line[3];
                            $insert_csv['pass_address'] = $csv_line[4];
                        }
                        $i++;
                        if((!empty($insert_csv['pass_username'])) && (!empty($insert_csv['pass_fullname'])) && (!empty($insert_csv['pass_parent_name'])) && (!empty($insert_csv['pass_contact_no'])) && (!empty($insert_csv['pass_address'])) ){

                            $current_time = date( 'Y-m-d H:i:s', time());
                            $data_new[] = array(
                                $user_id,
                                "'".$insert_csv['pass_username']."'",
                                "'".$insert_csv['pass_fullname']."'",
                                "'".$insert_csv['pass_parent_name']."'",
                                "'".$insert_csv['pass_contact_no']."'",
                                "'".$insert_csv['pass_address']."'",
                                "'"."1"."'",
                                "'"."0"."'",
                                "'".$current_time."'");
                            $total_records++;
                        } else {
                            $skipRecords[$k]['username'] = $csv_line[0];
                            $skipRecords[$k]['full name'] = $csv_line[1];
                            $totol_skip_records++;
                            $k++;                           
                        } 
                    }
                }
                if($total_records > 0)  {
                    $key_new = "service_user_id, pass_username, pass_fullname, pass_parent_name, pass_contact_no, pass_address, pass_status, is_deleted, created_date";
                    $result = $this->common_model->insert_ignore(TB_PASSENGER,$key_new,$data_new);
                } else {
                    $result = 0;
                }
                fclose($fp) or die("can't close file");
                $this->response(array("status"=>true,"total_records_inserted"=> $result, "total_skip_record"=>$totol_skip_records,"skip_records"=>$skipRecords), 200);exit;
            }
        }
    }  

    // demo for terms & condition
    public function demo_terms_get()
    {
        $resultTermsList = $this->Common_model->select("title, description", TB_TERMS, array("status" => "1", "term_id" => 6));
        // result
        if (count($resultTermsList) > 0) {                        
            $this->response(array('status' => true, 'message' => 'Data found', 'resultTerms' => $resultTermsList), 200); exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Data not found.'), 200); exit;
        }    
    }

    public function service_provider_exists ($user_id)
    { 
       // Check user available or not
        $cond = array("service_provider_id" => $user_id, "sp_status" => "1");
        $exist_users = $this->common_model->select('service_provider_id',service_provider_ut,$cond); 
            // If user available return 1 otherwise 0
        if ( count($exist_users) > 0 ) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
      Author:Priya
     * Title       : GET NOTIFICATION COUNT
     * Description : GET NOTIFICATION LIST COUNT BY USER ID 
     * @param Integer user_id
     * @param Integer app_id
     * @return Array
     */

    public function getNotCount_post ()
    {    
        $postData = $this->post();
        if(empty($postData['user_id']))
        {
            $this->response(array("status" => false,"message" => 'User id is required.'), 200); exit;
        } else if($postData['app_id'] == '')
        {
            $this->response(array("status" => false,"message" => 'App is required.'), 200); exit;
        } else {

            $userId = $postData['user_id'];
            $appId = $postData['app_id'];
            $getAll = $this->common_model->select("*", TB_NOTIFICATION, array('user_id' => $userId, 'app_id' => $appId ) );
            
            if($getAll) {
                $count = count($getAll);

                $this->response(array("status" => true,"message" => 'notification count',"totalCount" => $count ), 200); exit;
            } else {

                $this->response(array("status" => false,"message" => 'No notification list found.'), 200); exit;
            }
        }
    } 



    /**
      Author:Priya
     * Title       : READ NOTIFICATION
     * Description : READ NOTIFICATION
     * @param Integer user_id
     * @param Integer app_id
     * @param Integer notification_id
     * @return Array
     */

    public function readNotification_post ()
    {    
        $postData = $this->post();
        if(empty($postData['user_id']))
        {
            $this->response(array("status" => false,"message" => 'User id is required.'), 200); exit;
        } else if($postData['app_id'] == '')
        {
            $this->response(array("status" => false,"message" => 'App is required.'), 200); exit;
        } else if($postData['notification_id'] == '')
        {
            $this->response(array("status" => false,"message" => 'Notification id is required.'), 200); exit;
        } else {

            $userId = $postData['user_id'];
            $appId = $postData['app_id'];
            $notId = $postData['notification_id'];
            $checkExists = $this->common_model->select("*", TB_NOTIFICATION, array('user_id' => $userId, 'app_id' => $appId, 'notification_id' => $notId ) );
            
            if($checkExists) {
                
                $cond = array('user_id' => $userId, 'app_id' => $appId, 'notification_id' => $notId);
                $updateData = array('read_status' => 1);
                $result = $this->Common_model->update(TB_NOTIFICATION, $cond, $updateData);

                if($result) {

                    $this->response(array("status" => true,"message" => 'Read notification successfully.','notiMeassage' => $checkExists[0]['message'] ), 200); exit;
                } else {

                    $this->response(array("status" => false,"message" => 'Already read notification.' ), 200); exit;
                }
  
            } else {

                $this->response(array("status" => false,"message" => 'Notification is not exists.'), 200); exit;
            }
        }
    } 

    public function get_mime_type($file) 
    {
        //echo $file;
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $file);

       // print_r($mime);
        
    }

    public function carPooling_post()
    {
        $data = json_decode(file_get_contents('php://input'));

        
        //$this->input->post('booking_data');
        $rideData = $data->booking_data;

        print_r($data->booking_data->ride_data);

        foreach($data->booking_data->ride_data as $key => $val)
        {
            print_r($val->loc_data[0]->child_data);
        }


    }


    /**
      Author:Priya
     * Title       : BOOK RIDE FOR GROUP
     * Description : BOOK RIDE FOR GROUP
     * @return Array
     */

    public function bookRideGroup_post()
    {
        $postData = $_POST;
        if($postData['bookJSON'] == '') {
            $this->response(array("status" => true,"message" => 'booking data is required.' ), 200); exit;

        } else {

        $bookJSON = '{  "booking_data": {
                            "parent_id": "4",
                            "booking_date": "2018-08-07",
                            "service_id": "1",
                            "ride_data": [{
                                "start_date": "2018-08-10,2018-08-13",
                                "end_date": "2018-08-11,2018-08-14",
                                "pick_up_time": "10:00:00",
                                "same_location": "0",
                                "group_id": 1,
                                "pick_up_point": "Apache High Street, Laxman Nagar, Baner, Pune, Maharashtra",
                                "drop_point": "Spot 18multiplex, Wakad - Bhosari BRTS Road, Mana-mandir Society, Rahatani, Pimpri-Chinchwad, Maharashtra",
                                "pick_latlong": "18.5694127,73.7723752",
                                "drop_latlong": "18.593666,73.7837689",
                                "is_car_pooling": "1",
                                "actual_distance" : "10",
                                "actual_cost" : "222",
                                "comment": "",
                                "mobile_no": "1234567890",
                                "child_data": [{
                                    "child_id": "5"
                                }, {
                                    "child_id": "5"
                                }, {
                                    "child_id": "5"
                                }]
                            },
                            {
                                "start_date": "2018-08-10,2018-08-13",
                                "end_date": "2018-08-11,2018-08-14",
                                "pick_up_time": "10:00:00",
                                "same_location": "0",
                                "group_id": 1,
                                "pick_up_point": "Apache High Street, Laxman Nagar, Baner, Pune, Maharashtra",
                                "drop_point": "Spot 18multiplex, Wakad - Bhosari BRTS Road, Mana-mandir Society, Rahatani, Pimpri-Chinchwad, Maharashtra",
                                "pick_latlong": "18.5694127,73.7723752",
                                "drop_latlong": "18.593666,73.7837689",
                                "is_car_pooling": "1",
                                "actual_distance" : "10",
                                "actual_cost" : "222",
                                "comment": "",
                                "mobile_no": "1234567890",
                                "child_data": [{
                                    "child_id": "5"
                                }, {
                                    "child_id": "5"
                                }, {
                                    "child_id": "5"
                                }]
                            }]
                        }
                     }';
            $bookDetails = json_decode($bookJSON, true);
            // $bookDetails = json_decode($postData['bookJSON'], true);

            foreach ($bookDetails as $key1 => $value1) {
                $service_user_id = $value1['parent_id'];
                $rideData = $value1['ride_data'];
                $booking_date = $value1['booking_date'];
                $serviceId = $value1['service_id'];

                $mainBooking = array("service_user_id" => $service_user_id,
                                     "booking_date" => $booking_date,
                                     "booking_status" => 0,
                                     "status" => 1,
                                     "created_date" => date('Y-m-d')
                                );

                $result = $this->common_model->insert(TB_BOOKING,$mainBooking);


                if($result) {

                    foreach ($rideData as $keyR => $valueR) {
                        $pickTime = $valueR['pick_up_time'];
                        $sameLocation = $valueR['same_location'];
                        $groupId = $valueR['group_id'];
                        $pickPoint = $valueR['pick_up_point'];
                        $dropPoint = $valueR['drop_point'];
                        $mobile_no = $valueR['mobile_no'];
                        $comment = $valueR['comment'];
                        $carPooling = $valueR['is_car_pooling'];
                        $startDate = explode(",", $valueR['start_date']);
                        $endDate = explode(",", $valueR['end_date']);
                        $pickLatLong = explode(",", $valueR['pick_latlong']);
                        $dropLatLong = explode(",", $valueR['drop_latlong']);
                        $sdate=[];
                        $edate=[];
                        foreach ($startDate as $keyS => $valueS) {
                            $sdate[$keyS]['start']=$valueS;
                        }

                        foreach ($endDate as $keyE => $valueE) {
                            $edate[$keyE]['end']=$valueE;
                        }

                        $resultD = array();
                        foreach($sdate as $key=>$val){
                            $val2 = $edate[$key];
                            $resultD[$key] = $val + $val2;
                        }

                        foreach ($resultD as $keyD => $valueD) {
                            $rideBooking = array("booking_id" => $result,
                                     "group_id" => $groupId,
                                     "pick_up_lat" => $pickLatLong[0],
                                     "pick_up_lng" => $pickLatLong[1],
                                     "drop_lat" => $dropLatLong[0],
                                     "drop_lng" => $dropLatLong[1],
                                     "scheduled_pick_up_date" => $valueD['start'],
                                     "scheduled_drop_date" => $valueD['end'],
                                     "status" => 1,
                                     "created_date" => date('Y-m-d')
                                );

                            $result1 = $this->common_model->insert(TB_RIDE,$rideBooking);

                            // child detail foreach data
                            foreach ($valueR['child_data'] as $keyC => $valueC) {
                                $subBooking = array("booking_id" => $result,
                                                    "service_id" => $serviceId,
                                                    "service_booking_id" => $result1,
                                                    "pass_id" => $valueC['child_id'],
                                                    "sub_booking_date" => $valueD['start'],
                                                    "sub_book_status" => "1",
                                                    "created_at" => date('Y-m-d'),
                                                    "status" => 1,
                                                    "updated_at" => date('Y-m-d')
                                            );
                                $result2 = $this->common_model->insert(TB_BOOKING_DETAILS,$subBooking);
                            }

                        }

                    }

                    $this->response(array("status" => true,"message" => 'Booking successfully'), 200); exit;

                } else {
                    $this->response(array("status" => false,"message" => 'Failed booking'), 200); exit;
                }

            }

        }

    } 


    /*public function editGroup_post()
    {
        $data = json_decode(file_get_contents('php://input'));

        $chield = explode(',',$data->group_members);

        $checkExistingChield = $this->common_model->select_in('*',TB_GROUP_MEMBER,'passenger_id',$chield);

        if(count($checkExistingChield) > 0)
        {
            $this->response(array('status' => 'error','message' => 'Member already exist'));
        }
        else
        {
            foreach($chield as $key => $val)
            {
                $insertData[] = array(

                    'group_id' => $data->group_id,
                    'passenger_id' => $val,
                    'status' => '1',
                    'is_deleted' => '0',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')

                );
            }

            print_r($insertData);
            exit();
        }



    }*/

    /**
     * Author : Kiran
     * Title : edit_group_name
     * Description : edit group name
     *
     * @param Integer group_id
     * @param Varchar group_name
     * @param Varchar description
     * @return Array
     */

    public function edit_group_name_post()
    {
        $postData = $this->post();
        // Input validation
        if(empty(trim($postData['group_name'])))
        {
            $this->response(array("status"=>false, "message" => 'Group name is required.'), 200);
            exit;
        }
        if(empty(trim($postData['group_id'])))
        {
            $this->response(array("status"=>false, "message" => 'Group id is required.'), 200);
            exit;
        }
        if(empty(trim($postData['group_desc'])))
        {
            $this->response(array("status"=>false, "message" => 'Description is required.'), 200);
            exit;
        }

        // check group name is exist or not
        $cond = array("group_name"=>trim($postData['group_name']), "group_id !=" => $postData['group_id']);
        $resultCheckGroupNameExist = $this->common_model->select("group_name",TB_GROUP_MASTER,$cond);
        if(count($resultCheckGroupNameExist) > 0){
            $this->response(array("status" => false, "message" => 'Group name already exists'), 200);exit;
        } else {
            // update group member
            $cond1 = array("group_id"=>$postData['group_id']);
            $data = array("group_name"=>$postData['group_name'],"description" => $postData['group_desc'],"updated_date" => date( 'Y-m-d H:i:s', time()));
            $resultUpdateGroupName = $this->common_model->update(TB_GROUP_MASTER,$cond1,$data);
            if($resultUpdateGroupName) {
                $this->response(array("status" => true, "message" => "Group name has been updated successfully"), 200); exit;
            } else {
                $this->response(array("status" => false, "message" => 'Something went wrong'), 200); exit;
            }
        }
    } 



    /** Author : Priya
     * Title : GET SERVICE PROVIDERS CALENDER DATES
     * @param : service_provider_id
     * @return array
     */
    public function get_provider_calender_post()
    {

        $postData = $this->post();
        if(empty($postData['service_provider_id']))
        {
            $this->response(array("status" => false,"message" => 'Service provider id is required.'), 200); exit;
        } else {

            $spId = $postData['service_provider_id'];
            
            $checkExists = $this->common_model->select("unavaibility_id, unavail_start_date, unavail_end_date", TB_SP_UNAVAILABILITY, array("service_provider_id" => $spId));

            if(count($checkExists) != 0) {

                $this->response(array("status" => true,"message" => 'unavailble data.', "unavailble" => $checkExists ), 200); exit;
            } else {

                $this->response(array("status" => false,"message" => 'No unavailble date data found.'), 200); exit;
            }
        }
    }


    public function set_provider_unavailability_post()
    {

        echo "rrrrrrrrr";

        exit();
        $postData = $this->post();
        if(empty($postData['service_provider_id']))
        {
            $this->response(array("status" => false,"message" => 'Service provider id is required.'), 200); exit;
        } else if(empty($postData['unavail_date']))
        {
            $this->response(array("status" => false,"message" => 'date is required.'), 200); exit;
        } else if(empty($postData['start_time']))
        {
            $this->response(array("status" => false,"message" => 'Start time is required.'), 200); exit;
        }
        else if(empty($postData['end_time']))
        {
            $this->response(array("status" => false,"message" => 'End time is required.'), 200); exit;
        } else 
        {
            $spId = $postData['service_provider_id'];
            $allDates = $postData['unavail_date'];
            $dateArray = explode(',', $allDates);
            foreach ($dateArray as $key => $value)
            {
                $un_start_date = $value." ".$postData['start_time'];
                $un_end_date = $value." ".$postData['end_time'];
                $dateData[] = array(
                  "'".$spId."'",
                  "'".$un_start_date."'",
                  "'".$un_end_date."'",
                  "'".date("Y-m-d H:i:s")."'"
                  );                
            }

            echo "tetetetete";
            $key_data = "service_provider_id,unavail_start_date,unavail_end_date,created_date";
            $result = $this->common_model->insert_ignore(TB_SP_UNAVAILABILITY,$key_data,$dateData);

            echo $this->db->last_query();

            exit();
            if($result)
            {
                $this->response(array("status" => true,"message" => 'Your unavailability set successfully.'), 200); exit;
            }else
            {
                $this->response(array("status" => false,"message" => 'Your unavailability is already set.'), 200); exit;
            }
        }
    }

    
    
    

}
