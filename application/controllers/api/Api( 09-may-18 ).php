<?php 
/*
Author: Ram K
Page: APi
Description: Created Api for App.
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("data_model");
        $this->load->model("login_model");
        $this->load->model("common_model");
        $this->load->library('Common');
        $this->load->library("email"); 
        $this->load->helper("email_template"); 
        $this->load->helper("cias"); 
        $this->load->library("fileupload");
        $this->load->library('form_validation');
		date_default_timezone_set("Asia/Kolkata");
        header('Access-Control-Allow-Origin: *');
        
    }    
   /* Register user */
/*    public function register_post()
    {
        $postData = $this->post();
        if($postData !='')
        {
            if($postData['user_email'] != '' && $postData['mobile_no'] != '')
            {
                $isExist = $this->data_model->checkEmailExists($postData['user_email']);

                if(count($isExist) !=0)
                {
                    $this->response(array("status"=>false,"message"=> 'Email id already exists'),300);exit;
                }else{
                        $userdata = array(
                        'user_email' => $this->post('user_email'),
                        'user_password' => getHashedPassword(trim($this->post('password'))),
                        'user_name' => $this->post('user_name'),
                        'user_phone_number' => $this->post('mobile_no'),
                        'user_category_id' => $this->post('category_id'),
                        //'user_sub_category_id' => $this->post('sub_category_id'),
                        'user_status' => 1,
                        'user_gender' => $this->post('gender') ? $this->post('gender') : '',
                        'user_address' => $this->post('address') ? $this->post('address') : '',
                        'user_no_of_kids' => $this->post('user_no_of_kids') ? $this->post('user_no_of_kids') : '',
                        'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                        'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                        'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                        'user_type' => $this->post('user_type'),
                    );
                    $user = $this->db->insert(TB_USERS, $userdata);
                    $user_id = $this->db->insert_id();
                       if ($user_id) {
                        if ($this->post('user_no_of_kids') > 0 && $this->post('user_kids')) {
                            foreach ($this->post('user_kids') as $kid) {
                                $kids['kid_name'] = isset($kid['name']) ? $kid['name'] : '';
                                $kids['kid_gender'] = isset($kid['gender']) ? $kid['gender'] : '';
                                $kids['kid_birthdate'] = isset($kid['birthdate']) ? $kid['birthdate'] : '';
                                $kids['kid_age'] = isset($kid['age']) ? $kid['age'] : '';
                                $kids['kid_status'] = 1;
                                $kids['user_id'] = $user_id;
                                $kids_all[] = $kids;
                            }
                            $this->db->insert_batch(TB_KIDS, $kids_all);
                        } 
                        $this->response(array('status' => true, 'message' => 'User Created Sucessfully', 'user_id' => $user_id),200);exit;
                    } else {
                        $this->response(array("status" => false, "message" => "Something went wrong. !!!"),300);exit;
                    }
                }
            }else{
                $this->response(array("status"=>false,"message"=> 'Please add email and mobile no.'), 300);exit;
            }

        }else{
            $this->response(array("status"=>false,"message"=> 'Please fill data.'), 300);exit;
        }
        
	}*/

    public function register_post()
    {       
        $postData = $this->post();
        if($postData !='')
        {
            if($postData['user_email'] != '' && $postData['mobile_no'] != '' && $postData['user_type'] != '')
            {
                $isExist = $this->data_model->checkEmailExists($postData['user_email']);

                if(count($isExist) !=0)
                {
                    $this->response(array("status"=>false,"message"=> 'Email id already exists'), 300);exit;
                }else{

                    $isMobileExist = $this->common_model->select("user_phone_number",TB_USERS,array('user_phone_number'=>$postData['mobile_no']));
                    if(count($isMobileExist) != 0)
                    {
                        $this->response(array("status"=>false,"message"=> 'Mobile no already exists'), 300);exit;
                    } else {
                        if($postData['user_type'] == 2) 
                        {
                            $userdata = array(
                                'user_email' => $this->post('user_email'),
                                'user_password' => getHashedPassword(trim($this->post('password'))),
                                'user_name' => $this->post('user_name'),
                                'user_phone_number' => $this->post('mobile_no'),
                                'user_status' => '1',
                                'user_gender' => $this->post('gender') ? $this->post('gender') : '',
                                'user_address' => $this->post('address') ? $this->post('address') : '',
                                'user_no_of_kids' => $this->post('user_no_of_kids') ? $this->post('user_no_of_kids') : '',
                                'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                                'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                                'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                                'roleId' => 2,
                            );
                            $user = $this->db->insert(TB_USERS, $userdata);
                            $user_id = $this->db->insert_id();
                               if ($user_id) {
                                if ($this->post('user_no_of_kids') > 0 && $this->post('user_kids')) {
                                    foreach ($this->post('user_kids') as $kid) {
                                        $kids['kid_name'] = isset($kid['name']) ? $kid['name'] : '';
                                        $kids['kid_gender'] = isset($kid['gender']) ? $kid['gender'] : '';
                                        $kids['kid_birthdate'] = isset($kid['birthdate']) ? $kid['birthdate'] : '';
                                        $kids['kid_age'] = isset($kid['age']) ? $kid['age'] : '';
                                        $kids['kid_status'] = 1;
                                        $kids['user_id'] = $user_id;
                                        $kids_all[] = $kids;
                                    }
                                    $this->db->insert_batch(TB_KIDS, $kids_all);
                                }
                                $this->response(array('status' => true, 'message' => 'User has been created sucessfully', 'user_id' => $user_id),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                        }
                        else
                        {
                            $upload_car_pics = $licenseCopy= $certification =$car_pics="";
                            $scan_copy_of_license = "";
                            $upload_certification = "";
                            $temp = "";
                            $fileImages = $_FILES;
                            $allFiles = array();
                            if(!empty($fileImages)) {
                                $j =1;                          
                                foreach ($fileImages as $key => $value) {
                                    $cpt = count($_FILES[$key]['name']);
                                    if($j == 1)
                                        $temp = 'car_img';
                                    else if($j == 2)
                                        $temp = 'license_img';
                                    else
                                        $temp = 'certification_img';
                                    $files_name = array();
                                    for($i=0; $i<$cpt; $i++){
                                        $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                        $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                        $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                        $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                        $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                        $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                        $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                        $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                        $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                        $files_name[$i]['size'] = $_FILES[$temp]['size'];
                                    }
                                    $allFiles[$temp]=$files_name;
                                    $j++;                            
                                }
                                foreach ($allFiles as $key => $value) {
                                    $i = 0;                                
                                    for($j=0;$j < count($value); $j++) { 
                                        if (strcmp($key,'car_img') == 0 ) {                                   
                                            $upload_car_pics = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
                                            $car_pics .= "|".$upload_car_pics; 
                                        } else if (strcmp($key,'certification_img') == 0 ) {
                                            $upload_certification = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/certification/'); 
                                            $certification .= "|".$upload_certification; 
                                        } else if(strcmp($key,'license_img') == 0 ){
                                            $scan_copy_of_license = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
                                            $licenseCopy .= "|".$scan_copy_of_license;                                         
                                        } else {}         
                                        $i++;                                                     
                                    }
                                }
                            }     
                            
                            $userdata = array(
                                'user_email' => $this->post('user_email'),
                                'user_password' => getHashedPassword(trim($this->post('password'))),
                                'user_name' => $this->post('user_name'),
                                'user_phone_number' => $this->post('mobile_no'),
                                'user_status' => '1',
                                'user_gender' => $this->post('gender') ? $this->post('gender') : '',
                                'user_address' => $this->post('address') ? $this->post('address') : '',
                                'roleId' => 3,
                                'user_birth_date' => $this->post('dob') ? $this->post('dob') : '',
                                'user_age' => $this->post('age') ? $this->post('age') : '', 
                                'is_your_own_car' => $this->post('own_car') ? $this->post('own_car') : '',
                                'driving_license_type' => $this->post('driver_license') ? $this->post('driver_license') : '', 
                                'license_no' => $this->post('license_no') ? $this->post('license_no') : '',
                                'valid_from_date' => $this->post('valid_from') ? $this->post('valid_from') : '',
                                'valid_until_date' => $this->post('valid_until') ? $this->post('valid_until') : '',
                                'car_model' => $this->post('car_model') ? $this->post('car_model') : '',
                                'car_register_no' => $this->post('car_reg_no') ? $this->post('car_reg_no') : '',
                                'school' => $this->post('school_name') ? $this->post('school_name') : '',
                                'degree' => $this->post('degree') ? $this->post('degree') : '',
                                'specialization' => $this->post('specialization') ? $this->post('specialization') : '',
                                'education_from' => $this->post('from_date') ? $this->post('from_date') : '',
                                'education_to' => $this->post('to_date') ? $this->post('to_date') : '',
                                'certification_in' => $this->post('certification_name') ? $this->post('certification_name') : '',
                                'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                                'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                                'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                                'license_pic'=>trim($licenseCopy,"|"),
                                'certificate'=>trim($certification,"|"),
                                'car_pic'=>trim($car_pics,"|")
                            );                            
                            $user = $this->db->insert(TB_USERS, $userdata);
                            $user_id = $this->db->insert_id();
                            if ($user_id) {
                                $this->response(array('status' => true, 'message' => 'Driver register has been created sucessfully', 'user_id' => $user_id),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                        }
                    }
                }
            }else{
                $this->response(array("status"=>false,"message"=> 'Please add user_type, email and mobile no.'), 300);exit;
            }

        }else{
            $this->response(array("status"=>false,"message"=> 'Please fill data.'), 300);exit;
        }        
    }

    /**
     * driver_update
     * @param user_id
     */
    public function driver_update_post() {
        $postData = $_POST;    
        if($postData['user_id'] != "" && $postData['user_id'] >0)
        {
            $user_id = $this->common_model->select("user_email,user_password,user_name,user_phone_number,user_gender,user_address,user_birth_date,user_age,is_your_own_car,driving_license_type,license_no,valid_from_date,valid_until_date,car_model,car_register_no,school,degree,specialization,education_from,education_to,certification_in,license_pic,certificate,car_pic",TB_USERS,array('user_id'=>$postData['user_id']));
            if(empty($user_id))
            {
                $this->response(array("status"=>false,"message"=> 'User id does not exists'), 300);exit;
            } else {    
                $upload_car_pics = $licenseCopy= $certification =$car_pics="";
                $scan_copy_of_license = "";
                $upload_certification = "";
                $temp = "";
                $fileImages = $_FILES;
                $allFiles = array();
                if(!empty($fileImages)) {
                    $j =1;                          
                    foreach ($fileImages as $key => $value) {
                        $cpt = count($_FILES[$key]['name']);
                        if($j == 1)
                            $temp = 'car_img';
                        else if($j == 2)
                            $temp = 'license_img';
                        else
                            $temp = 'certification_img';
                       // echo $cpt;die();
                        $files_name = array();
                        for($i=0; $i<$cpt; $i++){
                            $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                            $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                            $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                            $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                            $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                            $files_name[$i]['name'] = $_FILES[$temp]['name'];
                            $files_name[$i]['type'] = $_FILES[$temp]['type'];
                            $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                            $files_name[$i]['error'] = $_FILES[$temp]['error'];
                            $files_name[$i]['size'] = $_FILES[$temp]['size'];
                        }
                        $allFiles[$temp]=$files_name;
                        $j++;                            
                    }
                    foreach ($allFiles as $key => $value) {
                        $i = 0;                                
                        for($j=0;$j < count($value); $j++) { 
                            if (strcmp($key,'car_img') == 0 ) {                                   
                                //echo $key." car <br/>";
                                $upload_car_pics = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
                                $car_pics .= "|".$upload_car_pics; 
                            } else if (strcmp($key,'certification_img') == 0 ) {
                                //echo $key." cetifi <br/>";
                                $upload_certification = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/certification/'); 
                                $certification .= "|".$upload_certification; 
                            } else if(strcmp($key,'license_img') == 0 ){
                                //echo $key." liece <br/>";
                                $scan_copy_of_license = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
                                $licenseCopy .= "|".$scan_copy_of_license;                                         
                            } else {}         
                            $i++;                                                     
                        }
                    }    
                } else {
                    $car_pics = $user_id[0]['car_pic'] ? $user_id[0]['car_pic'] :'';
                    $certification = $user_id[0]['certificate'] ? $user_id[0]['certificate'] :'';
                    $licenseCopy = $user_id[0]['license_pic'] ? $user_id[0]['license_pic'] :'';
                }
                      
                //print_r($user_id);
                $userdata = array(
                    //'user_email' => $this->post('user_email'),
                    //'user_password' => getHashedPassword(trim($this->post('password'))),
                    'user_name' => $this->post('user_name'),
                    //'user_phone_number' => $this->post('mobile_no'),
                    'user_gender' => $this->post('gender'),
                    'user_address' => $this->post('address'),
                    'user_birth_date' => $this->post('dob'),
                    'user_age' => $this->post('age'), 
                    'is_your_own_car' => $this->post('own_car'),
                    'driving_license_type' => $this->post('driver_license'), 
                    'license_no' => $this->post('license_no'),
                    'valid_from_date' => $this->post('valid_from'),
                    'valid_until_date' => $this->post('valid_until'),
                    'car_model' => $this->post('car_model'),
                    'car_register_no' => $this->post('car_reg_no'),
                    'school' => $this->post('school_name'),
                    'degree' => $this->post('degree'),
                    'specialization' => $this->post('specialization'),
                    'education_from' => $this->post('from_date'),
                    'education_to' => $this->post('to_date'),
                    'certification_in' => $this->post('certification_name'),
                    'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                    'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                    'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                    'license_pic'=>trim($licenseCopy,"|"),
                    'certificate'=>trim($certification,"|"),
                    'car_pic'=>trim($car_pics,"|")
                );
                $user = $this->db->update(TB_USERS,$userdata,array('user_id' =>$this->post('user_id')));
                if($user) {
                    $this->response(array("status"=>true,"message"=> 'Driver has been updated successfully'), 200);exit;
                } else {
                    $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
                }
            }
        } else {
            $this->response(array("status"=>false,"message"=> 'Please enter the user id.'), 300);exit;
        }
    }

    /* Login */

    public function login_post()
    {        
        $postData = $_POST; 
       
        $userArr = $this->login_model->login($postData["user_email"],$postData['password']);
        if(count($userArr)>0){
            
            if($userArr[0]['user_status'] == '1'){
                if($userArr[0]['roleId'] == $postData['user_type']){
                    $user_token = md5(uniqid(rand(), true));   
                    $cond = array("user_email"=>$postData["user_email"]);
                    
                    $userData = $this->data_model->getAllUsers($cond);
                    $this->session->set_userdata("userLoggedin", $userData[0]);
                    //Update device token
                    
                    if(isset($postData["device_token"]) && $postData["device_token"] != ""){
                        $this->common_model->update(TB_USERS,array("user_id"=>$userData[0]['user_id']),array('user_device_token'=>$postData["device_token"]));
                    }
                    elseif(isset($postData["device_id"]) && $postData["device_id"] != ""){
                        $this->common_model->update(TB_USERS,array("user_id"=>$userData[0]['user_id']),array('iser_device_id'=>$postData["device_id"]));
                    }
                    $this->response(array("status"=>true,"user_token"=>$user_token ,"message"=> "You have successfully login.","user"=> $userArr[0]), 200); exit;
                } else {
                    $this->response(array("status"=>false, "message"=> "Please select correnct user type"), 300);exit;
                }
            }
            else{
                
                $this->response(array("status"=>false, "message"=> "Your account is inactive."), 300);exit;
            }
            
        }else{
            $this->response(array("status"=>false, "message"=> "Email or password is incorrect."), 300);exit;
        }
    }


    /**
     * Forgot password
     */
    function forgot_password_post()
    {
        $postData = $_POST;
        if(trim($postData["user_email"]) == ""){
            $this->response(array("status"=>false, "message"=> "Please enter email."), 300);exit;
        }
        else
        {            
            $email=$postData['user_email'];
            $userdata=$this->common_model->select("user_id,user_name",TB_USERS,array("user_email"=>trim($email)));
            if(!count($userdata))
            {                
                $this->response(array("status"=>false, "message"=> "Please enter correct email address, this email address does not exist."), 300);exit;
            }
            else
            {
                $userId=$userdata[0]['user_id'];
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 8; $i++) {
                    $randstring.= $characters[rand(0, strlen($characters))];
                }
                                
                $update = $this->common_model->update(TB_USERS,array("user_id"=>$userdata[0]['user_id']),array('user_password'=>getHashedPassword($randstring)));
                if($update)
                {
                    $hostname = $this->config->item('hostname');
                     $usrname = $userdata[0]['user_name'];
                     $config['mailtype'] ='html';
                     $config['charset'] ='iso-8859-1';
                     $this->email->initialize($config);
                     $from  = EMAIL_FROM; 
                     //$this->messageBody  .= email_header();
                     $this->messageBody  = email_header();
                     $this->messageBody  .= "Hello ".$usrname." 
                                            <br/><br/>Please refer this password.  
                                            <br/>Password: " . $randstring ."
                                             ";
                    
                     $this->messageBody  .= email_footer();
                     //echo $this->messageBody;
                     //die;
                     $this->email->from($from, $from);
                     $this->email->to("$email");

                     $this->email->subject('Your new Password');
                     $this->email->message($this->messageBody); 
                         
                     $this->email->send();
                    $user_token = md5(uniqid(rand(), true));

                    $data = array("user_token"=> $user_token,"user_id"=> $userId);

                    $this->response(array("status"=>true, "result" => $data, "message" => "Your password has been sent to your email address. please check your email address."), 200);exit;
                }else{
                    $this->response(array("status"=>false, "user_token" => $user_token, "message" => "Something went wrong !!!"), 300);exit;
                }
                      
                     
            }
        }
    }

    /**
     * Change password
     */
    function change_password_post(){
            $postData = $_POST;
            if($this->user_exists($postData["user_id"])==0){
                $this->response(array("status"=>false,"message"=>'User id is not exists'), 300);exit;
            }
            $cond_check = array('user_id' => $postData['user_id']);
            $currentUser = $this->login_model->validUser(TB_USERS,'user_password,user_name',$cond_check);

           if(!empty($currentUser)){
            if(verifyHashedPassword(trim($postData['old_pwd']), $currentUser[0]['user_password'])){
                if(count($currentUser)==0){

                $this->response(array("status"=>false,"message"=>'You have entered incorrect old password.'), 300);exit;
                }
            
                $cond = array("user_id" => $postData['user_id']);
                $updateData = array("user_password" => getHashedPassword($postData['new_pwd']));
                $result = $this->common_model->update(TB_USERS,$cond,$updateData);
                if($result)
                {
                $this->response(array("status"=>true,"message"=>'Password has been updated successfully.'), 200);exit;
                }
                else
                {
                $this->response(array("status"=>false,"message"=>'Error occured during updating Password.'), 300);exit;
                } 

            } else {
              $this->response(array("status"=>false,"message"=>'Old password and new password should not be same.'), 300);exit;
            }
        }
             
             
    }

/**
     * my_profile_get
     * @param type $user_id
     */
    function getProfile_post() {
        $postData = $_POST;
        if($this->user_exists($postData["user_id"])==0){
            $this->response(array("status"=>false,"message"=>'User is not exists.'), 300);exit;
        }
        if($postData["user_id"]!='') {                
            $cond = array("user_id" => $postData["user_id"]);
            $parentData= $this->common_model->select("user_pic,user_id,user_name,user_gender,user_device_id,user_address,user_age,user_birth_date,user_no_of_kids,user_email,user_type,user_phone_number",TB_USERS,$cond);
            $link = base_url().USER_PROFILE_IMG.$parentData[0]['user_pic'];

            $data['user_id'] = $parentData[0]['user_id'] ? $parentData[0]['user_id']:'';
            $data['user_name'] = $parentData[0]['user_name'] ? $parentData[0]['user_name']:'';
            $data['user_email'] = $parentData[0]['user_email'] ? $parentData[0]['user_email']:'';
            $data['user_type'] = $parentData[0]['user_type'] ? $parentData[0]['user_type']:'';
            $data['user_phone_number'] = $parentData[0]['user_phone_number'] ? $parentData[0]['user_phone_number']:'';
            $data['user_no_of_kids'] = $parentData[0]['user_no_of_kids'] ? $parentData[0]['user_no_of_kids']:'';
            $data['user_birth_date'] = $parentData[0]['user_birth_date'] ? $parentData[0]['user_birth_date']:'';
            $data['user_age'] = $parentData[0]['user_age'] ? $parentData[0]['user_age']:'';
            $data['user_pic'] = $link;
            $data['user_gender'] = $parentData[0]['user_gender'] ? $parentData[0]['user_gender']:'';
            $data['user_address'] = $parentData[0]['user_address'] ? $parentData[0]['user_address']:'';
            $data['user_device_id'] = $parentData[0]['user_device_id'] ? $parentData[0]['user_device_id']:'';
            
            /*$this->response(array("status"=>true, "user_details"=> $data['userDetails']), 200);exit;
            print_r($data['userDetails']);exit;*/
            $kidData = $this->common_model->select("kid_id,kid_name,kid_gender,kid_birthdate, YEAR( CURDATE( ) ) - YEAR( kid_birthdate ) AS kid_age",TB_KIDS,array("user_id"=>$postData["user_id"]));
            //echo $this->db->last_query();die;
           
            if(count($kidData)>0)
            {
                foreach ($kidData as $key => $value) {
                    $kidInfo[] = array('kid_id'=>$value['kid_id'],
                    'kid_name'=>$value['kid_name'],
                    'kid_gender'=>$value['kid_gender'], 
                    'kid_birthdate'=> $value['kid_birthdate'],                 
                    'kid_age'=>$value['kid_age']   
                    ); 
                }               
                $data['kidsDetails'] =$kidInfo;
            }    
            $this->response(array("status"=>true, "user_details"=> $data), 200);exit;
        }
    }

    public function save_profilePic_post()
    { 
        $postData = $_POST;
       
        if($this->user_exists($postData["user_id"])==0){
            $this->response(array("status"=>false,"message"=>'User is not exists.'),300);exit;
        }
        
        $user_id = $postData["user_id"];
        if($_FILES['user_pic']['name'] != "")
        {   
            // $config['upload_path'] = USER_PROFILE_IMG;
            // $config['allowed_types'] = 'jpg|png|jpeg|JPG';
            // $config['max_size']     = '2147483648';
            // $result = $this->fileupload->save_file('user_pic',$config);
            if (isset($_FILES['user_pic']) && !empty($_FILES['user_pic'])) {
             $result = $this->common_model->upload_image($_FILES['user_pic'], './' . USER_PROFILE_IMG);
             // echo "<pre>";print_r($user_img_repo);die;
                // $user_img = isset($user_img_repo['filename']) && !empty($user_img_repo['filename']) ? base_url() . USER_PROFILE_IMG . $user_img_repo['filename'] : '';
            }

        
            if(isset($result['status']) == "error")
            {
                $this->response(array("status"=>false, "message"=> strip_tags($result['message'])), 300); exit;               
            }
          else
            {
                $profile_data['user_pic']=$result['filename'];
            }
            
        }
        if($postData)
        {            
            $where = array("user_id" => $user_id);            
            $update_profile = $this->common_model->update(TB_USERS,$where,$profile_data);
            if($update_profile)
            {
                $link = base_url().USER_PROFILE_IMG.$profile_data['user_pic'];
                
                $arr_result = array(
                        'status' => true, 
                        'msg' => 'User profile pic uploaded successfully.',
                        'url' => $link,
                        'user_id' => $user_id
                        );
                  $this->response(array("status"=>true,'result'=>$arr_result), 200); exit;                
            }
            else{
                /////erroe msg
                $this->response(array("status"=>false, "message"=> 'Something went wrong !!!'), 300); exit;
            }            
        }else 
        {
             $this->response(array("status"=>false, "message"=> 'Something went wrong !!!'), 300); exit;
        }
        
    }

    /**
     * Check if user exists
     */
    public function user_exists($user_id){ 
         $cond = array("user_id" => $user_id,"user_status"=>"1");
         $exist_users = $this->data_model->getAllUsers($cond); 
         if(count($exist_users)>0) {
             return 1;
         }
         else{
             return 0;
         }
    }

/**
     * Edit user
     * Added by Rajendra pawar
     */
    /*  public function user_edit_post() {
        $data = $_POST;
        if (isset($data['user_id']) && $data['user_id'] > 0) {
            //get user_type

            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_role=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result))
            {
                $this->response(array('status' => false, 'error' => 'Invalid user'), 300);exit;
            } else {                   
            //check email 
            $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null);    
            if ($emailExist) {
                $this->response(array("status" => false, "message" => "Duplicate email!!!"));exit;
            }
             if ($result[0]['role'] == USER_TYPE_SERVICE_PROVIDER) {
                $booking_data=$this->common_model->select("service_provider_user_id",TB_SERVICE_REQUESTS,array("service_provider_user_id"=>trim($data['user_id'])));    

                if (count($booking_data) > 0 && (isset($data['category']) || isset($data['sub_category']))) {
                    $this->response(array("status" => false, "message" => "Category and sub_category can't able to update because booking for this service user"));exit;
                }                
            }
            $user_birth_date = $this->post('user_birth_date');
            //upload data
            if (isset($_FILES['user_pic']) && !empty($_FILES['user_pic'])) {
             $user_img_repo = $this->common_model->upload_image($_FILES['user_pic'], './' . USER_PROFILE_IMG);
                $user_img = isset($user_img_repo['filename']) && !empty($user_img_repo['filename']) ? base_url() . USER_PROFILE_IMG . $user_img_repo['filename'] : '';
            }
            if (isset($_FILES['certificate']) && !empty($_FILES['certificate'])) {
                $certificate_repo = $this->common_model->upload_image($_FILES['certificate'], './' . CERTIFICATE_IMG_PATH);
                $certificate = isset($certificate_repo['filename']) && !empty($certificate_repo['filename']) ? base_url() . CERTIFICATE_IMG_PATH . $certificate_repo['filename'] : '';
            }
            if (isset($_FILES['license_pic']) && !empty($_FILES['license_pic'])) {
                $license_pic_repo = $this->common_model->upload_image($_FILES['license_pic'], './' . LICENSE_IMG_PATH);
                $license_pic = isset($license_pic_repo['filename']) && !empty($license_pic_repo['filename']) ? base_url() . LICENSE_IMG_PATH . $license_pic_repo['filename'] : '';
            }
            if (isset($_FILES['car_pic']) && !empty($_FILES['car_pic'])) {
                $car_pic_repo = $this->common_model->upload_image($_FILES['car_pic'], './' . CAR_PIC_IMG_PATH);
                $car_pic = isset($car_pic_repo['filename']) && !empty($car_pic_repo['filename']) ? base_url() . CAR_PIC_IMG_PATH . $car_pic_repo['filename'] : '';
            }

            //create user
            $userdata = array(
                'user_email' => $this->post('user_email'),
                'user_name' => $this->post('user_name'),
                'user_phone_number' => $this->post('mobile_no'),
                'user_gender' => $this->post('gender'),
                'user_address' => $this->post('address'),
                'user_no_of_kids' => $this->post('user_no_of_kids'),
                'user_device_id' => $this->post('user_device_id'),
                'user_device_tocken' => $this->post('user_device_tocken'),
                'user_device_type' => $this->post('user_device_type'),
                'user_birth_date' => $user_birth_date && (strtotime($user_birth_date) > 0) ? date('Y-m-d', strtotime($user_birth_date)) : '',
                'user_age' => $this->post('user_age'),
                'is_your_own_car' => $this->post('is_your_own_car'),
                'driving_license_type' => $this->post('driving_license_type'),
                'license_no' => $this->post('license_no'),
                'valid_from_date' => $this->post('valid_from_date'),
                'valid_until_date' => $this->post('valid_until_date'),
                'car_model' => $this->post('car_model'),
                'car_register_no' => $this->post('car_register_no'),
                'car_age' => $this->post('car_age'),
                'car_pic' => isset($car_pic) ? $car_pic : '',
                'license_pic' => isset($license_pic) ? $license_pic : '',
                'school' => $this->post('school'),
                'degree' => $this->post('degree'),
                'specialization' => $this->post('specialization'),
                'education_from' => $this->post('education_from'),
                'education_to' => $this->post('education_to'),
                'certification_in' => $this->post('certification_in'),
                'certificate' => isset($certificate) ? $certificate : '',
                'user_pic' => isset($user_img) ? $user_img : '',
                'user_category_id' => $this->post('category'),
                'user_sub_category_id' => $this->post('sub_category'),
            );

            try {
               $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
            } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()));exit;
            }
            
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
        } else {
            $this->response(array('status' => false, 'error' => 'Provide user_id'), 300);exit;
        }
    } */

     /**
     * Edit user
     * Added by Rajendra pawar
     */
   function services_list_get() {
    $result_1 =$this->common_model->select("category",TB_CATEGORIES,array("status"=>1));
    $result_2 =$this->common_model->select("name as sub_categories",TB_SUBCATEGORIES,array("status"=>1));
    $data= array("category"=> $result_1,"sub_categories"=> $result_2);
    $this->response(array('status' => true, 'message' => $data),200);exit;
    }

    /**
     * Edit user
     * Added by Rajendra pawar
     */
    public function user_edit_post() {
        $data = $_POST;
        if (isset($data['user_id']) && $data['user_id'] > 0) {
        
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result))
            {
                $this->response(array('status' => false, 'message' => 'Invalid user'), 300);exit;
            } else {                   
            //check email 
             $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null); 

            if ($emailExist) {
                $this->response(array("status" => false, "message" => "Duplicate email!!!"),300);exit;
            }           

            $userdata = array(
                'user_name' => $this->post('user_name'),
                'user_gender' => $this->post('gender'),
                'user_address' => $this->post('address'),
                'user_email' => $this->post('user_email'),                
                'user_phone_number' => $this->post('mobile_no'),             
               
            );

            try {
               $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
            } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
            }
            
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
        } else {
            $this->response(array('status' => false, 'message' => 'Provide user_id'), 300);exit;
        }
    }


   public function kid_edit_post() {
        $data = $_POST;

        $kidData = array(
                'kid_name' => $this->post('kid_name'),
                'kid_gender' => $this->post('kid_gender'),
                //'kid_age' => $this->post('kid_age'),
                'user_id' => $this->post('user_id'),   
                'kid_birthdate'=> $this->post('dob'),     
                'kid_status' =>1,
            );
        if (isset($data['user_id']) && $data['user_id'] > 0) { 
            
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result))
            {
                $this->response(array('status' =>false, 'message' => 'Invalid user'), 300);exit;
            }
        if (isset($data['kid_id']) && $data['kid_id'] > 0) {
              try {
               $this->common_model->update(TB_KIDS,array("kid_id"=>$data['kid_id']),$kidData);
               $this->response(array('status' => true, 'message' => 'Kid updated successfully.'), 200);exit;
            } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
            }

         } else{
         try {
               $this->common_model->insert(TB_KIDS,$kidData);
               $this->response(array('status' => true, 'message' => 'Kid saved successfully.'), 200);exit;
            } catch (Exception $ex) {
                $this->response(array('status' => false, 'message' => $ex->getCode()),300);exit;
            }
      

     }
    } else{
       $this->response(array('status' =>false, 'message' => 'Provide user_id'), 300);exit; 
    }
  }

   /**
     * validate_booking
     * @param type $post
     */
    function validate_booking($post) {
        
        $bookingdata = [];
        $duration = array(DAILY,WEEKLY,MONTHLY);
        if (trim($post['duration']) == "") {
            $this->response(array('status' => false, 'message' => 'Please enter duration daily or weekly or monthly'), 300);exit;
        }
        if(!in_array(trim($post['duration']),$duration)) {
            $this->response(array('status' => false, 'message' => $post['duration'].' Invalid duration'), 300);exit;
        }
        if (trim($post['booking_service_type']) == MY_PREFERENCE && (!isset($post['booking_my_preference_service_users']) || !is_array($post['booking_my_preference_service_users']))) {
            $this->response(array('status' => false, 'message' => 'You must provide an booking_my_preference_service_users in array format.'), 300);exit;
        }
        if (trim($post['booking_service_type']) == MY_MATCHES && (isset($post['booking_service_user']) && $post['booking_service_user'] < 1)) {
            $this->response(array('status' => false, 'message' => 'You must provide an booking_service_user.'), 300);exit;
        }
        if ($post['duration'] == DAILY && strtotime($post['date']) < strtotime(date('Y-m-d'))) {
            $this->response(array('status' => false, 'message' => 'Please enter date greater than today and date format is Y-m-d(2018-01-31)'), 300);exit;
        }
        if (($post['duration'] == WEEKLY || $post['duration'] == MONTHLY) && ((strtotime($post['start_date']) < strtotime(date('Y-m-d'))) || (strtotime($post['start_date']) > strtotime($post['end_date'])) || (strtotime($post['end_date']) < strtotime($post['start_date'])))) {
            $this->response(array('status' => false, 'message' => 'Please enter valid start and end date and date format is Y-m-d(2018-01-31)'), 300);exit;
        }
        $user = $this->common_model->select("user_status,user_type,roleId",TB_USERS,array('user_id'=>$post['user_id']));
        if (!$user) {
            $this->response(array('status' => false, 'message' => 'Invalid user'), 300);exit;
        }

        $service_provider = $this->common_model->select("user_id",TB_USERS,array('user_id'=>$this->post('service_provider_user_id'),'roleId'=>ROLE_SERVICE_PROVIDER,'user_status'=>'1'));
       
        if (!$service_provider) {
            $this->response(array('status' => false, 'message' => 'Invalid service provider, please enter valid service_provider_user_id.'), 300);exit;
        }

        

        if ($user[0]['user_status'] != 1) {
            $this->response(array('status' => false, 'message' => 'User not active'), 300);exit;
        }
        if ($user[0]['roleId'] != USER_TYPE_PARENT) {
            $this->response(array('status' => false, 'message' => 'User type not parent'), 300);exit;
        }

        $catdata = $this->common->validate_category($post);
        if (trim($post['duration']) == DAILY) {
            $bookingdata['booking_date'] = date('Y-m-d', strtotime($post['date']));
            $bookingdata['booking_total_days'] = 1;
        } else {
            $bookingdata['booking_start_date'] = date('Y-m-d', strtotime($post['start_date']));
            $bookingdata['booking_end_date'] = date('Y-m-d', strtotime($post['end_date']));
            $start_date = $post['start_date'];
            $datetime1 = new DateTime($post['start_date']);
            $datetime2 = new DateTime($post['end_date']);
            $interval = date_diff($datetime1, $datetime2);
            
            $bookingdata['booking_total_days'] = $interval->days;
            if ($post['duration'] == 'weekly') {
                $bookingdata['booking_total_weeks'] = ceil(($interval->days) / 7);
            }
            if ($post['duration'] == 'monthly') {
                $bookingdata['booking_total_moths'] = ($interval->format('%m')) + 1;
            }
        }
        
        $bookingdata['booking_pick_up_location'] = $post['pick_up_location'];
        $bookingdata['pick_up_location_lat'] = $post['pick_up_location_lat'];
        $bookingdata['pick_up_location_lng'] = $post['pick_up_location_lng'];
        $bookingdata['booking_drop_off_location'] = $post['drop_off_location'];
        $bookingdata['drop_off_location_lat'] = $post['drop_off_location_lat'];
        $bookingdata['drop_off_location_lng'] = $post['drop_off_location_lng'];
        $bookingdata['booking_pick_up_time'] = $post['pick_up_time'];
        $bookingdata['booking_drop_off_time'] = $post['drop_off_time'];
        $bookingdata['booking_category_id'] = $catdata['category_id'];
        $bookingdata['booking_sub_category_id'] = isset($catdata['sub_category_id']) ? $catdata['sub_category_id'] : '';
        $bookingdata['booking_secret_code'] = $post['secret_code'];
        $bookingdata['booking_duration'] = $post['duration'];
        $bookingdata['user_id'] = $post['user_id'];
        $booking_service_type = json_decode(SERVICE_TYPES, true);
        $bookingdata['booking_service_type'] = $post['booking_service_type'] && in_array($post['booking_service_type'], $booking_service_type) ? $post['booking_service_type'] : PLACE_REQUEST;
        $bookingdata['booking_no_off_kids'] = count($post['child_id']);
        
      //  print_r($bookingdata);die;
        return $bookingdata;
    }

      /**
     * booking post
     */
    function booking_post() {
        $data = $this->post();        
        $bookingdata = $this->validate_booking($data);
        if ($bookingdata) {
            $booking = $this->db->insert(TB_BOOKINGS, $bookingdata);
            $booking_id = $this->db->insert_id();
            if ($booking_id) {
                $booking_service_user = '';
                if ($this->post('booking_service_type') == MY_PREFERENCE) {
                    $booking_service_user = $this->post('booking_my_preference_service_users');
                }
                if ($this->post('booking_service_type') == MY_MATCHES) {
                    $booking_service_user = $this->post('booking_service_user');
                }

                if ($this->post('child_id')) {
                    $user_kids = $this->post('child_id');
                        $kids_all = array(
                        'kids_id' => isset($user_kids) ? $user_kids : '',
                        'booking_id' => $booking_id
                        );
                                       
                $this->db->insert(TB_KIDS_BOOKING, $kids_all);
                }
                $respone['booking_status']['success'] = 'Booking has been saved successfully';
                $respone['booking_status']['booking_id'] = $booking_id;
               
                $service_request = array(
                    "service_booking_id"=> $booking_id,
                    "service_parent_user_id"=> $this->post('user_id'),
                    "service_provider_user_id"=> $this->post('service_provider_user_id'),
                    "service_request_status"=>PENDING
                    );
                 $result = $this->db->insert(TB_SERVICE_REQUESTS, $service_request);

                 if (!$result) {
                 $respone['message'] = 'Request not send to driver, please try again!!!';
                } else {
                    $userData  = $this->common_model->select('user_id,user_name',TB_USERS,array('user_id' => $this->post('user_id')));
                    $userName = isset($userData[0]['user_name'])?$userData[0]['user_name']:"Someone";
                    $message = $userName." has booked a ride"; 
                    $service_provider_device_id = $this->common_model->select("user_device_id",TB_USERS,array("user_id"=>$this->post('service_provider_user_id')));
                    $device_id = isset($service_provider_device_id[0]['user_device_id'])?$service_provider_device_id[0]['user_device_id']:"";
                    $myData = array("message"=>"Booking the ride please check it.");
                    $this->sendPushNotification($device_id,$message,$myData);

                    $checkAlreadyMyPref = $this->common_model->select("mp_id,trip_count",TB_MY_PREFERENCE,array("parent_id"=>$this->post('user_id'),"driver_id"=>$this->post('service_provider_user_id')));
                    if(count($checkAlreadyMyPref) > 0) {
                        $my_preference = array(
                            "trip_count" =>$checkAlreadyMyPref[0]['trip_count']+1,
                            "updated_at" => date('Y-m-d H:i:s')
                        );
                        $result = $this->common_model->update(TB_MY_PREFERENCE,array("parent_id"=>$this->post('user_id'),"driver_id"=>$this->post('service_provider_user_id')),$my_preference);
                    } else {
                        $my_preference = array(
                            "parent_id" =>$this->post('user_id'),
                            "driver_id" =>$this->post('service_provider_user_id'),
                            "trip_count" =>1,
                            "created_at" => date('Y-m-d H:i:s')
                        );
                        $result = $this->db->insert(TB_MY_PREFERENCE, $my_preference);

                    }                    
                    $respone['message'] =  'Request has been sent successfully';
                } 
                $this->response(array('status' => true, 'message' => $respone), 200);exit;
            }
        }
        $this->response(array('status' => false, 'message' => 'Some error has been occurred!!!'), 300);exit;
    }

      /**
     * my_bookings
     * @param type $user_id
     */
    function my_bookings_post() {
        $user_id = $_POST['user_id'];
        if ($user_id > 0) {
                $post = $this->post();       
                $catdata = $this->common->validate_category($this->post());
                $cond = ['bookings.booking_category_id' => $catdata['category_id']];
                if (isset($catdata['sub_category_id']) && $catdata['sub_category_id'] > 0) {
                    $cond['bookings.booking_sub_category_id'] = $catdata['sub_category_id'];
                }
                $user = $this->common_model->select("user_status,user_type,roleId",TB_USERS,array('user_id'=>$post['user_id']));
                if (count($user) > 0) {
                    if ($user[0]['roleId'] == USER_TYPE_SERVICE_PROVIDER) {
                        $like = array();
                        $conds = array(TB_BOOKINGS.".user_id"=>$user_id,"booking_status"=>$this->post('booking_status'));
                        $select = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id,kid_id,kid_name,kid_gender,kid_birthdate,".TB_CATEGORIES.".category,".TB_SUBCATEGORIES.".name,kid_age,kid_status";
                        $table_name = TB_BOOKINGS;
                        $join = array(
                            TB_CATEGORIES => TB_CATEGORIES . '.category_id=' . TB_BOOKINGS . '.booking_category_id',
                            TB_SUBCATEGORIES => TB_SUBCATEGORIES . '.sub_category_id=' . TB_BOOKINGS . '.booking_sub_category_id',
                            TB_KIDS => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id');
                        $default_order_column = "created_at";

                        $service_requests = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array(),'',$join);
                       
                        foreach ($service_requests as $key=>$service_request) {
                            $data[$key]['booking_id'] = $service_request['booking_id'];
                            $data[$key]['booking_duration'] = $service_request['booking_duration'];
                            $data[$key]['booking_date'] = $service_request['booking_date'];
                            $data[$key]['booking_total_days'] = $service_request['booking_total_days'];
                            $data[$key]['booking_pick_up_location'] = $service_request['booking_pick_up_location'];
                            $data[$key]['pick_up_location_lat'] = $service_request['pick_up_location_lat'];
                            $data[$key]['pick_up_location_lng'] = $service_request['pick_up_location_lng'];
                            $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                            $data[$key]['drop_off_location_lat'] = $service_request['drop_off_location_lat'];
                            $data[$key]['drop_off_location_lng'] = $service_request['drop_off_location_lng'];
                            $data[$key]['booking_pick_up_time'] = $service_request['booking_pick_up_time'];
                            $data[$key]['booking_drop_off_time'] = $service_request['booking_drop_off_time'];
                            $data[$key]['booking_category_id'] = $service_request['booking_category_id'];
                            $data[$key]['booking_sub_category_id'] = $service_request['booking_sub_category_id'];
                            $data[$key]['booking_no_off_kids'] = $service_request['booking_no_off_kids'];
                            $data[$key]['booking_price'] = $service_request['booking_price'];
                            $data[$key]['booking_total_weeks'] = $service_request['booking_total_weeks'];
                            $data[$key]['booking_total_moths'] = $service_request['booking_total_moths'];
                            $data[$key]['booking_status'] = $service_request['booking_status'];
                            $data[$key]['user_id'] = $service_request['user_id'];
                            $data[$key]['booking_service_type'] = $service_request['booking_service_type'];
                            $data[$key]['created_at'] = $service_request['created_at'];
                            $data[$key]['category'] = $service_request['category'];
                            $data[$key]['sub_category'] = $service_request['name'];
                            $data[$key]['kid_id'] = $service_request['kid_id'];
                            $data[$key]['kid_name'] = $service_request['kid_name'];
                            $data[$key]['kid_gender'] = $service_request['kid_gender'];
                            $data[$key]['kid_birthdate'] = $service_request['kid_birthdate'];
                            $data[$key]['kid_age'] = $service_request['kid_age'];
                        }
                    } else {
                        $cond['user_id'] = $user_id;
                        $cond['bookings.booking_status'] = $this->post('booking_status');
                        
                        $like = array();
                        $conds = array(TB_BOOKINGS.".user_id"=>$user_id,"booking_status"=>$this->post('booking_status'));
                        $select = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id,kid_id,kid_name,kid_gender,kid_birthdate,kid_age,kid_status";
                        $table_name = TB_BOOKINGS;
                        $join = array(
                            TB_KIDS => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id');
                        $default_order_column = "created_at";

                        $service_requests = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array(),'',$join);
                         
                        $data = array();
                        foreach ($service_requests as $key => $service_request) {
                            $data[$key]['booking_id'] = $service_request['booking_id'];
                            $data[$key]['booking_duration'] = $service_request['booking_duration'];
                            $data[$key]['booking_date'] = $service_request['booking_date'];
                            $data[$key]['booking_total_days'] = $service_request['booking_total_days'];
                            $data[$key]['booking_pick_up_location'] = $service_request['booking_pick_up_location'];
                            $data[$key]['pick_up_location_lat'] = $service_request['pick_up_location_lat'];
                            $data[$key]['pick_up_location_lng'] = $service_request['pick_up_location_lng'];
                            $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                            $data[$key]['drop_off_location_lat'] = $service_request['drop_off_location_lat'];
                            $data[$key]['drop_off_location_lng'] = $service_request['drop_off_location_lng'];
                            $data[$key]['booking_pick_up_time'] = $service_request['booking_pick_up_time'];
                            $data[$key]['booking_drop_off_time'] = $service_request['booking_drop_off_time'];
                            $data[$key]['booking_category_id'] = $service_request['booking_category_id'];
                            $data[$key]['booking_sub_category_id'] = $service_request['booking_sub_category_id'];
                            $data[$key]['booking_no_off_kids'] = $service_request['booking_no_off_kids'];
                            $data[$key]['booking_price'] = $service_request['booking_price'];
                            $data[$key]['booking_total_weeks'] = $service_request['booking_total_weeks'];
                            $data[$key]['booking_total_moths'] = $service_request['booking_total_moths'];
                            $data[$key]['booking_status'] = $service_request['booking_status'];
                            $data[$key]['user_id'] = $service_request['user_id'];
                            $data[$key]['booking_service_type'] = $service_request['booking_service_type'];
                            $data[$key]['created_at'] = $service_request['created_at'];
                            $data[$key]['kid_id'] = $service_request['kid_id'];
                            $data[$key]['kid_name'] = $service_request['kid_name'];
                            $data[$key]['kid_gender'] = $service_request['kid_gender'];
                            $data[$key]['kid_birthdate'] = $service_request['kid_birthdate'];
                            $data[$key]['kid_age'] = $service_request['kid_age'];
                        }

                    }
                    $result['booking_details'] = $data;
                    $this->response(array('status' => true, 'result' => $result),200);exit;
                } else {
                    $this->response(array('status' => false, 'message' => 'Invalid user_id'), 300);exit;
                }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide user_id'), 300);exit;
        }
    }


    /**
     * accept_or_cancel_get
     * @param type $service_request_id
     * @param type $status
     */
   /* function accept_or_cancel_post() {
        $service_request_id = $_POST['service_request_id'];
        if ($service_request_id > 0) {
            // Check service request is exist or not 
            
            $service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id));
            
            if (!$service_requests) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300);exit;
            }

            //start service request
            if ($_POST['status'] == 'ACCEPTED') {
                //Check that booking already started by anouther one 

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>'IN_PROCESS'));

                //Check another service not started in that duration
                //hold for the moment
                $validbooking = 1;
                if ($validbooking) {
                    if (count($is_service_requests) < 1) {
                        //Start that request
                        $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>'IN_PROCESS'));
                        $updateData = array('service_request_status' => 'IN_PROCESS');
                        $start_service =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id,"service_request_status"=>'PENDING'),$updateData);

                        $updateData = array('booking_status' => 'IN_PROCESS');
                        $start_booking =  $this->common_model->update(TB_BOOKINGS,array("booking_id"=>$service_requests[0]['service_booking_id'],"booking_status"=>'PENDING'),$updateData);
                        
                        if ($start_service && $start_booking) {
                            $this->response(array('status' => true, 'message' => 'Service accepted successfully'));exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Service already accepted'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'You can\'n able to start this request, because already another booking is in process in that duration'), 300);exit;
                }
            }

            //cancel service request
            if ($_POST['status'] == 'CANCELLED') {

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_booking_id'=>$service_requests[0]['service_booking_id'],'service_request_status'=>'CANCELLED'));

                if (count($is_service_requests) < 1) {
                    //Cancel that request                
                    $cancelservice = Servicerequest::where(['service_requests.service_request_id' => $service_request_id])->update(['service_requests.service_request_status' => $_POST['status']]);

                    $updateData = array('service_request_status' => 'CANCELLED');
                    $cancelservice =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id),$updateData);


                    if ($cancelservice) {
                        $this->response(array('status' => true, 'message' => 'Service has been cancelled successfully'));exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service has been already cancelled'), 300);exit;
                }
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300);exit;
        }
    } */
     function booking_status_post() {
        $service_request_id = $_POST['service_request_id'];
        if ($service_request_id > 0) {
            // Check service request is exist or not 
            
            $service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id));
            
            if (!$service_requests) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300);exit;
            }

            //Accept service request
            if ($_POST['status'] == ACCEPTED) {
                //Check that booking already started by anouther one 

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>IN_PROCESS));

                //Check another service not started in that duration
                //hold for the moment
                $validbooking = 1;
                if ($validbooking) {
                    if (count($is_service_requests) < 1) {
                        //Start that request
                        
                       $accept_service =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id,"service_request_status"=>PENDING),array('service_request_status' => IN_PROCESS));

                        $accept_booking =  $this->common_model->update(TB_BOOKINGS,array("booking_id"=>$service_request_id,"booking_status"=>PENDING),array('booking_status' => IN_PROCESS));
                        
                        if ($accept_service && $accept_booking) {
                            $this->response(array('status' => true, 'message' => 'Service has been accepted successfully'),200);exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Service already accepted'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'You can\'n able to start this request, because already another booking is in process in that duration'), 300);exit;
                }
            }

            //cancel service request
            if ($_POST['status'] == CANCELLED) {

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_booking_id'=>$service_request_id,'service_request_status'=>CANCELLED));

                if (count($is_service_requests) < 1) {                    
                    
                    $cancel_service =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id),array('service_request_status' => CANCELLED));

                    $cancel_booking=  $this->common_model->update(TB_BOOKINGS,array('booking_id' => $service_request_id),array('booking_status' => CANCELLED));

                    if ($cancel_service & $cancel_booking) {
                        $this->response(array('status' => true, 'message' => 'Service has been cancelled successfully'),200);exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service already cancelled'), 300);exit;
                }
            }


             // Start service request
            if ($_POST['status'] == START) {
              $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>START));
             //echo $this->db->last_query();die;

                     if (count($is_service_requests) > 0) {

                     $this->response(array('status' => false, 'message' => 'Service already started.'), 300);exit;
                    } else{
                                  
                     $start_service = $this->common_model->update(TB_SERVICE_REQUESTS,array('service_request_id' => $service_request_id),array('service_request_status' => START));           

                        if ($start_service) {
                            $this->response(array('status' => true, 'message' => 'Service has been started successfully'),200);exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                        }
                    } 
                 
            }

            // End service request
            if ($_POST['status'] == END) {
              $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>END));

                     if (count($is_service_requests) > 0) {
                        $this->response(array('status' => false, 'message' => 'Service already end.'), 300);exit;
                    } else{
                                  
                     $end_service = $this->common_model->update(TB_SERVICE_REQUESTS,array('service_request_id' => $service_request_id),array('service_request_status' => END));  

                     $end_booking=  $this->common_model->update(TB_BOOKINGS,array('booking_id' => $service_request_id),array('booking_status' => COMPLETED));        

                        if ($end_service && $end_booking) {
                              $this->response(array('status' => true, 'message' => 'Service has been ended successfully'),200);exit;
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                        }
                    } 
                 
            }



        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300);exit;
        }
    }

    /**
     * slider_list_get 
     */
    function slider_list_get() {
        $sliderList = $this->common_model->select("id,title,banner_img",TB_BANNER,array("banner_status" => 1));
        $data = array();
        if(count($sliderList) > 0)
        {
            foreach ($sliderList as $key => $slider) {
                $data[$key]['id'] = $slider['id'];
                $data[$key]['title'] = $slider['title'];
                $data[$key]['banner_img'] = base_url().$slider['banner_img'];                    
            }
            $this->response(array('status' => true, 'slider_list' => $data),200);exit;
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'no slider images are available '), 300);exit;
        }
    }

    /**
     * start_or_end_service
     * @param type $service_request_id
     * @param type $status
     */
    function start_or_end_service_post() {
        $postData = $_POST;
        if ($postData['service_request_id'] > 0) {
            // Check service request is exist or not 
            $service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id' => $postData['service_request_id'], 'service_request_status' => IN_PROCESS));
            if (!$service_requests) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300);exit;
            }
            //Start service request
            if ($postData['status'] == START || $postData['status'] == END) {
                $checkstatus = AVAILABLE;
                $statuname = 'ended';
                if ($postData['status'] == START) {
                    $checkstatus = NOT_AVAILABLE;
                    $statuname = 'started';
                }
                //Check that sp already started service
                $is_service_requests = $this->common_model->select("*",TB_USERS,array('user_id' => $service_requests[0]['service_provider_user_id'], 'is_user_available' => $checkstatus));
                if (count($is_service_requests) < 1) {
                    //Update that service provider to not available
                    $update = $this->common_model->update(TB_USERS,array('user_id' => $service_requests[0]['service_provider_user_id']),array('is_user_available' => $checkstatus));
                    if ($update) {
                        if ($postData['status'] == END) {
                            //if end service complete that booking
                            $this->common_model->update(TB_SERVICE_REQUESTS,array('service_request_id' => $postData['service_request_id']),array('service_request_status' => COMPLETED));
                            $this->common_model->update(TB_BOOKINGS,array('booking_id' => $service_requests[0]['service_booking_id']),array('booking_status' => COMPLETED));
                        }
                        $this->response(array('status' => true, 'message' => 'Service ' . $statuname . ' successfully'),200);exit;
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);exit;
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service ' . $statuname . ' already'), 300);exit;
                }
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid status'), 300);exit;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300);exit;
        }
    }

    /**
     * price_list_get
     */
    function price_list_get() {
        $cond = array(TB_CATEGORIES.'.status'=>1,TB_SUBCATEGORIES.'.status'=>1);
        $jointype=array(TB_CATEGORIES=>"LEFT",TB_SUBCATEGORIES=>"LEFT");
        $join = array(TB_CATEGORIES=>TB_CATEGORIES.".category_id = ".TB_RIDE_PRICE.".c_id",TB_SUBCATEGORIES=>TB_SUBCATEGORIES.".sub_category_id = ".TB_RIDE_PRICE.".sc_id");
        $priceList = $this->Common_model->selectJoin("rp_id,name,category,rp_cost,rp_per_km,rp_per_hour",TB_RIDE_PRICE,$cond,array(),$join,$jointype);
        $data = array();
        if(count($priceList) > 0)
        {
            $keyCat = $keyTut = 0;
            foreach ($priceList as $key => $price) {
                if($price['category'] == "Rides") {                   
                    $ride[$key]['id'] = $price['rp_id'];
                    $ride[$key]['category'] = $price['category'];
                    $ride[$key]['sub_category'] = $price['name'];
                    $ride[$key]['cost'] = '$'.$price['rp_cost'];
                    $ride[$key]['hour'] = $price['rp_per_hour'];
                }
                else if($price['category'] == "After School Care") {
                    $afterSchoolCare[$keyCat]['id'] = $price['rp_id'];
                    $afterSchoolCare[$keyCat]['category'] = $price['category'];
                    $afterSchoolCare[$keyCat]['sub_category'] = $price['name'];
                    $afterSchoolCare[$keyCat]['cost'] = '$'.$price['rp_cost'];
                    $afterSchoolCare[$keyCat]['km'] = $price['rp_per_km'];
                    $keyCat++; 
                }
                else if($price['category'] == "Premier Tutoring") {
                    $tutoring[$keyTut]['id'] = $price['rp_id'];
                    $tutoring[$keyTut]['category'] = $price['category'];
                    $tutoring[$keyTut]['sub_category'] = $price['name'];
                    $tutoring[$keyTut]['cost'] = '$'.$price['rp_cost'];
                    $tutoring[$keyTut]['hour'] = $price['rp_per_hour'];
                    $keyTut++;
                }
                else {

                }
                
            }

            $data['Rides'] = $ride;
            $data['After School Care'] = $afterSchoolCare;
            $data['Premier Tutoring'] = $tutoring; 
            $this->response(array('status' => true, 'price_list' => $data),200);exit;
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'No data are available '), 300);exit;
        }      
    }

    /**
     * read_notification_post
     * @param type userId
     * @param type notifId
     */
    function read_notification_post() {
        $postData = $_POST;
        if($postData['userId'] != "" && $postData['notifId'] != "") {
            $checkNotif = $this->common_model->select("n_id",TB_NOTIFICATION,array('user_id' => $postData['userId'],'n_id'=>$postData['notifId']));
            if(count($checkNotif) > 0 ) {
                 $updateNotif = $this->common_model->update(TB_NOTIFICATION,array('user_id' => $postData['userId'],'n_id'=>$postData['notifId']),array('n_read' => "0"));
                if($updateNotif) {
                    $this->response(array('status' => true, 'message' => 'The notification has been read successfully .'), 200);exit;
                } else {
                    $this->response(array('status' => false, 'message' => 'You have already read notification.'), 300);exit;
                }
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300);exit;
            }           
        } else {
            $this->response(array('status' => false, 'message' => 'All fields are required. '), 300);exit;
        }        
    }

    /**
     * notification_list
     * @param type userId
     * @param type notifId
     */

    function notification_listing_post() {
        $postData = $_POST;
        if($postData['userId'] != "" && $postData['notifId'] != "") {
            $checkNotif = $this->common_model->select("n_content,user_id,n_id",TB_NOTIFICATION,array('user_id' => $postData['userId'],'n_id'=>$postData['notifId'],'n_read'=> "1"));
            if($checkNotif) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.','notification_list'=>$checkNotif), 200);exit;
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300);exit;
            }                 
        } else {
            $checkNotif = $this->common_model->select("n_content,user_id,n_id",TB_NOTIFICATION,array('n_read'=> "1"));
            if(count($checkNotif) > 0) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.','notification_list'=>$checkNotif), 200);exit;
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300);exit;
            }            
        }     
    }

    /**
     * faq_list
     * @param No paramater
     */
    function faq_list_get() {
        $cond = array(TB_CATEGORIES.'.status'=>1,TB_SUBCATEGORIES.'.status'=>1);
        $jointype=array(TB_CATEGORIES=>"LEFT",TB_SUBCATEGORIES=>"LEFT");
        $join = array(TB_CATEGORIES=>TB_CATEGORIES.".category_id = ".TB_FAQ.".category_id",TB_SUBCATEGORIES=>TB_SUBCATEGORIES.".sub_category_id = ".TB_FAQ.".subcategory_id");
        $faqList = $this->Common_model->selectJoin("id,name,category,faq_description,faq_title",TB_FAQ,$cond,array(),$join,$jointype);
       $data = array();           
        if(count($faqList) > 0) {
            foreach ($faqList as $key => $faq) {
                $data[$key]['id'] = $faq['id'];
                $data[$key]['categoryName'] = $faq['category'];
                $data[$key]['subCategoryName'] = $faq['name'];
                $data[$key]['description'] = $faq['faq_description'];
                $data[$key]['title'] = $faq['faq_title'];
            }
            $this->response(array('status' => true, 'message' => 'FAQ list.','faq_list'=>$data), 200);exit;
        } else {
            $this->response(array('status' => false, 'message' => 'Faq data not available.'), 300);exit;
        }       
    }

    /**
     * tutoring_list_get
     */
    function tutoring_list_get() {
        $cond = array();       
        $join = array(TB_TEACHERS=>TB_TEACHERS.".id = ".TB_SUBJECT_MASTER.".teacher_id");
        $subList = $this->Common_model->select_join(TB_TEACHERS.".id,sub_id,teacher_name",TB_SUBJECT_MASTER,$cond,array(),array(),$join);
        $data = array();
        if(count($subList) > 0)
        {
            foreach ($subList as $key => $sub) {
                $subArray = explode(',', $sub['sub_id']);
                $subjects = $this->Common_model->select_where_in("id,sub_name",TB_SUBJECTS,array(),$subArray);
                $data[$sub['teacher_name']]=$subjects;
            }
            $this->response(array('status' => true, 'teacher_list' => $data),200);exit;
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'No data are available '), 300);exit;
        }      
    }

        /**
     * service_list
     * @param category_id
     * @param sub_category_id
     * @param status
     */
    public function getServiceList_post() {
        $postData = $_POST;
        if (($postData['category_id'] > 0) && ($this->input->post('status') != "") && ($postData['sub_category_id'] > 0))
        {
            // Check category request is exist or not 
            $category_requests = $this->common_model->select("category_id",TB_CATEGORIES,array('category_id' => $postData['category_id'], 'status' => '1'));
            if (!$category_requests) 
            {
                $this->response(array('status' => false, 'message' => 'Category request not available'), 300);exit;
            }
            else
            {
                // Check sub category request is exist or not 
                $sub_category_requests = $this->common_model->select("sub_category_id",TB_SUBCATEGORIES,array('sub_category_id' => $postData['sub_category_id'], 'status' => '1'));
                if (!$sub_category_requests) 
                {
                    $this->response(array('status' => false, 'message' => 'Sub category request not available'), 300);exit;
                }
                else
                {
                    if($postData['status'] != "") 
                    {
                        // Check status request is exist or not 
                        $status_requests = $this->common_model->select("booking_id",TB_BOOKINGS,array('booking_status' => $postData['status']));
                        if (!$status_requests) 
                        {
                            $this->response(array('status' => false, 'message' => 'Status request not available'), 300);exit;
                        }
                        else 
                        {
                            $result = $this->common_model->selectJoin(TB_USERS.".user_id,booking_id,user_name,user_pic",TB_USERS,array("booking_status"=>trim($postData['status']),"booking_category_id"=>trim($postData['category_id']),"booking_sub_category_id"=>trim($postData['sub_category_id'])),array(),array(TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id'));
                            if(count($result) > 0)
                            {
                                $this->response(array('status' => true, 'service_list'=>$result), 200);exit;
                            }
                            else
                            {
                                $this->response(array('status' => false, 'message' => 'Data not found'), 300);exit;
                            }
                        }
                    }
                    else
                    {
                        $this->response(array('status' => false, 'message' => 'Status is required'), 300);exit;
                    }
                }
            }            
        }
        else
        {
           $this->response(array('status' => false, 'message' => 'Please provide category id, sub category id & status'), 300);exit;
        }        
    }

    /**
     * booking_service_view
     * @param category_id
     * @param sub_category_id
     * @param status
     * @param user_id
     * @param booking_id
     */
  /*  public function booking_service_view_post() {
        $postData = $_POST;
        if ($postData['category_id'] != "" && $postData['booking_id'] != "" && $postData['status'] != "" && $postData['sub_category_id'] != "" && $postData['user_id'] != "")
        {
            $result = $this->common_model->selectJoin(TB_USERS.".user_id,user_name,user_phone_number,booking_pick_up_location,booking_drop_off_location,booking_pick_up_time,booking_drop_off_time,booking_duration,booking_id",TB_USERS,array("booking_status"=>trim($postData['status']),"booking_category_id"=>trim($postData['category_id']),"booking_sub_category_id"=>trim($postData['sub_category_id']),TB_USERS.".user_id"=>trim($postData['user_id']),"booking_id"=>$postData['booking_id']),array(),array(TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id'));
            if(count($result) > 0)
            {
                $user_id = $result[0]['user_id'] ? $result[0]['user_id']:'';
                $parent_name = $result[0]['user_name'] ? $result[0]['user_name']:'';
                $parent_no = $result[0]['user_phone_number'] ? $result[0]['user_phone_number']:'';
                $pick_up_point = $result[0]['booking_pick_up_location'] ? $result[0]['booking_pick_up_location']:'';
                $drop_off_point = $result[0]['booking_drop_off_location'] ? $result[0]['booking_drop_off_location']:'';
                $pick_up_time = $result[0]['booking_pick_up_time'] ? $result[0]['booking_pick_up_time']:'';
                $drop_off_time = $result[0]['booking_drop_off_time'] ? $result[0]['booking_drop_off_time']:'';
                $service_type = $result[0]['booking_duration'] ? $result[0]['booking_duration']:'';
                $serviceResult = $this->common_model->select("booking_date,booking_start_date,booking_end_date,booking_total_days,booking_secret_code,booking_price,booking_total_weeks,booking_total_moths",TB_BOOKINGS,array("booking_duration"=>$service_type,"booking_id"=>$postData['booking_id']));
                if(count($serviceResult) > 0)
                {
                    if($service_type == "daily")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['days'] = $value['booking_total_days']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }                        
                    }
                    else if($service_type == "weekly")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date'];
                            $serviceList[$key]['weekly'] = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }

                    }
                    else
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date']; 
                            $serviceList[$key]['monthly'] = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }

                    }
                }
                else
                {
                    $serviceList = array(); 
                }                
                
                $kidResult = $this->common_model->select("kid_id,kid_gender,kid_age,kid_name",TB_KIDS,array("user_id"=>$postData['user_id']));
                if(count($kidResult) > 0)
                {
                    foreach ($kidResult as $key => $value) {
                        $kidsList[$key]['id'] = $value['kid_id'];
                        $kidsList[$key]['name'] = $value['kid_name'];
                        $kidsList[$key]['gender'] = $value['kid_gender'];
                        $kidsList[$key]['age'] = $value['kid_age'];                    
                    }
                }
                else 
                {
                    $kidsList = array();
                }
                $this->response(array('status' => true,'user_id'=>$user_id,'pick_up_point'=>$pick_up_point,'drop_off_point'=>$drop_off_point,'pick_up_time'=>$pick_up_time,'drop_off_time'=>$drop_off_time,'service_type'=>$service_type,'parent_no'=>$parent_no,'parent_name'=>$parent_name,'service_view'=>$serviceList,'kids_list'=>$kidsList), 200);exit;
            }
            else
            {
                $this->response(array('status' => false, 'message' => 'Data not found'), 300);exit;
            }
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters'), 300);exit;
        }
    } */

      /**
     * getServiceDetail     
     * @param user_id
     * @param booking_id
     */

    public function getServiceDetail_post() {
        $postData = $_POST;
        if ( $postData['booking_id'] != ""   && $postData['user_id'] != "")
        {
            $result = $this->common_model->selectJoin(TB_USERS.".user_id,user_name,user_phone_number,booking_pick_up_location,booking_drop_off_location,booking_pick_up_time,booking_drop_off_time,booking_duration,booking_id",TB_USERS,array(TB_USERS.".user_id"=>trim($postData['user_id']),"booking_id"=>$postData['booking_id']),array(),array(TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id'));
            if(count($result) > 0)
            {
                $user_id = $result[0]['user_id'] ? $result[0]['user_id']:'';
                $parent_name = $result[0]['user_name'] ? $result[0]['user_name']:'';
                $parent_no = $result[0]['user_phone_number'] ? $result[0]['user_phone_number']:'';
                $pick_up_point = $result[0]['booking_pick_up_location'] ? $result[0]['booking_pick_up_location']:'';
                $drop_off_point = $result[0]['booking_drop_off_location'] ? $result[0]['booking_drop_off_location']:'';
                $pick_up_time = $result[0]['booking_pick_up_time'] ? $result[0]['booking_pick_up_time']:'';
                $drop_off_time = $result[0]['booking_drop_off_time'] ? $result[0]['booking_drop_off_time']:'';
                $service_type = $result[0]['booking_duration'] ? $result[0]['booking_duration']:'';
                $serviceResult = $this->common_model->select("booking_date,booking_start_date,booking_end_date,booking_total_days,booking_secret_code,booking_price,booking_total_weeks,booking_total_moths",TB_BOOKINGS,array("booking_duration"=>$service_type,"booking_id"=>$postData['booking_id']));
                $duration=0;
                if($pick_up_time !='' && $drop_off_time !='') {
                $total      = strtotime($drop_off_time) - strtotime($pick_up_time);
                $hours      = floor($total / 60 / 60);
                $minutes    = round(($total - ($hours * 60 * 60)) / 60);
                $duration= $hours.'.'.$minutes;                    
                }
                if(count($serviceResult) > 0)
                {
                    if($service_type == "daily")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['days'] = $value['booking_total_days']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }                        
                    }
                    else if($service_type == "weekly")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date'];
                            $serviceList[$key]['weekly'] = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }

                    }
                    else
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date']; 
                            $serviceList[$key]['monthly'] = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['price'] = $value['booking_price'];                             
                        }

                    }
                }
                else
                {
                    $serviceList = array(); 
                }     
                $subQuery='';           
                $kidIdResult =  $this->common_model->select("kids_id",TB_KIDS_BOOKING,array("booking_id"=>$postData['booking_id'])); 
                if(count($kidIdResult) > 0)
                {
                foreach ($kidIdResult as $key => $value) 
                {
                $subQuery= $value['kids_id'];    
                }
              }
              $kidResult = $this->common_model->select_where_in_with_no_quote("kid_id,kid_gender,kid_age,kid_name",TB_KIDS,"kid_id",$subQuery);

               // echo $this->db->last_query();die;
                if(count($kidResult) > 0)
                {
                    foreach ($kidResult as $key => $value) {
                        $kidsList[$key]['id'] = $value['kid_id'];
                        $kidsList[$key]['name'] = $value['kid_name'];
                        $kidsList[$key]['gender'] = $value['kid_gender'];
                        $kidsList[$key]['age'] = $value['kid_age'];                    
                    }
                }
                else 
                {
                    $kidsList = array();
                }
                $this->response(array('status' => true,'user_id'=>$user_id,'pick_up_point'=>$pick_up_point,'drop_off_point'=>$drop_off_point,'pick_up_time'=>$pick_up_time,'drop_off_time'=>$drop_off_time,'trip_duration'=>$duration,'service_type'=>$service_type,'parent_no'=>$parent_no,'parent_name'=>$parent_name,'service_view'=>$serviceList,'kids_list'=>$kidsList), 200);exit;
            }
            else
            {
                $this->response(array('status' => false, 'message' => 'Data not found'), 300);exit;
            }
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'Please enter all parameters'), 300);exit;
        }
    }

    public function serviceAction_post() {
        $postData = $_POST;
        if ( $postData['booking_id'] != ""   && $postData['user_id'] != "")
        {
            $result = $this->common_model->selectJoin(TB_USERS.".user_id,user_name,user_phone_number,booking_pick_up_location,booking_drop_off_location,booking_pick_up_time,booking_drop_off_time,booking_duration,booking_id",TB_USERS,array(TB_USERS.".user_id"=>trim($postData['user_id']),"booking_id"=>$postData['booking_id']),array(),array(TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id'));
            if(count($result) > 0)
            {
                $user_id = $result[0]['user_id'] ? $result[0]['user_id']:'';
                $pick_up_time = $result[0]['booking_pick_up_time'] ? $result[0]['booking_pick_up_time']:'';
                $drop_off_time = $result[0]['booking_drop_off_time'] ? $result[0]['booking_drop_off_time']:'';
                $service_type = $result[0]['booking_duration'] ? $result[0]['booking_duration']:'';
                $serviceResult = $this->common_model->select("booking_date,booking_start_date,booking_end_date,booking_total_days,booking_secret_code,booking_price,booking_total_weeks,booking_total_moths",TB_BOOKINGS,array("booking_duration"=>$service_type,"booking_id"=>$postData['booking_id']));
              
                if(count($serviceResult) > 0)
                {
                    if($service_type == "daily")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['total_days'] = $value['booking_total_days']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }                        
                    }
                    else if($service_type == "weekly")
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date'];
                            $serviceList[$key]['total_weeks'] = $value['booking_total_weeks']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }

                    }
                    else
                    {
                        foreach ($serviceResult as $key => $value) {
                            $serviceList[$key]['start_date'] = $value['booking_start_date']; 
                            $serviceList[$key]['end_date'] = $value['booking_end_date']; 
                            $serviceList[$key]['total_months'] = $value['booking_total_moths']; 
                            $serviceList[$key]['secret_code'] = $value['booking_secret_code']; 
                            $serviceList[$key]['total_price'] = $value['booking_price'];                             
                        }

                    }
                }
                else
                {
                    $serviceList = array(); 
                }     
                               
                $this->response(array('status' => true,'user_id'=>$user_id,'booking_id'=>$postData['booking_id'],'service_type'=>$service_type,'service_view'=>$serviceList), 200);exit;
            }
            else{ $this->response(array('status' => false, 'message' => 'Data not found'), 300);exit;
            }
        }
        else{$this->response(array('status' => false, 'message' => 'Please enter all parameters'), 300);exit;
    }    
  }
  /**
     * update current latitude and longitude
     * @param $booking_id
     * @param current_lat
     * @param current_lng
     */
    function current_lat_and_lng_post() {
        $postData = $_POST;
        if ($postData['booking_id'] != "" && $postData['current_location_lat'] != "" && $postData['current_location_lng'] != "")
        {     
            $result = $this->db->update(TB_BOOKINGS,array('current_location_lat'=>$postData['current_location_lat'],'current_location_lng'=>$postData['current_location_lng']),array('booking_id'=>$postData['booking_id']));   
            if (!$result) {
                $this->response(array('status' => false, 'message' => 'something went wrong ,please try again!!!'), 300);exit;
               
            } else {
               $this->response(array('status' => true,'booking_id'=>$postData['booking_id'],'Current latitude'=>$postData['current_location_lat'],'Current longitude'=>$postData['current_location_lng'],'message'=>'Current latitude and longitude has been updated successfully'), 200);exit;
                return ;
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please enter booking id , current latitude and longitude.'), 300);exit;
        }
    } 
    
    public function sendPushNotification($device_id,$message,$data)
    {     
        $content = array(
            "en" => $message
        );
        $fields = array(
            'app_id' => ONSIGNALKEY,
            'include_player_ids' =>(array)$device_id,
            'data' => $data,
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
        print_r($response);exit;            
        curl_close($ch);
    }

    /**
     * driver_location
     * description : update of driver current location get lat & long
     * @param : user_id
     * @param : loc_lat
     * @param : loc_lng
     */
    public function driver_location_post()
    {
        $postData = $this->post();
        if($postData['user_id'] != "" && $postData['loc_lat'] != "" && $postData['loc_lng'] != "") {
            if($postData['user_id'] > 0 ) {
                $driverIdChecked = $this->common_model->select("user_id",TB_USERS,array("user_id"=>$postData['user_id'],"roleId"=>3));
                if(count($driverIdChecked) > 0){                               
                    $getDriverId = $this->common_model->select("user_id",TB_DRIVER_LOCATION,array("user_id"=>$postData['user_id']));
                    if(count($getDriverId) > 0) {
                        $userdata = array("loc_lat"=>$postData['loc_lat'],"loc_long"=>$postData['loc_lng'],"updated_at"=>date("Y-m-d H:i:s"));
                        $user = $this->db->update(TB_DRIVER_LOCATION,$userdata,array('user_id' =>$this->post('user_id')));
                        if($user) {
                            $this->response(array("status"=>true,"message"=> 'Driver location has been updated successfully'), 200);exit;
                        } else {
                            $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
                        }
                    } else {
                        $userdata = array("user_id"=>$postData['user_id'],"loc_lat"=>$postData['loc_lat'],"loc_long"=>$postData['loc_lng'],"created_at"=>date("Y-m-d H:i:s"));
                        $user = $this->db->insert(TB_DRIVER_LOCATION, $userdata);
                        if($user) {
                            $this->response(array("status"=>true,"message"=> 'Driver location has been inserted successfully'), 200);exit;
                        } else {
                            $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
                        }
                    }
                }else {
                    $this->response(array("status"=>false,"message"=> 'User id is not driver or exists.'), 300);exit;
                } 
            } else {
                $this->response(array("status"=>false,"message"=> 'Provide proper user id'), 300);exit;
            }
        } else {
            $this->response(array("status"=>false,"message"=> 'Please provide userid, lattitude & loggitude.'), 300);exit;
        }
    }
    /**
     * upload video stream
     * @param : booking_id
     * @param : video_stream
     */
    public function video_streaming_post()
    {       
        $postData = $this->post();
        $streaming_file = "";
        if($this->post('booking_id') !='' && $_FILES['video_stream']['name'] != "" && isset($_FILES['video_stream']) && !empty($_FILES['video_stream']))
          {
                        
          $result = $this->common_model->upload_video($this->post('booking_id'),$_FILES['video_stream'], './' . VIDEO_STREAM);
                         

            if(isset($result['status']) == "error")
            {
                $this->response(array("status"=>false, "message"=> strip_tags($result['message'])), 300); exit;               
            }
          else
            {
            $video_data = array("booking_id"=>$this->post('booking_id'),"file_path"=>$result['filename'],"isDeleted"=>0,);
             
            $video_streaming = $this->db->insert(TB_VIDEO_STREAM, $video_data);
            if($video_streaming)
                {
                $this->response(array("status"=>true, "message"=> "Video has been uploaded successfully."), 200); exit;     
                }else {
                $this->response(array("status"=>false, "message"=> "Some error has been occured, please try again!!!"), 300); exit; 
                }                
            }                  

        }else{
            $this->response(array("status"=>false,"message"=> 'Please fill data.'), 300);exit;
        }        
    } 

    /**
     * method :find_driver_loc (post)
     * @param : lattitude
     * @param : loggitude     
     * description : find the current location of the driver within 5 miles 
     */

    public function find_driver_loc_post() {
        $postData = $this->post();
        if($postData['lattitude'] != "" && $postData['loggitude'] != "") {            
            $cond = array(TB_DRIVER_LOCATION.'.updated_at'=>date("Y-m-d"),TB_USERS.'.roleId'=>3);
            $jointype=array(TB_DRIVER_LOCATION=>"LEFT");
            $join = array(TB_DRIVER_LOCATION=>TB_DRIVER_LOCATION.".user_id = ".TB_USERS.".user_id");
            $allDriverLoc = $this->common_model->selectJoin(TB_DRIVER_LOCATION.".user_id,loc_long,loc_lat,user_email,user_name,user_phone_number,user_gender,user_address,user_birth_date,user_age,is_your_own_car,user_category_id,user_sub_category_id,driving_license_type,license_no,valid_from_date,valid_until_date,car_model,car_register_no,car_seating_capacity,car_manufacturing_year,car_age,car_pic,license_pic,school,degree,specialization,education_from,education_to,certification_in,certificate,user_pic",TB_USERS,$cond,array(),$join,$jointype);
            if(count($allDriverLoc) > 0) {
                $data = array();
                $i = 0; 
                foreach ($allDriverLoc as $key => $value) {
                    $carpics =$license = $certification = array();
                    $milesLoc = round($this->distance($postData['lattitude'], $postData['loggitude'], $value['loc_lat'], $value['loc_long'], "M"));           
                    if($milesLoc <= 5 && $milesLoc >= 0) {                       
                        $data[$i]["driver_id"] = $value['user_id'];
                        $data[$i]["email"] = $value['user_email'];
                        $data[$i]["name"] = $value['user_name'];
                        $data[$i]["mobile_no"] = $value['user_phone_number'];                        
                        $data[$i]["gender"] = $value['user_gender'];
                        $data[$i]["address"] = $value['user_address'];
                        $data[$i]["dob"] = $value['user_birth_date'];
                        $data[$i]["age"] = $value['user_age'];
                        $data[$i]["is_your_own_car"] = $value['is_your_own_car'];
                        $data[$i]["driving_license_type"] = $value['driving_license_type'];
                        $data[$i]["license_no"] = $value['license_no'];
                        $data[$i]["valid_from_date"] = $value['valid_from_date'];
                        $data[$i]["valid_until_date"] = $value['valid_until_date'];
                        $data[$i]["car_model"] = $value['car_model'];
                        $data[$i]["car_register_no"] = $value['car_register_no'];
                        $data[$i]["car_seating_capacity"] = $value['car_seating_capacity'];
                        $data[$i]["car_manufacturing_year"] = $value['car_manufacturing_year'];
                        $data[$i]["car_age"] = $value['car_age'];
                        $data[$i]["school"] = $value['school'];
                        $data[$i]["degree"] = $value['degree'];
                        $data[$i]["specialization"] = $value['specialization'];
                        $data[$i]["education_from"] = $value['education_from'];
                        $data[$i]["education_to"] = $value['education_to'];
                        $data[$i]["certification_in"] = $value['certification_in'];

                        $licenseArr = explode("|", $value['license_pic']);
                        
                        if(!empty($licenseArr[0]))
                        {
                            foreach ($licenseArr as $lkey => $lvalue) {
                                $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                            }
                        }

                        $carArr = explode("|", $value['car_pic']);
                        if(!empty($carArr[0]))
                        {
                            foreach ($carArr as $ckey => $carValue) {
                                $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                            }
                        }

                        $certificateArr = explode("|", $value['certificate']);
                        if(!empty($certificateArr[0]))
                        {
                            foreach ($certificateArr as $cetkey => $certifValue) {
                                $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                            }
                        }
                                    
                        $data[$i]["license_pics"] = $license; 
                        $data[$i]["car_pics"] = $carpics;                                               
                        $data[$i]["certificate_pics"] = $certification;
                        $data[$i]["user_pics"] = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: "";                       
                        $i++;
                    } 
                }   
                           
                if(empty($data)) {
                    $this->response(array("status"=>false,"message"=> 'No drivers found within 5 miles.'), 300);exit;        
                } else {
                    $this->response(array("status"=>true,"drivers_list"=> $data), 200);exit;        
                }                
            } else {
                $this->response(array("status"=>false,"message"=> 'Today no drivers available.'), 300);exit;    
            }
        } else {
            $this->response(array("status"=>false,"message"=> 'Please enter lattitude & loggitude.'), 300);exit;
        }
    }

    /**
     * method: distance
     * calculate distance miles , kilometers and Nautical miles
     */
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * method:driver_details (post)
     * description: get specifi driver details 
     */
    public function driver_details_post() {
        $postData = $this->post();
        if($postData['user_id'] != "") {
            $allDrivers = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
            if(count($allDrivers) > 0){ 
                $status = $allDrivers?$allDrivers[0]['roleId']:"";
                if($status == 3 && $status != "") {
                    $driverArr= array();                           
                    foreach ($allDrivers as $key => $value) {
                        $driverArr[$key]["driver_id"] = $value['user_id'];
                        $driverArr[$key]["email"] = $value['user_email'];
                        $driverArr[$key]["name"] = $value['user_name'];
                        $driverArr[$key]["mobile_no"] = $value['user_phone_number'];                        
                        $driverArr[$key]["gender"] = $value['user_gender'];
                        $driverArr[$key]["address"] = $value['user_address'];
                        $driverArr[$key]["dob"] = $value['user_birth_date'];
                        $driverArr[$key]["age"] = $value['user_age'];
                        $driverArr[$key]["category_id"]= $value["user_category_id"];
                        $driverArr[$key]["sub_category_id"]= $value["user_sub_category_id"];
                        $driverArr[$key]["is_your_own_car"] = $value['is_your_own_car'];
                        $driverArr[$key]["driving_license_type"] = $value['driving_license_type'];
                        $driverArr[$key]["license_no"] = $value['license_no'];
                        $driverArr[$key]["valid_from_date"] = $value['valid_from_date'];
                        $driverArr[$key]["valid_until_date"] = $value['valid_until_date'];
                        $driverArr[$key]["car_model"] = $value['car_model'];
                        $driverArr[$key]["car_register_no"] = $value['car_register_no'];
                        $driverArr[$key]["car_seating_capacity"] = $value['car_seating_capacity'];
                        $driverArr[$key]["car_manufacturing_year"] = $value['car_manufacturing_year'];
                        $driverArr[$key]["car_age"] = $value['car_age'];
                        $driverArr[$key]["school"] = $value['school'];
                        $driverArr[$key]["degree"] = $value['degree'];
                        $driverArr[$key]["specialization"] = $value['specialization'];
                        $driverArr[$key]["education_from"] = $value['education_from'];
                        $driverArr[$key]["education_to"] = $value['education_to'];
                        $driverArr[$key]["certification_in"] = $value['certification_in'];

                        $licenseArr = explode("|", $value['license_pic']);
                        
                        if(!empty($licenseArr[0]))
                        {
                            foreach ($licenseArr as $lkey => $lvalue) {
                                $license[$lkey]['license_images'] = base_url().'uploads/driver/licenseCopy/'.$lvalue;
                            }
                        }

                        $carArr = explode("|", $value['car_pic']);
                        if(!empty($carArr[0]))
                        {
                            foreach ($carArr as $ckey => $carValue) {
                                $carpics[$ckey]['car_images'] = base_url().'uploads/driver/carPics/'.$carValue;
                            }
                        }

                        $certificateArr = explode("|", $value['certificate']);
                        if(!empty($certificateArr[0]))
                        {
                            foreach ($certificateArr as $cetkey => $certifValue) {
                                $certification[$cetkey]['certification_images'] = base_url().'uploads/driver/certification/'.$certifValue;
                            }
                        }
                                    
                        $driverArr[$key]["license_pics"] = $license; 
                        $driverArr[$key]["car_pics"] = $carpics;                                               
                        $driverArr[$key]["certificate_pics"] = $certification;
                        $driverArr[$key]["user_pics"] = $value['user_pic'] ? base_url().'uploads/driver/driverImg/'.$value['user_pic']: "";
                    }
                    $this->response(array("status"=>true,"driver_list"=> $driverArr), 200);exit;
                } else {
                    $this->response(array("status"=>false,"message"=> 'User is not driver or blank.'), 300);exit;
                }
            } else {
                $this->response(array("status"=>false,"message"=> 'No drivers has been exists.'), 300);exit;
            }
            
        } else {
            $this->response(array("status"=>false,"message"=> 'Please enter the user id'), 300);exit;
        }
    }
    
     /**
     * Video list
     * @param : user_id
     */
    public function video_list_post()
    {       
       
        if($this->post('user_id') !='')
          {
                        
          $cond = array(TB_USERS.'.isDeleted'=>0,TB_USERS.'.user_status'=>'1',TB_BOOKINGS.'.isDeleted'=>0,TB_VIDEO_STREAM.'.isDeleted'=>0,TB_USERS.'.user_id'=>$this->post('user_id'));
         $jointype=array(TB_USERS=>"LEFT",TB_BOOKINGS=>"LEFT");
  
         $join = array(TB_BOOKINGS=>TB_BOOKINGS.".booking_id = ".TB_VIDEO_STREAM.".booking_id",TB_USERS=>TB_USERS.".user_id = ".TB_BOOKINGS.".user_id");       
       
         $videoList = $this->Common_model->selectJoin("user_name,user_phone_number,user_gender,user_address,file_path",TB_VIDEO_STREAM,$cond,array(),$join,$jointype);
          if($videoList)
            {
             $this->response(array("status"=>true,"list" =>$videoList,"message"=> "Video List."), 200); exit;   
            } else{
            $this->response(array("status"=>false, "message"=> "Video not found."), 300); exit;     
            }

          } else {
           $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit; 
       }

    }  
public function license_pics_post()
    {       
        $postData = $this->post();       
        if($this->post('user_id') !='')
        {
               
         $user_id = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
            if(count($user_id) > 0){

                if(isset($_FILES['license_pics']['tmp_name']) =='') {

                   $this->response(array("status"=>false,"message"=> 'Please upload license copy pic.'), 300);exit;  
                
                    } else{

                            $licenseCopy= "";
                            $scan_copy_of_license = "";
                           
                            $temp = "";
                            $fileImages = $_FILES;
                            $allFiles = array();
                            if(!empty($fileImages)) {
                               foreach ($fileImages as $key => $value) {
                                    $cpt = count($_FILES[$key]['name']);
                                   
                                    $temp = 'license_img';
                                   
                                    $files_name = array();
                                    for($i=0; $i<$cpt; $i++){
                                        $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                        $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                        $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                        $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                        $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                        $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                        $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                        $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                        $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                        $files_name[$i]['size'] = $_FILES[$temp]['size'];
                                    }
                                    $allFiles[$temp]=$files_name;
                                                             
                                }
                                foreach ($allFiles as $key => $value) {
                                    $i = 0;                                
                                    for($j=0;$j < count($value); $j++) { 
                                        
                                         if(strcmp($key,'license_img') == 0 ){
                                            $scan_copy_of_license = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/licenseCopy/'); 
                                           
                                           if(is_array($scan_copy_of_license))
                                            {
                                            $this->response($scan_copy_of_license,300);exit;

                                            } else{
                                             $licenseCopy .= "|".$scan_copy_of_license;    
                                         }
                                                                                   
                                       
                                        } 
                                        $i++;                                                     
                                    }
                                }                                
                            
                            $userdata = array(
                                'license_pic'=>trim($licenseCopy,"|")                         
                            );                            
                            $user = $this->db->update(TB_USERS, $userdata);
                            if ($user) {
                                $this->response(array('status' => true, 'message' => 'license copy pic has been uploaded sucessfully.'),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                   } else {
                    $this->response(array("status"=>false,"message"=> 'Please upload license copy pic.'), 300);exit;  
                   }         

               }
               
                    
            }else{
            $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
           }               

        }else{
            $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
        }        
    }



   public function car_pics_post()
    {       
        $postData = $this->post();       
        if($this->post('user_id') !='')
        {
               
         $user_id = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
            if(count($user_id) > 0){

                if(isset($_FILES['car_pics']['tmp_name']) =='') {

                   $this->response(array("status"=>false,"message"=> 'Please upload car pic.'), 300);exit;  
                
                    } else{

                           $upload_car_pics  = $car_pics="";
                           
                            $temp = "";
                            $fileImages = $_FILES;
                            $allFiles = array();
                            if(!empty($fileImages)) {
                               foreach ($fileImages as $key => $value) {
                                    $cpt = count($_FILES[$key]['name']);
                                   
                                    $temp = 'car_img';
                                   
                                    $files_name = array();
                                    for($i=0; $i<$cpt; $i++){
                                        $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                        $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                        $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                        $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                        $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                        $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                        $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                        $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                        $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                        $files_name[$i]['size'] = $_FILES[$temp]['size'];
                                    }
                                    $allFiles[$temp]=$files_name;
                                                             
                                }
                                foreach ($allFiles as $key => $value) {
                                    $i = 0;                                
                                    for($j=0;$j < count($value); $j++) { 
                                        
                                          if (strcmp($key,'car_img') == 0 ) {                                   
                                            $upload_car_pics = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/carPics/'); 
                                                                                      
                                           if(is_array($upload_car_pics))
                                            {
                                            $this->response($upload_car_pics,300);exit;

                                            } else{
                                             $car_pics .= "|".$upload_car_pics;    
                                         }
                                                                                   
                                       
                                        } 
                                        $i++;                                                     
                                    }
                                }                                
                            
                            $userdata = array(
                                'car_pic'=>trim($car_pics,"|")                         
                            );                            
                            $user = $this->db->update(TB_USERS, $userdata);
                            if ($user) {
                                $this->response(array('status' => true, 'message' => 'car pic has been uploaded sucessfully.'),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                   } else {
                    $this->response(array("status"=>false,"message"=> 'Please upload car pic.'), 300);exit;  
                   }         

               }
               
                    
            }else{
            $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
           }               

        }else{
            $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
        }        
    }


    public function certificate_pics_post()
    {       
        $postData = $this->post();       
        if($this->post('user_id') !='')
        {
               
         $user_id = $this->common_model->select("*",TB_USERS,array("user_id"=>$postData['user_id']));
            if(count($user_id) > 0){

                if(isset($_FILES['certificate_pics']['tmp_name']) =='') {

                   $this->response(array("status"=>false,"message"=> 'Please upload certificate pic.'), 300);exit;  
                
                    } else{

                          $upload_certification = $certification ="";
                           
                            $temp = "";
                            $fileImages = $_FILES;
                            $allFiles = array();
                            if(!empty($fileImages)) {
                               foreach ($fileImages as $key => $value) {
                                    $cpt = count($_FILES[$key]['name']);
                                   $temp = 'certification_img';
                                    $files_name = array();
                                    for($i=0; $i<$cpt; $i++){
                                        $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                                        $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                                        $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                                        $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                                        $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                                        $files_name[$i]['name'] = $_FILES[$temp]['name'];
                                        $files_name[$i]['type'] = $_FILES[$temp]['type'];
                                        $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                                        $files_name[$i]['error'] = $_FILES[$temp]['error'];
                                        $files_name[$i]['size'] = $_FILES[$temp]['size'];
                                    }
                                    $allFiles[$temp]=$files_name;
                                                             
                                }
                                foreach ($allFiles as $key => $value) {
                                    $i = 0;                                
                                    for($j=0;$j < count($value); $j++) { 
                                        

                                         if (strcmp($key,'certification_img') == 0 ) {
                                            $upload_certification = $this->common_model->multiple_image_upload($value[$j],'uploads/driver/certification/'); 
                                         if(is_array($upload_certification))
                                            {
                                            $this->response($upload_certification,300);exit;

                                            } else{
                                             $certification .= "|".$upload_certification;    
                                         }
                                                                                   
                                       
                                        } 
                                        $i++;                                                     
                                    }
                                }                                
                            
                            $userdata = array(
                                'certificate'=>trim($certification,"|")                         
                            );                            
                            $user = $this->db->update(TB_USERS, $userdata);
                            if ($user) {
                                $this->response(array('status' => true, 'message' => 'certificate pic has been uploaded sucessfully.'),200);exit;
                            } else {
                                $this->response(array("status" => false, "message" => "Something went wrong. !!"),300);exit;
                            }
                   } else {
                    $this->response(array("status"=>false,"message"=> 'Please upload certificate pic.'), 300);exit;  
                   }         

               }
               
                    
            }else{
            $this->response(array("status"=>false,"message"=> 'User id not exist.'), 300);exit;
           }               

        }else{
            $this->response(array("status"=>false,"message"=> 'Please provide user id.'), 300);exit;
        }        
    }   

    /**
     * demo_post
     * @param lat,lng
     * @return  json
     */
    function lat_lng_post() {
        $params = (array) json_decode(file_get_contents('php://input'), TRUE);
        if(empty($params['location']['coords']['latitude']) || empty($params['location']['coords']['longitude']))
        {
           $this->response(array("status"=>fasle, "message"=> "Please provide lattitude & longitude."), 400);exit; 
        }
                
        $arrData = array("current_lat"=>$params['location']['coords']['latitude'],"current_lng"=>$params['location']['coords']['longitude']);
        $resultData = $this->Common_model->insert(TB_DEMO, $arrData);
        if($resultData) {
            $this->response(array("status"=>true,"message"=> '1 record has been inserted successfully'), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'Something went wrong'), 300);exit;
        }        
    }

    /**
     * latest_lat_lng
     */
    public function latestLatLng_get()
    {        
        $latestLatLng = $this->db->order_by('id',"desc")
                ->limit(1)
                ->get(TB_DEMO)
                ->row();      
        $data = array();

        if(count($latestLatLng) > 0) {
            $latLng['id']=$latestLatLng->id;
            $latLng['lattitude']=$latestLatLng->current_lat;
            $latLng['longitude']=$latestLatLng->current_lng;
            $data[] = $latLng;
            $this->response(array("status"=>true,"result"=>$data), 200);exit;
        } else {
            $this->response(array("status"=>false,"message"=> 'No lattitude & longitude are exists.'), 400);exit;
        }
    }

    
}