<?php 
/*
Author: Ram K
Page: APi
Description: Created Api for App.
*/

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model("data_model");
        $this->load->model("login_model");
        $this->load->model("common_model");
        $this->load->library('Common');
        $this->load->library("email"); 
        $this->load->helper("email_template"); 
        $this->load->helper("cias"); 
        $this->load->library("fileupload");
        $this->load->library('form_validation');
		date_default_timezone_set("Asia/Singapore");
        header('Access-Control-Allow-Origin: *');
        
    }    
   /* Register user */
    public function register_post()
    {
        $postData = $this->post();
        if($postData !='')
        {
            if($postData['user_email'] != '' && $postData['mobile_no'] != '')
            {
                $isExist = $this->data_model->checkEmailExists($postData['user_email']);

                if(count($isExist) !=0)
                {
                    $this->response(array("status"=>"error","message"=> 'Email id already exists'), 200);exit;
                }else{
                        $userdata = array(
                        'user_email' => $this->post('user_email'),
                        'user_password' => getHashedPassword(trim($this->post('password'))),
                        'user_name' => $this->post('user_name'),
                        'user_phone_number' => $this->post('mobile_no'),
                        'user_status' => 1,
                        'user_gender' => $this->post('gender') ? $this->post('gender') : '',
                        'user_address' => $this->post('address') ? $this->post('address') : '',
                        'user_no_of_kids' => $this->post('user_no_of_kids') ? $this->post('user_no_of_kids') : '',
                        'user_device_id' => $this->post('user_device_id') ? $this->post('user_device_id') : '',
                        'user_device_token' => $this->post('user_device_token') ? $this->post('user_device_token') : '',
                        'user_device_type' => $this->post('user_device_type') ? $this->post('user_device_type') : '',
                        'user_type' => $this->post('user_type'),
                    );
                    $user = $this->db->insert(TB_USERS, $userdata);
                    $user_id = $this->db->insert_id();
                       if ($user_id) {
                        if ($this->post('user_no_of_kids') > 0 && $this->post('user_kids')) {
                            foreach ($this->post('user_kids') as $kid) {
                                $kids['kid_name'] = isset($kid['name']) ? $kid['name'] : '';
                                $kids['kid_gender'] = isset($kid['gender']) ? $kid['gender'] : '';
                                $kids['kid_birthdate'] = isset($kid['birthdate']) ? $kid['birthdate'] : '';
                                $kids['kid_age'] = isset($kid['age']) ? $kid['age'] : '';
                                $kids['kid_status'] = 1;
                                $kids['user_id'] = $user_id;
                                $kids_all[] = $kids;
                            }
                            $this->db->insert_batch(TB_KIDS, $kids_all);
                        }
                        $this->response(array('status' => true, 'message' => 'User Created Sucessfully', 'user_id' => $user_id));
                    } else {
                        $this->response(array("status" => false, "message" => "Something went wrong. !!"));
                    }
                }
            }else{
                $this->response(array("status"=>"error","message"=> 'Please add email and mobile no.'), 200);exit;
            }

        }else{
            $this->response(array("status"=>"error","message"=> 'Please fill data.'), 200);exit;
        }
        
	}

    /* Login */

    public function login_post()
    {        
        $postData = $_POST; 
       
        $userArr = $this->login_model->login($postData["user_email"],$postData['password']);
        if(count($userArr)>0){
            
            if($userArr[0]['user_status'] == '1'){
                 $user_token = md5(uniqid(rand(), true));
                
                
                    $cond = array("user_email"=>$postData["user_email"]);
                    
                    $userData = $this->data_model->getAllUsers($cond);
                    $this->session->set_userdata("userLoggedin", $userData[0]);
                    //Update device token
                    
                    if(isset($postData["device_token"]) && $postData["device_token"] != ""){
                        $this->common_model->update(TB_USERS,array("user_id"=>$userData[0]['user_id']),array('user_device_token'=>$postData["device_token"]));
                    }
                    elseif(isset($postData["device_id"]) && $postData["device_id"] != ""){
                        $this->common_model->update(TB_USERS,array("user_id"=>$userData[0]['user_id']),array('iser_device_id'=>$postData["device_id"]));
                    }
                    
                    
                    $this->response(array("status"=>"success","user_token"=>$user_token ,"msg"=> "You have successfully login.","user"=> $userArr[0]), 200); exit;
            }
            else{
                
                $this->response(array("status"=>"error", "msg"=> "Your account is inactive."), 200);exit;
            }
            
        }else{
            $this->response(array("status"=>"error", "msg"=> "Email or password is incorrect."), 200);
        }
  }


  /**
     * Forgot password
     */
    function forgot_password_post()
    {
        $postData = $_POST;
        if(trim($postData["user_email"]) == ""){
            $this->response(array("status"=>"error", "msg"=> "Please enter email."), 200);
        }
        else
        {            
            $email=$postData['user_email'];
            $userdata=$this->common_model->select("user_id,user_name",TB_USERS,array("user_email"=>trim($email)));
            if(!count($userdata))
            {
                //echo "Please enter correct email, this email does not exist.";
                //exit;
                $this->response(array("status"=>"error", "msg"=> "Please enter correct email address, this email address does not exist."), 200);
            }
            else
            {
                $userId=$userdata[0]['user_id'];
                $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 8; $i++) {
                    $randstring.= $characters[rand(0, strlen($characters))];
                }
                                
                $update = $this->common_model->update(TB_USERS,array("user_id"=>$userdata[0]['user_id']),array('user_password'=>getHashedPassword($randstring)));
                if($update)
                {
                    $hostname = $this->config->item('hostname');
                     $usrname = $userdata[0]['user_name'];
                     $config['mailtype'] ='html';
                     $config['charset'] ='iso-8859-1';
                     $this->email->initialize($config);
                     $from  = EMAIL_FROM; 
                     //$this->messageBody  .= email_header();
                     $this->messageBody  = email_header();
                     $this->messageBody  .= "Hello ".$usrname." 
                                            <br/><br/>Please refer this password.  
                                            <br/>Password: " . $randstring ."
                                             ";
                    
                     $this->messageBody  .= email_footer();
                     //echo $this->messageBody;
                     //die;
                     $this->email->from($from, $from);
                     $this->email->to("$email");

                     $this->email->subject('Your new Password');
                     $this->email->message($this->messageBody); 
                         
                     $this->email->send();
                    $user_token = md5(uniqid(rand(), true));

                    $data = array("user_token"=> $user_token,"user_id"=> $userId);

                    $this->response(array("status"=>"success", "result" => $data, "msg" => "Your password has been sent to your email address. please check your email address."), 200);exit;
                }else{
                    $this->response(array("status"=>"success", "user_token" => $user_token, "msg" => "Something went wrong !!!"), 200);exit;
                }
                      
                     
            }
        }
    }

    /**
     * Change password
     */
    function change_password_post(){
            $postData = $_POST;
            if($this->user_exists($postData["user_id"])==0){
                $this->response(array("status"=>0,"msg"=>'User id is not exists'), 200);exit;
            }
            $cond_check = array('user_id' => $postData['user_id']);
            $currentUser = $this->login_model->validUser(TB_USERS,'user_password,user_name',$cond_check);

           if(!empty($currentUser)){
            if(verifyHashedPassword(trim($postData['old_pwd']), $currentUser[0]['user_password'])){
                if(count($currentUser)==0){
                    echo json_encode(array('status' => 'error','message' => 'You have entered incorrect old password.')); exit;
                }
            
                $cond = array("user_id" => $postData['user_id']);
                $updateData = array("user_password" => getHashedPassword($postData['new_pwd']));
                $result = $this->common_model->update(TB_USERS,$cond,$updateData);
                if($result)
                {
                    echo json_encode(array('status' => 'success','message' => 'Password has been updated successfully.')); exit;
                }
                else
                {
                    echo json_encode(array('status' => 'error','message' => 'Error occured during updating Password.')); exit;
                } 

            } else {
               echo json_encode(array('status' => 'error','message' => 'Old password and new password should not be same.')); exit;
            }
        }
             
             
    }

/**
     * my_profile_get
     * @param type $user_id
     */
    function getProfile_post() {
        $postData = $_POST;
        if($this->user_exists($postData["user_id"])==0){
            $this->response(array("status"=>0,"msg"=>'User is not exists.'), 200);exit;
        }
        if($postData["user_id"]!='') {                
            $cond = array("user_id" => $postData["user_id"]);
            $parentData= $this->common_model->select("user_pic,user_id,user_name,user_gender,user_device_id,user_address,user_age,user_birth_date,user_no_of_kids,user_email,user_type,user_phone_number",TB_USERS,$cond);
            $link = base_url().USER_PROFILE_IMG.$parentData[0]['user_pic'];
            $data['user_id'] = $parentData[0]['user_id'];
            $data['user_name'] = $parentData[0]['user_name'];
            $data['user_email'] = $parentData[0]['user_email'];
            $data['user_type'] = $parentData[0]['user_type'];
            $data['user_phone_number'] = $parentData[0]['user_phone_number'];
            $data['user_no_of_kids'] = $parentData[0]['user_no_of_kids'];
            $data['user_birth_date'] = $parentData[0]['user_birth_date'];
            $data['user_age'] = $parentData[0]['user_age'];
            $data['user_pic'] = $link;
            $data['user_gender'] = $parentData[0]['user_gender'];
            $data['user_address'] = $parentData[0]['user_address'];
            $data['user_device_id'] = $parentData[0]['user_device_id'];
            
            /*$this->response(array("status"=>"success", "user_details"=> $data['userDetails']), 200);exit;
            print_r($data['userDetails']);exit;*/
            $kidData = $this->common_model->select("kid_id,kid_name,kid_gender,kid_age",TB_KIDS,array("user_id"=>$postData["user_id"]));
           // print_r($kidData);exit;
            if(count($kidData)>0)
            {
                foreach ($kidData as $key => $value) {
                    $kidInfo[] = array('kid_id'=>$value['kid_id'],
                    'kid_name'=>$value['kid_name'],
                    'kid_gender'=>$value['kid_gender'],                   
                    'kid_age'=>$value['kid_age']   
                    ); 
                }               
                $data['kidsDetails'] =$kidInfo;
            }    
            $this->response(array("status"=>"success", "user_details"=> $data), 200);exit;
        }
    }

    public function save_profilePic_post()
    { 
        $postData = $_POST;
       // print_r($postData);exit;
        if($this->user_exists($postData["user_id"])==0){
            $this->response(array("status"=>0,"msg"=>'User is not exists.'), 200);exit;
        }
        
        $user_id = $postData["user_id"];
        if($_FILES['user_pic']['name'] != "")
        {   
            // $config['upload_path'] = USER_PROFILE_IMG;
            // $config['allowed_types'] = 'jpg|png|jpeg|JPG';
            // $config['max_size']     = '2147483648';
            // $result = $this->fileupload->save_file('user_pic',$config);
            if (isset($_FILES['user_pic']) && !empty($_FILES['user_pic'])) {
             $result = $this->common_model->upload_image($_FILES['user_pic'], './' . USER_PROFILE_IMG);
             // echo "<pre>";print_r($user_img_repo);die;
                // $user_img = isset($user_img_repo['filename']) && !empty($user_img_repo['filename']) ? base_url() . USER_PROFILE_IMG . $user_img_repo['filename'] : '';
            }

            // echo "<pre>";print_r($result);die;
            if(isset($result['status']) == "error")
            {
                $this->response(array("status"=>"error", "message"=> strip_tags($result['message'])), 200); exit;               
            }
          else
            {
                $profile_data['user_pic']=$result['filename'];
            }
            
        }
        if($postData)
        {            
            $where = array("user_id" => $user_id);            
            $update_profile = $this->common_model->update(TB_USERS,$where,$profile_data);
            if($update_profile)
            {
                $link = base_url().USER_PROFILE_IMG.$profile_data['user_pic'];
                
                $arr_result = array(
                        'status' => true, 
                        'msg' => 'User profile pic uploaded successfully.',
                        'url' => $link,
                        'user_id' => $user_id
                        );
                  $this->response(array("status"=>"success",'result'=>$arr_result), 200); exit;                
            }
            else{
                /////erroe msg
                $this->response(array("status"=>"error", "message"=> 'Something went wrong !!!'), 200); exit;
            }            
        }else 
        {
             $this->response(array("status"=>"failed", "message"=> 'Something went wrong !!!'), 200); exit;
        }
        
    }

    /**
     * Check if user exists
     */
    public function user_exists($user_id){ 
         $cond = array("user_id" => $user_id,"user_status"=>"1");
         $exist_users = $this->data_model->getAllUsers($cond); 
         if(count($exist_users)>0) {
             return 1;
         }
         else{
             return 0;
         }
    }

/**
     * Edit user
     * Added by Rajendra pawar
     */
    /*  public function user_edit_post() {
        $data = $_POST;
        if (isset($data['user_id']) && $data['user_id'] > 0) {
            //get user_type

            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_role=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result))
            {
                $this->response(array('status' => false, 'error' => 'Invalid user'), 300);exit;
            } else {                   
            //check email 
            $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null);    
            if ($emailExist) {
                $this->response(array("status" => false, "message" => "Duplicate email!!!"));exit;
            }
             if ($result[0]['role'] == USER_TYPE_SERVICE_PROVIDER) {
                $booking_data=$this->common_model->select("service_provider_user_id",TB_SERVICE_REQUESTS,array("service_provider_user_id"=>trim($data['user_id'])));    

                if (count($booking_data) > 0 && (isset($data['category']) || isset($data['sub_category']))) {
                    $this->response(array("status" => false, "message" => "Category and sub_category can't able to update because booking for this service user"));exit;
                }                
            }
            $user_birth_date = $this->post('user_birth_date');
            //upload data
            if (isset($_FILES['user_pic']) && !empty($_FILES['user_pic'])) {
             $user_img_repo = $this->common_model->upload_image($_FILES['user_pic'], './' . USER_PROFILE_IMG);
                $user_img = isset($user_img_repo['filename']) && !empty($user_img_repo['filename']) ? base_url() . USER_PROFILE_IMG . $user_img_repo['filename'] : '';
            }
            if (isset($_FILES['certificate']) && !empty($_FILES['certificate'])) {
                $certificate_repo = $this->common_model->upload_image($_FILES['certificate'], './' . CERTIFICATE_IMG_PATH);
                $certificate = isset($certificate_repo['filename']) && !empty($certificate_repo['filename']) ? base_url() . CERTIFICATE_IMG_PATH . $certificate_repo['filename'] : '';
            }
            if (isset($_FILES['license_pic']) && !empty($_FILES['license_pic'])) {
                $license_pic_repo = $this->common_model->upload_image($_FILES['license_pic'], './' . LICENSE_IMG_PATH);
                $license_pic = isset($license_pic_repo['filename']) && !empty($license_pic_repo['filename']) ? base_url() . LICENSE_IMG_PATH . $license_pic_repo['filename'] : '';
            }
            if (isset($_FILES['car_pic']) && !empty($_FILES['car_pic'])) {
                $car_pic_repo = $this->common_model->upload_image($_FILES['car_pic'], './' . CAR_PIC_IMG_PATH);
                $car_pic = isset($car_pic_repo['filename']) && !empty($car_pic_repo['filename']) ? base_url() . CAR_PIC_IMG_PATH . $car_pic_repo['filename'] : '';
            }

            //create user
            $userdata = array(
                'user_email' => $this->post('user_email'),
                'user_name' => $this->post('user_name'),
                'user_phone_number' => $this->post('mobile_no'),
                'user_gender' => $this->post('gender'),
                'user_address' => $this->post('address'),
                'user_no_of_kids' => $this->post('user_no_of_kids'),
                'user_device_id' => $this->post('user_device_id'),
                'user_device_tocken' => $this->post('user_device_tocken'),
                'user_device_type' => $this->post('user_device_type'),
                'user_birth_date' => $user_birth_date && (strtotime($user_birth_date) > 0) ? date('Y-m-d', strtotime($user_birth_date)) : '',
                'user_age' => $this->post('user_age'),
                'is_your_own_car' => $this->post('is_your_own_car'),
                'driving_license_type' => $this->post('driving_license_type'),
                'license_no' => $this->post('license_no'),
                'valid_from_date' => $this->post('valid_from_date'),
                'valid_until_date' => $this->post('valid_until_date'),
                'car_model' => $this->post('car_model'),
                'car_register_no' => $this->post('car_register_no'),
                'car_age' => $this->post('car_age'),
                'car_pic' => isset($car_pic) ? $car_pic : '',
                'license_pic' => isset($license_pic) ? $license_pic : '',
                'school' => $this->post('school'),
                'degree' => $this->post('degree'),
                'specialization' => $this->post('specialization'),
                'education_from' => $this->post('education_from'),
                'education_to' => $this->post('education_to'),
                'certification_in' => $this->post('certification_in'),
                'certificate' => isset($certificate) ? $certificate : '',
                'user_pic' => isset($user_img) ? $user_img : '',
                'user_category_id' => $this->post('category'),
                'user_sub_category_id' => $this->post('sub_category'),
            );

            try {
               $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
            } catch (Exception $ex) {
                $this->response(array('status' => FALSE, 'message' => $ex->getCode()));exit;
            }
            
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
        } else {
            $this->response(array('status' => false, 'error' => 'Provide user_id'), 300);exit;
        }
    } */

     /**
     * Edit user
     * Added by Rajendra pawar
     */
   function services_list_get() {
    $result_1 =$this->common_model->select("category",TB_CATEGORIES,array("status"=>1));
    $result_2 =$this->common_model->select("name as sub_categories",TB_SUBCATEGORIES,array("status"=>1));
    $data= array("category"=> $result_1,"sub_categories"=> $result_2);
    $this->response(array('status' => TRUE, 'message' => $data));
    }

    /**
     * Edit user
     * Added by Rajendra pawar
     */
    public function user_edit_post() {
        $data = $_POST;
        if (isset($data['user_id']) && $data['user_id'] > 0) {
        
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result))
            {
                $this->response(array('status' => false, 'error' => 'Invalid user'), 300);exit;
            } else {                   
            //check email 
             $emailExist = $this->common_model->select_join("user_email",TB_USERS,array("user_email"=>trim($this->post('user_email')),"user_id !="=> $this->post('user_id')),array(),array(),array(),null); 

            if ($emailExist) {
                $this->response(array("status" => false, "message" => "Duplicate email!!!"));exit;
            }           

            $userdata = array(
                'user_name' => $this->post('user_name'),
                'user_gender' => $this->post('gender'),
                'user_address' => $this->post('address'),
                'user_email' => $this->post('user_email'),                
                'user_phone_number' => $this->post('mobile_no'),             
               
            );

            try {
               $this->common_model->update(TB_USERS,array("user_id"=>$data['user_id']),$userdata);
            } catch (Exception $ex) {
                $this->response(array('status' => FALSE, 'message' => $ex->getCode()));exit;
            }
            
            $this->response(array('status' => true, 'message' => 'User updated successfully.'), 200);exit;
        }
        } else {
            $this->response(array('status' => false, 'error' => 'Provide user_id'), 300);exit;
        }
    }


   public function kid_edit_post() {
        $data = $_POST;

        $kidData = array(
                'kid_name' => $this->post('kid_name'),
                'kid_gender' => $this->post('kid_gender'),
                'kid_age' => $this->post('kid_age'),
                'user_id' => $this->post('user_id'),       
                'kid_status' =>1,
            );
        if (isset($data['user_id']) && $data['user_id'] > 0) { 
            
            $result=$this->common_model->select_join("user_id,role",TB_USERS,array("user_id"=>trim($data['user_id'])),array(),array(),array(TB_ROLES=>'tbl_users.roleId = tbl_roles.roleId'),null);
            if(!count($result))
            {
                $this->response(array('status' => false, 'error' => 'Invalid user'), 300);exit;
            }
        if (isset($data['kid_id']) && $data['kid_id'] > 0) {
              try {
               $this->common_model->update(TB_KIDS,array("kid_id"=>$data['kid_id']),$kidData);
               $this->response(array('status' => true, 'message' => 'Kid updated successfully.'), 200);exit;
            } catch (Exception $ex) {
                $this->response(array('status' => FALSE, 'message' => $ex->getCode()));exit;
            }

         } else{
         try {
               $this->common_model->insert(TB_KIDS,$kidData);
               $this->response(array('status' => true, 'message' => 'Kid saved successfully.'), 200);exit;
            } catch (Exception $ex) {
                $this->response(array('status' => FALSE, 'message' => $ex->getCode()));exit;
            }
      

     }
    } else{
       $this->response(array('status' => false, 'error' => 'Provide user_id'), 300);exit; 
    }
  }

   /**
     * validate_booking
     * @param type $post
     */
    function validate_booking($post) {
        // echo "<pre>";print_r();die;
        $bookingdata = [];
        $duration = array(DAILY,WEEKLY,MONTHLY);
        if (trim($post['duration']) == "") {
            $this->response(array('status' => false, 'message' => 'Please enter duration daily or weekly or monthly'), 300);
        }
        if(!in_array(trim($post['duration']),$duration)) {
            $this->response(array('status' => false, 'message' => $post['duration'].' Invalid duration'), 300);
        }
        if (trim($post['booking_service_type']) == MY_PREFERENCE && (!isset($post['booking_my_preference_service_users']) || !is_array($post['booking_my_preference_service_users']))) {
            $this->response(array('status' => false, 'error' => 'You must provide an booking_my_preference_service_users in array format.'), 300);
        }
        if (trim($post['booking_service_type']) == MY_MATCHES && (isset($post['booking_service_user']) && $post['booking_service_user'] < 1)) {
            $this->response(array('status' => false, 'error' => 'You must provide an booking_service_user.'), 300);
        }
        if ($post['duration'] == DAILY && strtotime($post['date']) < strtotime(date('Y-m-d'))) {
            $this->response(array('status' => false, 'message' => 'Please enter date greater than today and date format is Y-m-d(2018-01-31)'), 300);
        }
        if (($post['duration'] == WEEKLY || $post['duration'] == MONTHLY) && ((strtotime($post['start_date']) < strtotime(date('Y-m-d'))) || (strtotime($post['start_date']) > strtotime($post['end_date'])) || (strtotime($post['end_date']) < strtotime($post['start_date'])))) {
            $this->response(array('status' => false, 'message' => 'Please enter valid start and end date and date format is Y-m-d(2018-01-31)'), 300);
        }
        $user = $this->common_model->select("user_status,user_type,roleId",TB_USERS,array('user_id'=>$post['user_id']));
        if (!$user) {
            $this->response(array('status' => false, 'message' => 'Invalid user'), 200);
        }
        if ($user[0]['user_status'] != 1) {
            $this->response(array('status' => false, 'message' => 'User not active'), 200);
        }
        if ($user[0]['roleId'] != USER_TYPE_PARENT) {
            $this->response(array('status' => false, 'message' => 'User type not parent'), 200);
        }

        $catdata = $this->common->validate_category($post);
        if (trim($post['duration']) == DAILY) {
            $bookingdata['booking_date'] = date('Y-m-d', strtotime($post['date']));
            $bookingdata['booking_total_days'] = 1;
        } else {
            $bookingdata['booking_start_date'] = date('Y-m-d', strtotime($post['start_date']));
            $bookingdata['booking_end_date'] = date('Y-m-d', strtotime($post['end_date']));
            $start_date = $post['start_date'];
            $datetime1 = new DateTime($post['start_date']);
            $datetime2 = new DateTime($post['end_date']);
            $interval = date_diff($datetime1, $datetime2);
            //print_r($interval);exit;
            $bookingdata['booking_total_days'] = $interval->days;
            if ($post['duration'] == 'weekly') {
                $bookingdata['booking_total_weeks'] = ceil(($interval->days) / 7);
            }
            if ($post['duration'] == 'monthly') {
                $bookingdata['booking_total_moths'] = ($interval->format('%m')) + 1;
            }
        }
        
        $bookingdata['booking_pick_up_location'] = $post['pick_up_location'];
        $bookingdata['pick_up_location_lat'] = $post['pick_up_location_lat'];
        $bookingdata['pick_up_location_lng'] = $post['pick_up_location_lng'];
        $bookingdata['booking_drop_off_location'] = $post['drop_off_location'];
        $bookingdata['drop_off_location_lat'] = $post['drop_off_location_lat'];
        $bookingdata['drop_off_location_lng'] = $post['drop_off_location_lng'];
        $bookingdata['booking_pick_up_time'] = $post['pick_up_time'];
        $bookingdata['booking_drop_off_time'] = $post['drop_off_time'];
        $bookingdata['booking_category_id'] = $catdata['category_id'];
        $bookingdata['booking_sub_category_id'] = isset($catdata['sub_category_id']) ? $catdata['sub_category_id'] : '';
        $bookingdata['booking_secret_code'] = $post['secret_code'];
        $bookingdata['booking_duration'] = $post['duration'];
        $bookingdata['user_id'] = $post['user_id'];
        $booking_service_type = json_decode(SERVICE_TYPES, true);
        $bookingdata['booking_service_type'] = $post['booking_service_type'] && in_array($post['booking_service_type'], $booking_service_type) ? $post['booking_service_type'] : PLACE_REQUEST;
        $bookingdata['booking_no_off_kids'] = count($post['user_kids']);
        //echo '<pre>';print_r($bookingdata);exit;
        return $bookingdata;
    }

      /**
     * booking post
     */
    function booking_post() {
        $data = $this->post();
        $bookingdata = $this->validate_booking($data);
        if ($bookingdata) {
            $booking = $this->db->insert(TB_BOOKINGS, $bookingdata);
            $booking_id = $this->db->insert_id();
            if ($booking_id) {
                $booking_service_user = '';
                if ($this->post('booking_service_type') == MY_PREFERENCE) {
                    $booking_service_user = $this->post('booking_my_preference_service_users');
                }
                if ($this->post('booking_service_type') == MY_MATCHES) {
                    $booking_service_user = $this->post('booking_service_user');
                }

                if ($this->post('user_kids')) {
                    $user_kids = json_decode($this->post('user_kids'),true);
                    //echo "<pre>";print_r($user_kids);die;
                    foreach ($user_kids as $kid) {
                        // $kids['kid_name'] = isset($kid['name']) ? $kid['name'] : '';
                        // $kids['kid_gender'] = isset($kid['gender']) ? $kid['gender'] : '';
                        // $kids['kid_age'] = isset($kid['age']) ? $kid['age'] : '';
                        // $kids['kid_status'] = 1;
                        // $kids['user_id'] = $this->post('user_id');
                        $kids['kids_id'] = isset($kid['id']) ? $kid['id'] : '';
                        $kids['booking_id'] = $booking_id;
                        $kids_all[] = $kids;
                    }
                   
                    $this->db->insert_batch(TB_KIDS_BOOKING, $kids_all);
                }
                $respone['booking_status']['success'] = 'Booking successfully';
                $respone['booking_status']['booking_id'] = $booking_id;
                $respone['message'] = $this->place_request($booking_id, $this->post('user_id'), $bookingdata['booking_service_type'], $booking_service_user);
                $this->response(array('status' => true, 'message' => $respone), 200);
            }
        }
        $this->response(array('status' => false, 'message' => 'Some error has been occurred!!!'), 300);
    }

    /**
     * place_request
     * @param type $booking_id
     * @param type $user_id
     * @param type $booking_service_type
     * @param type $booking_service_users
     */
    function place_request($booking_id, $user_id, $booking_service_type, $booking_service_users) {
        $cond = ['u.roleId' => USER_TYPE_SERVICE_PROVIDER, 'u.user_status' => 1, 'sr.service_request_status', AVAILABLE];
        switch ($booking_service_type) {
            case PLACE_REQUEST:
                //get all service_providers which are available
                $query = $this->db->select('u.user_id')->from(TB_USERS . ' as u');
                $service_users = $query->join(TB_SERVICE_REQUESTS . " as sr", "sr.service_provider_user_id = u.user_id", "LEFT")->where($cond)->get();
                break;
            case MY_PREFERENCE:
                //get only selected multiple service_providers
                $query = $this->db->select('u.user_id')->from(TB_USERS . ' as u');
                $service_users = $query->join(TB_SERVICE_REQUESTS . " as sr", "sr.service_provider_user_id = u.user_id", "LEFT")->where_in('user_id', $booking_service_users)->where($cond)->get();
                break;
            case MY_MATCHES:
                //get single user 
                $query = $this->db->select('u.user_id')->from(TB_USERS . ' as u');
                $service_users = $query->join(TB_SERVICE_REQUESTS . " as sr", "sr.service_provider_user_id = u.user_id", "LEFT")->where('user_id', $booking_service_users)->where($cond)->get();
                break;
        }
        if ($service_users->result_array()) {
            return $this->insert_service_request($booking_id, $user_id, $service_users->result_array());
        } else {
            return 'Drivers not found';
        }
    }

    /**
     * insert_service_request
     * @param type $booking_id
     * @param type $user_id
     * @param type $service_users
     * @return type
     */
    function insert_service_request($booking_id, $user_id, $service_users) {
        $result = 0;
        if (!empty($service_users)) {
            foreach ($service_users as $service_user) {
                $service_request['service_booking_id'] = $booking_id;
                $service_request['service_parent_user_id'] = $user_id;
                $service_request['service_provider_user_id'] = $service_user['user_id'];
                $service_requests[] = $service_request;
            }
            $result = $this->db->insert_batch(TB_SERVICE_REQUESTS, $service_requests);
        //echo '$result <pre>';print_r($result);exit;
        }
        if (!$result) {
            return 'Request not send to driver, please try again!!!';
        } else {
            return 'Request send successfully';
        }
    }


    /**
     * my_bookings
     * @param type $user_id
     */
    function my_bookings_post() {
        $user_id = $_POST['user_id'];
        if ($user_id > 0) {
            $post = $this->post();
       
                $catdata = $this->common->validate_category($this->post());
                $cond = ['bookings.booking_category_id' => $catdata['category_id']];
                if (isset($catdata['sub_category_id']) && $catdata['sub_category_id'] > 0) {
                    $cond['bookings.booking_sub_category_id'] = $catdata['sub_category_id'];
                }
                 $user = $this->common_model->select("user_status,user_type,roleId",TB_USERS,array('user_id'=>$post['user_id']));
                if (count($user) > 0) {
                    if ($user[0]['roleId'] == USER_TYPE_SERVICE_PROVIDER) {
                        $like = array();
                        $conds = array(TB_BOOKINGS.".user_id"=>$user_id,"booking_status"=>$this->post('booking_status'));
                        $select = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id,kid_id,kid_name,kid_gender,kid_birthdate,".TB_CATEGORIES.".category,".TB_SUBCATEGORIES.".name,kid_age,kid_status";
                        $table_name = TB_BOOKINGS;
                        $join = array(
                            TB_CATEGORIES => TB_CATEGORIES . '.category_id=' . TB_BOOKINGS . '.booking_category_id',
                            TB_SUBCATEGORIES => TB_SUBCATEGORIES . '.sub_category_id=' . TB_BOOKINGS . '.booking_sub_category_id',
                            TB_KIDS => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id');
                        $default_order_column = "created_at";

                        $service_requests = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array(),'',$join);
                       
                        foreach ($service_requests as $key=>$service_request) {
                            $data[$key]['booking_id'] = $service_request['booking_id'];
                            $data[$key]['booking_duration'] = $service_request['booking_duration'];
                            $data[$key]['booking_date'] = $service_request['booking_date'];
                            $data[$key]['booking_total_days'] = $service_request['booking_total_days'];
                            $data[$key]['booking_pick_up_location'] = $service_request['booking_pick_up_location'];
                            $data[$key]['pick_up_location_lat'] = $service_request['pick_up_location_lat'];
                            $data[$key]['pick_up_location_lng'] = $service_request['pick_up_location_lng'];
                            $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                            $data[$key]['drop_off_location_lat'] = $service_request['drop_off_location_lat'];
                            $data[$key]['drop_off_location_lng'] = $service_request['drop_off_location_lng'];
                            $data[$key]['booking_pick_up_time'] = $service_request['booking_pick_up_time'];
                            $data[$key]['booking_drop_off_time'] = $service_request['booking_drop_off_time'];
                            $data[$key]['booking_category_id'] = $service_request['booking_category_id'];
                            $data[$key]['booking_sub_category_id'] = $service_request['booking_sub_category_id'];
                            $data[$key]['booking_no_off_kids'] = $service_request['booking_no_off_kids'];
                            $data[$key]['booking_price'] = $service_request['booking_price'];
                            $data[$key]['booking_total_weeks'] = $service_request['booking_total_weeks'];
                            $data[$key]['booking_total_moths'] = $service_request['booking_total_moths'];
                             $data[$key]['booking_status'] = $service_request['booking_status'];
                             $data[$key]['user_id'] = $service_request['user_id'];
                             $data[$key]['booking_service_type'] = $service_request['booking_service_type'];
                             $data[$key]['created_at'] = $service_request['created_at'];
                             $data[$key]['category'] = $service_request['category'];
                             $data[$key]['sub_category'] = $service_request['name'];
                             $data[$key]['kid_id'] = $service_request['kid_id'];
                             $data[$key]['kid_name'] = $service_request['kid_name'];
                             $data[$key]['kid_gender'] = $service_request['kid_gender'];
                             $data[$key]['kid_birthdate'] = $service_request['kid_birthdate'];
                             $data[$key]['kid_age'] = $service_request['kid_age'];
                        }
                    } else {
                        $cond['user_id'] = $user_id;
                        $cond['bookings.booking_status'] = $this->post('booking_status');
                        
                        $like = array();
                        $conds = array(TB_BOOKINGS.".user_id"=>$user_id,"booking_status"=>$this->post('booking_status'));
                        $select = TB_BOOKINGS . ".*,".TB_BOOKINGS.".user_id,kid_id,kid_name,kid_gender,kid_birthdate,kid_age,kid_status";
                        $table_name = TB_BOOKINGS;
                        $join = array(
                            TB_KIDS => TB_KIDS . '.user_id=' . TB_BOOKINGS . '.user_id',
                        TB_KIDS_BOOKING => TB_KIDS_BOOKING . '.booking_id=' . TB_BOOKINGS . '.booking_id');
                        $default_order_column = "created_at";

                        $service_requests = $this->common_model->getRowsPerPage($select,$table_name,$conds,$like,array(),'',$join);
                         // echo $this->db->last_query();die;
                        $data = array();
                        foreach ($service_requests as $key => $service_request) {
                            $data[$key]['booking_id'] = $service_request['booking_id'];
                            $data[$key]['booking_duration'] = $service_request['booking_duration'];
                            $data[$key]['booking_date'] = $service_request['booking_date'];
                            $data[$key]['booking_total_days'] = $service_request['booking_total_days'];
                            $data[$key]['booking_pick_up_location'] = $service_request['booking_pick_up_location'];
                            $data[$key]['pick_up_location_lat'] = $service_request['pick_up_location_lat'];
                            $data[$key]['pick_up_location_lng'] = $service_request['pick_up_location_lng'];
                            $data[$key]['booking_drop_off_location'] = $service_request['booking_drop_off_location'];
                            $data[$key]['drop_off_location_lat'] = $service_request['drop_off_location_lat'];
                            $data[$key]['drop_off_location_lng'] = $service_request['drop_off_location_lng'];
                            $data[$key]['booking_pick_up_time'] = $service_request['booking_pick_up_time'];
                            $data[$key]['booking_drop_off_time'] = $service_request['booking_drop_off_time'];
                            $data[$key]['booking_category_id'] = $service_request['booking_category_id'];
                            $data[$key]['booking_sub_category_id'] = $service_request['booking_sub_category_id'];
                            $data[$key]['booking_no_off_kids'] = $service_request['booking_no_off_kids'];
                            $data[$key]['booking_price'] = $service_request['booking_price'];
                            $data[$key]['booking_total_weeks'] = $service_request['booking_total_weeks'];
                            $data[$key]['booking_total_moths'] = $service_request['booking_total_moths'];
                             $data[$key]['booking_status'] = $service_request['booking_status'];
                             $data[$key]['user_id'] = $service_request['user_id'];
                             $data[$key]['booking_service_type'] = $service_request['booking_service_type'];
                             $data[$key]['created_at'] = $service_request['created_at'];
                             $data[$key]['kid_id'] = $service_request['kid_id'];
                             $data[$key]['kid_name'] = $service_request['kid_name'];
                             $data[$key]['kid_gender'] = $service_request['kid_gender'];
                             $data[$key]['kid_birthdate'] = $service_request['kid_birthdate'];
                             $data[$key]['kid_age'] = $service_request['kid_age'];
                        }

                    }
                    $result['booking_details'] = $data;
                    $this->response(array('status' => true, 'result' => $result));
                } else {
                    $this->response(array('status' => false, 'message' => 'Invalid user_id'), 300);
                }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide user_id'), 300);
        }
    }


    /**
     * accept_or_cancel_get
     * @param type $service_request_id
     * @param type $status
     */
    function accept_or_cancel_post() {
        $service_request_id = $_POST['service_request_id'];
        if ($service_request_id > 0) {
            // Check service request is exist or not 
            
            $service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id));
            // echo "<pre>";print_r($service_requests);die;
            if (!$service_requests) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300);
            }

            //start service request
            if ($_POST['status'] == 'ACCEPTED') {
                //Check that booking already started by anouther one 

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>'IN_PROCESS'));

                //Check another service not started in that duration
                //hold for the moment
                $validbooking = 1;
                if ($validbooking) {
                    if (count($is_service_requests) < 1) {
                        //Start that request
                        $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id'=>$service_request_id,'service_request_status'=>'IN_PROCESS'));
                        $updateData = array('service_request_status' => 'IN_PROCESS');
                        $start_service =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id,"service_request_status"=>'PENDING'),$updateData);

                        $updateData = array('booking_status' => 'IN_PROCESS');
                        $start_booking =  $this->common_model->update(TB_BOOKINGS,array("booking_id"=>$service_requests[0]['service_booking_id'],"booking_status"=>'PENDING'),$updateData);
                        
                        if ($start_service && $start_booking) {
                            $this->response(array('status' => true, 'message' => 'Service accepted successfully'));
                        } else {
                            $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);
                        }
                    } else {
                        $this->response(array('status' => false, 'message' => 'Service already accepted'), 300);
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'You can\'n able to start this request, because already another booking is in process in that duration'), 300);
                }
            }

            //cancel service request
            if ($status == 'CANCELLED') {

                $is_service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_booking_id'=>$service_requests['service_booking_id'],'service_request_status'=>'CANCELLED'));

                if (count($is_service_requests) < 1) {
                    //Cancel that request                
                    $cancelservice = Servicerequest::where(['service_requests.service_request_id' => $service_request_id])->update(['service_requests.service_request_status' => $status]);

                    $updateData = array('service_request_status' => 'CANCELLED');
                    $cancelservice =  $this->common_model->update(TB_SERVICE_REQUESTS,array("service_request_id"=>$service_request_id),$updateData);


                    if ($cancelservice) {
                        $this->response(array('status' => true, 'message' => 'Service has been cancelled successfully'));
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service has been already cancelled'), 300);
                }
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300);
        }
    }

    /**
     * slider_list_get 
     */
    function slider_list_get() {
        $sliderList = $this->common_model->select("id,title,banner_img",TB_BANNER,array("banner_status" => 1));
        $data = array();
        if(count($sliderList) > 0)
        {
            foreach ($sliderList as $key => $slider) {
                $data[$key]['id'] = $slider['id'];
                $data[$key]['title'] = $slider['title'];
                $data[$key]['banner_img'] = base_url().$slider['banner_img'];                    
            }
            $this->response(array('status' => true, 'slider_list' => $data),200);
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'no slider images are available '), 300);
        }
    }

    /**
     * start_or_end_service
     * @param type $service_request_id
     * @param type $status
     */
    function start_or_end_service_post() {
        $postData = $_POST;
        if ($postData['service_request_id'] > 0) {
            // Check service request is exist or not 
            $service_requests = $this->common_model->select("*",TB_SERVICE_REQUESTS,array('service_request_id' => $postData['service_request_id'], 'service_request_status' => IN_PROCESS));
            if (!$service_requests) {
                $this->response(array('status' => false, 'message' => 'Service request not available'), 300);
            }
            //Start service request
            if ($postData['status'] == START || $postData['status'] == END) {
                $checkstatus = AVAILABLE;
                $statuname = 'ended';
                if ($postData['status'] == START) {
                    $checkstatus = NOT_AVAILABLE;
                    $statuname = 'started';
                }
                //Check that sp already started service
                $is_service_requests = $this->common_model->select("*",TB_USERS,array('user_id' => $service_requests[0]['service_provider_user_id'], 'is_user_available' => $checkstatus));
                if (count($is_service_requests) < 1) {
                    //Update that service provider to not available
                    $update = $this->common_model->update(TB_USERS,array('user_id' => $service_requests[0]['service_provider_user_id']),array('is_user_available' => $checkstatus));
                    if ($update) {
                        if ($postData['status'] == END) {
                            //if end service complete that booking
                            $this->common_model->update(TB_SERVICE_REQUESTS,array('service_request_id' => $postData['service_request_id']),array('service_request_status' => COMPLETED));
                            $this->common_model->update(TB_BOOKINGS,array('booking_id' => $service_requests[0]['service_booking_id']),array('booking_status' => COMPLETED));
                        }
                        $this->response(array('status' => true, 'message' => 'Service ' . $statuname . ' successfully'));
                    } else {
                        $this->response(array('status' => false, 'message' => 'Some error has been occured, please try again!!!'), 300);
                    }
                } else {
                    $this->response(array('status' => false, 'message' => 'Service ' . $statuname . ' already'), 300);
                }
            } else {
                $this->response(array('status' => false, 'message' => 'Invalid status'), 300);
            }
        } else {
            $this->response(array('status' => false, 'message' => 'Please provide service_request_id'), 300);
        }
    }

    /**
     * price_list_get
     */
    function price_list_get() {
        $cond = array(TB_CATEGORIES.'.status'=>1,TB_SUBCATEGORIES.'.status'=>1);
        $jointype=array(TB_CATEGORIES=>"LEFT",TB_SUBCATEGORIES=>"LEFT");
        $join = array(TB_CATEGORIES=>TB_CATEGORIES.".category_id = ".TB_RIDE_PRICE.".c_id",TB_SUBCATEGORIES=>TB_SUBCATEGORIES.".sub_category_id = ".TB_RIDE_PRICE.".sc_id");
        $priceList = $this->Common_model->selectJoin("rp_id,name,category,rp_cost,rp_per_km,rp_per_hour",TB_RIDE_PRICE,$cond,array(),$join,$jointype);
        $data = array();
        if(count($priceList) > 0)
        {
            $keyCat = $keyTut = 0;
            foreach ($priceList as $key => $price) {
                if($price['category'] == "Rides") {                   
                    $ride[$key]['id'] = $price['rp_id'];
                    $ride[$key]['category'] = $price['category'];
                    $ride[$key]['sub_category'] = $price['name'];
                    $ride[$key]['cost'] = $price['rp_cost'];
                    $ride[$key]['hour'] = $price['rp_per_hour'];
                }
                else if($price['category'] == "After School Care") {
                    $afterSchoolCare[$keyCat]['id'] = $price['rp_id'];
                    $afterSchoolCare[$keyCat]['category'] = $price['category'];
                    $afterSchoolCare[$keyCat]['sub_category'] = $price['name'];
                    $afterSchoolCare[$keyCat]['cost'] = $price['rp_cost'];
                    $afterSchoolCare[$keyCat]['km'] = $price['rp_per_km'];
                    $keyCat++; 
                }
                else if($price['category'] == "Premier Tutoring") {
                    $tutoring[$keyTut]['id'] = $price['rp_id'];
                    $tutoring[$keyTut]['category'] = $price['category'];
                    $tutoring[$keyTut]['sub_category'] = $price['name'];
                    $tutoring[$keyTut]['cost'] = $price['rp_cost'];
                    $tutoring[$keyTut]['hour'] = $price['rp_per_hour'];
                    $keyTut++;
                }
                else {

                }
                
            }

            $data['Rides'] = $ride;
            $data['After School Care'] = $afterSchoolCare;
            $data['Premier Tutoring'] = $tutoring; 
            $this->response(array('status' => true, 'price_list' => $data),200);
        }
        else
        {
            $this->response(array('status' => false, 'message' => 'No data are available '), 300);
        }      
    }

    /**
     * read_notification_post
     * @param type userId
     * @param type notifId
     */
    function read_notification_post() {
        $postData = $_POST;
        if($postData['userId'] != "" && $postData['notifId'] != "") {
            $checkNotif = $this->common_model->select("n_id",TB_NOTIFICATION,array('user_id' => $postData['userId'],'n_id'=>$postData['notifId']));
            if(count($checkNotif) > 0 ) {
                 $updateNotif = $this->common_model->update(TB_NOTIFICATION,array('user_id' => $postData['userId'],'n_id'=>$postData['notifId']),array('n_read' => "0"));
                if($updateNotif) {
                    $this->response(array('status' => true, 'message' => 'The notification has been read successfully .'), 200);
                } else {
                    $this->response(array('status' => false, 'message' => 'You have already read notification.'), 300);
                }
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300);
            }           
        } else {
            $this->response(array('status' => false, 'message' => 'All fields are required. '), 300);
        }        
    }

    /**
     * notification_list
     * @param type userId
     * @param type notifId
     */

    function notification_listing_post() {
        $postData = $_POST;
        if($postData['userId'] != "" && $postData['notifId'] != "") {
            $checkNotif = $this->common_model->select("n_content,user_id,n_id",TB_NOTIFICATION,array('user_id' => $postData['userId'],'n_id'=>$postData['notifId'],'n_read'=> "1"));
            if($checkNotif) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.','notification_list'=>$checkNotif), 200);
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300);
            }                 
        } else {
            $checkNotif = $this->common_model->select("n_content,user_id,n_id",TB_NOTIFICATION,array('n_read'=> "1"));
            if(count($checkNotif) > 0) {
                $this->response(array('status' => true, 'message' => 'Send admin notification.','notification_list'=>$checkNotif), 200);
            } else {
                $this->response(array('status' => false, 'message' => 'You have provided wrong parameter list.'), 300);
            }            
        }     
    }

    /**
     * faq_list     
     */
    function faq_list_get() {
        $cond = array(TB_CATEGORIES.'.status'=>1,TB_SUBCATEGORIES.'.status'=>1);
        $jointype=array(TB_CATEGORIES=>"LEFT",TB_SUBCATEGORIES=>"LEFT");
        $join = array(TB_CATEGORIES=>TB_CATEGORIES.".category_id = ".TB_FAQ.".category_id",TB_SUBCATEGORIES=>TB_SUBCATEGORIES.".sub_category_id = ".TB_FAQ.".subcategory_id");
        $faqList = $this->Common_model->selectJoin("id,name,category,faq_description,faq_title",TB_FAQ,$cond,array(),$join,$jointype);
       $data = array();           
        if(count($faqList) > 0) {
            foreach ($faqList as $key => $faq) {
                $data[$key]['id'] = $faq['id'];
                $data[$key]['categoryName'] = $faq['category'];
                $data[$key]['subCategoryName'] = $faq['name'];
                $data[$key]['description'] = $faq['faq_description'];
                $data[$key]['title'] = $faq['faq_title'];
            }
            $this->response(array('status' => true, 'message' => 'FAQ list.','faq_list'=>$data), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'Faq data not available.'), 300);
        }       
    }
}