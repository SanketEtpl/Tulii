<?php
/*
* @author : kiran N.
* description: manage the faq data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Faq_controller extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : FAQ management', 
            'isActive'  => 'active'
        );
        $this->load->model('Faq_model','faq');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');       
            $this->data['roleData'] = $this->Common_model->select('*', TB_ROLES, array("role_id !=" => ROLE_ADMIN)); 
            $this->load->view('includes/header',$this->data);
            $this->load->view('faq_management/faq',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of FAQ list data
    {
        if(is_user_logged_in()){            
            $faqData = $this->faq->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($faqData as $faq) {
                $faqId = $this->encrypt->encode($faq->faq_id);
                $titleFaq = $subTitleFaq = $descFaq= $titleCut= $descCut="";
                if(strlen($faq->faq_title) > 100) {                                                    
                    $titleCut = substr($faq->faq_title, 0, 100);
                    $titleFaq = substr($titleCut, 0, strrpos($titleCut, ' ')).'...'; 
                } else {
                    $titleFaq = $faq->faq_title;
                }

                if(strlen($faq->faq_sub_title) > 100) {
                    $titleSubCut = substr($faq->faq_sub_title, 0, 100);
                    $subTitleFaq = substr($titleSubCut, 0, strrpos($titleSubCut, ' ')).'...'; 
                } else {
                    $subTitleFaq = $faq->faq_sub_title;
                } 

                if(strlen($faq->faq_description) > 150) {                                                    
                    $descCut = substr($faq->faq_description, 0, 150);
                    $descFaq = substr($descCut, 0, strrpos($descCut, ' ')).'<a data-id="'.$i.'" data-role-name="'.$faq->role_name.'" data-row-id="'.$faqId.'" class="btn btn-sm btn-primary" onclick="viewDesc(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Read more">
                                <i class="fa fa-eye"> Read more</i>
                            </a>'; 
                } else {
                    $descFaq = $faq->faq_description;
                }
                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $faq->role_name;
                $row[] = $titleFaq;                
                $row[] = $subTitleFaq;
                $row[] = $descFaq;     
                                                         
                $row[] =
                '   
                    <a data-id="'.$i.'" data-role-name="'.$faq->role_name.'" data-row-id="'.$faqId.'" class="btn btn-sm btn-info" onclick="viewFAQ(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i>
                    </a>                
                    <a data-id="'.$i.'" data-row-id="'.$faqId.'" data-role-name="'.$faq->role_name.'" class="btn btn-sm btn-info" onclick="editFAQ(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-id="'.$i.'" data-row-id="'.$faqId.'" class="btn btn-sm btn-danger" onclick="deleteFaq(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>                           
                ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->faq->count_all(),
                "recordsFiltered" => $this->faq->count_filtered(),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteFAQ() // delete record of FAQ 
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]) {
                    $data = array(
                        "is_deleted" => "1",
                        "update_date" => date('Y-m-d H:i:s')
                    );
                    $deleteId = $this->Common_model->update(TB_FAQ, array('faq_id' => $this->encrypt->decode($postData['key'])), $data);
                    if($deleteId) {                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"FAQ record has been deleted successfully.")); exit;  
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Something went wrong.")); exit; 
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewFAQ() // view data of FAQ  
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]) {                    
                    $faqData = $this->Common_model->select("*", TB_FAQ, array('faq_id' => $this->encrypt->decode($postData['key'])));
                    if($faqData) {                                                
                        echo json_encode(array("status"=>"success","action" => "view", "faqData" => $faqData[0], "role_name" => $postData['role_name'])); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of faq list
    public function addFAQ()
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post();
                if(!empty($postData["editData"])) { // update data
                    $updateArr = array(  
                        "faq_title" => $postData['title'],
                        "faq_sub_title" => $postData['sub_title'],
                        "faq_description" => $postData['description'],
                        "role_id" => $postData['userRole'],
                        "update_date" => date("Y-m-d H:s:i")  
                    );
                    $updateId = $this->Common_model->update(TB_FAQ, array('faq_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                    if($updateId){
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"FAQ has been updated successfully.")); exit;   
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {       
                    if(!empty($postData)){ // add data               
                    $data = array(
                        "faq_title" => $postData['title'],
                        "faq_sub_title" => $postData['sub_title'],
                        "faq_description" => $postData['description'],                        
                        "role_id" => $postData['userRole'],    
                        "status" => "1",
                        "is_deleted" => "0",           
                        "created_date" => date("Y-m-d H:s:i")           
                    );
                    $insertId = $this->Common_model->insert(TB_FAQ,$data);
                    if($insertId){
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"FAQ has been added successfully.")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the faq information.")); exit;    
                    }
                }                   
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}