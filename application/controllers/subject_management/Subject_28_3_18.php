<?php
/*
* @author : kiran N.
* description: manage the subject of tutors data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Subject extends CI_Controller
{
	public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Subject list'        
        );
        $this->load->model('Subject_model','subject');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');                               
             $this->load->view('subject_management/subject',$this->data);            
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){            
            $list = $this->subject->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($list as $rec) {
                $userId = $this->encrypt->encode($rec->id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $rec->teacher_name;
                $temp=''; 
                $list_id = $this->subject->select_where_in('sub_name',TB_SUBJECTS,$rec->sub_id);
                if($list_id){
               
               foreach($list_id as $dt)
               {
                if($temp == ''){
                $temp .=  $dt['sub_name'] ;     
                }else{
                $temp .= " , ". $dt['sub_name']; 
                }               
               } 
               }
                $row[] = $temp;                         
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editSubject(this)" href="javascript:void(0)">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger" onclick="deleteSubject(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->subject->count_all(),
                            "recordsFiltered" => $this->subject->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteSubject() // delete record of price list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_SUBJECT_MASTER,array('id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Subject record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewSubject() // view data of subject  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                // $subData = $this->Common_model->select("id,teacher_id,sub_id",TB_SUBJECT_MASTER,array('id'=>$this->encrypt->decode($postData['key'])));

            $subData=$this->Common_model->select_join("sub_id,teacher_name",TB_SUBJECT_MASTER,array("tbl_subject_master.id"=>$this->encrypt->decode($postData['key'])),array(),array(),array(TB_SUBJECTS=>'tbl_subjects.id = tbl_subject_master.sub_id',TB_TEACHERS=>'tbl_teachers.id = tbl_subject_master.teacher_id'),null);

                                            
                    if($subData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","subData"=>$subData[0])); exit;//,"subjects"=>$subjects[0],"teachers"=>$teachers[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of price list
    public function assignTeacher()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                
                if(! $postData['teacher_name']){
                    echo json_encode(array("status"=>"warning","message"=>"Please fill teacher name.")); exit;    
                }
                $subject_id='';
                $subject_data = $this->Common_model->select("id",TB_TEACHERS,array('teacher_name'=>trim($postData['teacher_name']))); 

                 if($subject_data){
                    $subject_id= $subject_data[0]['id'];
                } 
                if($subject_id =='')
                {
                 $recordInsert = $this->Common_model->insert(TB_TEACHERS,array("teacher_name" =>$postData['teacher_name']));  
                 if($recordInsert > 0){
                    $subject_id= $recordInsert;
                }
                }
                  
                if($subject_id =='')
                {
                 echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Something went wrong. !!")); exit; 
                 }
                    if(!empty($postData["editData"])) { // update data
                        $updateArr = array(  
                            "sub_id" =>$postData['subject_name'],
                            "teacher_id" =>$subject_id                             
                        );
                        $updateId = $this->Common_model->update(TB_SUBJECT_MASTER,array('id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                        if($updateId){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Subject has been updated successfully.")); exit;   
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                        }
                    } else {       
                        if(!empty($postData)){ // add data               
                        $data = array(
                            "sub_id" =>$postData['subject_name'],
                            "teacher_id" =>$subject_id,
                            "created_at"=>date("Y-m-d H:s:i")           
                            );
                        $recordInsert = $this->Common_model->insert(TB_SUBJECT_MASTER,$data);
                        if($recordInsert){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Subject has been inserted successfully ")); exit;    
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    }
                    else{
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the subject information")); exit;    
                        }
                    }  
                    
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
     // add subject
    public function addSubject()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                
                $checkExists = $this->Common_model->select("id",TB_SUBJECTS,array('sub_name'=>trim($postData['subject_name']))); 
                if(count($checkExists) > 0){
                    echo json_encode(array("status"=>"warning","message"=>"Subject already exist")); exit;    
                } else {                     
                          
                      if(!empty($postData)){               
                        $data = array(
                            "sub_name" =>$postData['subject_name']         
                            );
                        $recordInsert = $this->Common_model->insert(TB_SUBJECTS,$data);
                        if($recordInsert){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Subject has been added successfully ")); exit;    
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    }
                    else{
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the subject information")); exit;    
                        }
                   
                }     
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
    public function teacherList()
    {
    if(is_user_logged_in()){
    if (isset($_POST['keyword'])){
      $keyword = $_POST['keyword'];
      $data = $this->subject->get_teacher($keyword);
       echo json_encode($data);
    }

    }
  }
  public function showSubject(){
   if(is_user_logged_in()){
    $list = $this->Common_model->select('id,sub_name',TB_SUBJECTS);
    if($list){
       $data=''; 
     foreach($list as $dt)
          {
          $data .="<option value='".$dt['id']."'> ".$dt['sub_name']."</option>";  
           } 
     $output = array(
                        "status" => 'success',
                        "data" => $data,
                         ); 
    } else{
     $output = array(
                        "status" => 'error',
                        "data" =>'',
                         );   
    } 
    
    
    echo json_encode($output);   
   } 
  }
}