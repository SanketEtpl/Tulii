<?php 

/*

Author : Rajendra pawar 
Page :  EnquiryList_controller.php
Description : EnquiryList controller use for enquiry managment module

*/
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class EnquiryList_controller extends BaseController
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('enquiry_management/enquiry_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	$this->global['pageTitle'] = 'Tulii : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
            
    }

  function listOperation()
  { 
        if($this->isAdmin() == TRUE)
        {
           $this->global['pageTitle'] = 'Tulii : Enquiry Listing';
            $this->loadViews("enquiry_management/enquiryList", $this->global, NULL , NULL); 
           
        }
        else
        {
                  
           $this->loadThis(); 
    }

}

  function enquiryList()
  { 
        if($this->isAdmin() == TRUE)
        {
           $list = $this->enquiry_model->get_datatables();  

        $data = array();
        $no = $_POST['start'];
        foreach($list as $record)
          {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->service_type;
            $row[] = $record->name;
            $row[] = $record->address;
            $row[] = $record->contact_number;
            $row[] = $record->email;
            $row[] = '<a data-tenquiryid="'.$record->enquiry_id.'"  class="btn btn-sm btn-info viewEnquiry"  href="javascript:void(0)"><i class="fa fa-eye"></i></a>';
            
            $data[] = $row;
          }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->enquiry_model->count_all(),
                        "recordsFiltered" => $this->enquiry_model->count_filtered(),
                        "data" => $data,
                );          

          echo json_encode($output);           
        }
        else
        {
            $this->loadThis();       

    }

}
 function viewEnquiryDetails()
    {
        if($this->isAdmin() == TRUE)
        {
           $tenquiryId = $this->input->post('tenquiryId');
            $result=$this->enquiry_model->select_query("service_type,name,address,contact_number,email,
            gender,birth_date,age,driving_license,driving_license_no,valid_from,valid_until,car_model,
            Car_registration_no,school_name,degree,specialization,from_date,to_date,Certification_name",TB_ENQUIRY,array("enquiry_id"=>$tenquiryId));
                if ($result > 0) { 

                 $service_type= $result[0]['service_type'];
                 $name=$result[0]['name'];
                 $address=$result[0]['address'];
                 $contact_number = $result[0]['contact_number'];
                 $email = $result[0]['email'];
                 $gender = $result[0]['gender'];
                 if($result[0]['birth_date']) {
                 $birth_date = date("d/m/Y", strtotime($result[0]['birth_date']));   
                 } else {
                  $birth_date = "";  
                 }    
                 $age = $result[0]['age'];
                $driving_license = $result[0]['driving_license'];
                $driving_license_no = $result[0]['driving_license_no'];
                if($result[0]['valid_from']) {
                $valid_from = date("d/m/Y", strtotime($result[0]['valid_from']));   
                 } else {
                  $valid_from = "";  
                 }
                 if($result[0]['valid_until']) {
                 $valid_until = date("d/m/Y", strtotime($result[0]['valid_until']));   
                 } else {
                  $valid_until = "";  
                 }     
                 $car_model = $result[0]['car_model'];
                $Car_registration_no = $result[0]['Car_registration_no'];
                $school_name = $result[0]['school_name'];
                $degree = $result[0]['degree'];
                $specialization = $result[0]['specialization'];
                if($result[0]['from_date']) {
                 $from = date("d/m/Y", strtotime($result[0]['from_date']));   
                 } else {
                  $from = "";  
                 } 
                 if($result[0]['to_date']) {
                 $to = date("d/m/Y", strtotime($result[0]['to_date']));   
                 } else {
                  $to = "";  
                 } 
                $Certification_name = $result[0]['Certification_name'];$data='<div class="box">
                     <form role="form"  method="post" id="editcareDriverFrm" name="editcareDriverFrm">
                        <div class="box-body">
                            <div class="row">              
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="service">Service Type: </label>
                                        <lable>'.$service_type.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name: </label>
                                        <lable>'.$name.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address: </label>
                                        <lable>'.$address.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Mobile: </label>
                                        <lable>'.$contact_number.'</lable>
                                    </div>
                                </div>
                            </div>
                
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email: </label>
                                        <lable>'.$email.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gender">Gender: </label>
                                        <lable>'.$gender.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="birthDate">Date Of Birth: </label>
                                        <lable>'.$birth_date.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="age">Age: </label>
                                        <lable>'.$age.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drivingLicense">Driving License Type: </label>
                                        <lable>'.$driving_license.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drivingLicenseNo">Driving License No: </label>
                                        <lable>'.$driving_license_no.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="validFrom">Valid From: </label>
                                        <lable>'.$valid_from.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="validUntil">Valid Until: </label>
                                        <lable>'.$valid_until.'</lable>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="carModel">Car Model: </label>
                                        <lable>'.$car_model.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="CarRegistrationNo">Car Registration No: </label>
                                        <lable>'.$Car_registration_no.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="schoolName">School Name: </label>
                                        <lable>'.$school_name.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="degree">Degree: </label>
                                        <lable>'.$degree.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="specialization">Specialization: </label>
                                        <lable>'.$specialization.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="from">From: </label>
                                        <lable>'.$from.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="to">To: </label>
                                        <lable>'.$to.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="CertificationName">Certification Name: </label>
                                        <lable>'.$Certification_name.'</lable>
                                    </div>
                                </div>
                            </div>                       


                        </div><!-- /.box-body -->   
                         </form>                
                </div>';  
                echo json_encode(array("status" => TRUE,"data" => $data));  
                }
            else { echo(json_encode(array("status"=>FALSE))); } 
             
        }
        else
        {
        echo(json_encode(array('status'=>'access')));  
        }
    }
}