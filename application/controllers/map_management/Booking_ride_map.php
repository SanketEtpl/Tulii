<?php
/*
* @author : Kiran.
* description: manage the booking ride map data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Booking_ride_map extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Current booking ride map', 
            'isActive'  => 'active'
        );
        $this->load->model("Booking_ride_map_model","Ride_map");
        $this->load->model("common_model");        
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->view('includes/header',$this->data);
            $this->load->view('map_management/booking_ride_map');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of booking ride list data
    {
        if(is_user_logged_in()){                  
            $rideData = $this->Ride_map->get_datatables();              
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($rideData as $value) {
                $status = "";
                $bookingType = "";
                if($value->booking_status == 1) {
                    $status = "Scheduled";        
                } else if($value->booking_status == 2) {
                    $status = "Ongoing";        
                } else if($value->booking_status == 3) {
                    $status = "Closed";        
                } else {
                    $status = "N/A";        
                }

                if($value->is_ride == "1") {
                    $booking = "ride";
                } else if ($value->is_care == "1") {
                    $booking = "care";
                } else {
                    $booking = "tutor";
                }      

                $bID = $value->booking_id;
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($value->user_name);
                $row[] = $value->booking_date;  
                $row[] = $status;  
                if($value->booking_status != 1)  {                    
                    $param= "'$bID','$booking'";
                    $event = 'onclick="viewBookingRide('.$param.');" ';              
                    
                    $row[] ='   <a data-id="'.$i.'" class="btn btn-sm btn-info" '.$event.' href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Map">
                                <i class="fa fa-map-marker"></i>
                            </a>                
                                                     
                        ';
                    } else {
                        $row[] = '';
                    }
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Ride_map->count_all(),
                "recordsFiltered" => $this->Ride_map->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewMapList() // view data of booking ride   
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                  
                if( !empty($postData['key']) ) {
                    $mapData = $this->Common_model->select("current_lat as lat,current_lng as lng",TB_BOOKING_LAT_LNG,array("booking_id" => $postData['key']));     
                    if($postData['bookingType'] == "ride") {
                        $resultStartEndLatLng = $this->Common_model->select("pick_up_point,drop_point,pick_latlong,drop_latlong",TB_RIDE_BOOKING,array("booking_id" => $postData['key']));
                    }
                    if($mapData){                                                
                        echo json_encode(array("status" => "success", "action" => "view", "rideMap" => $mapData,'latLng'=>$resultStartEndLatLng[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }                  
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide booking id.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}