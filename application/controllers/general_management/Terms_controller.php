<?php 
/*
  Author : Kiran
  Page :  Terms_controller.php
  Description : Terms controller use for Terms & Condition functionality
*/
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Terms_controller extends CI_Controller
{
	public function __construct()
  {
    parent::__construct();
    $this->data = array(
      'pageTitle' => 'Tulii : General management',
      'isActive' => 'active',
      'roleData' => $this->Common_model->select("*", TB_ROLES, array("role_status" => "1", "role_id !=" => 1))
    );
    $this->load->model('TermsModel','terms'); 
  }
  
  // Default page open
  public function index()
  {
    if(is_user_logged_in()) {  
      $this->load->view("includes/header", $this->data);
      $this->load->view('general_management/Terms', $this->data);            
      $this->load->view("includes/footer");     
    } else {
      $this->session->sess_destroy();
      redirect('login');
    }
  }

  public function termsList() // list of service provider and tutor data
  {   
    if(is_user_logged_in()) {
      $resultTermsList = $this->terms->get_datatables();
      $data = array();
      $no = $_POST['start'];
      $i = 1;
      foreach ($resultTermsList as $termsData) {
        $termsId = $this->encrypt->encode($termsData->term_id);            
        $no++;
        $row = array();
        $row[] = $no;
        $row[] = $termsData->role_name;
        $row[] = $termsData->title;
        $row[] = '
          <a data-id="'.$i.'" data-row-id="'.$termsId.'" class="btn btn-sm btn-primary" onclick="viewTerms(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i>
          </a>
          <a data-id="'.$i.'" data-row-id="'.$termsId.'" class="btn btn-sm btn-info" onclick="editTermsData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i>
          </a>
          <a data-id="'.$i.'" data-row-id="'.$termsId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteTermsData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i>
          </a>                                   
        ';
        
        $data[] = $row;        
        $i++;
      }

      $output = array(
        "draw" => $_POST['draw'],
        "recordsTotal" => $this->terms->count_all(),
        "recordsFiltered" => $this->terms->count_filtered(),
        "data" => $data,
      );

      //output to json format
      echo json_encode($output);
    } else {
      echo(json_encode(array('status'=>'logout')));
      $this->session->sess_destroy();
      redirect('login');
    } 
  }

  // view terms
  function viewTerms()
  {
    if(is_ajax_request()) {
      if(is_user_logged_in()) {
        $key = $this->input->post('key');
        $termsId = $this->encrypt->decode($key);
        $resultTermsDesc = $this->Common_model->select("role_id, title, description", TB_TERMS, array("term_id" => $termsId));
        echo(json_encode(array('status' => 'success', "data" => $resultTermsDesc[0])));
      } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      }
    }  
  }
  
  // Delete terms
  function deleteTerms()
  {
    if(is_ajax_request()) {
      if(is_user_logged_in()) {
        $key = $this->input->post('key');
        $termsId = $this->encrypt->decode($key);
        $resultTermsDelete = $this->Common_model->update(TB_TERMS, array("term_id" => $termsId), array("isDeleted" => "1"));
        if($resultTermsDelete) {
          echo(json_encode(array('status' => 'success', "msg" => "Terms & Condition has been deleted successfully.")));
        } else {
          echo(json_encode(array('status' => 'error', "msg" => "Terms & Condition has been deleted fail.")));  
        }        
      } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      }
    }  
  }

  function addEditTerms()
  { 
    if(is_ajax_request()) {
      if(is_user_logged_in()) { 
        $postData = $this->input->post();
        if(!empty($postData['editData'])) {
          $cond = array("term_id" => $this->encrypt->decode($postData['editData']));
          $data = array('title' => $postData['title'], 'description' => $postData['terms_data'],'udpated_at'=>date('Y-m-d H:i:s'));
          $resultUpdate = $this->Common_model->update(TB_TERMS, $cond, $data);  
          if ($resultUpdate) { 
            echo(json_encode(array('status'=>"success" , "msg" => "Terms & Condition has been updated successfully."))); 
          } else { 
            echo(json_encode(array('status'=> "error", "msg" => "Something went wrong."))); 
          }
        } else {
          $checkRoleIdExists = $this->Common_model->select("role_id", TB_TERMS, array('role_id' => $postData['userRole'], 'isDeleted' => '0'));
          if (count($checkRoleIdExists) > 0 ) {
            echo(json_encode(array('status'=>"warning", "msg" => "Terms & Condition already exists for this role"))); 
          } else {
            $data = array('role_id' => $postData['userRole'], 'title' => $postData['title'], 'description' => $postData['terms_data'], 'status' => '1', 'isDeleted' => '0', 'created_date' => date('Y-m-d H:i:s'));
            $result = $this->Common_model->insert(TB_TERMS, $data);  
            if ($result) { 
              echo(json_encode(array('status'=>"success" , "msg" => "Terms & Condition has been inserted successfully."))); 
            } else { 
              echo(json_encode(array('status'=> "error", "msg" => "Something went wrong."))); 
            }
          }       
        }    
      } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      } 
    }
  }
}

