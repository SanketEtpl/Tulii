<?php 

/*

Author : Rajendra pawar 
Page :  Terms_controller.php
Description : Terms controller use for Terms & Condition functionality

*/
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class Terms_controller extends BaseController
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('general_management/terms_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
     public function index()
    {
    	//$this->global['pageTitle'] = 'Tulii : General management';
        
        //$this->loadViews("dashboard", $this->global, NULL , NULL);
        $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active'         
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('dashboard',$this->data);            
        $this->load->view("includes/footer");     
    }

  function termsDetail()
  { 
        if($this->isAdmin() == TRUE)
        {
          $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active',
            'termsRecords' => $this->terms_model->termsData()            
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('general_management/Terms',$this->data);            
        $this->load->view("includes/footer");
       /* $data['termsRecords'] = $this->terms_model->termsData(); 
        $this->global['pageTitle'] = 'Tulii : General management';
        $this->loadViews("general_management/Terms", $this->global, $data, NULL);*/
       }
        else
        {         	
       $this->loadThis(); 
       }

}
  function updateTerms()
  { 
        if($this->isAdmin() == TRUE)
        {
        $message_body = $this->input->post('message_body');
         $terms_id = $this->input->post('terms_id');
         $termsInfo = array('terms_message'=>$message_body, 'updated_at'=>date('Y-m-d H:i:s'),
                'updatedBy'=>$this->vendorId);
         $result = $this->terms_model->updateTermsModel($terms_id,$termsInfo);  

          if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }           
        }
        else
        {                  
        $this->loadThis(); 
       }

}
}

