<?php 

/*

Author : Rajendra pawar 
Page :  Rating_controller.php
Description : Rating controller use  for Rating functionality

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class Rating_controller extends BaseController
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('general_management/rating_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	//$this->global['pageTitle'] = 'Tulii : General management';
        
        //$this->loadViews("dashboard", $this->global, NULL , NULL);
        $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active'         
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('dashboard',$this->data);            
        $this->load->view("includes/footer"); 
            
    }

  function ratingDetails()
  { 
        if($this->isAdmin() == TRUE)
        {
            $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active'            
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('general_management/rating',$this->data);            
        $this->load->view("includes/footer");
        //$this->global['pageTitle'] = 'Tulii : General management';
       // $this->loadViews("general_management/rating", $this->global, NULL, NULL);              
        }
        else
        { 
         $this->loadThis();  
       }

}
   function deleteRating()
    {  

        if($this->isAdmin() == TRUE)
        {
         $ratingId = $this->input->post('ratingId');
            $ratingInfo = array('isDeleted'=>'1', 'updated_at'=>date('Y-m-d H:i:s'),
                'updatedBy'=>$this->vendorId);
            
            $result = $this->rating_model->deleteRating($ratingId, $ratingInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
        else
        {
           echo(json_encode(array('status'=>'access')));    
        }
    }
    function ratingList()
    {
      if($this->isAdmin() == TRUE)
        {
            $list = $this->rating_model->get_datatables();

        $data = array();
        $no = $_POST['start'];
        $i = 1;
        
         foreach($list as $record)
                        {

             $button ='<a class="btn btn-sm btn-danger deleteRating" href="#" data-ratingid="'. $record->rating_id .'"  data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>';
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->rating;
            $row[] = $record->rating_comment;
            $row[] = $button;
            $data[] = $row;
         }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->rating_model->count_all(),
                        "recordsFiltered" => $this->rating_model->count_filtered(),
                        "data" => $data,
                );          

          echo json_encode($output);
           
        }
        else
        {
       
     $this->loadThis();
    }   
           
    }
}