<?php 

/*

Author : Rajendra pawar 
Page :  AboutUs_controller.php
Description : About Us controller use for About us functionality

*/
if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class AboutUs_controller extends BaseController
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('general_management/about_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
     public function index()
    {

        $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active'         
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('dashboard',$this->data);            
        $this->load->view("includes/footer");   

    	//$this->global['pageTitle'] = 'Tulii : General management';
        
       // $this->loadViews("dashboard", $this->global, NULL , NULL);
            
    }

  function aboutUsDetail()
  { 
        if($this->isAdmin() == TRUE)
        {

          $this->data = array(
            'pageTitle' => 'Tulii : General management',
            'isActive' => 'active',
            'userRecords' => $this->about_model->aboutUsData()          
        );
        $this->load->view("includes/header",$this->data);
        $this->load->view('general_management/aboutUs',$this->data);            
        $this->load->view("includes/footer");
        /*$data['userRecords'] = $this->about_model->aboutUsData(); 
        $this->global['pageTitle'] = 'Tulii : General management';
        $this->loadViews("general_management/aboutUs", $this->global, $data, NULL);*/
           
        }
        else
        {  
        $this->loadThis();
       }

}
  function updateAbout()
  { 
        if($this->isAdmin() == TRUE)
        {
         $message_body = $this->input->post('message_body');
         $about_id = $this->input->post('about_id');
         $aboutInfo = array('about_message'=>$message_body, 'updated_at'=>date('Y-m-d H:i:s'),
                'updatedBy'=>$this->vendorId);
         $result = $this->about_model->updateAboutModel($about_id,$aboutInfo);  

          if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }  

           
        }
        else
        {                  
           $this->loadThis();        
       }

}
}

