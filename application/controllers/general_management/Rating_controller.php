<?php
/*
* @author : kiran N.
* description: manage the rating data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Rating_controller extends CI_Controller
{
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Rating management', 
            'isActive'  => 'active'
        );
        $this->load->model('Rating_model','rating');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');       
            $this->data['roleData'] = $this->Common_model->select('*', TB_ROLES, array("role_id !=" => ROLE_ADMIN)); 
            $this->load->view('includes/header',$this->data);
            $this->load->view('general_management/rating',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of rating list data
    {
        if(is_user_logged_in()){            
            $resultRatingData = $this->rating->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($resultRatingData as $rating) {
                $ratingId = $this->encrypt->encode($rating->rtype_id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $rating->role_name;
                $row[] = $rating->rating_type;                                             
                $row[] =
                '                  
                    <a data-id="'.$i.'" data-row-id="'.$ratingId.'" class="btn btn-sm btn-info" onclick="editRating(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-id="'.$i.'" data-row-id="'.$ratingId.'" class="btn btn-sm btn-danger" onclick="deleteRating(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>                           
                ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->rating->count_all(),
                "recordsFiltered" => $this->rating->count_filtered(),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteRating() // delete record of Rating 
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]) {
                    $checkedUseRatingId = $this->Common_model->select("*", TB_RATE_DETAILS, array('rating_id' => $this->encrypt->decode($postData['key'])));
                    if(count($checkedUseRatingId) > 0) {
                        echo json_encode(array("status"=>"warning","action"=>"delete","msg"=>"This rating assigned in rating details.")); exit; 
                    } else {
                        $data = array(
                            "is_deleted" => "1",
                            "updated_date" => date('Y-m-d H:i:s')
                        );
                        $deleteId = $this->Common_model->update(TB_RATE_TYPE, array('rtype_id' => $this->encrypt->decode($postData['key'])), $data);
                        if($deleteId) {                                                
                            echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Rating record has been deleted successfully.")); exit;  
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Something went wrong.")); exit; 
                        }
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewRating() // view data of Rating  
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]) {                    
                    $ratingData = $this->Common_model->select("*", TB_RATE_TYPE, array('rtype_id' => $this->encrypt->decode($postData['key'])));
                    if(count($ratingData) > 0) {                                                
                        echo json_encode(array("status"=>"success","action" => "view", "ratingData" => $ratingData[0])); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of rating
    public function addRating()
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post();
                if(!empty($postData["editData"])) { // update data
                    $updateArr = array(  
                        "rating_type" => $postData['title'],
                        "role_id" => $postData['userRole'],
                        "updated_date" => date("Y-m-d H:i:s")                        
                    );
                    $updateId = $this->Common_model->update(TB_RATE_TYPE, array('rtype_id' => $this->encrypt->decode($postData['editData'])),$updateArr);
                    if($updateId){
                        echo json_encode(array("status"=>"success","action"=>"update","message" => "Rating has been updated successfully.")); exit;   
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {       
                    if(!empty($postData)){ // add data               
                    $data = array(
                        "rating_type" => $postData['title'],
                        "role_id" => $postData['userRole'],    
                        "status" => "1",
                        "is_deleted" => "0",           
                        "created_date" => date("Y-m-d H:s:i")           
                    );
                    $insertId = $this->Common_model->insert(TB_RATE_TYPE, $data);
                    if($insertId){
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Rating has been added successfully.")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the rating information.")); exit;    
                    }
                }                   
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}