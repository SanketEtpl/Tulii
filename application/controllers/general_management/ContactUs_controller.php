<?php 
/*
Author : Kiran n
Page :  ContactUs_controller.php
Description : Contact Us controller use for contact us functionality
*/

if(!defined('BASEPATH')) exit('No direct script access allowed'); 

class ContactUs_controller extends CI_Controller
{
	public function __construct()
  {
    ob_start();
    parent::__construct();
    $this->load->model('Contact_model', 'contact');                
    $this->data = array(
      'pageTitle' => 'Tulii : General management of contact us',
      'isActive' => 'active'         
    );
  }

  public function index()
  {      
    if(is_user_logged_in()) {
      $this->load->view("includes/header",$this->data);
      $this->load->view('general_management/contactUs');            
      $this->load->view("includes/footer");   
    } else {
      $this->session->sess_destroy();
      redirect('login');
    }     
  }


  public function contactList()
  {
    if(is_ajax_request()) {
      if(is_user_logged_in()) {
        $resultContactList = $this->contact->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach($resultContactList as $record)
        {
          $no++;
          $row = array();
          $row[] = $no;
          $row[] = $record->contact_name;
          $row[] = $record->contact_email;
          $row[] = $record->contact_phone_number;
          $row[] = '<a class="btn btn-sm btn-info viewContact" href="javascript:void(0)" data-contactid="'. $record->contact_id .'"   data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>
          ';
          $data[] = $row;
        }

        $output = array(
          "draw" => $_POST['draw'],
          "recordsTotal" => $this->contact->count_all(),
          "recordsFiltered" => $this->contact->count_filtered(),
          "data" => $data,
        );          

        echo json_encode($output);         
      } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      } 
    }    
  }

  function viewContactDetails()
  {
    if(is_ajax_request()) {
      if(is_user_logged_in()) {
        $contactId = $this->input->post('contact_id');      
        $resultContactDetails = $this->Common_model->select("contact_message", TB_CONTACT, array("contact_id" => $contactId, "status" => "1"));        

        if( $resultContactDetails ) {            
          echo json_encode(array("status" => TRUE,"data" => $resultContactDetails[0]));
        } else { 
          echo(json_encode(array("status"=>FALSE))); 
        }        
      } else {
        echo(json_encode(array('status'=>'logout')));
        $this->session->sess_destroy();
        redirect('login');
      } 
    }    
  }
}