<?php 

/*

Author : Rajendra pawar 
Page :  CareDriver_controller.php
Description : Care Driver controller use for care driver managment functionality

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class CareDriver_controller extends BaseController
{
	public function __construct()
    {
       
        parent::__construct();
        $this->load->model('care_driver_management/careDrive_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	$this->global['pageTitle'] = 'Tulii : Dashboard';
        
        $this->loadViews("dashboard", $this->global, NULL , NULL);
            
    }

  function listOperation()
  { 
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
           
        }
        else
        {

            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->careDrive_model->careDriverListingCount($searchText);

            $returns = $this->paginationCompress_new( "userListing/", $count, 5 );
            
            $data['userRecords'] = $this->careDrive_model->careDriverListing($searchText, $returns["page"], $returns["segment"]);
            $this->global['pageTitle'] = 'Tulii : Care Driver Listing';

            $this->loadViews("care_driver_management/careDriverList", $this->global, $data , NULL);	      
           
    }

}
 function deleteCareDriver()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
             $result = $this->common_model->update(TB_USERS,array("user_id"=>$userId), array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    function careDriverStatus()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $user_status = $this->input->post('user_status');
            $result = $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_status'=>$user_status,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
            
            if ($result > 0) { 
             echo(json_encode(array('status'=>TRUE))); 

          }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
   function editCareDriverDetails()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $result=$this->common_model->select("user_name,user_email,user_phone_number",TB_USERS,array("user_id"=>$userId));
                if ($result > 0) {             
               echo json_encode(array("status" => TRUE,"data" => $result[0]));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }
        }
    }
  function editCareDriver()
  {
   if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
           
        }
        else
        {

            $this->load->library('form_validation');   
            

            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
             if($this->form_validation->run() == FALSE)
            {
                $this->listOperation();    
            }
            else {

            if($this->input->post())
            {
            $userId = $this->input->post('userId');     
            $user_name = $this->input->post('fname');
            $user_email = $this->input->post('email');
            $user_phone_number = $this->input->post('mobile');
            $password = $this->input->post('password');
           if(empty($password))  
                {  
              $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_name'=>$user_name,'user_email'=>$user_email,'user_phone_number'=>$user_phone_number,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));

              } else {

                $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_name'=>$user_name,'user_email'=>$user_email,'user_password'=>getHashedPassword($password),'user_phone_number'=>$user_phone_number,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
              }
            
            } 
        $this->listOperation();
        } 
    }

 }
 function checkEmilExist()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $user_id = $this->input->post('userId');
            $user_email = $this->input->post('email');
             if(empty($user_id))  
            {   $user_id =0;  }  
           if(empty($user_email))  
            {  $user_email = $this->input->post('email_new');  }  
            $result=$this->careDrive_model->select_exist(TB_USERS,$user_id,$user_email);

            if(empty($this->input->post('userId')))  {
               if(empty($result)){ echo("true"); }
               else { echo("false"); }
            } else{


            if(empty($result))  
            {
            echo(json_encode(array("status"=>FALSE)));
            }
            else {             
              echo(json_encode(array('status'=>TRUE)));          
          }
          }  
        }
    }


  function addCareDriver()
  {
   if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
           
        }
        else
        {

            $this->load->library('form_validation');   
            

            $this->form_validation->set_rules('fname_new','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email_new','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password_new','Password','matches[cpassword_new]|max_length[20]');
            $this->form_validation->set_rules('cpassword_new','Confirm Password','matches[password_new]|max_length[20]');
            $this->form_validation->set_rules('mobile_new','Mobile Number','required|min_length[10]|xss_clean');
             if($this->form_validation->run() == FALSE)
            {
                $this->listOperation();    
            }
            else {

            if($this->input->post())
            {
            $user_name = $this->input->post('fname_new');
            $user_email = $this->input->post('email_new');
            $user_phone_number = $this->input->post('mobile_new');
            $password = $this->input->post('password_new');
            $roleId = 4;
             $result_old=$this->common_model->select("roleId",TB_role,array("role"=>'Care Driver'));

              if(!empty($result_old))
                {  

                $roleId = $result_old[0]['roleId'];  
               
                } 


              $this->common_model->insert(TB_USERS,array('user_name'=>$user_name,'user_email'=>$user_email,'user_password'=>getHashedPassword($password),'user_phone_number'=>$user_phone_number,'roleId'=>$roleId,'createdBy'=>$this->vendorId, 'created_at'=>date('Y-m-d H:i:s')));    
            
            } 
         $this->listOperation();
        } 
    }

 }
}