<?php 

/*

Author : Rajendra pawar 
Page :  CareDriver_controller.php
Description : Care Driver controller use for care driver managment functionality

*/

if(!defined('BASEPATH')) exit('No direct script access allowed');
 
 require APPPATH . '/libraries/BaseController.php';

class CareDriver_controller extends BaseController
{
	public function __construct()
    {
       ob_start();
        parent::__construct();
        $this->load->model('care_driver_management/careDrive_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->load->helper('url');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	$this->global['pageTitle'] = 'Tulii : Dashboard';
      $this->loadViews("dashboard", $this->global, NULL , NULL);
            
    }

  function listOperation()
  { 
        if($this->isAdmin() == TRUE)
        {
            $this->global['pageTitle'] = 'Tulii : Care Driver Listing';
            $this->loadViews("care_driver_management/careDriverList", $this->global, NULL , NULL);  
           
        }
        else
        {
       $this->loadThis();           
           
    }

}
 function deleteCareDriver()
    {
        if($this->isAdmin() == TRUE)
        {
            $userId = $this->input->post('userId');
             $result = $this->common_model->update(TB_USERS,array("user_id"=>$userId), array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
        else
        {
        echo(json_encode(array('status'=>'access')));    
        }
    }
    function careDriverStatus()
    {
        if($this->isAdmin() == TRUE)
        {
           $userId = $this->input->post('userId');
            $user_status = $this->input->post('user_status');
            $result = $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_status'=>$user_status,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
            
            if ($result > 0) { 
             echo(json_encode(array('status'=>TRUE))); 

          }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
        else
        {
         echo(json_encode(array('status'=>'access')));  
        }
    }
   function editCareDriverDetails()
    {
        if($this->isAdmin() == TRUE)
        {
           $userId = $this->input->post('userId');
            $result=$this->common_model->select("user_name,user_email,user_phone_number",TB_USERS,array("user_id"=>$userId));
                if ($result > 0) { 

                 $user_name= $result[0]['user_name'];
                 $user_email=$result[0]['user_email'];
                 $user_phone_number=$result[0]['user_phone_number'];
                
                $data='<div class="box">
                     <form role="form"  method="post" id="editcareDriverFrm" name="editcareDriverFrm">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" id="fname" placeholder="Full Name" name="fname" value="'.$user_name.'" maxlength="128">
                                        <input type="hidden" value="'.$userId.'" name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" onblur="checkEmilExist(\'validate\');" onchange="checkEmilExist(\'validate\');" class="form-control required" id="email" placeholder="Enter email" name="email" value="'.$user_email.'"  maxlength="128">
                                      <label for="email_new" class="error" style="display:none;" id="email_error_id">Email already taken</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required" id="mobile" placeholder="Mobile Number" value="'.$user_phone_number.'" name="mobile"  maxlength="10">
                                    </div>
                                </div>
                               </div>
                        </div><!-- /.box-body -->   
                         </form>                
                </div>';   
               echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }
            
        }
        else
        {
         echo(json_encode(array('status'=>'access')));  
        }
    }
  function editCareDriver()
  {
   if($this->isAdmin() == TRUE)
        {
         $this->load->library('form_validation');           

            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
             if($this->form_validation->run() == FALSE)
            {
                $this->listOperation();    
            }
            else {

            if($this->input->post())
            {
            $userId = $this->input->post('userId');     
            $user_name = $this->input->post('fname');
            $user_email = $this->input->post('email');
            $user_phone_number = $this->input->post('mobile');
            $password = $this->input->post('password');
           if(empty($password))  
                {  
              $result= $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_name'=>$user_name,'user_email'=>$user_email,'user_phone_number'=>$user_phone_number,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));

              } else {

               $result= $this->common_model->update(TB_USERS,array("user_id"=>$userId),array('user_name'=>$user_name,'user_email'=>$user_email,'user_password'=>getHashedPassword($password),'user_phone_number'=>$user_phone_number,'updatedBy'=>$this->vendorId, 'updated_at'=>date('Y-m-d H:i:s')));
              }
              if ($result > 0) { 
             echo(json_encode(array('status'=>TRUE))); 

          }
            else { echo(json_encode(array('status'=>FALSE))); }
            
            } 
        
        } 

           
        }
        else
        {
      $this->loadThis();            
    }

 }
 function checkEmilExist()
    {
        if($this->isAdmin() == TRUE)
        {
         $user_id = $this->input->post('userId');
            $user_email = $this->input->post('email');
             if(empty($user_id))  
            {   $user_id =0;  }  
           if(empty($user_email))  
            {  $user_email = $this->input->post('email_new');  }  
            $result=$this->careDrive_model->select_exist(TB_USERS,$user_id,$user_email);

            if(empty($this->input->post('userId')))  {
               if(empty($result)){ echo("true"); }
               else { echo("false"); }
            } else{


            if(empty($result))  
            {
            echo(json_encode(array("status"=>FALSE)));
            }
            else {             
              echo(json_encode(array('status'=>TRUE)));          
          }
          }     

        }
        else
        {
        echo(json_encode(array('status'=>'access')));   
        }
    }


  function addCareDriver()
  {
    $result = 0;
   if($this->isAdmin() == TRUE)
        { $this->load->library('form_validation');           

            $this->form_validation->set_rules('fname_new','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('email_new','Email','trim|required|valid_email|xss_clean|max_length[128]');
            $this->form_validation->set_rules('password_new','Password','matches[cpassword_new]|max_length[20]');
            $this->form_validation->set_rules('cpassword_new','Confirm Password','matches[password_new]|max_length[20]');
            $this->form_validation->set_rules('mobile_new','Mobile Number','required|min_length[10]|xss_clean');
             if($this->form_validation->run() == FALSE)
            {
              $result = 0;  
            }
            else {

            if($this->input->post())
            {
            $user_name = $this->input->post('fname_new');
            $user_email = $this->input->post('email_new');
            $user_phone_number = $this->input->post('mobile_new');
            $password = $this->input->post('password_new');
            $roleId = 4;
             $result_old=$this->common_model->select("roleId",TB_ROLES,array("role"=>'Care Driver'));

              if(!empty($result_old))
                {  

                $roleId = $result_old[0]['roleId'];  
               
                } 


             $result= $this->common_model->insert(TB_USERS,array('user_name'=>$user_name,'user_email'=>$user_email,'user_password'=>getHashedPassword($password),'user_phone_number'=>$user_phone_number,'roleId'=>$roleId,'createdBy'=>$this->vendorId, 'created_at'=>date('Y-m-d H:i:s')));    
            
            } 
         
        } 
        if ($result > 0) { 
             echo(json_encode(array('status'=>TRUE))); 

          }
            else { echo(json_encode(array('status'=>FALSE))); }   
           
        }
        else
        {
        $this->loadThis();
            
       }
    
 }

  function careDriverList()
  { 
        if($this->isAdmin() == TRUE)
        {
         $list = $this->careDrive_model->get_datatables();  

         $data = array();
        $no = $_POST['start'];
        $i = 1;
        $status_id=1;
        $rowActive_id="active".$status_id;
        $rowInActive_id="inActive".$status_id;

         foreach($list as $record)
                        {
//  print_r($list);exit;
                  $button ="";  
                    
                      if($record->user_status == 1)
                      {  $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="careDriverStatus('.$record->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');" ';
                         $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="careDriverStatus('.$record->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');"';  } 
                    else{ $active_btn_class='class="btn btn-sm btn-active-disable" onclick="careDriverStatus('.$record->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');"';
                          $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="careDriverStatus('.$record->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');" ';
                      }

                $button ='<button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button>&nbsp;';
                $button.='<button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button>';

                 $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->user_email;
            $row[] = $record->user_phone_number;
            $row[] = $button;
             $row[] ='   <a data-userid="'.$record->user_id.'"  class="btn btn-sm btn-info editCareDriver"  href="javascript:void(0)">
                            <i class="fa fa-pencil"></i>
                        </a>
                         <a data-userid="'.$record->user_id.'" class="btn btn-sm btn-danger deleteCareDriver"  href="javascript:void(0)">
                            <i class="fa fa-trash"></i>
                        </a>';
$data[] = $row;
                     $status_id++;
                    $rowActive_id="active".$status_id;
                    $rowInActive_id="inActive".$status_id;

         }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->careDrive_model->count_all(),
                        "recordsFiltered" => $this->careDrive_model->count_filtered(),
                        "data" => $data,
                );          

          echo json_encode($output);  
           
        }
        else
        {
          $this->loadThis();    

    }

}
function addCareDriverPopup()
{
  $data='<div class="box">
                   <form role="form"  method="post" id="addcareDriverfrm" name="addcareDriverfrm">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname_new">Full Name</label>
                                        <input type="text" class="form-control required" id="fname_new" placeholder="Full Name" name="fname_new"  maxlength="128">                                       
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email_new">Email address</label>
                                        <input type="email" class="form-control required" id="email_new" placeholder="Enter email" name="email_new"  maxlength="128">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password_new">Password</label>
                                        <input type="password" class="form-control required" id="password_new" placeholder="Password" name="password_new" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword_new">Confirm Password</label>
                                        <input type="password" class="form-control required" id="cpassword_new" placeholder="Confirm Password" name="cpassword_new" maxlength="10">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile_new">Mobile Number</label>
                                        <input type="text" class="form-control required" id="mobile_new" placeholder="Mobile Number" name="mobile_new"  maxlength="10">
                                    </div>
                                </div>
                                  
                            </div>
                        </div><!-- /.box-body -->     
                         </form>              
                </div>';
       echo json_encode(array("status" => TRUE,"data" => $data));

  }
}