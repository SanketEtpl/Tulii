<?php
/*
* @author : Kiran.
* description: manage the Organization data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Organization_controller extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Organization management', 
            'isActive'  => 'active'
        );
        $this->load->model("Organization_models","Organization");
        $this->load->model("common_model");
        $this->load->model("Pagination_models");   
        $this->load->library('pagination');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->view('includes/header',$this->data);
            $this->load->view('organization/organization');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function ajax_list() // list of school list data
    {
        if(is_user_logged_in()){          
            $orgData = $this->Organization->get_datatables();              
            $data = array();
            $no = $_POST['start'];
            $i = 1;

            foreach ($orgData as $value) {
                $userID = $this->encrypt->encode($value->user_id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($value->user_name);
                $row[] = $value->user_phone_number;
                $row[] = $value->user_email;
                $row[] = $value->user_address;                
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userID.'" class="btn btn-sm btn-info" onclick="viewOrgEmp(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                
                                                     
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(

                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Organization->count_all(),
                "recordsFiltered" => $this->Organization->count_filtered(),
                "data" => $data,

            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function viewEmpList() // view data of students  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                //$postData = $this->input->post();             
                //if(!empty($postData["key"])) {
                   // echo "hello codeigniter"; exit;
                    /*$emp_details = $this->common_model->select("CONCAT(stud_name,' ',stud_username) as empName,stud_age as age,stud_address as address,parent_phone as phone,parent_email as email",TB_STUDENT,array("user_id"=>$this->encrypt->decode($postData["key"])));
                    $rows = '';
                    if (!empty($emp_details)) {
                        foreach ($emp_details as $emp) {
                            
                                $rows .= '                                        
                                <tr>                                              
                                  <td  class="text-left">' . $emp['empName'] . '</td>
                                  <td  class="text-left">' . $emp['age'] . '</td>
                                  <td  class="text-left">' . $emp['address'] . '</td>
                                  <td  class="text-left">' . $emp['phone'] . '</td>
                                  <td  class="text-left">' . $emp['email'] . '</td>
                                </tr>';
                        }
                    } else {
                        $rows .= '<tr><td colspan="10" align="center">No Records Available.</td></tr>';
                    }*/

                    $orgData = $this->Pagination_models->get_datatables();              
                    //print_r($orgData);exit;
                    $data = array();
                    $no = $_POST['start'];
                    $i = 1;

                    foreach ($orgData as $value) {
                        //$userID = $this->encrypt->encode($value->stud_id);
                        //$no++;
                        $row = array();
                        //$row[] = $no;
                        $row[] = ucfirst($value->stud_username);
                        $row[] = $value->parent_email;
                        /*$row[] = $value->user_email;
                        $row[] = $value->user_address;                
                        $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userID.'" class="btn btn-sm btn-info" onclick="viewOrgEmp(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                        <i class="fa fa-eye"></i>
                                    </a>                
                                                             
                                ';*/
                        $data[] = $row;             
                        $i++;
                    }
             
                    $output = array(

                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Pagination_models->count_all(),
                        "recordsFiltered" => $this->Pagination_models->count_filtered(),
                        "data" => $data,

                    );

                    //output to json format
                    echo json_encode($output);



                   /* if($rows) {                                                
                        echo json_encode(array("status"=>"success","action"=>"view","rows"=>$rows)); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    } */                    
                /*} else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide user id.")); exit;   
                }*/
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

/*    public function viewEmpList() // view data of students  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();             
                if(!empty($postData["key"])) {

                    $emp_details = $this->common_model->select("CONCAT(stud_name,' ',stud_username) as empName,stud_age as age,stud_address as address,parent_phone as phone,parent_email as email",TB_STUDENT,array("user_id"=>$this->encrypt->decode($postData["key"])));
                    $rows = '';
                    if (!empty($emp_details)) {
                        foreach ($emp_details as $emp) {
                            
                                $rows .= '                                        
                                <tr>                                              
                                  <td  class="text-left">' . $emp['empName'] . '</td>
                                  <td  class="text-left">' . $emp['age'] . '</td>
                                  <td  class="text-left">' . $emp['address'] . '</td>
                                  <td  class="text-left">' . $emp['phone'] . '</td>
                                  <td  class="text-left">' . $emp['email'] . '</td>
                                </tr>';
                        }
                    } else {
                        $rows .= '<tr><td colspan="10" align="center">No Records Available.</td></tr>';
                    }

                    if($rows) {                                                
                        echo json_encode(array("status"=>"success","action"=>"view","rows"=>$rows)); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }                     
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provide user id.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } */
}