<?php
/*
* @author : kiran N.
* description: show the all user management controller
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 require APPPATH . '/libraries/BaseController.php';
class UserController extends BaseController {
 
    public function __construct()
    {

        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : User Profile'        
        );
        $this->load->model('User_model','User');
        $this->isLoggedIn();
    }
 
    public function index()
    {
        if($this->isAdmin() == TRUE){
            $this->load->helper('url');                                
            $this->load->view('user/user',$this->data);
        }
        else{
            $this->loadThis();    
        }
    }
 
    public function ajax_list() // list of parent data
    {
         if($this->isAdmin() == TRUE){ 
            $list = $this->User->get_datatables();        
            //echo $this->db->last_query();exit;
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $userData) {
                $userId = $this->encrypt->encode($userData->user_id);  
                $button ="";  
                if($userData->user_status == 1)
                {   
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus('.$userData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');" ';
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus('.$userData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');"';
                } 
                else
                {   
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus('.$userData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');"';
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus('.$userData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');" ';
                }
                $button ='<button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button>&nbsp;';
                $button.='<button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button>&nbsp;';
                $type="";               
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $userData->user_name;
                $row[] = $userData->user_email;
                $row[] = $userData->user_phone_number;
                $row[] = $userData->role;
                $row[] = $button;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editUserData(this)" href="javascript:void(0)">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteUserData(this)" href="javascript:void(0)">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->User->count_all(),
                            "recordsFiltered" => $this->User->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            echo(json_encode(array('status'=>'access')));
        }
    }

    public function userStatusChange() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'],'updatedBy'=>$this->session->userdata('userId'), 'updated_at'=>date('Y-m-d H:i:s')));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                echo(json_encode(array('status'=>'access')));
            }
        }        
    }

    public function deleteUser() // delete record of parent & also kids data
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"User has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                echo(json_encode(array('status'=>'access')));
            }
        }
    }

    // check email availability 
    public function check_email_availability()
    {
         if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            // check email & user type are already exist or not
            $data = $this->Common_model->select('user_id,user_email',TB_USERS,array('user_email'=>$postData['email']));
            if($data)
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>true,'message' => 'Email already exists.')));           
            }
            else
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>false)));                          
            }  
        } else {
            echo(json_encode(array('status'=>'access')));
        }         
    }

   public function viewUserInfo() // view data of user 
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $userData = $this->Common_model->select("user_id,user_email,user_name,user_phone_number,roleId,user_category_id,user_sub_category_id",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if($userData){  
                         
                        if($userData[0]['roleId'] == ROLE_SERVICE_PROVIDER)
                        {
                            $data='';         
                            $list = $this->Common_model->select("category_id,category",TB_CATEGORIES,array('status'=>1));
                   
                            if ($list > 0) { 
                            $data .='<div class="col-md-6">
                                    <div class="form-group">
                                    <label class="control-label" for="category">Category *</label>
                                    <select class="form-control" onchange="subCategory();"   required="true" id="category" name="category">
                                    <option value="" disable="" selected="" hidden="">Select category</option> '; 
                            foreach($list as $key => $value) {  // echo $value;exit;
                               if( $value['category_id'] == $userData[0]['user_category_id']) 
                               {
                               $selected="selected";
                               } else { $selected="";}
                              $data .='<option '.$selected.' value="'. $value['category_id'] .'">'. $value['category'] .'</option>'; 
                               }
                             $data .='</select></div></div>'; 
                               } 

              
                  $list_data =  $this->Common_model->select('sub_category_id,name',TB_SUBCATEGORIES,array('status'=>1,'category_id'=>$userData[0]['user_category_id']));
              
             if ($list_data > 0) { 
             $data.='<div id="sub_category_div"><div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="sub_category">Sub Category *</label>
                    <select class="form-control"  required="true" id="sub_category" name="sub_category">
                    <option value="" disable="" selected="" hidden="">Select sub category</option> '; 
              foreach($list_data as $key => $value) {  // echo $value;exit;
                  if( $value['sub_category_id'] == $userData[0]['user_sub_category_id']) 
               {
                $select_option="selected";
               } else { $select_option="";}
             $data .='<option '.$select_option.' value="'. $value['sub_category_id'] .'">'. $value['name'] .'</option>'; 
            }
            $data .='</select></div></div></div>'; 
             }
            echo json_encode(array("status"=>"success_both","action"=>"view","categoryData"=>$data,"parentData"=>$userData[0])); exit; 


                        } else {

                         echo json_encode(array("status"=>"success","action"=>"view","parentData"=>$userData[0])); exit;    
                        }                                                       
                        
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                echo(json_encode(array('status'=>'access')));
            } 
        }
    } 
     // add & update record of parent data
    public function addUser()
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
                $postData = $this->input->post();
                if(!empty($postData["editData"])) { // update data
                    $updateArr = array(  
                        "user_email" =>$postData['email'],
                        "roleId"=>$postData['userRole'],
                        "user_name"=>$postData['userName'],
                        "user_phone_number"=>$postData['phone'],
                        "user_category_id"=>$postData['category'],
                        "user_sub_category_id"=>$postData['sub_category'],
                        "updated_at"=>date("Y-m-d H:s:i")
                    );
                    $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                    if($updateId){
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"User has been updated successfully.")); exit;   
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {         

                    if(!empty($postData)){ // add data               
                    $data = array(
                        "user_email" =>$postData['email'],
                        "roleId"=>$postData['userRole'],
                        "user_name"=>$postData['userName'],
                        "user_phone_number"=>$postData['phone'],
                        "user_category_id"=>$postData['category'],
                        "user_sub_category_id"=>$postData['sub_category'],
                        "user_status"=>"1",
                        "user_password"=>getHashedPassword($postData['password']),
                        "is_user_available"=>"available",
                        "created_at"=>date("Y-m-d H:s:i")           
                        );
                    $recordInsertUser = $this->Common_model->insert(TB_USERS,$data);
                  
                    if($recordInsertUser){
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"User has been inserted successfully ")); exit;    
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                }
                else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the user information")); exit;    
                    }
                }  
            }
            else{
                echo(json_encode(array('status'=>'access')));
            } 
        }     
    } 

    function addCategory()
    {
        if($this->isAdmin() == TRUE)
        {
           
     $list = $this->Common_model->select("category_id,category",TB_CATEGORIES,array('status'=>1));
            $data='';    
             if ($list > 0) { 
             $data='<div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="category">Category *</label>
                    <select class="form-control" onchange="subCategory();" required="true" id="category" name="category">
                    <option value="" disable="" selected="" hidden="">Select category</option> '; 
              foreach($list as $key => $value) {  // echo $value;exit;
             $data .='<option value="'. $value['category_id'] .'">'. $value['category'] .'</option>'; 
            }
            $data .='</select></div></div>'; 
            echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE,"data" =>$data))); }        
        }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        }
    }
    function addSubCategory()
    {
        if($this->isAdmin() == TRUE)
        {
        $category_id = $this->input->post('category_id');   
       $list =  $this->Common_model->select('sub_category_id,name',TB_SUBCATEGORIES,array('status'=>1,'category_id'=>$category_id));
              $data='';    
             if ($list > 0) { 
             $data='<div id="sub_category_div"><div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="sub_category">Sub Category *</label>
                    <select class="form-control" required="true" id="sub_category" name="sub_category">
                    <option value="" disable="" selected="" hidden="">Select sub category</option> '; 
              foreach($list as $key => $value) {  // echo $value;exit;
             $data .='<option value="'. $value['sub_category_id'] .'">'. $value['name'] .'</option>'; 
            }
            $data .='</select></div></div></div>'; 
            echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE,"data" =>$data))); }        
        }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        }
    }
}