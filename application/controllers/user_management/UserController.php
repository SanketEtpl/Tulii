<?php
/*
* @author : kiran N.
* description: show the all user management controller
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 require APPPATH . '/libraries/BaseController.php';
class UserController extends BaseController {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : User management',
            'isActive' => 'active'        
        );
        $this->load->model('User_model','User');
        $this->load->helper("cias"); 
        $this->isLoggedIn();
        ini_set('display_errors', '0');
    }
 
    public function index()
    {
        if($this->isAdminOrParent() == TRUE){
            $this->load->helper('url');
             
            //$this->data['degree'] = $this->Common_model->select("*",TB_DEGREE);
            $this->data['roleData'] = $this->Common_model->select('*',TB_ROLES,array("roleID !="=>ROLE_ADMIN,"roleId !="=>ROLE_PARENTS)); 
            $this->load->view('includes/header',$this->data);
            $this->load->view('user/user',$this->data);
            $this->load->view('includes/footer');
        }
        else{
            $this->loadThis();    
        }
    }

    //get specialization
    public function specializationData()
    {       
        $postData = $this->input->post();
        $dataSpec = $this->Common_model->select("id,spec_name",TB_SPECILIZATION,array("degree_id"=>$postData['degreeID']));
        $specJSON='';
        // if record are exist
        if($dataSpec > 0)
        {
            $specJSON .= '<option value="" disable="" selected="" hidden="">-- Select specialization --</option>';
            foreach ($dataSpec as $key => $name) {
                $specJSON.= '<option value="'.$name['id'].'">'.$name['spec_name'].'</option>';
            }               
        }
        else
        {
            $specJSON.= '<option value="">Specialization not available</option>';
        }
        // convert in json format
        echo json_encode(array('specialization'=>$specJSON));       
    }
 
    public function ajax_list() // list of parent data
    {
         if($this->isAdminOrParent() == TRUE){ 
            $list = $this->User->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $userData) {
                $userId = $this->encrypt->encode($userData->user_id);  
                $button ="";  
                if($userData->user_status == 1)
                {   
                    $a= $userData->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="userStatus_popup('.$a.');" ';

                    $b= $userData->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="userStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $userData->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="userStatus_popup('.$c.');"';

                    $d= $userData->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="userStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                $newRecord = $userData->new_record_status == 1?'<button type="button" class="btn btn-secondary new-record" data-row-id="'.$userId.'" onclick="newRecord(this)" data-toggle="tooltip" data-placement="top" title="" data-original-title="New">New</button>&nbsp;&nbsp;':"&nbsp;&nbsp;";
                $urlEdit = "";
                if($userData->roleId == USER_TYPE_PARENT)
                    $urlEdit = '<a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editParentData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>';                    
                else 
                    $urlEdit = '<a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editUserData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>';
                
                $type="";               
                $no++;
                $row = array();
                $row[] = $no." ".$newRecord;
                $row[] = $userData->user_name;
                $row[] = $userData->user_email;
                $row[] = $userData->user_phone_number;
                $row[] = $userData->role;
                /* <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editUserData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>*/
                if($this->isAdmin() == TRUE){
                    $row[] = $button;
                   /* $row[] = $urlEdit.'  
                                <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteUserData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                    <i class="fa fa-trash"></i>
                                </a>                           
                            ';*/
                    if($userData->roleId == USER_TYPE_PARENT)
                        $row[] = '<a data-id="'.$i.'" data-row-id="'.$userId.'" data-role="'.$userData->role.'" class="btn btn-sm btn-info" onclick="viewsParentData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>';
                    else
                        $row[] = '<a data-id="'.$i.'" data-row-id="'.$userId.'" data-role="'.$userData->role.'" class="btn btn-sm btn-info" onclick="viewUserData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>';
                } else {
                    $row[] = $userData->category;
                    $row[] = $userData->name;
                }

                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->User->count_all(),
                            "recordsFiltered" => $this->User->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            echo(json_encode(array('status'=>'access')));
        }
    }

    public function newRecord() // new record change icon
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$this->encrypt->decode($postData['key'])),array('new_record_status'=>0));            
            if ($result)
                echo(json_encode(array('status'=>"success"))); 
            else
                echo(json_encode(array('status'=>"error"))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }

        // read notification status
    public function notificationStatus() 
    {
        $notificationData = $this->Common_model->fetch_no_of_records_order_by("user_id, roleId, 
            notification_status",TB_USERS,array("notification_status"=>"1"),1,0,"user_id","desc");
        if(count($notificationData[0]) > 1) {
            $user_type ="";
            if ( $notificationData[0]['roleId'] == ROLE_PARENTS ) {
                $user_type = 'Parent';
            } else if ( $notificationData[0]['roleId'] == ROLE_CARE_DRIVER ) {
                $user_type = 'Care driver';
            }
            else if ( $notificationData[0]['roleId'] == ROLE_TUTOR ) {
                $user_type = 'Tutor';
            }
            else if ( $notificationData[0]['roleId'] == SCHOOL ) {
                $user_type = 'School';
            }
            else if ( $notificationData[0]['roleId'] == ORGANIZATION ) {
                $user_type = 'Organization';
            }
            $this->Common_model->update(TB_USERS,array("user_id"=>$notificationData[0]['user_id']),array('notification_status' => "0"));  
            echo(json_encode(array('status'=>"success","message"=>"New ".$user_type." has been register."))); 
        } else {
            echo(json_encode(array('status'=>"error","message"=>"No records found.")));
        }
        
    }

    public function userStatusChange() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
                                                        /*,'updatedBy'=>$this->session->userdata('userId'),*/
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'], 'updated_at'=>date('Y-m-d H:i:s')));            
            
            if ($result) {
                if($postData['user_status'] == "1")
                    $msg = "Admin activated your account. Now you can access your account.";
                else
                    $msg = "Admin deactivated your account due to unsufficient information. You need to contact to admin for more information.";
                $userDetails = $this->Common_model->select('user_id,user_email,user_name,user_device_id',TB_USERS,array('user_id'=>$postData['userId']));      
                $message = '<tr> 
                <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Hello '.$userDetails[0]['user_name'].',</p>
                        <p>
                        '.$msg.' <br/>
                        Thanks and Regards.<br/>            
                        Tulii Admin         
                        </p>                    
                    </td>
                </tr>
                ';  
                sendEmail($userDetails[0]['user_email'],$userDetails[0]['user_name'],$message,"Your tulii account status");
                
                $message = "Your tulii account status"; 
                $myData = array("message"=>$msg);

                $allDeviceId = explode(",", $userDetails[0]['user_device_id']);
                foreach ($allDeviceId as $key => $value) {
                    $this->sendPushNotification($value,$message,$myData);                    
                }
                echo(json_encode(array('status'=>TRUE))); 
            }    
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                echo(json_encode(array('status'=>'access')));
            }
        }        
    }

    public function deleteUser() // delete record of parent & also kids data
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                   // echo $this->db->last_query();die;
                    //echo "<pre>";print_r($deleteId);die;
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"User has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                echo(json_encode(array('status'=>'access')));
            }
        }
    }

    // check email availability 
    public function check_email_availability()
    {
         if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            // check email & user type are already exist or not
            $data = $this->Common_model->select('user_id,user_email',TB_USERS,array('user_email'=>$postData['email'],"isDeleted"=>0));
            if($data)
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>true,'message' => 'Email already exists.')));           
            }
            else
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>false)));                          
            }  
        } else {
            echo(json_encode(array('status'=>'access')));
        }         
    }

    public function viewUserInfo() // view data of user 
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
                $postData = $this->input->post();  
                if($postData["key"]){
                                            /*user_category_id,user_sub_category_id,education_from,school,user_age,specialization,degree,education_to,upload_certificate,certification_name*/
                    $userData = $this->Common_model->select("user_id,user_email,user_name,user_phone_number,roleId,user_gender,user_birth_date",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    //$userData = $this->Common_model->select_join("*", TB_USERS, array("tbl_users.user_id" => $this->encrypt->decode($postData['key'])), array(), array(), array(TB_DRIVER => 'tbl_users.user_id = tbl_driver.user_id'), null);
                    if($userData){  
                        $today    = date('Y-m-d');
                        $ageDiff  = date_diff(date_create($userData[0]['user_birth_date']), date_create($today));
                        $userAge  = $ageDiff->format('%y');
                        //if($userData[0]['roleId'] == ROLE_SERVICE_PROVIDER)
                        if($userData[0]['roleId'] == ROLE_TUTOR)
                        {
                            $userData[0]['user_age']=$userAge;
                            
                            $data='';    

                            $tutorDetails=$this->Common_model->select("*",TB_TUTORS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                           
                            $userData[0]['school']=$tutorDetails[0]['tutor_organization'];
                            $userData[0]['degree']=$tutorDetails[0]['degree'];
                            $userData[0]['tutor_address']=$tutorDetails[0]['tutor_address'];
                            $userData[0]['address_type']=$tutorDetails[0]['address_type'];
                            
                            // $userData[0]['education_from']=$tutorDetails[0]['education_from'];
                            //$userData[0]['education_to']=$tutorDetails[0]['education_to'];
                            //$userData[0]['certification_name']=$tutorDetails[0]['certification_name'];
                            //$specializationJSON=$tutorDetails[0]['specialization'];

                            $userData[0]['user_age']=$userAge;
                            $userData[0]['upload_certificate']=$tutorDetails[0]['certificate_doc'];
                            /*$list = $this->Common_model->select("category_id,category",TB_TUTOR_CATEGORY,array('user_id'=>$this->encrypt->decode($postData['key'])));
                   
                            if (count($list) > 0) { 
                                $data .='<div class="col-md-6">
                                    <div class="form-group">
                                    <label class="control-label" for="category">Category<span class="required_star">*</span></label>
                                    <select class="form-control" onchange="subCategory();"   required="true" id="category" name="category">
                                    <option value="" disable="" selected="" hidden="">Select category</option> '; 
                                foreach($list as $key => $value) {  // echo $value;exit;
                                       if( $value['category_id'] == $userData[0]['user_category_id']) 
                                       {
                                             $selected="selected";
                                       } else { $selected="";}
                                       $data .='<option '.$selected.' value="'. $value['category_id'] .'">'. $value['category'] .'</option>'; 
                                   }
                                 $data .='</select></div></div>'; 
                            } 
              
                            $list_data =  $this->Common_model->select('sub_category_id,name',TB_SUBCATEGORIES,array('status'=>1,'category_id'=>$userData[0]['user_category_id']));*/
              
                             /*if (count($list_data) > 0) { 
                                 $data.='<div id="sub_category_div"><div class="col-md-6">
                                        <div class="form-group">
                                        <label class="control-label" for="sub_category">Sub Category<span class="required_star">*</span></label>
                                        <select class="form-control"  required="true" id="sub_category" name="sub_category">
                                        <option value="" disable="" selected="" hidden="">Select sub category</option> '; 
                                  foreach($list_data as $key => $value) {  // echo $value;exit;
                                      if( $value['sub_category_id'] == $userData[0]['user_sub_category_id']) 
                                   {
                                    $select_option="selected";
                                   } else { $select_option="";}
                                 $data .='<option '.$select_option.' value="'. $value['sub_category_id'] .'">'. $value['name'] .'</option>'; 
                                }
                                $data .='</select></div></div></div>'; 
                             }*/

                            /*$subCategory = $this->Common_model->select("id,spec_name",TB_SPECILIZATION,array('degree_id'=>$userData[0]['degree']));
                            $specializationJSON='';
                            if(count($subCategory) > 0)
                            {
                                $specializationJSON .= '<option value="" disable="" selected="" hidden="">-- Select specialization --</option>';
                                foreach ($subCategory as $key => $category) {
                                    $specializationJSON.= '<option value="'.$category['id'].'">'.$category['spec_name'].'</option>';
                                }               
                            }
                            else
                            {
                                $specializationJSON.= '<option value="">Specialization not available</option>';
                            }*/

                             echo json_encode(array("status"=>"success","action"=>"view","categoryData"=>$data,"parentData"=>$userData[0],"specializationJSON"=>$specializationJSON,'category'=>$userData[0]['user_category_id'])); exit; 

                        } 
                        else if($userData[0]['roleId'] == ROLE_CARE_DRIVER){
                            $specializationJSON='';
                            $driverDetails = $this->Common_model->select("*",TB_DRIVER,array('user_id'=>$this->encrypt->decode($postData['key'])));
                            //$degree= $this->Common_model->select("*",TB_DEGREE,array('id'=>$driverDetails[0]['degree']));
                            
                            $userData[0]['school']=$driverDetails[0]['school'];
                            $userData[0]['degree']=$driverDetails[0]['degree'];
                            $userData[0]['education_from']=$driverDetails[0]['education_from'];
                            $userData[0]['education_to']=$driverDetails[0]['education_to'];
                            $userData[0]['certification_name']=$driverDetails[0]['certification_name'];
                            $specializationJSON=$driverDetails[0]['specialization'];

                            $userData[0]['user_age']=$userAge;
                            $userData[0]['upload_certificate']=$driverDetails[0]['upload_certificate'];
                            
                           //echo "<pre>";print_r($specializationJSON);die;
                            echo json_encode(array("status"=>"success_both","action"=>"view","categoryData"=>$data,"parentData"=>$userData[0],"specializationJSON"=>$specializationJSON,'category'=>$userData[0]['user_category_id'])); exit; 


                        }
                        else {
                         echo json_encode(array("status"=>"success","action"=>"view","parentData"=>$userData[0])); exit;    
                        }                                                       
                        
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
            else{
                echo(json_encode(array('status'=>'access')));
            } 
        }
    } 
    // add & update record of parent data
    public function addUser()
    {
        if(is_ajax_request())
        {
             if($this->isAdmin() == TRUE){
                $postData = $this->input->post();
                if (empty($_FILES['uploadDocuments']['name'][0]) && !empty($postData["user_key1"])) {
                    $oldUploadFile = $this->Common_model->select("upload_certificate",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['user_key1'])));    
                    $documents = $oldUploadFile[0]['upload_certificate'] ? $oldUploadFile[0]['upload_certificate'] : "";
                } else {
                    $uploaddir = "./uploads/driver/certification/";                    
                    $upload_certification = $documents ="";                       
                    $temp = "";
                    $fileImages = $_FILES;
                    $allFiles = array();
                    $j =1;                          
                    foreach ($fileImages as $key => $value) {
                        $cpt = count($_FILES[$key]['name']);
                        $temp = 'document_img';
                        $files_name = array();
                        for($i=0; $i<$cpt; $i++){
                            $_FILES[$temp]['name']= $_FILES[$key]['name'][$i];
                            $_FILES[$temp]['type']= $_FILES[$key]['type'][$i];
                            $_FILES[$temp]['tmp_name']= $_FILES[$key]['tmp_name'][$i];
                            $_FILES[$temp]['error']= $_FILES[$key]['error'][$i];
                            $_FILES[$temp]['size']= $_FILES[$key]['size'][$i];    
                            $files_name[$i]['name'] = $_FILES[$temp]['name'];
                            $files_name[$i]['type'] = $_FILES[$temp]['type'];
                            $files_name[$i]['tmp_name'] = $_FILES[$temp]['tmp_name'];
                            $files_name[$i]['error'] = $_FILES[$temp]['error'];
                            $files_name[$i]['size'] = $_FILES[$temp]['size'];
                        }
                        $allFiles[$temp]=$files_name;
                        $j++;                            
                    }

                      
                    foreach ($allFiles as $key => $value) {
                        $i = 0;                                
                        for($j=0;$j < count($value); $j++) { 
                             if (strcmp($key,'document_img') == 0 ) {
                                $upload_certification = multiple_image_upload($value[$j],$uploaddir); 
                                
                             if(is_array($upload_certification))
                                {
                                    echo json_encode(array("status"=>"error","action"=>"insert","message"=>"You can not upload more that 2 Mb.")); exit; 
                                } else{
                                 $documents .= "|".$upload_certification;    
                                }
                            } 
                            $i++;                                                     
                        }
                    }  
                }    
                
                if(!empty($postData["user_key1"])) { // update data
                    if($postData['role']==3){
                        $updatedetails=array(
                            "degree"=>$postData['userDegree'],
                            "specialization"=>$postData['specialization'],
                            "education_from"=>$postData['valid_from'],
                            "education_to"=>$postData['valid_to'],
                            "certification_name"=>$postData['certificateName'],
                            "upload_certificate"=> trim($documents,"|"),
                            "school"=>$postData['schoolCollege']
                        );
                        $updateId = $this->Common_model->update(TB_DRIVER,array('user_id'=>$this->encrypt->decode($postData['user_key1'])),$updatedetails);
                    }

                    $updateArr = array(  
                       // "user_email" =>$postData['email'],
                        "roleId"=>$postData['role'],
                        "user_name"=>$postData['fname'],
                        "user_phone_number"=>$postData['contactNumber'],
                        //"user_category_id"=>isset($postData['category'])?$postData['category']:'',
                        //"user_sub_category_id"=>isset($postData['sub_category'])?$postData['sub_category']:'',
                        "user_gender"=>$postData["spGender"],
                        "user_birth_date" =>$postData['parentDOB'],
                        //"user_age" =>$postData['parentAge'],
                        "updated_at"=>date("Y-m-d H:s:i")
                    );
                    $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['user_key1'])),$updateArr);
                    if($updateId){
                       
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"User has been updated successfully.")); exit;   
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {         

                    if(!empty($postData)){ // add data                                 
                    $data = array(
                        "user_email" =>$postData['email'],
                        "roleId"=>$postData['role'],
                        "user_name"=>$postData['fname'],
                        "user_phone_number"=>$postData['contactNumber'],
                        //"user_category_id"=>isset($postData['category'])?$postData['category']:'',
                        //"user_sub_category_id"=>isset($postData['sub_category'])?$postData['sub_category']:'',
                        "user_status"=>"0",
                        
                        //"degree"=>$postData['userDegree'],
                        //"specialization"=>$postData['specialization'],
                        //"education_from"=>$postData['valid_from'],
                       // "education_to"=>$postData['valid_to'],
                        //"certification_name"=>$postData['certificateName'],
                        //"upload_certificate"=>trim($documents,"|"),
                        //"school"=>$postData['schoolCollege'],
                        "user_birth_date" =>$postData['parentDOB'],
                        //"user_age" =>$postData['parentAge'],

                        //"user_password"=>getHashedPassword($randstring),
                        "new_record_status"=>'1',
                        "user_gender"=>$postData["spGender"],
                        "user_password"=>getHashedPassword($postData['userPassword']),
                        "is_user_available"=>"available",
                        "created_at"=>date("Y-m-d H:s:i")           
                        );
                    $recordInsertUser = $this->Common_model->insert(TB_USERS,$data);
                  
                    if($recordInsertUser){

                        $message = '<tr> 
                                    <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                                        <p>Hello '.$postData['fname'].',</p>
                                            <p>
                                            Tulii admin has been created your account as a service provider.<br/>
                                            Your login credential has below.<br/>
                                            User name : <b>'.$postData['email'].'</b><br/>
                                            Password  : <b>'.$postData['userPassword'].'</b><br/>
                                            Thanks and Regards.<br/>            
                                            Tulii Admin         
                                            </p>                    
                                        </td>
                                    </tr>
                                    ';    
                        sendEmail($postData['email'],$postData['fname'],$message);                        
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"User has been inserted successfully.")); exit;    
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                }
                else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the user information.")); exit;    
                    }
                }  
            }
            else{
                echo(json_encode(array('status'=>'access')));
            } 
        }     
    } 

    function addCategory()
    {
        if($this->isAdmin() == TRUE)
        {
        $user_category_id = 0;
        $user_sub_category_id = 0;  
        $user_key = $this->input->post('user_key1'); 
        $userData = $this->Common_model->select("user_category_id,user_sub_category_id",TB_USERS,array('user_id'=>$this->encrypt->decode($user_key)));
        if($userData){
            $user_category_id = $userData[0]['user_category_id'];
            $user_sub_category_id = $userData[0]['user_sub_category_id'];     
        }
        if($user_category_id > 0 && $user_sub_category_id >0){ 
        $data='';         
        $list = $this->Common_model->select("category_id,category",TB_CATEGORIES,array('status'=>1));
        if (count($list) > 0) { 
                            $data .='<div class="col-md-6">
                                    <div class="form-group">
                                    <label class="control-label" for="category">Category <span class="required_star">*</span></label>
                                    <select class="form-control" onchange="subCategory();"   required="true" id="category" name="category">
                                    <option value="" disable="" selected="" hidden="">Select category</option> '; 
                            foreach($list as $key => $value) {  // echo $value;exit;
                               if( $value['category_id'] == $userData[0]['user_category_id']) 
                               {
                               $selected="selected";
                               } else { $selected="";}
                              $data .='<option '.$selected.' value="'. $value['category_id'] .'">'. $value['category'] .'</option>'; 
                               }
                             $data .='</select></div></div>'; 
                               }


         $list_data =  $this->Common_model->select('sub_category_id,name',TB_SUBCATEGORIES,array('status'=>1,'category_id'=>$userData[0]['user_category_id'])); 

          if (count($list_data) > 0) { 
             $data.='<div id="sub_category_div"><div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="sub_category">Sub Category<span class="required_star">*</span></label>
                    <select class="form-control"  required="true" id="sub_category" name="sub_category">
                    <option value="" disable="" selected="" hidden="">Select sub category</option> '; 
              foreach($list_data as $key => $value) {  // echo $value;exit;
                  if( $value['sub_category_id'] == $userData[0]['user_sub_category_id']) 
               {
                $select_option="selected";
               } else { $select_option="";}
             $data .='<option '.$select_option.' value="'. $value['sub_category_id'] .'">'. $value['name'] .'</option>'; 
            }
            $data .='</select></div></div></div>'; 
             }
       echo json_encode(array("status" => TRUE,"data" => $data));  
        

        } else{

      $list = $this->Common_model->select("category_id,category",TB_CATEGORIES,array('status'=>1));
            $data='';    
             if (count($list) > 0) { 
             $data='<div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="category">Category<span class="required_star">*</span></label>
                    <select class="form-control" onchange="subCategory();" required="true" id="category" name="category">
                    <option value="" disable="" selected="" hidden="">Select category</option> '; 
              foreach($list as $key => $value) {  // echo $value;exit;
             $data .='<option value="'. $value['category_id'] .'">'. $value['category'] .'</option>'; 
            }
            $data .='</select></div></div>'; 
            echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE,"data" =>$data))); }  
         }         
        }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        }
    }
    function addSubCategory()
    {
        if($this->isAdmin() == TRUE)
        {
        $category_id = $this->input->post('category_id');   
       $list =  $this->Common_model->select('sub_category_id,name',TB_SUBCATEGORIES,array('status'=>1,'category_id'=>$category_id));
              $data='';    
             if (count($list) > 0) { 
             $data='<div id="sub_category_div"><div class="col-md-6">
                    <div class="form-group">
                    <label class="control-label" for="sub_category">Sub Category<span class="required_star">*</span></label>
                    <select class="form-control" required="true" id="sub_category" name="sub_category">
                    <option value="" disable="" selected="" hidden="">Select sub category</option> '; 
              foreach($list as $key => $value) {  // echo $value;exit;
             $data .='<option value="'. $value['sub_category_id'] .'">'. $value['name'] .'</option>'; 
            }
            $data .='</select></div></div></div>'; 
            echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE,"data" =>$data))); }        
        }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        }
    }

    public function sendPushNotification($device_id,$message,$data)
    {     
        $content = array(
            "en" => $message
        );
        $fields = array(
            'app_id' => ONSIGNALKEY,
            'include_player_ids' =>(array)$device_id,
            'data' => $data,
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
       // print_r($response);exit;            
        curl_close($ch);
    }
}