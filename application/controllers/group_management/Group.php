<?php 

defined('BASEPATH') OR exit('No direct script access allowed'); 
class Group extends CI_Controller {

	public function __construct()
	{
		ob_start();
		parent::__construct();
		$this->data = array(
			'pageTitle' => 'Tulii : Group management',
			'isActive' => 'active'        
			);
		$this->load->model('Group_model','group_model');        
	}

	public function index()
	{
		if (is_user_logged_in()){   
			$this->load->view('includes/header',$this->data);
			$this->load->view('group_management/group_listing');
			$this->load->view('includes/footer');
		} else {
			$this->session->sess_destroy();
			redirect('login');
		}
	}


	public function ajax_list() // list of parent data
	{
		if(is_user_logged_in()){
			$list = $this->group_model->get_datatables();
			$data = array();
			$no = $_POST['start'];
			$i = 1;            
			foreach ($list as $grpData) {
				$grpId = $this->encrypt->encode($grpData->group_id);
				$status = ($grpData->group_status == 1)?"Active":"Inactive";
				$is_deleted = ($grpData->is_Deleted == 1)?"Deleted":"Not Deleted";
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $grpData->group_name;
				$row[] = $grpData->no_of_people;
				$row[] = $grpData->user_fullname;
				$row[] = $grpData->role_name;
				$row[] = $status;
				$row[] = $is_deleted;
				$row[] ='<a data-id="'.$i.'" data-row-id="'.$grpId.'" class="btn btn-sm btn-info" onclick="viewGroup(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i>
			</a>';
			$data[] = $row;            
			$i++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->group_model->count_all(),
			"recordsFiltered" => $this->group_model->count_filtered(),
			"data" => $data,
			);
            //output to json format
		echo json_encode($output);
		}else{
			$this->session->sess_destroy();
			redirect('login');
		} 
	}


	public function viewGroupMembers() // view data of parent & no. of kids
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                	$join=array();
                	$cond = array( TB_GROUP_MASTER.".group_status" => "1", TB_GROUP_MASTER.".is_Deleted" => "0", TB_GROUP_MASTER.".group_id" => $this->encrypt->decode($postData['key']));
			        $jointype = array(TB_PASSENGER => "LEFT", TB_GROUP_MEMBER => "LEFT");
			        $join = array(TB_GROUP_MEMBER => TB_GROUP_MEMBER.".group_id = ".TB_GROUP_MASTER.".group_id", TB_PASSENGER => TB_PASSENGER.".passenger_id = ".TB_GROUP_MEMBER.".passenger_id");
			        $grpData  = $this->Common_model->selectJoin("group_name, description, no_of_people, group_limit_date, pass_fullname, pass_dob, pass_gender, pass_address", TB_GROUP_MASTER, $cond, array(), $join, $jointype);
			        if(count($grpData) > 0){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","grpData"=>$grpData)); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"No group members are available.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 
}
?>