<?php
/*
* @author : kiran N.
* page : service provider controller
* description: show the all service provider data & management module
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 
class ServiceproviderController extends CI_Controller {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Service provider',
            'isActive' => 'active'        
        );
        $this->load->model('Serviceprovider_model','servicePrv');
    }
 
    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');
            $this->load->view('includes/header',$this->data);
            $this->load->view('service_provider/service_provider',$this->data);
            $this->load->view('includes/footer');
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        } 
    }
 
    public function ajax_list() // list of parent data
    {
        if(is_user_logged_in()){
            $list = $this->servicePrv->get_datatables();        
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $servicePvdData) {
                $userId = $this->encrypt->encode($servicePvdData->user_id);  
                $button ="";  
                if($servicePvdData->user_status == 1)
                {   
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="serviceProviderStatus('.$servicePvdData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');" ';
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="serviceProviderStatus('.$servicePvdData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');"';
                } 
                else
                {   
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="serviceProviderStatus('.$servicePvdData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');"';
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="serviceProviderStatus('.$servicePvdData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $servicePvdData->user_name;
                $row[] = $servicePvdData->user_email;
                $row[] = $servicePvdData->user_phone_number;
                $row[] = $servicePvdData->user_gender;
                $row[] = $button;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewServicepProvider(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteServiceProvider(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->servicePrv->count_all(),
                            "recordsFiltered" => $this->servicePrv->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function serviceProviderChangeStatus() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'],'updatedBy'=>$this->session->userdata('userId'), 'updated_at'=>date('Y-m-d H:i:s')));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }

    public function deleteSerivceProviderInfo() // delete record of parent & also kids data
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Service provider has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    public function viewServiceProviderInfo() // view data of parent & no. of kids
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $servicePvdData = $this->Common_model->select("user_id,user_email,user_name,user_phone_number,user_gender,user_address,user_birth_date",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if($servicePvdData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","servicePvdData"=>$servicePvdData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   
}