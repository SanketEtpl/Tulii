<?php
/*
* @author : kiran N.
* description: show the all parent data & management
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 
class ParentdetailsController extends CI_Controller {
 
    public function __construct()
    {

        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Parent Profile'        
        );
        $this->load->model('Parentdetails_model','Parentdetails');

    }
 
    public function index()
    {
        $this->load->helper('url');        
        $this->load->view('parent_management/parentProfile',$this->data);
    }
 
    public function ajax_list() // list of parent data
    {

        $list = $this->Parentdetails->get_datatables();        
        $data = array();
        $no = $_POST['start'];
        $i = 1;
        $status_id=1;
        $rowActive_id="active".$status_id;
        $rowInActive_id="inActive".$status_id;
        foreach ($list as $parentData) {
            $userId = $this->encrypt->encode($parentData->user_id);  
            $button ="";  
            if($parentData->user_status == 1)
            {   
                $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="parentProfileStatus('.$parentData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');" ';
                $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="parentProfileStatus('.$parentData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');"';
            } 
            else
            {   
                $active_btn_class='class="btn btn-sm btn-active-disable" onclick="parentProfileStatus('.$parentData->user_id.',1,'.$rowActive_id.','.$rowInActive_id.');"';
                $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="parentProfileStatus('.$parentData->user_id.',0,'.$rowActive_id.','.$rowInActive_id.');" ';
            }
            $button ='<button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button>&nbsp;';
            $button.='<button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button>&nbsp;';
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $parentData->user_name;
            $row[] = $parentData->user_email;
            $row[] = $parentData->user_phone_number;
            $row[] = $parentData->user_gender;
            $row[] = $button;
            $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewParentData(this)" href="javascript:void(0)">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editParentData(this)" href="javascript:void(0)">
                            <i class="fa fa-pencil"></i>
                        </a>
                        <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteParentData(this)" href="javascript:void(0)">
                            <i class="fa fa-trash"></i>
                        </a>                           
                    ';
            $data[] = $row;
            $status_id++;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            $i++;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Parentdetails->count_all(),
                        "recordsFiltered" => $this->Parentdetails->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function parentChangeStatus() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'],'updatedBy'=>$this->session->userdata('userId'), 'updated_at'=>date('Y-m-d H:i:s')));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            }else{
                echo json_encode(array("status"=>"logout"));
            }
        }        
    }

    public function deleteParentInfo() // delete record of parent & also kids data
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    $kidId = $this->Common_model->delete(TB_KIDS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId && $kidId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Parent user has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            }
        }
    }

    public function viewParentInfo() // view data of parent & no. of kids
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $parnetData = $this->Common_model->select("user_id,user_email,user_name,user_phone_number,user_gender,user_address,user_birth_date,user_no_of_kids",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    $kidsData = $this->Common_model->select("kid_birthdate,kid_name,kid_gender,kid_age,kid_id",TB_KIDS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    $kidsList=array();
                    foreach ($kidsData as $key => $value) {
                        $kidsList[]=array('kid_name'=>$value['kid_name'],'kid_gender'=>$value['kid_gender'],'kid_age'=>$value['kid_age'],'kid_id'=>$value['kid_id'],'kid_birthdate'=>date("m/d/Y",strtotime($value['kid_birthdate'])));
                    }                   
                    if($parnetData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","parentData"=>$parnetData[0],"kidsData"=>$kidsList)); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            }
        }
    } 

    // check email availability 
    public function check_email_availability()
    {
        $postData = $this->input->post();
        // check email & user type are already exist or not
        $data = $this->Common_model->select('user_id,user_email',TB_USERS,array('user_email'=>$postData['email']));
        if($data)
        {
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>true,'message' => 'Email ID already exists.')));           
        }
        else
        {
            $this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>false)));                          
        }           
    }

    // add & update record of parent data
    public function addParentData()
    {
        $postData = $this->input->post();
         $parentGender ="";
        if(isset($postData['parentGender']))
            $parentGender = $postData['parentGender'];
       
        if(!empty($postData["editData"])) { // update data
            $updateArr = array("user_email" =>$postData['email'],
                "user_name"=>$postData['parentName'],
                "user_phone_number"=>$postData['phone'],
                "user_gender"=>$parentGender,
                "user_no_of_kids"=>$postData['user_no_of_kids'],
                "user_address"=>$postData['address'],
                "updated_at"=>date("Y-m-d h:i:s"),  
                "updatedBy"=>$this->session->userdata('role')
            );
            $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
            $deleteId = $this->Common_model->delete(TB_KIDS,array('user_id'=>$this->encrypt->decode($postData['editData'])));
            if(!empty($postData['kidsData'])){
                foreach ($postData['kidsData'] as $key => $value) {
                        $data = array(
                            "kid_name" =>$value['name'],
                            "kid_gender"=>$value['kidgender'],
                            "kid_birthdate"=>date("Y-m-d",strtotime($value['dateOfBirth'])),
                            "kid_age"=>$value['age'],
                            "kid_status"=>1,
                            "user_id"=>$postData['parent_id'],
                            "created_at"=>date("Y-m-d H:s:i")                       
                            );
                        $recordInsertKids = $this->Common_model->insert(TB_KIDS,$data);
                }   
            }
            if($updateId){
                echo json_encode(array("status"=>"success","action"=>"update","message"=>"parent details has been updated successfully.")); exit;   
            }else{
                echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
            }
        } else {            
            if(!empty($postData)){ // add data               
            $data = array(
                "user_email" =>$postData['email'],
                "user_type"=>1,
                "user_name"=>$postData['parentName'],
                "roleId" =>2,
                "user_phone_number"=>$postData['phone'],
                "user_status"=>"1",
                "user_gender"=>$parentGender,
                "user_no_of_kids"=>$postData['user_no_of_kids'],
                "user_address"=>$postData['address'],
                "is_user_available"=>"available",
                "created_at"=>date("Y-m-d H:s:i")           
                );
            $recordInsertParent = $this->Common_model->insert(TB_USERS,$data);
            if(!empty($postData['kidsData'])){
                foreach ($postData['kidsData'] as $key => $value) {
                    $data = array(
                        "kid_name" =>$value['name'],
                        "kid_gender"=>$value['kidgender'],
                        "kid_birthdate"=>date("Y-m-d",strtotime($value['dateOfBirth'])),
                        "kid_age"=>$value['age'],
                        "kid_status"=>1,
                        "user_id"=>$recordInsertParent,
                        "created_at"=>date("Y-m-d H:s:i")                       
                        );
                    $recordInsertKids = $this->Common_model->insert(TB_KIDS,$data);
                }   
            }
            if($recordInsertParent){
                echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Parent user have been inserted successfully ")); exit;    
            }else{
                echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
            }
        }
        else{
                echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the parent information")); exit;    
            }
        }       
    } 
}