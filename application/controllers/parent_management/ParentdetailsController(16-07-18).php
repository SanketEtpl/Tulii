<?php
/*
* @author : kiran N.
* description: show the all parent data & management controller
*/
defined('BASEPATH') OR exit('No direct script access allowed');
 require APPPATH . '/libraries/BaseController.php';
class ParentdetailsController extends BaseController {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Parent management',
            'isActive'  => 'active'      
        );
        $this->load->helper('email_template_helper');
        $this->load->model('Parentdetails_model','Parentdetails');
        $this->isLoggedIn();
        ini_set('display_errors', '0');
    }
 
    public function index()
    {
         if($this->isAdmin() == TRUE){
            $this->load->helper('url');   
            $this->load->view('includes/header',$this->data);     
            $this->load->view('parent_management/parentProfile',$this->data);
            //$this->load->view('includes/footer');
        }
        else{
            $this->loadThis();     
        } 
    }
 
    public function ajax_list() // list of parent data
    {
        if($this->isAdmin() == TRUE){
            $list = $this->Parentdetails->get_datatables();        
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $parentData) {
                $userId = $this->encrypt->encode($parentData->user_id);  
                $button ="";  
                 if($parentData->user_status == 1)
                {   
                    $a= $parentData->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="parentProfileStatus_popup('.$a.');" ';

                    $b= $parentData->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="parentProfileStatus_popup('.$b.');"';
                } 
                else
                {   
                    $c= $parentData->user_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="parentProfileStatus_popup('.$c.');"';

                     $d= $parentData->user_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="parentProfileStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.' data-toggle="tooltip" data-placement="top" title="Active" data-original-title="Active">Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.' data-toggle="tooltip" data-placement="top" title="In-active" data-original-title="In-active">InActive</button></span>&nbsp;';
                $no++;
                $newRecord = $parentData->new_record_status == 1?'<button type="button" class="btn btn-secondary new-record" data-row-id="'.$userId.'" onclick="newRecord(this)" data-toggle="tooltip" data-placement="top" title="" data-original-title="New">New</button>&nbsp;&nbsp;':"&nbsp;&nbsp;";
                $row = array();
                $row[] = $no." ".$newRecord;
                $row[] = ucfirst(strtolower($parentData->user_name));
                $row[] = $parentData->user_email;
                $row[] = $parentData->user_phone_number;
                $row[] = ucfirst(strtolower($parentData->user_gender)); 
                $row[] = $button;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewParentData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editParentData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteParentData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->Parentdetails->count_all(),
                            "recordsFiltered" => $this->Parentdetails->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            echo(json_encode(array('status'=>'access'))); 
        } 
    }

    public function parentChangeStatus() // chnage the status fo parent active or inactive
    {
        if(is_ajax_request())
        {
           if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$postData['userId']),array('user_status'=>$postData['user_status'],'updatedBy'=>$this->session->userdata('userId'), 'updated_at'=>date('Y-m-d H:i:s')));            
            if ($result) {
                if($postData['user_status'] == "1")
                    $msg = "Admin activated your account. Now you can access your account.";
                else
                    $msg = "Admin deactivated your account due to unsufficient information. You need to contact to admin for more information.";
                $userDetails = $this->Common_model->select('user_id,user_email,user_name,user_device_id',TB_USERS,array('user_id'=>$postData['userId']));      
                $message = '<tr> 
                <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Hello '.$userDetails[0]['user_name'].',</p>
                        <p>
                        '.$msg.' <br/>
                        Thanks and Regards.<br/>            
                        Tulii Admin         
                        </p>                    
                    </td>
                </tr>
                ';  
                sendEmail($userDetails[0]['user_email'],$userDetails[0]['user_name'],$message,"Your tulii account status");
                 $message = "Your tulii account status"; 
                $myData = array("message"=>$msg);
                $allDeviceId = explode(",", $userDetails[0]['user_device_id']);
                foreach ($allDeviceId as $key => $value) {
                    $this->sendPushNotification($value,$message,$myData);                    
                }
                echo(json_encode(array('status'=>TRUE))); 
            }
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                echo(json_encode(array('status'=>'access')));  
            } 
        }        
    }

    public function newRecord() // new record change icon
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_USERS,array("user_id"=>$this->encrypt->decode($postData['key'])),array('new_record_status'=>0));            
            if ($result)
                echo(json_encode(array('status'=>"success"))); 
            else
                echo(json_encode(array('status'=>"error"))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }

    public function deleteParentInfo() // delete record of parent & also kids data
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                //print_r($postData);exit;
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                    $kidId = $this->Common_model->update(TB_KIDS,array('user_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Parent user has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                 echo(json_encode(array('status'=>'access'))); 
            } 
        }
    }

     public function viewParentInfo() // view data of parent & no. of kids
    {
        if(is_ajax_request())
        {
           if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $parentData = $this->Common_model->select("user_id,user_email,user_name,user_phone_number,user_gender,user_address,user_birth_date,user_no_of_kids",TB_USERS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    $kidsData = $this->Common_model->select("kid_birthdate,kid_name,kid_gender,kid_age,kid_id",TB_KIDS,array('user_id'=>$this->encrypt->decode($postData['key'])));
                    $kidsList=array();
                    $kid_count=0;
                    foreach ($kidsData as $key => $value) {
                        $kidsList[]=array('kid_name'=>$value['kid_name'],'kid_gender'=>$value['kid_gender'],'kid_age'=>$value['kid_age'],'kid_id'=>$value['kid_id'],'kid_birthdate'=>date("m/d/Y",strtotime($value['kid_birthdate'])));
                        $kid_count ++;
                    }                   
                    if($parentData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","parentData"=>$parentData[0],"kidsData"=>$kidsList,"kid_count"=>$kid_count)); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                echo(json_encode(array('status'=>'access'))); 
            } 
        }
    } 


    // check email availability 
    public function check_email_availability()
    {
        if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            // check email & user type are already exist or not
            $data = $this->Common_model->select('user_id,user_email',TB_USERS,array('user_email'=>$postData['email']));
            if($data)
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>true,'message' => 'Email ID already exists.')));           
            }
            else
            {
                $this->output
                ->set_content_type("application/json")
                ->set_output(json_encode(array('status'=>false)));                          
            }         
        } else {
            echo(json_encode(array('status'=>'access'))); 
        }   
    }

    // add & update record of parent data
    public function addParentData()
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
                $postData = $this->input->post();
                $parentGender ="";
                if(isset($postData['parentGender']))
                    $parentGender = $postData['parentGender'];
                if(!empty($postData["editData"])) { // update data
                    $updateArr = array("user_email" =>$postData['email'],
                        "user_name"=>$postData['parentName'],
                        "user_phone_number"=>$postData['phone'],
                        "user_gender"=>$parentGender,
                        "user_no_of_kids"=>$postData['user_no_of_kids'],
                        "user_address"=>$postData['address'],
                        "user_add_lat_lng"=>$postData['lat'].','.$postData['lng'],
                        "updated_at"=>date("Y-m-d h:i:s"),  
                        "updatedBy"=>$this->session->userdata('role')
                    );
                    $updateId = $this->Common_model->update(TB_USERS,array('user_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                    $deleteId = $this->Common_model->delete(TB_KIDS,array('user_id'=>$this->encrypt->decode($postData['editData'])));
                    if(!empty($postData['kidsData'])){
                        foreach ($postData['kidsData'] as $key => $value) {
                            $data = array(
                                "kid_name" =>$value['name'],
                                "kid_gender"=>$value['kidgender'],
                                "kid_birthdate"=>date("Y-m-d",strtotime($value['dateOfBirth'])),
                                "kid_age"=>$value['age'],
                                "kid_status"=>1,
                                "user_id"=>$postData['parent_id'],
                                "updated_at"=>date("Y-m-d H:s:i")                       
                                );
                            $recordInsertKids = $this->Common_model->insert(TB_KIDS,$data);
                        }   
                    }                 
                    if($updateId){
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"parent details has been updated successfully.")); exit;   
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {            
                    if(!empty($postData)){ // add data   
                    $characters = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $randstring = '';
                    for ($i = 0; $i < 8; $i++) {
                        $randstring.= $characters[rand(0, strlen($characters))];
                    }
                    //print_r($randstring);exit;

                    $data = array(
                        "user_email" =>$postData['email'],
                        "user_name"=>$postData['parentName'],
                        "roleId" =>2,
                        "user_phone_number"=>$postData['phone'],
                        "user_status"=>"1",
                        "user_gender"=>$parentGender,
                        "user_no_of_kids"=>$postData['user_no_of_kids'],
                        "user_address"=>$postData['address'],
                        "user_add_lat_lng"=>$postData['lat'].','.$postData['lng'],
                        "is_user_available"=>"available",
                        "new_record_status"=>1,
                        "user_password"=>getHashedPassword($randstring),
                        "created_at"=>date("Y-m-d H:s:i")           
                        );
                    $recordInsertParent = $this->Common_model->insert(TB_USERS,$data);
                    if(!empty($postData['kidsData'])){
                        foreach ($postData['kidsData'] as $key => $value) {
                            $data = array(
                                "kid_name" =>$value['name'],
                                "kid_gender"=>$value['kidgender'],
                                "kid_birthdate"=>date("Y-m-d",strtotime($value['dateOfBirth'])),
                                "kid_age"=>$value['age'],
                                "kid_status"=>1,
                                "user_id"=>$recordInsertParent,
                                "created_at"=>date("Y-m-d H:s:i")                       
                                );
                            $recordInsertKids = $this->Common_model->insert(TB_KIDS,$data);
                        }   
                    }
                    if($recordInsertParent){
                        $message = '<tr> 
                        <td style="font-size:16px;word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                            <p>Hello '.$postData['parentName'].',</p>
                                <p>
                                Tulii admin has been created your account as a parent.<br/>
                                Your login credential has below.<br/>
                                User name : <b>'.$postData['email'].'</b><br/>
                                Password  : <b>'.$randstring.'</b><br/>
                                Thanks and Regards.<br/>            
                                Tulii Admin         
                                </p>                    
                            </td>
                        </tr>
                        ';  
                        sendEmail($postData['email'],$postData['parentName'],$message);
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Parent user have been inserted successfully.")); exit;    
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                }
                else{
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the parent information.")); exit;    
                    }
                } 
        } else {
                echo(json_encode(array('status'=>'access'))); 
            } 
        }         
    } 


    public function sendPushNotification($device_id,$message,$data)
    {     
        $content = array(
            "en" => $message
        );
        $fields = array(
            'app_id' => ONSIGNALKEY,
            'include_player_ids' =>(array)$device_id,
            'data' => $data,
            'contents' => $content
        );
        
        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
         'Authorization: Basic OGViZTZkMjgtMjRlYy00YWQ0LWIzMWYtNmE2ZWI5MDhmZTUw'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);  
       // print_r($response);exit;            
        curl_close($ch);
    }
}