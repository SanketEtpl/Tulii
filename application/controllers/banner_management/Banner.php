<?php
/*
* @ author : kiran N.
* @ date : 14-08-2018
* @ description: Banner management
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Banner extends CI_Controller
{

	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Banner management',
            'isActive'  => 'active'        
        );
        $this->load->model('Banner_model','banner');
        $this->load->helper(array('form', 'url'));
        $this->load->library('image_lib');
        $config['allowed_types'] =   "gif|png|jpg|jpeg|JPG|JPEG|PNG";  
        $config['max_size']      =   "2097152"; 
        $config['upload_path']   =  "./uploads/banner" ;   
        $this->load->library('upload', $config);

    }

    public function index()
    {
        if(is_user_logged_in()) {
            $this->load->helper('url');   
            $this->load->view('includes/header',$this->data);     
            $this->load->view('banner_management/banner');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function bannerList() // list of banner data
    {
        if(is_user_logged_in()){            
            $resultBannerList = $this->banner->get_datatables(); 

            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($resultBannerList as $bannerData) {      
                $bannerId = $this->encrypt->encode($bannerData->banner_id);
                $button ="";  
                if($bannerData->banner_status == "1") { //Active or Inactive status button
                   
                    $a= $bannerData->banner_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class=' disabled class="btn btn-sm btn-active-enable" onclick="BannerStatus_popup('.$a.');" ';

                    $b= $bannerData->banner_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class='class="btn btn-sm btn-inactive-disable" onclick="BannerStatus_popup('.$b.');"';
                } else {   
                    $c= $bannerData->banner_id.",1,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-active-disable" onclick="BannerStatus_popup('.$c.');"';

                    $d= $bannerData->banner_id.",0,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-inactive-enable" onclick="BannerStatus_popup('.$d.');" ';
                }
                $button ='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Active"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Active</button></span>&nbsp;';
                $button.='<span data-toggle="tooltip" data-placement="top" title="" data-original-title="In-active"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>InActive</button></span>&nbsp;';
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $bannerData->title;
                $row[] = file_exists("uploads/banner/".$bannerData->banner_img) ? "<img src='".'uploads/banner/'.$bannerData->banner_img."' data-toggle='tooltip' width='100' height='50' data-placement='right' title='".$bannerData->title."'>":"<img src='assets/images/img-not-found.jpg' width='100' height='50' data-toggle='tooltip' data-placement='right' title='No image'>";                
                $row[] = $button;
                $row[] = '  <a data-id="'.$i.'" data-row-id="'.$bannerId.'" class="btn btn-sm btn-info" onclick="editBanner(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>  
                            <a data-id="'.$i.'" data-row-id="'.$bannerId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteBanner(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';       
                $data[] = $row;   
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;          
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->banner->count_all(),
                "recordsFiltered" => $this->banner->count_filtered(),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function bannerStatus() // chnage the status fo banner active or inactive
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_BANNER,array("banner_id"=>$postData['userId']),array('banner_status'=>$postData['user_status'], 'update_date'=>date('Y-m-d H:i:s')));            
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }        
    }


    public function deleteBanner() // delete record of banner list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $data = array(
                        "is_deleted" => "1",
                        "update_date" => date("Y-m-d H:i:s")
                    );
                    $deleteId = $this->Common_model->update(TB_BANNER, array('banner_id' => $this->encrypt->decode($postData['key'])), $data);
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Banner image has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewBanner() // view record of banner details
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $getImage = $this->Common_model->select("title,banner_img",TB_BANNER,array('banner_id'=>$this->encrypt->decode($postData['key'])));
                    if($getImage[0]){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","bannerData"=>$getImage[0])); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    // add record of banner list
    public function addBanner()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $doc ="";
                $postData = $this->input->post();   
                if(isset($_FILES["banner_img"]["name"]))   
                {   
                    $file_name = $_FILES["banner_img"]["name"];
                    $original_file_name = $file_name;
                    $random = rand(1, 100000);
                    $date = date("Y_m_d_H_s_i");
                    $makeRandom = $random;
                    $file_name_rename = $makeRandom;
                    $explode = explode('.', $file_name);
                    if(count($explode) >= 2)
                    {
                        $new_file = $file_name_rename.$date.'.'.$explode[1];               
                        $config['upload_path'] = "./uploads/banner/";
                        $config['allowed_types'] ="gif|jpeg|png|jpg";
                        $config['file_name'] = $new_file;
                        $config['max_size'] = '2097152';
                        $config['overwrite'] = FALSE;
                        $this->load->library('upload',$config);
                        $this->upload->initialize($config);
                        if(!$this->upload->do_upload("banner_img")) {
                        $error = $this->upload->display_errors();
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>$error)); exit;  
                        } else {
                        $doc = $new_file;
                        }
                    }
                }           
                else
                {
                    $doc ="";   
                }                          
                if(!empty($postData['user_key'])){
                    $deleteImage = $this->Common_model->select("banner_img",TB_BANNER,array('banner_id'=>$this->encrypt->decode($postData['user_key'])));
                    if(file_exists("uploads/banner/".$deleteImage[0]['banner_img']))
                        unlink("uploads/banner/".$deleteImage[0]['banner_img']);                    
                   
                      
                    if(!empty($postData)){ // add data               
                        $data = array(
                            "banner_img" => $doc,
                            "title" => $postData['banner_name'],
                            "update_date" => date("Y-m-d H:s:i")           
                            );
                        $recordUpdateSlider = $this->Common_model->update(TB_BANNER,array('banner_id'=>$this->encrypt->decode($postData['user_key'])),$data);
                        if($recordUpdateSlider){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Banner image has been updated successfully.")); exit;    
                            } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                        }
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please fill the banner information.")); exit;    
                    }   
                } else {                      
                    if(!empty($postData)){ // add data               
                        $data = array(
                            "banner_img" => $doc,
                            "title" => $postData['banner_name'],
                            "banner_status"=> "1",
                            "created_date"=>date("Y-m-d H:s:i")           
                            );
                        $recordInsertUser = $this->Common_model->insert(TB_BANNER,$data);
                        if($recordInsertUser){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Banner image has been inserted successfully.")); exit;    
                            } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the banner information.")); exit;    
                    }             
                }                                    
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}