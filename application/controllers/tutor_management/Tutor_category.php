<?php
/*
* @author : kiran N.
* description: manage the tutor list of tutors category data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Tutor_category extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Tutor category list',
            'isActive' => 'active'         
        );
        $this->load->model('Tutor_category_model','tutor_category');
    }

    public function index()
    {
        if(is_user_logged_in()) {
            $this->load->view("includes/header",$this->data);
            $this->load->view('tutor_management/tutorCategory');   
            $this->load->view('includes/footer');                    
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function categoryList() // list of tutor list data
    {
        if(is_user_logged_in()){   
            $resutlTutorCategoryList = $this->tutor_category->get_datatables(); 
            $data = array();
            $no = $_POST['start'];
            $i = 1;             
            foreach ($resutlTutorCategoryList as $tcValue) {
                $tcId = $this->encrypt->encode($tcValue->id);
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($tcValue->category_name);
                $row[] = '                    
                    <a data-id="'.$i.'" data-row-id="'.$tcId.'" class="btn btn-sm btn-info" onclick="editTutorCategory(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a data-id="'.$i.'" data-row-id="'.$tcId.'" class="btn btn-sm btn-danger" onclick="deleteTutorCategory(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                        <i class="fa fa-trash"></i>
                    </a>                           
                ';
                $data[] = $row;                             
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->tutor_category->count_all(),
                "recordsFiltered" => $this->tutor_category->count_filtered(),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteTutorCategory() // delete record of tutor category list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $checkTutorCatAssign = $this->Common_model->select("tutor_booking_id", TB_TUTOR_BOOKING, array('cat_id' => trim($this->encrypt->decode($postData['key']))));
                    if(count($checkTutorCatAssign) > 0) {
                        echo json_encode(array("status" => "warning" ,"action" => "delete", "msg" => "This tutor category assigned someone in tutor booking table.")); exit; 
                    } else {
                        $data = array(
                            "is_deleted" => "1",
                            "update_date" => date('Y-m-d H:i:s')  
                        );
                        $deleteId = $this->Common_model->update(TB_TUTOR_CATEGORY,array('tut_cat_id'=>$this->encrypt->decode($postData['key'])), $data);
                        if($deleteId){                                                
                            echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Tutor category record has been deleted successfully.")); exit;  
                        }else{
                            echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                        }
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewTutorCategory() // view data of tutor  
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if(! empty($postData["key"])){               
                    $resultViewTutorData = $this->Common_model->select("tut_cat_id, category_name", TB_TUTOR_CATEGORY, array('tut_cat_id' => $this->encrypt->decode($postData["key"])));
                
                    if(count($resultViewTutorData) > 0) {                                                
                        echo json_encode(array("status" => "success", "action" => "view","tutorData" => $resultViewTutorData[0])); exit;
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add tutor category
    public function addTutorCategory()
    {
        if(is_ajax_request()) {
            if(is_user_logged_in()) {
                $postData = $this->input->post(); 
                if(!empty($postData['edit_id'])) {
                    $checkExists = $this->Common_model->select("tut_cat_id", TB_TUTOR_CATEGORY, array('category_name' => trim($postData['category_name']), 'tut_cat_id !=' => $postData['edit_id'], 'is_deleted' => '0')); 
                    if(count($checkExists) > 0){
                        echo json_encode(array("status"=>"warning","message"=>"Tutor category already exists.")); exit;    
                    } else {                                              
                        $data = array(
                            "category_name" => $postData['category_name'],
                            "update_date" => date('Y-m-d H:i:s')                            
                        );
                        $resultTutorCatUpdated = $this->Common_model->update(TB_TUTOR_CATEGORY, array('tut_cat_id' => $postData['edit_id']), $data);
                        if($resultTutorCatUpdated) {
                            echo json_encode(array("status" => "success", "action" => "update", "message" => "Tutor category has been updated successfully.")); exit;    
                        } else {
                            echo json_encode(array("status" => "error","action" => "update", "message" => "Please try again.")); exit; 
                        }
                    }
                } else {
                    $checkExists = $this->Common_model->select("tut_cat_id",TB_TUTOR_CATEGORY,array('category_name'=>trim($postData['category_name']), 'is_deleted' => '0')); 
                    if(count($checkExists) > 0){
                        echo json_encode(array("status"=>"warning","message"=>"Tutor category already exists.")); exit;    
                    } else {                                              
                        $data = array(
                            "category_name" => $postData['category_name'],
                            "status" => "1",
                            "created_date" => date('Y-m-d H:i:s')        
                        );
                        $resultTutorCatAdd = $this->Common_model->insert(TB_TUTOR_CATEGORY,$data);

                        if($resultTutorCatAdd) {
                            echo json_encode(array("status" => "success", "action" => "insert", "message" => "Tutor category has been added successfully.")); exit;    
                        } else {
                            echo json_encode(array("status" => "error","action" => "insert", "message" => "Please try again.")); exit; 
                        }
                    }    
                }                     
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}