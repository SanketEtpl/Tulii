<?php
/*
* @author : kiran N.
* description: manage the price of rides data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Price_list extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Price management',
            'isActive'  => 'active'       
        );
        $this->load->model('Price_list_model','priceList');
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->load->helper('url');       
            $this->data['categories'] = $this->Common_model->select('category,category_id',TB_CATEGORIES,array("status"=>1));                          
            $this->load->view('includes/header',$this->data);
            $this->load->view('price_management/price_list',$this->data);
            $this->load->view('includes/footer');
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    //get categories
    public function get_category()
    {       
        $postData = $this->input->post();
        $dataCategory = $this->Common_model->select("sub_category_id,name",TB_SUBCATEGORIES,array("status"=>1,"category_id"=>$postData['categoryID']));
        $categoryJSON='';
        // if record are exist
        if(count($dataCategory) > 0) {
            $categoryJSON .= '<option value="" disable="" selected="" hidden="">-- Select sub category --</option>';
            foreach ($dataCategory as $key => $name) {
                $categoryJSON.= '<option value="'.$name['sub_category_id'].'">'.$name['name'].'</option>';
            }       
            $status = "true";
        } else {
            $status = "false";
            $categoryJSON.= '<option value="">Category not available</option>';
        }
        // convert in json format
        echo json_encode(array('subCategory'=>$categoryJSON,"status"=>$status));       
    }

    public function ajax_list() // list of price list data
    {
        if(is_user_logged_in()){            
            $list = $this->priceList->get_datatables();  
            /*echo "<pres>";
            echo $this->db->last_query(); exit;*/      
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($list as $plData) {
                $userId = $this->encrypt->encode($plData->rp_id);
                $pkm=$phr ="None"; 
                if(!empty($plData->rp_per_km))
                $pkm = $plData->rp_per_km;
                if(!empty($plData->rp_per_hour))
                $phr = $plData->rp_per_hour;  
                if($pkm == 1)
                    $price = "$".$plData->rp_cost."/km";
                else
                    $price = "$".$plData->rp_cost."/hr";
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $plData->category;
                $row[] = $plData->name;                              
                /*$row[] = $pkm;
                $row[] = $phr;*/
                $row[] = $price;                
                $row[] ='  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="editPriceData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deletePriceList(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->priceList->count_all(),
                            "recordsFiltered" => $this->priceList->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deletePriceList() // delete record of price list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->delete(TB_RIDE_PRICE,array('rp_id'=>$this->encrypt->decode($postData['key'])));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Price record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewPriceInfo() // view data of price  
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $subCat ="";
                    $priceData = $this->Common_model->select("rp_id,rp_cost,rp_per_km,c_id,sc_id",TB_RIDE_PRICE,array('rp_id'=>$this->encrypt->decode($postData['key'])));
                    if($priceData[0]['c_id'] == 1 || $priceData[0]['c_id'] == 4) {
                        $subCatData = $this->Common_model->select("sub_category_id,name",TB_SUBCATEGORIES
                            ,array('sub_category_id'=>$priceData[0]['sc_id']));
                        $subID = $subCatData[0]['sub_category_id'];
                        $name = $subCatData[0]['name'];
                    } else {
                        $subID = "";
                        $name = "";
                    }
                    if($priceData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","priceData"=>$priceData[0],'subCategory'=>'<option value="'.$subID.'">'.$name.'</option>')); exit; 
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    } 

    // add & update record of price list
    public function addPrice()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();
                $cond = array("tbl_ride_price.c_id" => $postData['categories'],"tbl_ride_price.sc_id" => $postData['subCategory']);
                $jointype=array("tbl_categories"=>"LEFT","tbl_subcategories"=>"LEFT");
                $join = array("tbl_categories"=>"tbl_categories.category_id = tbl_ride_price.c_id","tbl_subcategories"=>"tbl_subcategories.sub_category_id = tbl_ride_price.sc_id");
                $priceListData = $this->Common_model->selectJoin("distinct(category)",TB_RIDE_PRICE,$cond,array(),$join,$jointype);
                if(empty($postData['editData']) && count($priceListData) > 0) {                    
                    echo json_encode(array("status"=>"warning","message"=>$priceListData[0]['category']." price is already exists.")); exit;   
                }                
                else
                {  
                    $phr = $pkm = $cost = '';
                    if($postData['categories'] == 1 || $postData['categories'] == 3 || $postData['categories'] == 4) {
                        $phr = 1;   
                        $cost = $postData['price_per_hour'];
                    }

                    if($postData['categories'] == 2 ) {
                        $pkm = 1;   
                        $cost = $postData['price_per_km'];
                    }

                    if(!empty($postData["editData"])) { // update data
                        $updateArr = array(  
                            "c_id" =>$postData['categories'],
                            "sc_id" =>$postData['subCategory'],
                            "rp_cost"=> $cost,
                            "rp_per_hour"=>$phr,
                            "rp_per_km" => $pkm,
                            "updated_at"=>date("Y-m-d H:s:i")  
                        );
                        $updateId = $this->Common_model->update(TB_RIDE_PRICE,array('rp_id'=>$this->encrypt->decode($postData['editData'])),$updateArr);
                        if($updateId){
                            echo json_encode(array("status"=>"success","action"=>"update","message"=>"Price has been updated successfully.")); exit;   
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                        }
                    } else {       
                        if(!empty($postData)){ // add data               
                        $data = array(
                            "c_id" =>$postData['categories'],
                            "sc_id" =>$postData['subCategory'],
                            "rp_cost"=> $cost,
                            "rp_per_hour"=>$phr,
                            "rp_per_km" => $pkm,                 
                            "created_at"=>date("Y-m-d H:s:i")           
                            );
                        $recordInsertUser = $this->Common_model->insert(TB_RIDE_PRICE,$data);
                        if($recordInsertUser){
                            echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Price has been inserted successfully.")); exit;    
                        } else {
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                        }
                    }
                    else{
                            echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please fill the user information.")); exit;    
                        }
                    }  
                } 
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    } 
}