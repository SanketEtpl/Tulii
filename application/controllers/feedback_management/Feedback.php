<?php
/*
* @author : Kiran.
* description: manage the user feedback data
*/
defined("BASEPATH") OR exit("No direct script access allowed");

class Feedback extends CI_Controller
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : User feedback', 
            'isActive'  => 'active'
        );
        $this->load->model("Feedback_model","feedback");
        $this->load->model("common_model");        
        date_default_timezone_set("Asia/Kolkata");
    }

    public function index()
    {
        if(is_user_logged_in()){
            $this->data['roleData'] = $this->Common_model->select('*',TB_ROLES,array("roleID !="=>ROLE_ADMIN)); 
            $this->load->view('includes/header',$this->data);
            $this->load->view('feedback_management/feedback');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function feedback_list() // list of feedback data
    {
        if(is_user_logged_in()){
            $resultFeedbackData = $this->feedback->get_datatables();   
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            
            foreach ($resultFeedbackData as $value) {
                $fbID = $this->encrypt->encode($value->fb_id);                                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = ucfirst($value->user_feedback);
                $row[] = ucfirst($value->role);  
                
                $row[] = '  <a data-id="'.$i.'" data-row-id="'.$fbID.'" class="btn btn-sm btn-info"   onclick="editFeedback(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$fbID.'" class="btn btn-sm btn-danger" onclick="deleteFeedback(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                    
                $data[] = $row;             
                $i++;
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->feedback->count_all(),
                "recordsFiltered" => $this->feedback->count_filtered(),
                "data" => $data
            );

            //output to json format
            echo json_encode($output);
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function deleteFeedback() // delete record of feedback list
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $resultDeleteFeedback = $this->Common_model->delete(TB_USER_FEEDBACK,array('fb_id'=>$this->encrypt->decode($postData['key'])));
                    if($resultDeleteFeedback){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Feedback record has been deleted successfully.")); exit;  
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            }
        }
    }

    public function viewFeedback()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                   
                if(!empty($postData["key"])){
                    $resultFeedback = $this->Common_model->select("fb_id, user_feedback, role_id", TB_USER_FEEDBACK, array('fb_id'=>$this->encrypt->decode($postData['key'])));
                    if($resultFeedback){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","feedbackData"=>$resultFeedback[0])); exit;
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please check your feedback id is not empty.")); exit;   
                }
            } else {
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }

    // add & update feedback
    public function addUpdateFeedback()
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()){
                $postData = $this->input->post();                
                if(!empty($postData['editId'])){   
                    $condition = array("fb_id"=>$this->encrypt->decode($postData['editId']));
                    $updateData = array(
                        "user_feedback" =>$postData["feedback"],
                        "role_id" => $postData["roleType"],
                        "updated_at" => date("Y-m-d H:i:s")
                    );  
                    $resultUpdateFeedback = $this->Common_model->update(TB_USER_FEEDBACK, $condition, $updateData); 
                    
                    if(count($resultUpdateFeedback) > 0) {
                        echo json_encode(array("status"=>"success","action"=>"update","message"=>"Feedback has been updated successfully.")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"update","message"=>"Please try again.")); exit; 
                    }
                } else {
                    $data = array(
                        "user_feedback" =>$postData["feedback"],
                        "role_id" => $postData["roleType"],
                        "created_at" => date("Y-m-d H:i:s")         
                    );
                    $resultAddFeedback = $this->Common_model->insert(TB_USER_FEEDBACK,$data);
                    if ( $resultAddFeedback ) {
                        echo json_encode(array("status"=>"success","action"=>"insert","message"=>"Feedback has been added successfully.")); exit;    
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"insert","message"=>"Please try again.")); exit; 
                    }
                }    
            } else { 
                $this->session->sess_destroy();
                redirect('login');
            }             
        }     
    }    
}