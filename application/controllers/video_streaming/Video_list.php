<?php 
/*
Author : Rajendra pawar 
Page :  video_list.php
Description : Show video streaming list.
*/
if(!defined('BASEPATH')) exit('No direct script access allowed'); 
 require APPPATH . '/libraries/BaseController.php';

class Video_list extends BaseController
{

	public function __construct()
    {
        parent::__construct();        
        $this->load->model('video_streaming/Video_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
      $this->data = array(
            'pageTitle' => 'Tulii : Dashboard',
            'isActive' => 'active'        
        );
        $this->load->view('includes/header',$this->data);
        $this->load->view('dashboard',$this->data);
        $this->load->view('includes/footer');            
    }
    public function video()
    {
    	if($this->isAdminOrServiceProvider() == TRUE)
        {

          $this->data = array(
            'pageTitle' => 'Tulii : Video Streaming',
            'isActive' => 'active'        
          );
          $this->load->view('includes/header',$this->data);
          $this->load->view('video_streaming/video',$this->data);
          $this->load->view('includes/footer'); 
        }
        else
        {         
         $this->loadThis();          
    }            
    } 

    function videoList()
    { 
        if($this->isAdminOrServiceProvider() == TRUE)
        {
        $list = $this->Video_model->get_datatables();  
        $data = array();
        $no = $_POST['start'];
        $status_id=1;
        
        foreach($list as $record)
          {
            
            if ($record->booking_start_date !='0000-00-00') {
             $start_date= date("d-m-Y", strtotime($record->booking_start_date));
            } 
            else {
             $start_date=''; 
            }

            if ($record->booking_end_date !='0000-00-00') {
             $end_date= date("d-m-Y", strtotime($record->booking_end_date));
            } 
            else {
             $end_date=''; 
            }


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->booking_duration;
            $row[] = $start_date;
            $row[] = $end_date;
            $row[] = $record->booking_pick_up_location;
            $row[] = $record->booking_drop_off_location;
            
           $path= AWS_VIDEO_URL.$record->file_path; 
           $row[] = '<a data-videopath="'.$path.'"  class="btn btn-sm btn-info viewVideo"  href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Play"><i class="fa fa-play-circle"></i></a>
             <a data-videoid="'. $this->encrypt->encode($record->id).'"  class="btn btn-sm btn-danger deleteVideo" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>';            
            $data[] = $row;
           $status_id ++; 
          }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Video_model->count_all(),
                       "recordsFiltered" => $this->Video_model->count_filtered(),
                        "data" => $data,
                         );          

          echo json_encode($output);         
        }
        else
        {
      $this->loadThis();   
    }

}
public function deleteVideo() 
    {
        if(is_ajax_request())
        {
             if($this->isAdminOrServiceProvider() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_VIDEO_STREAM,array('id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>'1'));

                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Video has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                echo(json_encode(array('status'=>'access')));
            }
        }
    }

}
?>