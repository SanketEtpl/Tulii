<?php
/*
* @author : kiran N.
* page : Resolution center controller
* description: show the all Resolution center data & management module
*/
defined('BASEPATH') OR exit('No direct script access allowed');
   require APPPATH . '/libraries/BaseController.php';
class Resolution extends BaseController {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Resolution center',
            'isActive'  => 'active'       
        );
        $this->load->model('Resolution_model','resolution');
         $this->isLoggedIn();
    }
 
    public function index()
    {
        if($this->isAdmin() == TRUE){
            $this->load->helper('url');   
            $this->load->view('includes/header',$this->data); 
            $this->load->view('resolution_center/issues',$this->data);
            $this->load->view('includes/footer');
        } else {
            $this->loadThis();
        } 
    }
 
    public function ajax_list() // list of issues data
    {
        if($this->isAdmin() == TRUE){
            $list = $this->resolution->get_datatables();  
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            $status_id=1;
            $rowActive_id="active".$status_id;
            $rowInActive_id="inActive".$status_id;
            foreach ($list as $resData) {
                $userId = $this->encrypt->encode($resData->id);  
                $button ="";  
                 if($resData->issue_status == "1")
                {   
                    $a= $resData->id.",2,'$rowActive_id','$rowInActive_id'";
                    $active_btn_class='class="btn btn-sm btn-warning" onclick="changeIssueStatus_popup('.$a.');" ';
                    $button = '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Peding"><button id="'.$rowActive_id.'" '.$active_btn_class.'>Pedding</button></span>&nbsp;';
                } 
                else
                {   
                    $d= $resData->id.",1,'$rowActive_id','$rowInActive_id'";
                    $inactive_btn_class=' disabled class="btn btn-sm btn-success" onclick="changeIssueStatus_popup('.$d.');" ';
                    $button = '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Resolved"><button id="'.$rowInActive_id.'" '.$inactive_btn_class.'>Resolved</button></span>&nbsp;';
                }
                
                
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $resData->parent_name;
                $row[] = $resData->parent_number;
                $row[] = $resData->service_provider;
                $row[] = $resData->issue;
                $row[] = $resData->issue_date;
                $row[] = $button;
                $row[] ='   <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-info" onclick="viewIssue(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a data-id="'.$i.'" data-row-id="'.$userId.'" class="btn btn-sm btn-danger deleteUser" onclick="deleteIssue(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
                                <i class="fa fa-trash"></i>
                            </a>                           
                        ';
                $data[] = $row;    
                $status_id++;
                $rowActive_id="active".$status_id;
                $rowInActive_id="inActive".$status_id;           
                $i++;
            }
     
            $output = array(
                            "draw" => $_POST['draw'],
                            "recordsTotal" => $this->resolution->count_all(),
                            "recordsFiltered" => $this->resolution->count_filtered(),
                            "data" => $data,
                    );
            //output to json format
            echo json_encode($output);
        }
        else{
            echo(json_encode(array('status'=>'access'))); 
        } 
    }

     public function changeIssueStatus() // chnage the status of issue pending or resolved
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
            $postData = $this->input->post();
            $result = $this->Common_model->update(TB_RESOLUTION_CENTER,array("id"=>$postData['userId']),array('issue_status'=>$postData['user_status'],'updatedBy'=>$this->session->userdata('userId'), 'updated_on'=>date('Y-m-d H:i:s')));            
           /* echo $this->db->last_query();
            print_r($postData);exit;*/
            if ($result)
                echo(json_encode(array('status'=>TRUE))); 
            else
                echo(json_encode(array('status'=>FALSE))); 
            } else {
                echo(json_encode(array('status'=>'access'))); 
            } 
        }        
    }

    public function deleteIssue() // delete record of issues
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_RESOLUTION_CENTER,array('id'=>$this->encrypt->decode($postData['key'])),array("isDelete"=>'0'));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Issue record has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            } else {
                echo(json_encode(array('status'=>'access'))); 
            } 
        }
    }

    public function viewIssue() // view data of issue information
    {
        if(is_ajax_request())
        {
            if($this->isAdmin() == TRUE){
                $imgExist =0; 
                $postData = $this->input->post();                   
                //print_r($postData);exit;
                if($postData["key"]){
                    $issueData = $this->Common_model->select("*",TB_RESOLUTION_CENTER,array('id'=>$this->encrypt->decode($postData['key'])));
                    if($issueData){                                                
                        echo json_encode(array("status"=>"success","action"=>"view","issueData"=>$issueData[0])); exit; 
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                }
            } else {
                echo(json_encode(array('status'=>'access'))); 
            } 
        }
    }   
}