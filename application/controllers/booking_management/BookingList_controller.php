<?php 
/*
Author : Rajendra pawar 
Page :  BookingList_controller.php
Description : BookingList controller use for track booking details in admin
*/
if(!defined('BASEPATH')) exit('No direct script access allowed'); 
 require APPPATH . '/libraries/BaseController.php';

class BookingList_controller extends BaseController
{
	public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->load->model('booking_management/booking_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
       $this->data = array(
            'pageTitle' => 'Tulii : Booking management',
            'isActive' => 'active'         
        );
    	//$this->data['pageTitle'] = 'Tulii : Booking management';  
      $this->load->view("includes/header",$this->data);      
      $this->load->view("dashboard", $this->data, NULL , NULL);            
      $this->load->view("includes/footer");
    }

  function rides()
  { 
     //   if($this->isAdminOrServiceProvider() == TRUE)  {
           $this->data = array(
            'pageTitle' => 'Tulii : Booking management',
            'isActive' => 'active'         
        );
           $this->load->view("includes/header",$this->data);  
         //$this->global['pageTitle'] = 'Tulii : Booking management';
         $this->load->view("booking_management/rides", $this->data, NULL , NULL);         
         $this->load->view("includes/footer");
     /*   }
        else
        {         
         $this->loadThis();          
    } */

}
 function schoolCare()
  { 
       // if($this->isAdminOrServiceProvider() == TRUE)   {
         //$this->global['pageTitle'] = 'Tulii : Booking management';
      $this->data = array(
            'pageTitle' => 'Tulii : Booking management',
            'isActive' => 'active'         
        );
         $this->load->view("includes/header",$this->data);
         $this->load->view("booking_management/schoolCare", $this->data, NULL , NULL);         
         $this->load->view("includes/footer");
       /* }
        else
        {         
         $this->loadThis();          
    } */

}
function tutor()
  { 
      //  if($this->isAdminOrServiceProvider() == TRUE)  {
      $this->data = array(
            'pageTitle' => 'Tulii : Booking management',
            'isActive' => 'active'         
        );
        // $this->global['pageTitle'] = 'Tulii : Booking management';
        $this->load->view("includes/header",$this->data);
        $this->load->view("booking_management/tutor", $this->global, NULL , NULL);         
        $this->load->view("includes/footer");
      /*  }
        else
        {         
         $this->loadThis();          
    } */

}

  function bookingList()
  { 
       // if($this->isAdminOrServiceProvider() == TRUE) {
        
         $category = $this->input->post('category');    
        $list = $this->booking_model->get_datatables(array("tbl_users.isDeleted"=>0,"tbl_kids.kid_status ="=>1,"tbl_bookings.isDeleted"=>0,"tbl_categories.category"=>$category));  
        $data = array();
        $no = $_POST['start'];
        foreach($list as $record)
          {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->kid_name;
             if($record->booking_date != '0000-00-00') {
                $row[] = date("d/m/Y", strtotime($record->booking_date));
                } else { $row[] = '';}

             
            $row[] = ucfirst($record->booking_status);
            $row[] = $record->booking_duration;
            $row[] = $record->booking_price;
            
            $temp ='<a data-bookingid="'.$record->booking_id.'"  class="btn btn-sm btn-info viewBooking"  href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>';
            if($this->session->userdata('role') == ROLE_ADMIN || $this->session->userdata('role') == ROLE_SERVICE_PROVIDER) {
            $temp .='<a data-bookingid="'. $this->encrypt->encode($record->booking_id).'" data-category="'.$category.'"  class="btn btn-sm btn-danger deleteBooking" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>';    
            }
            $row[] = $temp;     
            $data[] = $row;
          }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->booking_model->count_all(),
                        "recordsFiltered" => $this->booking_model->count_filtered(array("tbl_users.isDeleted"=>0,"tbl_kids.kid_status ="=>1,"tbl_bookings.isDeleted"=>0,"tbl_categories.category"=>$category)),
                        "data" => $data,
                         );          

          echo json_encode($output);         
       /* }
        else
        {
      $this->loadThis();   
    } */

}
  function viewBookingDetails()
    {
       // if($this->isAdminOrServiceProvider() == TRUE)
        //{
            $bookingId = $this->input->post('bookingId');
          
            $result=$this->common_model->select_join("user_name,user_phone_number,kid_name,
                kid_gender,booking_start_date,booking_end_date, case when ( booking_total_days > 0  and booking_total_days is not null) then booking_total_days when ( booking_total_weeks > 0 and booking_total_weeks is not null) then booking_total_weeks when ( booking_total_moths > 0 and booking_total_moths is not null) then booking_total_moths else 0 END as booking_total_days,
                booking_pick_up_location,booking_drop_off_location,booking_pick_up_time,
                booking_drop_off_time,category,name,booking_price,booking_status,booking_duration",TB_USERS,array("tbl_bookings.booking_id"=>$bookingId),array(),array(),array(TB_KIDS=>'tbl_users.user_id = tbl_kids.user_id',TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id',TB_CATEGORIES=>'tbl_bookings.booking_category_id = tbl_categories.category_id',TB_SUBCATEGORIES=>'tbl_bookings.booking_sub_category_id = tbl_subcategories.sub_category_id'),null);



                if ($result > 0) { 

                $user_name= !empty($result[0]['user_name'])?$result[0]['user_name']:"N/A";
                $user_phone_number= !empty($result[0]['user_phone_number'])?$result[0]['user_phone_number']:"N/A";
                $kid_name = !empty($result[0]['kid_name'])?$result[0]['kid_name']:"N/A";
                $kid_gender = !empty($result[0]['kid_gender'])?$result[0]['kid_gender']:"N/A";

                if($result[0]['booking_start_date'] != '0000-00-00') {
                $booking_start_date =  date("d/m/Y", strtotime($result[0]['booking_start_date']));
                } else {$booking_start_date = 'N/A';}
                if($result[0]['booking_end_date'] != '0000-00-00') {
                $booking_end_date = date("d/m/Y", strtotime($result[0]['booking_end_date'])); 
                } else {$booking_end_date = 'N/A';}
                
                $booking_total_days = !empty($result[0]['booking_total_days'])?$result[0]['booking_total_days']:"N/A";
                $booking_pick_up_location = !empty($result[0]['booking_pick_up_location'])?$result[0]['booking_pick_up_location']:"N/A";
                $booking_drop_off_location = !empty($result[0]['booking_drop_off_location'])?$result[0]['booking_drop_off_location']:"N/A";
                $booking_pick_up_time = !empty($result[0]['booking_pick_up_time'])?$result[0]['booking_pick_up_time']:"N/A";
                $booking_drop_off_time = !empty($result[0]['booking_drop_off_time'])?$result[0]['booking_drop_off_time']:"N/A";
                $category = !empty($result[0]['category'])?$result[0]['category']:"N/A";
                $name = !empty($result[0]['name'])?$result[0]['name']:"N/A";
                $booking_price = !empty($result[0]['booking_price'])?$result[0]['booking_price']:"N/A";

                 $booking_status = !empty($result[0]['booking_status'])?$result[0]['booking_status']:"N/A";
                 $booking_duration = !empty($result[0]['booking_duration'])?$result[0]['booking_duration']:"N/A";

                 if($booking_duration =='weekly')
                  { $duration= 'Weeks';} 
                elseif($booking_duration =='daily')
                 { $duration='Days';} 
               else { $duration='Months'; }

                $data='<div class="box">
                     <form role="form"  method="post" id="editcareDriverFrm" name="editcareDriverFrm">
                        <div class="box-body">
                            <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Parent Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Name: </label>
                                        <lable>'.$user_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile: </label>
                                        <lable>'.$user_phone_number.'</lable>
                                    </div>
                                </div>
                            </div>
                             <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Kid\'s Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="kid">Name: </label>
                                        <lable>'.$kid_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kid_gender">Gender: </label>
                                        <lable>'.$kid_gender.'</lable>
                                    </div>
                                </div>
                            </div>
                            

                             <div class="row">
                             <h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Booking Details</h4>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_days">Status: </label>
                                        <lable>'.$booking_status.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pick_up_location">Duration: </label>
                                         <lable>'.$booking_duration.'</lable>
                                    </div>
                                </div>
                            </div>
                               <div class="row">               
                          
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Start_date">Start Date: </label>
                                        <lable>'.$booking_start_date.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="end_date">End Date: </label>
                                         <lable>'.$booking_end_date.'</lable>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_days">Total '. $duration .': </label>
                                        <lable>'.$booking_total_days.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pick_up_location">Pick Up Point: </label>
                                         <lable>'.$booking_pick_up_location.'</lable>
                                    </div>
                                </div>
                            </div><div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drop_off_location">Drop Off Point: </label>
                                        <lable>'.$booking_drop_off_location.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pick_up_time">Pick Up time: </label>
                                         <lable>'.$booking_pick_up_time.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drop_off_time">Drop Off Time: </label>
                                        <lable>'.$booking_drop_off_time.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="category">Category: </label>
                                         <lable>'.$category.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">';
                              if($name){  
                             $data .='<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Sub Category: </label>
                                        <lable>'.$name.'</lable>
                                    </div>
                                </div>';
                                  }
                                 $data .='<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">price: </label>
                                         <lable>'.$booking_price.'</lable>
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.box-body -->   
                         </form>                
                </div>'; 
               echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }        
       /* }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        } */
    }
    public function deleteBooking() 
    {
        if(is_ajax_request())
        {
             // if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_BOOKINGS,array('booking_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Booking has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            //} else {
                echo(json_encode(array('status'=>'access')));
           // }
        }
    }
}
?>