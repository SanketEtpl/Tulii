<?php 
/*
Author : Rajendra pawar 
Page :  BookingList_controller.php
Description : BookingList controller use for track booking details in admin
*/
if(!defined('BASEPATH')) exit('No direct script access allowed'); 
 require APPPATH . '/libraries/BaseController.php';

class BookingList_controller extends BaseController
{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('booking_management/booking_model');
        $this->load->model('common_model');
        $this->load->model('login_model');
        $this->isLoggedIn();   
    }
    public function index()
    {
    	$this->global['pageTitle'] = 'Tulii : Dashboard';        
        $this->loadViews("dashboard", $this->global, NULL , NULL);            
    }

  function rides()
  { 
     //   if($this->isAdminOrServiceProvider() == TRUE)  {
         $this->global['pageTitle'] = 'Tulii : Booking Listing';
         $this->loadViews("booking_management/rides", $this->global, NULL , NULL);         
     /*   }
        else
        {         
         $this->loadThis();          
    } */

}
 function schoolCare()
  { 
       // if($this->isAdminOrServiceProvider() == TRUE)   {
         $this->global['pageTitle'] = 'Tulii : Booking Listing';
         $this->loadViews("booking_management/schoolCare", $this->global, NULL , NULL);         
       /* }
        else
        {         
         $this->loadThis();          
    } */

}
function tutor()
  { 
      //  if($this->isAdminOrServiceProvider() == TRUE)  {
         $this->global['pageTitle'] = 'Tulii : Booking Listing';
         $this->loadViews("booking_management/tutor", $this->global, NULL , NULL);         
      /*  }
        else
        {         
         $this->loadThis();          
    } */

}

  function bookingList()
  { 
       // if($this->isAdminOrServiceProvider() == TRUE) {
        
         $category = $this->input->post('category');    
        $list = $this->booking_model->get_datatables(array("tbl_users.isDeleted"=>0,"tbl_kids.kid_status ="=>1,"tbl_bookings.isDeleted"=>0,"tbl_categories.category"=>$category));  
        $data = array();
        $no = $_POST['start'];
        foreach($list as $record)
          {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $record->user_name;
            $row[] = $record->kid_name;
             if($record->booking_date != '0000-00-00') {
                $row[] = date("d/m/Y", strtotime($record->booking_date));
                } else { $row[] = '';}

             
            $row[] = ucfirst($record->booking_status);
            $row[] = $record->booking_price;

            $temp ='<a data-bookingid="'.$record->booking_id.'"  class="btn btn-sm btn-info viewBooking"  href="javascript:void(0)"><i class="fa fa-eye"></i></a>';
            if($this->session->userdata('role') == ROLE_ADMIN || $this->session->userdata('role') == ROLE_SERVICE_PROVIDER) {
            $temp .='<a data-bookingid="'. $this->encrypt->encode($record->booking_id).'" data-category="'.$category.'"  class="btn btn-sm btn-danger deleteBooking" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';    
            }
            $row[] = $temp;     
            $data[] = $row;
          }

          $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->booking_model->count_all(),
                        "recordsFiltered" => $this->booking_model->count_filtered(array("tbl_users.isDeleted"=>0,"tbl_kids.kid_status ="=>1,"tbl_bookings.isDeleted"=>0,"tbl_categories.category"=>$category)),
                        "data" => $data,
                         );          

          echo json_encode($output);         
       /* }
        else
        {
      $this->loadThis();   
    } */

}
  function viewBookingDetails()
    {
       // if($this->isAdminOrServiceProvider() == TRUE)
        //{
            $bookingId = $this->input->post('bookingId');
          
            $result=$this->common_model->select_join("user_name,user_phone_number,kid_name,
                kid_gender,booking_start_date,booking_end_date,booking_total_days,
                booking_pick_up_location,booking_drop_off_location,booking_pick_up_time,
                booking_drop_off_time,category,name,booking_price",TB_USERS,array("tbl_bookings.booking_id"=>$bookingId),array(),array(),array(TB_KIDS=>'tbl_users.user_id = tbl_kids.user_id',TB_BOOKINGS=>'tbl_users.user_id = tbl_bookings.user_id',TB_CATEGORIES=>'tbl_bookings.booking_category_id = tbl_categories.category_id',TB_SUBCATEGORIES=>'tbl_bookings.booking_sub_category_id = tbl_subcategories.sub_category_id'),null);



                if ($result > 0) { 

                $user_name= $result[0]['user_name'];
                $user_phone_number=$result[0]['user_phone_number'];
                $kid_name = $result[0]['kid_name'];
                $kid_gender = $result[0]['kid_gender'];

                if($result[0]['booking_start_date'] != '0000-00-00') {
                $booking_start_date =  date("d/m/Y", strtotime($result[0]['booking_start_date']));
                } else {$booking_start_date = '';}
                if($result[0]['booking_end_date'] != '0000-00-00') {
                $booking_end_date = date("d/m/Y", strtotime($result[0]['booking_end_date'])); 
                } else {$booking_end_date = '';}
                
                $booking_total_days = $result[0]['booking_total_days'];
                $booking_pick_up_location = $result[0]['booking_pick_up_location'];
                $booking_drop_off_location = $result[0]['booking_drop_off_location'];
                $booking_pick_up_time = $result[0]['booking_pick_up_time'];
                $booking_drop_off_time = $result[0]['booking_drop_off_time'];
                $category = $result[0]['category'];
                $name = $result[0]['name'];
                $booking_price = $result[0]['booking_price'];

                $data='<div class="box">
                     <form role="form"  method="post" id="editcareDriverFrm" name="editcareDriverFrm">
                        <div class="box-body">
                            <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Parent Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Name: </label>
                                        <lable>'.$user_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile: </label>
                                        <lable>'.$user_phone_number.'</lable>
                                    </div>
                                </div>
                            </div>
                             <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Kid\'s Details</h4>               
                          
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="kid">Name: </label>
                                        <lable>'.$kid_name.'</lable>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="kid_gender">Gender: </label>
                                        <lable>'.$kid_gender.'</lable>
                                    </div>
                                </div>
                            </div>
                            
                               <div class="row"><h4 style="padding-left:3%;border-bottom: 1px solid #111;padding-bottom: 1%;">Booking Details</h4>               
                          
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Start_date">Start Date: </label>
                                        <lable>'.$booking_start_date.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="end_date">End Date: </label>
                                         <lable>'.$booking_end_date.'</lable>
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total_days">Total Days: </label>
                                        <lable>'.$booking_total_days.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pick_up_location">Pick Up Location: </label>
                                         <lable>'.$booking_pick_up_location.'</lable>
                                    </div>
                                </div>
                            </div><div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drop_off_location">Drop Off Location: </label>
                                        <lable>'.$booking_drop_off_location.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pick_up_time">Pick Up time: </label>
                                         <lable>'.$booking_pick_up_time.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="drop_off_time">Drop Off Time: </label>
                                        <lable>'.$booking_drop_off_time.'</lable>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="category">Category: </label>
                                         <lable>'.$category.'</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">';
                              if($name){  
                             $data .='<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Sub Category: </label>
                                        <lable>'.$name.'</lable>
                                    </div>
                                </div>';
                                  }
                                 $data .='<div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">price: </label>
                                         <lable>'.$booking_price.'</lable>
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.box-body -->   
                         </form>                
                </div>'; 
               echo json_encode(array("status" => TRUE,"data" => $data));             
          }
            else { echo(json_encode(array("status"=>FALSE))); }        
       /* }
        else
        {
          echo(json_encode(array('status'=>'access')));                 
        } */
    }
    public function deleteBooking() 
    {
        if(is_ajax_request())
        {
             // if($this->isAdmin() == TRUE){
                $postData = $this->input->post();                   
                if($postData["key"]){
                    $deleteId = $this->Common_model->update(TB_BOOKINGS,array('booking_id'=>$this->encrypt->decode($postData['key'])),array('isDeleted'=>1));
                    if($deleteId){                                                
                        echo json_encode(array("status"=>"success","action"=>"delete","msg"=>"Booking has been deleted successfully.")); exit;  
                    }else{
                        echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                    }
                }
                else{
                    echo json_encode(array("status"=>"error","action"=>"delete","msg"=>"Please try again.")); exit; 
                }
            //} else {
                echo(json_encode(array('status'=>'access')));
           // }
        }
    }
}
?>