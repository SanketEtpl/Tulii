<?php
/*
* @author : kiran N.
* title : booking_complaints.php
* description : Booking complaints from parent
*/

defined('BASEPATH') OR exit('No direct script access allowed'); 
class Booking_complaints extends CI_Controller {
 
    public function __construct()
    {
        ob_start();
        parent::__construct();
        $this->data = array(
            'pageTitle' => 'Tulii : Booking management of booking complaints',
            'isActive' => 'active'        
        );
        $this->load->model('Booking_complaints_model','booking_complaints');        
    }
 
    public function index()
    {
        if (is_user_logged_in()){   
            $this->load->view('includes/header', $this->data);
            $this->load->view('booking_management/bookingComplaints');
            $this->load->view('includes/footer');
        } else {
            $this->session->sess_destroy();
            redirect('login');
        }
    }

    public function bookingComplaintsList() // list of booking complaint data
    {   
        if(is_user_logged_in()) {
            $resultbookingComplaintList = $this->booking_complaints->get_datatables();        
            $data = array();
            $no = $_POST['start'];
            $i = 1;
            foreach ($resultbookingComplaintList as $complaintData) {
                $cId = $this->encrypt->encode($complaintData->complaine_id);                                 
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $complaintData->user_fullname;
                $row[] = $complaintData->complaine_register_date;
                $row[] = $complaintData->description;
                $row[] = $complaintData->complaint_status;
                $row[] = '
                            <a data-id="'.$i.'" data-row-id="'.$cId.'" data-complaint-by="'.$complaintData->user_fullname.'" class="btn btn-sm btn-primary" onclick="viewBookingComplaintsData(this)" href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                <i class="fa fa-eye"></i>
                            </a>                           
                        ';
                
                $data[] = $row;   
                $i++;             
            }
     
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->booking_complaints->count_all(),
                "recordsFiltered" => $this->booking_complaints->count_filtered(),
                "data" => $data,
            );

            //output to json format
            echo json_encode($output);
        } else {
            echo(json_encode(array('status'=>'logout')));
            $this->session->sess_destroy();
            redirect('login');
        } 
    }

    public function viewBookingComplaintsInfo() // view data of booking complaints 
    {
        if(is_ajax_request())
        {
            if(is_user_logged_in()) {    
                $postData = $this->input->post();  
                if($postData["key"]){
                    $cond = array(TB_BOOKING_COMPLAINTS.".status"=> '1', TB_BOOKING_COMPLAINTS.".complaine_id" => $this->encrypt->decode($postData['key']));
                    $join = array(TB_SERVICE_PROVIDER => TB_SERVICE_PROVIDER.".service_provider_id = ".TB_BOOKING_COMPLAINTS.".user_to", TB_RIDE_BOOKING => TB_RIDE_BOOKING.".booking_id = ".TB_BOOKING_COMPLAINTS.".booking_id", TB_CARE_BOOKING => TB_CARE_BOOKING.".booking_id = ".TB_BOOKING_COMPLAINTS.".booking_id", TB_TUTOR_BOOKING => TB_TUTOR_BOOKING.".booking_id = ".TB_BOOKING_COMPLAINTS.".booking_id");
                    $resultBookingCompDetails = $this->Common_model->select_join("sp_fullname, complaine_register_date, description, complaint_status, ride_start_date, ride_end_date, pick_up_point, drop_point, care_start_date, care_end_date, care_location, care_start_time, care_end_time, tut_start_date, tut_end_date, start_time, end_time, tutor_booking_id, care_id, ride_id", TB_BOOKING_COMPLAINTS, $cond, array(), array(), $join ,null);
                    
                    $startDate = (!empty($resultBookingCompDetails[0]['ride_id'])) ? $resultBookingCompDetails[0]['ride_start_date'] : ((!empty($resultBookingCompDetails[0]['care_id']))  ? $resultBookingCompDetails[0]['care_start_date'] : $resultBookingCompDetails[0]['tut_start_date']);
                    $endDate = (!empty($resultBookingCompDetails[0]['ride_id'])) ? $resultBookingCompDetails[0]['ride_end_date'] : ((!empty($resultBookingCompDetails[0]['care_id']))  ? $resultBookingCompDetails[0]['care_end_date'] : $resultBookingCompDetails[0]['tut_end_date']);

                    $bookingData = "";
                    if(!empty($resultBookingCompDetails[0]['ride_id'])) {
                        $bookingData = '
                            <div class="row">                                
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="rpickup">Pick Up Point: </label>
                                        <lable>'.$resultBookingCompDetails[0]['pick_up_point'].'</lable>
                                    </div>                              
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rDropPoint">Drop Point: </label>
                                        <lable>'.$resultBookingCompDetails[0]['drop_point'].'</lable>
                                    </div>
                                </div>
                            </div>                            
                        ';
                    } else if(!empty($resultBookingCompDetails[0]['care_id'])) {
                        $bookingData = '
                            <div class="row">                                
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="cl">Care Location: </label>
                                        <lable>'.$resultBookingCompDetails[0]['care_location'].'</lable>
                                    </div>                              
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cOn">Booking Complaint On: </label>
                                        <lable>'.$resultBookingCompDetails[0]['drop_point'].'</lable>
                                    </div>
                                </div>
                            </div>
                        ';
                    } else {
                        $bookingData = '
                            <div class="row">                                
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="st">Tutor Start Time: </label>
                                        <lable>'.$resultBookingCompDetails[0]['start_time'].'</lable>
                                    </div>                              
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="et">Tutor End Time: </label>
                                        <lable>'.$resultBookingCompDetails[0]['end_time'].'</lable>
                                    </div>
                                </div>
                            </div>
                        ';
                    }

                    if(count($resultBookingCompDetails) > 0){                        
                        $data='
                        <div class="box">
                            <form name="bookingComplaintForm">
                                <div class="box-body">
                                    <div class="row">                                
                                        <div class="col-md-6">                                
                                            <div class="form-group">
                                                <label for="fname">Complaint By: </label>
                                                <lable>'.$postData['complaint_by'].'</lable>
                                            </div>                              
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="cOn">Complaint On: </label>
                                                <lable>'.$resultBookingCompDetails[0]['sp_fullname'].'</lable>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                   
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="date">Complaint Date: </label>
                                                <lable>'.$resultBookingCompDetails[0]['complaine_register_date'].'</lable>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="description">Description: </label>
                                                <lable>'.$resultBookingCompDetails[0]['description'].'</lable>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                   
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="complaint_status">Complaint Status: </label>
                                                <lable>'.$resultBookingCompDetails[0]['complaint_status'].'</lable>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="row">                                   
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="bsd">Booking Start Date: </label>
                                                <lable>'.$startDate.'</lable>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="bed">Booking End Date: </label>
                                                <lable>'.$endDate.'</lable>
                                            </div>
                                        </div>
                                    </div>
                                    '.$bookingData.'                                   
                                </div>                                                         
                            </div><!-- /.box-body -->   
                        </form>                
                    </div>'; 
                    echo json_encode(array("status" => 'success',"data" => $data));
                    } else {
                        echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please try again.")); exit;   
                    }
                } else {
                    echo json_encode(array("status"=>"error","action"=>"view","msg"=>"Please provider booking complaint id and try again.")); exit;   
                }
            } else {
                echo(json_encode(array('status'=>'logout')));
                $this->session->sess_destroy();
                redirect('login');
            } 
        }
    }   
}