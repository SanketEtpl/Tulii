<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Fileupload
{
	function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->library('session');
		$this->ci->load->library('upload');
		$this->ci->load->database();
		$this->ci->load->model("common_model");
	}
	public function save_file($filename,$config)
	{
		$this->ci->upload->initialize($config);
		if(!$this->ci->upload->do_upload($filename))
		{
			$result['message'] = $this->ci->upload->display_errors();
			$result['status']="error";
			return  $result;
		}
		else
		{
			return $this->ci->upload->data();
		}
	}	
	
}
?>